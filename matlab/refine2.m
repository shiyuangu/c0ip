% refine2.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: refine the mesh based on the given error estimator and
% Dorfler bulk criterion. Both the input mesh meshIn and the output
% meshOut are type 3 mesh. The output vector update records the
% parent of the newly generated cell

function [meshOut,updated]=refine2(meshIn, eta, theta)
%both meshIn and meshOut are type 3 mesh
node=meshIn.node; T=meshIn.T;  
edge=[T(:,[1 2]);T(:,[2 3]);T(:,[1 3])];
edge=unique(sort(edge,2),'rows');
nN=size(node,1); nT=size(T,1); nE=size(edge,1);
dE2t=sparse(T(:,[1 2 3]), T(:,[2 3 1]), [1:nT 1:nT 1:nT],nN,nN);
p2e=sparse(edge(:,[1 2]),edge(:,[2 1]),[1:nE,1:nE],nN,nN);
% Get bases
     edgeLen=zeros(nT,3);
     edgeLen(:,1)=(node(T(:,2),1)-node(T(:,3),1)).^2+...
         (node(T(:,2),2)-node(T(:,3),2)).^2;
     edgeLen(:,2)=(node(T(:,1),1)-node(T(:,3),1)).^2+...
         (node(T(:,1),2)-node(T(:,3),2)).^2;
     edgeLen(:,3)=(node(T(:,1),1)-node(T(:,2),1)).^2+...
         (node(T(:,1),2)-node(T(:,2),2)).^2;
     [tmp,locBase]=max(edgeLen,[],2); 
%
%mark
    psum=sum(eta)*theta; [tmp,map]=sort(-eta); tsum=0; 
    marker=zeros(nE,1);  processedT=logical(zeros(nT,1)); 
    i=0;
    while (tsum<psum)
        i=i+1;
        cT=map(i); 
        if (~processedT(cT)) 
            flag=true;
        end
        while(flag)
            tsum=tsum+eta(cT); processedT(cT)=true; 
            switch locBase(cT)
                case 1
                    v1=T(cT,2); v2=T(cT,3);
                case 2
                    v1=T(cT,3); v2=T(cT,1);
                case 3
                    v1=T(cT,1); v2=T(cT,2);
            end
            gBase=p2e(v1,v2);
            if(marker(gBase)>0)
                flag=false;
            else
               nN=nN+1;
               node(nN,:)=mean(node([v1 v2],:),1);
               marker(gBase)=nN;
               cT=dE2t(v2,v1);
               if(cT==0 || processedT(cT)) flag=false; end 
            end
        end 
    end
%
%divide 
updated=zeros(nT,1);
for i=1:nT
    if(processedT(i))
         p=T(i,:);
         for j=1:locBase(i)-1
           p=p([2 3 1]);    
         end 
         gBase=p2e(p(2),p(3));pro=p2e(p(1),p(2)); post=p2e(p(3),p(1));
         p=[p marker(gBase) marker(post) marker(pro)];
         if(p(5)>0 && p(6)>0)
             %fprintf(1,'red\n');
             T(i,:)=p([4 5 6]);updated(i)=i;
             T(end+1,:)=p([3 5 4]);updated(end+1)=i;
             T(end+1,:)=p([5 1 6]);updated(end+1)=i;
             T(end+1,:)=p([4 6 2]);updated(end+1)=i;
         else     
             T(i,:)=p([4 1 2]); updated(i)=i;    
             T(end+1,:)=p([4 3 1]); updated(end+1)=i;
             if(p(5)>0)
                 T(end,:)=p([5 4 3]);
                 T(end+1,:)=p([5 1 4]); updated(end+1)=i;
             end
             if(p(6)>0)
                 T(i,:)=p([6 4 1]);
                 T(end+1,:)=p([6 2 4]);updated(end+1)=i;
             end
        end
    end
end
meshOut.node=node; meshOut.T=T;
%figure;trimesh(mesh3.T,mesh3.node(:,1),mesh3.node(:,2),zeros(size(mesh3.node,1),1));