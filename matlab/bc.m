function rv = bc(x,on_boundary)
% BC - rv: return value, boolean, true indicates the nodes is on the boundary condition; x is a R^n vector, n=2 for 2d domain; on_boundary is a boolean value indicating whether x is on the boundary of the mesh. 
%   
tol=1000*realmin;
if(on_boundary && abs(x(1))<tol && abs(x(2))<tol)
   rv=true;
else 
   rv=false;
end
