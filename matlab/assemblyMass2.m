function M=assemblyMass2(mesh2) 
% return the nGI*nGI Mass matrix for P2 element
  T=mesh2.oT;nT=size(T,1);
  nM=mesh2.nGI;M=sparse(nM,nM);
  v=zeros(2,3);
  C=[1/30 -1/180 -1/180 -1/45  0      0;
    -1/180  1/30  -1/180  0  -1/45   0;
    -1/180  -1/180 1/30   0    0   -1/45;
    -1/45     0      0   8/45  4/45  4/45;
       0    -1/45    0   4/45  8/45  4/45;
       0      0    -1/45 4/45  4/45  8/45];  
  for i=1:nT
      for j=1:3
        v(:,j)=mesh2.iP(T(i,j)).coordinate';  
      end
      area=0.5*det([v;1 1 1]);
      M(T(i,:), T(i,:))=M(T(i,:),T(i,:))+area*C;
  end
end