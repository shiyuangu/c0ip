function [B,phi]= assemblyBPhi(mesh2)
% generate the matrix B and the vector phi   
% B represents the inner prodoct of the Nodal basis of  
% (v,w)_k=1/3\sum\sum|T|v(p)w(p)
% B is diagonal matrices, which is represented by a vector.
% phi is a vector whose components equal to 1 at the midpoints and 0
% otherwise. 
nT=size(mesh2.T,1);
nGI=mesh2.nGI;
T=mesh2.T;
B=zeros(1,nGI);phi=zeros(nGI,1);
v=zeros(2,3);
for i=1:nT
  for j=1:3
      v(:,j)=mesh2.iP(T(i,j)).coordinate'; 
  end
  area=0.5*abs(det([v;1 1 1]));
  for j=1:6
      B(T(i,j))=B(T(i,j))+area;
  end
  for j=4:6
      phi(T(i,j))=1;
  end
end
B=B/3; 
end

