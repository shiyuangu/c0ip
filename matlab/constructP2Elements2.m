% construct P2 element; only for oriented triangle. 
function elements=constructP2Elements2(mesh2)
T=mesh2.T;
n=size(T,1);
for i=1:n
    v1=mesh2.iP(T(i,1)).coordinate;
    v2=mesh2.iP(T(i,2)).coordinate;
    v3=mesh2.iP(T(i,3)).coordinate; 
    m1=mesh2.iP(T(i,4)).coordinate;  
    m2=mesh2.iP(T(i,5)).coordinate;
    m3=mesh2.iP(T(i,6)).coordinate; 
    R=[v1' v2' v3'; 1 1 1];
    detR=det(R);
    elements(i,1).gI=T(i,1);
    elements(i,1).a=4*(detR)^(-2);
    elements(i,1).A=[v2' v3'];
    elements(i,1).B=[m3' m2'];
    %
    elements(i,2).gI=T(i,2);
    elements(i,2).a=4*(detR)^(-2);
    elements(i,2).A=[v3' v1'];
    elements(i,2).B=[m1' m3'];
    
    elements(i,3).gI=T(i,3);
    elements(i,3).a=4*(detR)^(-2);
    elements(i,3).A=[v1' v2'];
    elements(i,3).B=[m2' m1'];
    
    elements(i,4).gI=T(i,4);
    elements(i,4).a=4*(detR)^(-2);
    elements(i,4).A=[v3' v1'];
    elements(i,4).B=[v1' v2'];
    
    elements(i,5).gI=T(i,5);
    elements(i,5).a=4*(detR)^(-2);
    elements(i,5).A=[v1' v2'];
    elements(i,5).B=[v2' v3'];
    
    elements(i,6).gI=T(i,6);
    elements(i,6).a=4*(detR)^(-2);
    elements(i,6).A=[v2' v3'];
    elements(i,6).B=[v3' v1'];
end