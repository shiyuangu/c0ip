% assemble the interpolation matrix between the coarse mesh meshC
% and the finer mesh meshF. parent is a vector indictate the parent
% triangle in the coarse mesh for every triangle in the finer mesh. 
% meshC and meshF are meshes of type 2
% for P2 langrange elements
function I=assemblyIntp(meshC,meshF,parent)   
nGIC=meshC.nGI; nGIF=meshF.nGI; 
I=sparse(nGIF,nGIC);
nT=size(meshF.T,1);
S=[1    0    0     0    3/8    3/8;
    0    0    0   -1/8    0    -1/8;
    0    0    0   -1/8   -1/8    0 ;
    0    0    0    1/4    0      0 ;
    0    0    1    1/2   3/4     0 ;
    0    1    0    1/2    0      3/4];
for i=1:nT
          map1=zeros(1,6);
          tcoor=meshF.iP(meshF.oT(i,1)).coordinate;
          tmp2=parent(i);
          if(meshC.iP(meshC.oT(tmp2,1)).coordinate==tcoor)
              tmp1=1;
          elseif(meshC.iP(meshC.oT(tmp2,2)).coordinate==tcoor)
              tmp1=2;
          elseif(meshC.iP(meshC.oT(tmp2,3)).coordinate==tcoor)
              tmp1=3;
          else tmp1=0;
          end
          if(tmp1~=0)
            map1(1)=tmp1;
            map1(2)=myAdd1(2,tmp1-1);
            map1(3)=myAdd1(3,tmp1-1);
            map1(4)=myAdd2(4,tmp1-1);
            map1(5)=myAdd2(5,tmp1-1);
            map1(6)=myAdd2(6,tmp1-1);
            I(meshF.oT(i,1:6),meshC.oT(tmp2,map1))=S';
          end
end
function y=myAdd1(x,i)
y=x+i;
if y>3
    y=y-3;
end

function y=myAdd2(x,i)
y=x+i;
if y>6
   y=y-3;
end 