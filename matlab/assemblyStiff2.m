% compute 2nd order stiffness matrix 
function S=assemblyStiff2(mesh2)
T=mesh2.oT; nT=size(T,1);
nS=mesh2.nGI;
S=sparse(nS,nS);
D=[0 1 -1;
   -1 0 1;
   1 -1 0];
E=[2/3  1/3 1/3;
   1/3  2/3 1/3
   1/3  1/3 2/3];
Q=[-1  0  0 -2  2  2;
    0 -1  0  2 -2  2;
    0  0 -1  2  2 -2;
    2  0  0  2 -2 -2;
    0  2  0 -2  2 -2;
    0  0  2 -2 -2  2];
localS=zeros(6,6);
for i=1:nT
    for j=1:3
        v(j,:)=mesh2.iP(T(i,j)).coordinate;
    end
    P=([v(1,:)' v(2,:)' v(3,:)';1 1 1]);
    if(det(P)<0)
       disp('The triangle is not oriented correctly');
    end
    B=1/(2*det(P))*(P*D)'*(P*D);
    C=[B       2/3*B;
       2/3*B    B.*E ];
    localS=Q'*C*Q;
    S(T(i,:),T(i,:))=S(T(i,:),T(i,:))+localS;
end
