function uy = uy(r,theta)
%UY
%    UY = UY(R,THETA)

%    This function was generated by the Symbolic Math Toolbox version 5.5.
%    22-Aug-2011 15:39:36

t424 = cos(theta);
t426 = r.^2;
t431 = t424.^2;
t432 = t426.*t431;
t425 = t432-1.0;
t427 = sin(theta);
t433 = t427.^2;
t434 = t426.*t433;
t428 = t434-1.0;
t429 = theta.*(2.0./3.0);
t430 = cos(t429);
t435 = t428.^2;
t436 = r.^(7.0./3.0);
t437 = t425.^2;
t438 = r.^(1.0e1./3.0);
uy = t427.*(r.^(1.0./3.0).*t430.*t435.*t437.*(4.0./3.0)+t425.*t430.*t431.*t435.*t436.*4.0+t428.*t430.*t433.*t436.*t437.*4.0)-(t424.*(r.^(4.0./3.0).*t435.*t437.*sin(t429).*(2.0./3.0)+t424.*t425.*t427.*t430.*t435.*t438.*4.0-t424.*t427.*t428.*t430.*t437.*t438.*4.0))./r;
