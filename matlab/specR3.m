% specR3.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: use power method to compute inv(B)*A on
% the subspace orthogonal to vector phi. Assume B is invertible on
% the whole space. C.f. specR2.m when B is not invertible 
function [lambda,v]=specR3(A, B, phi)
tol=1e-3;
maxit=1e6; 
i=0; lambda=0; 
RandStream.setDefaultStream(RandStream('mt19937ar','seed',100));
z0=rand(size(A,1),1); z0=(z0-0.5)*2;
v=proj(z0,phi);
while 1
    i=i+1;
    lambdaP=lambda;
    rhs=A*v;
    q=proj(B\rhs,phi);
    [nf,index]=max(abs(q));
    v=q/q(index); v=proj(v,phi);
    lambda=v'*A*v/(v'*B*v);
    if (abs((lambda-lambdaP)/lambda)<tol)
        break
    end
    if i>maxit
        disp('fail to converge.')
        break
    end
end

function vout=proj(vin,phi)
%vout=vin;
x=dot(vin,phi)/(phi'*phi);
vout=vin-x*phi;
return;