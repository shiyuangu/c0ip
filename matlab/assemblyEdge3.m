% Assembly the edge terms in the C0IP
function E=assemblyEdge3(mesh2,elements,eta)
%for oriented triangles, returen nGI*nGI matrix 
nEi=size(mesh2.Ei,1);
nE=mesh2.nGI; E=sparse(nE,nE);
localS=zeros(9,9);
for i=1:nEi
    map=zeros(1,9);
    map(1)=mesh2.Ei(i,3);
    map(2)=mesh2.Ei(i,1);
    map(3)=mesh2.Ei(i,2);
    p1(1:2)=mesh2.iP(map(2)).coordinate(1:2);
    p2(1:2)=mesh2.iP(map(3)).coordinate(1:2);
    iT(1)=mesh2.Ei(i,4); iT(2)=mesh2.Ei(i,5);
    map(4)=setdiff(mesh2.T(iT(1),1:3),map(2:3));
    map(5:6)=setdiff(mesh2.T(iT(1),4:6),map(1));
    map(7:9)=setdiff(mesh2.T(iT(2),1:6),map(1:3));
    len=norm(p2-p1,2);l=(p2-p1)/len;
    n(1,1:2)=[l(2), -l(1)];
    p3(1:2)=mesh2.iP(map(4)).coordinate(1:2);
     if det([p1' p2' p3';1 1 1])>0
         n(2,1:2)=-n(1,1:2);
     else 
         n(2,1:2)=n(1,1:2);
         n(1,1:2)=-n(1,1:2);
     end
    v=zeros(9,4);
    for k=1:2
        for j=1:6
            a=elements(iT(k),j).a;
            A=elements(iT(k),j).A;
            B=elements(iT(k),j).B;
            t1=[A(2,1)-A(2,2) A(1,2)-A(1,1)]*n(k,:)';
            t2=[B(2,1)-B(2,2) B(1,2)-B(1,1)]*n(k,:)';
            t3=find(map==mesh2.T(iT(k),j));
            v(t3,1)=v(t3,1)-a*t1*det([p1' B;1 1 1])-a*t2*det([p1' A;1 1 1]);
            v(t3,3)=v(t3,3)-a*t1*det([p2' B;1 1 1])-a*t2*det([p2' A;1 1 1]);
            v(t3,2)=(v(t3,1)+v(t3,3))/2;
            v(t3,4)=v(t3,4)+a*t1*t2;
        end
    end
    for p=1:9
        for q=p:9
          localS(p,q)=len*v(p,4)*v(q,2)+len*v(q,4)*v(p,2)+eta*(v(p,1)*v(q,1)/6+2*v(p,2)*v(q,2)/3+v(p,3)*v(q,3)/6);
          localS(q,p)=localS(p,q);
        end
    end
    E(map,map)=E(map,map)+localS;
end
nEb=size(mesh2.Eb,1);
localS=zeros(6); 
for i=1:nEb
    iT=mesh2.Eb(i,4);
    map=zeros(1,6);
    map(1:6)=mesh2.T(iT,1:6);
    p1(1:2)=mesh2.iP(mesh2.Eb(i,1)).coordinate(1:2);
    p2(1:2)=mesh2.iP(mesh2.Eb(i,2)).coordinate(1:2);
    len=norm(p2-p1,2);l=(p2-p1)/len;
    n=[l(2), -l(1)];
    idP3=setdiff(map(1:3),[mesh2.Eb(i,1) mesh2.Eb(i,2)]);
    p3(1:2)=mesh2.iP(idP3).coordinate(1:2);
     if det([p1' p2' p3';1 1 1])<0
         n=-n(1:2);
     end
    v=zeros(6,4);
        for j=1:6
            a=elements(iT,j).a;
            A=elements(iT,j).A;
            B=elements(iT,j).B;
            t1=[A(2,1)-A(2,2) A(1,2)-A(1,1)]*n';
            t2=[B(2,1)-B(2,2) B(1,2)-B(1,1)]*n';
            t3=j;
            v(t3,1)=-a*t1*det([p1' B;1 1 1])-a*t2*det([p1' A;1 1 1]);
            v(t3,3)=-a*t1*det([p2' B;1 1 1])-a*t2*det([p2' A;1 1 1]);
            v(t3,2)=(v(t3,1)+v(t3,3))/2;
            v(t3,4)=2*a*t1*t2;
        end
    for p=1:6
        for q=p:6
          localS(p,q)=len*v(p,4)*v(q,2)+len*v(q,4)*v(p,2)+eta*(v(p,1)*v(q,1)/6+2*v(p,2)*v(q,2)/3+v(p,3)*v(q,3)/6);
          localS(q,p)=localS(p,q);
        end
    end
%     LI=find(mesh2.free(map));
%     GGI=mesh2.GIGGI(map(LI));
%     E(GGI,GGI)=E(GGI,GGI)+localS(LI,LI); 
      E(map,map)=E(map,map)+localS;
end