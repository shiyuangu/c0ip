function rv=f0(x,y)
% compute f=Delta^2 by DDu.m
r=(x^2+y^2)^(0.5);
theta=acos(x/r);
if(y<0)
    theta=2*pi-theta;
end
rv=DDu(r,theta);