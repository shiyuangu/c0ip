function Vs=constructMGVec(meshes2)
% Construct the mass vector for P2 element for 2nd Neumann 
nmesh=size(meshes2,2);
for i=1:nmesh
   Vs{i}=zeros(1,meshes2{i}.nGI);
   for j=1:size(meshes2{i}.T,1)
       p1=meshes2{i}.iP(meshes2{i}.T(j,1)).coordinate;
       p2=meshes2{i}.iP(meshes2{i}.T(j,2)).coordinate;
       p3=meshes2{i}.iP(meshes2{i}.T(j,3)).coordinate;
       area=0.5*abs(det([1 p1;1 p2;1 p3]'));
       Vs{i}(meshes2{i}.T(j,4:6))=Vs{i}(meshes2{i}.T(j,4:6))+area/3*ones(1,3);
   end 
end
