function rq=computeSpecRia3(k,As,Bs,Phi)
% find the spectrum radius in the of inv(Bs{k})*As{k} in the space
% orthogonal to Phi(w.r.t the usual l2 inner product)
[lambda,v]=specR3(As{k}, Bs{k}, Phi{k});
rq=v'*As{k}*v/(v'*Bs{k}*v);
disp(sprintf('lamda=%g, Rayleigh quotient=%g',lambda,rq));