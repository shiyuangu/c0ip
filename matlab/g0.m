function rv = g0(x,y)
% G0 - compute g0 using extern ux.m, uy.m
r=(x^2+y^2)^(0.5);
theta=acos(x/r);
if(y<0)
    theta=2*pi-theta;
end
if((x==1 && y>0)||(x==0 &&y<0))
    rv=ux(r,theta);
else if(y==1)
    rv=uy(r,theta);
    else if(x==-1)
           rv=-ux(r,theta);
        else if((x<0 && y==-1) ||(x>0 && y==0))
           rv=-uy(r,theta);
            else
                disp('in g0: points being evaluated but should not\n');
            end
        end
    end
end