% compute the volume integral for 4th term in C0IP 
function DD=assemblyTri3(mesh2,elements)
T=mesh2.T; nT=size(T,1);
nDD=mesh2.nGI;DD=sparse(nDD,nDD);
iP=mesh2.iP;
localS=zeros(6,6);
for i=1:nT
    for j=1:3
      v(j,1:2)=mesh2.iP(T(i,j)).coordinate(1:2);
    end
    area=0.5*abs(det([v(1,:)' v(2,:)' v(3,:)'; 1 1 1]));
    for p=1:6
        a=elements(i,p).a; 
        A=elements(i,p).A;
        B=elements(i,p).B;
        g11=A(2,1)-A(2,2); g12=A(1,2)-A(1,1);
        g21=B(2,1)-B(2,2); g22=B(1,2)-B(1,1);
        wp=a*[2*g11*g21 g12*g21+g11*g22 g12*g21+g11*g22 2*g22*g12];
        for q=p:6 
           a=elements(i,q).a; 
           A=elements(i,q).A;
           B=elements(i,q).B;
           g11=A(2,1)-A(2,2); g12=A(1,2)-A(1,1);
           g21=B(2,1)-B(2,2); g22=B(1,2)-B(1,1);
           wq=a*[2*g11*g21 g12*g21+g11*g22 g12*g21+g11*g22 2*g22*g12];
           localS(p,q)=area*wp*wq';
           localS(q,p)=localS(p,q);
        end
    end
    DD(T(i,:),T(i,:))=DD(T(i,:),T(i,:))+localS; 
end