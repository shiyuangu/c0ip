% compute the bulk integral of RHS: \int fv dx.
function F=bulkInt(mesh2,elements,f)
F=zeros(mesh2.nGI,1);
nT=size(mesh2.T,1);
for i=1:nT
    mp1=mesh2.iP(mesh2.T(i,4)).coordinate(1:2);
    mp2=mesh2.iP(mesh2.T(i,5)).coordinate(1:2);
    mp3=mesh2.iP(mesh2.T(i,6)).coordinate(1:2);
     v1=mesh2.iP(mesh2.T(i,1)).coordinate(1:2);
     v2=mesh2.iP(mesh2.T(i,2)).coordinate(1:2);
     v3=mesh2.iP(mesh2.T(i,3)).coordinate(1:2);
     area=abs(0.5*det([v1' v2' v3';1 1 1]));
    for j=1:6
        a=elements(i,j).a;
        A=elements(i,j).A;
        B=elements(i,j).B;
        F(mesh2.T(i,j))=F(mesh2.T(i,j))+area/3*(f(mp1(1),mp1(2))*a*det([mp1' A; 1 1 1])*det([mp1' B;1 1 1])+...
                                                f(mp2(1),mp2(2))*a*det([mp2' A; 1 1 1])*det([mp2' B;1 1 1])+...
                                                f(mp3(1),mp3(2))*a*det([mp3' A; 1 1 1])*det([mp3' B;1 1 1]));
    end
end