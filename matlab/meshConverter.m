% convert type 2 and type 3 mesh; type1 mesh is obsolete;
function meshOut=meshConverter(meshIn,channel)
switch channel
    case '32'    %%%meshIn must be oriented. 
        edgeIn=[meshIn.T(:,[2 3]);meshIn.T(:,[3 1]);meshIn.T(:,[1 2])];
        edgeIn=unique(sort(edgeIn,2),'rows');
        nN=size(meshIn.node,1); nE=size(edgeIn,1); nT=size(meshIn.T,1);
        pp2e=sparse(edgeIn(:,[1 2]),edgeIn(:,[2 1]),[1:nE,1:nE],nN,nN);
        pp2t=sparse(meshIn.T(:,[1 2 3]),meshIn.T(:,[2 3 1]),[1:nT,1:nT,1:nT],nN,nN);
        flagN=zeros(nN,1); flagE=zeros(nE,1);
        p=size(meshIn.node,1); meshOut.T=zeros(nT,6); meshOut.iP=[]; meshOut.Eb=[];meshOut.Ei=[];
        for i=1:nT
            for j=1:3
                pxIn=meshIn.T(i,j);
                if(flagN(pxIn)<0.5)
                    flagN(pxIn)=pxIn;  % a mapping between the vertices. This line can be changed to other mapping scheme
                    meshOut.iP(flagN(pxIn)).coordinate=meshIn.node(pxIn,:);
                    meshOut.iP(flagN(pxIn)).vertex=true;
                    meshOut.iP(flagN(pxIn)).boundary=false;
                end    
            end
            meshOut.T(i,1:3)=flagN(meshIn.T(i,:));
            tmpTIn=meshIn.T(i,:);
            for j=1:3
                eI=pp2e(tmpTIn(2),tmpTIn(3));
                if(flagE(eI)<0.5)                   
                  p=p+1; flagE(eI)=p;
                  meshOut.iP(p).coordinate=mean(meshIn.node([tmpTIn(2) tmpTIn(3)],:));
                  meshOut.iP(p).vertex=false;
                  meshOut.T(i,3+j)=p;
                  neighbor=pp2t(tmpTIn(3),tmpTIn(2));
                  if(neighbor<0.5)
                      meshOut.iP(p).boundary=true; 
                      meshOut.iP(flagN(tmpTIn(2))).boundary=true;
                      meshOut.iP(flagN(tmpTIn(3))).boundary=true;
                      meshOut.Eb(end+1,1:4)=[flagN(tmpTIn(2)) flagN(tmpTIn(3)) p i];
                  else 
                      meshOut.iP(p).boundary=false; 
                      meshOut.Ei(end+1,1:5)=[flagN(tmpTIn(2)) flagN(tmpTIn(3)) p i neighbor];
                  end
                else 
                  meshOut.T(i,3+j)=flagE(eI);  
                end
                tmpTIn=tmpTIn([2 3 1]);
            end
        end
        meshOut.nGI=max(size(meshOut.iP));            
        meshOut.free=false(1,meshOut.nGI);
        for i=1:meshOut.nGI
            meshOut.free(i)=~bc(meshOut.iP(i).coordinate,meshOut.iP(i).boundary);
        end
        meshOut.nDOF=sum(meshOut.free);
        
        % mapping from global indices to the global indices
        % after removing the non degree of freedom, zero
        % indicating non degree of freedom. 
        meshOut.GIGGI=zeros(1,meshOut.nGI);
        % inverse mapping of GIGGI
        meshOut.GGIGI=zeros(1,meshOut.nDOF);
        p=0;
        for i=1:meshOut.nGI
            if(meshOut.free(i))
              p=p+1; 
              meshOut.GIGGI(i)=p;
              meshOut.GGIGI(p)=i;
            end
        end
        meshOut.oT=meshOut.T;
end

