% testReg.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: uniform mesh refinement. 
%
function [u,mesh3,seta,nDOF,err]=testReg(mesh3,f,g,gtau,q,penalty,tol,maxit) %%for Uniform refinement

seta=zeros(1,maxit);
nDOF=zeros(1,maxit);
err=zeros(1,maxit);
for i=1:maxit
    mesh2=meshConverter(mesh3,'32');
    elements=constructP2Elements2(mesh2);
    [u,Ac,ix]=BHSolver4(mesh2,elements,f,g,gtau,q,penalty);
   
    eta=errEst3(mesh2,elements,u,f,g,gtau,q,penalty);  
    seta(i)=sqrt(sum(eta)*0.5);
    nDOF(i)=mesh2.nDOF;
    % below: compute true error
    %    exU=zeros(1,mesh2.nGI);
    %    for l=1:mesh2.nGI
    %        exU(l)=exSol(mesh2.iP(l).coordinate(1), mesh2.iP(l).coordinate(2));   
    %    end
    %   err(i)=sqrt((exU(ix)-u(ix))*Ac*(exU(ix)-u(ix))');
    
    err(i)=sqrt(errHnorm(mesh2,elements,u,g,penalty));
    s=sprintf('step %d: #dof=%d, sum of eta=%g, true error=%g',i,nDOF(i),seta(i),err(i));
    disp(s);
    if seta(i)<tol
        break;
    else
       if i<maxit
         [mesh3,updated]=refineReg(mesh3); %%regular refine
       end
    end
 
end
