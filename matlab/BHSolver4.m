% BHSolver4.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: solve the following biharmonic equations
%        \Delta^2 u=f; (\partial u)/(\partial n)=g; (\partial \Delta u)/(\partial n)=q;
% and gtau is the tangential derivative  of g on the boundary 
%
function [uG,Ac,ix]=BHSolver4(mesh2,elements,f,g,gtau,q,penalty)

Dc=assemblyTri3(mesh2,elements);
Ec=assemblyEdge3(mesh2,elements,penalty);
ix=find(mesh2.free);
Ac=Dc(ix, ix)+Ec(ix,ix);
bInt=bndInt2(mesh2,elements,g,gtau,q,penalty);
F=bulkInt(mesh2,elements,f);
RHS=F(ix)+bInt(ix);
uGG=Ac\RHS; uG=zeros(1,mesh2.nGI);
uG(ix)=uGG;