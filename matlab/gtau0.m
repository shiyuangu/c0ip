function rv = gtau0(x,y)
% GTAU0 - compute the tangential derivative of g; uxy
r=(x^2+y^2)^(0.5);
theta=acos(x/r);
if(y<0)
    theta=2*pi-theta;
end
   if((x==1 && y>0)||(x==0 &&y<0)||(x==-1))
       rv=uxy(r,theta);
   else if ((x<0 && y==-1) ||(x>0 && y==0) || (y==1))
           rv=-uxy(r,theta);
   else
        disp('something wrong in gtau0.');       
        end
   end
