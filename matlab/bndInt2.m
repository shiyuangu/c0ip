% compute the boundary intergral 
function v=bndInt2(mesh2,elements,g,gtau,q,penalty)
% Oct 30, 2009  rewrite bndInt?...? for debug purpose;
nSize=mesh2.nGI; 
v=zeros(nSize,1);
nEb=size(mesh2.Eb,1);
for i=1:nEb
    mp=mesh2.iP(mesh2.Eb(i,3)).coordinate(1:2);
    p1=mesh2.iP(mesh2.Eb(i,1)).coordinate(1:2);
    p2=mesh2.iP(mesh2.Eb(i,2)).coordinate(1:2);
    indexT=mesh2.Eb(i,4);
    indexP3=setdiff(mesh2.T(indexT,1:3),[mesh2.Eb(i,1) mesh2.Eb(i,2)]);
    p3=mesh2.iP(indexP3).coordinate(1:2);
    len=norm(p2-p1,2);
    tau=(p2-p1)/len; n=[tau(2),-tau(1)];
    if(det([p1' p2' p3';1 1 1])<0)
        n=-n; tau=-tau;
    end
    gp1=mp+sqrt(3)/6*len*tau; 
    gp2=mp-sqrt(3)/6*len*tau;
    for j=1:6
       a=elements(indexT,j).a;
       A=elements(indexT,j).A; 
       B=elements(indexT,j).B;
       gI=mesh2.T(indexT,j);
       % below: compute -(q,v);
       v(gI)=v(gI)-len/2*(q(gp1(1),gp1(2))*a*det([gp1' A; 1 1 1])*det([gp1' B; 1 1 1])+...
                          q(gp2(1),gp2(2))*a*det([gp2' A; 1 1 1])*det([gp2' B; 1 1 1]));
                
       % below: compute -(g,\p^2 v/\p n ^2)         
       C=n*[A(2,1)-A(2,2) A(1,2)-A(1,1)]';
       D=n*[B(2,1)-B(2,2) B(1,2)-B(1,1)]';
       v(gI)=v(gI)-a*len*C*D*(g(gp1(1),gp1(2))+g(gp2(1),gp2(2)));
       
       % below: compute +sig/|e|*(g,\p v/\p n)
       v(gI)=v(gI)+penalty*a/2*(g(gp1(1),gp1(2))*(C*det([gp1' B; 1 1 1])+D*det([gp1' A; 1 1 1]))+...
                                g(gp2(1),gp2(2))*(C*det([gp2' B; 1 1 1])+D*det([gp2' A; 1 1 1])));
       
       % below: compute +(gtau,\p v/\p tau)
       C=tau*[A(2,1)-A(2,2), A(1,2)-A(1,1)]';
       D=tau*[B(2,1)-B(2,2), B(1,2)-B(1,1)]';
       v(gI)=v(gI)+a*len/2*(gtau(gp1(1),gp1(2))*(C*det([gp1' B; 1 1 1])+D*det([gp1' A; 1 1 1]))+...
                            gtau(gp2(1),gp2(2))*(C*det([gp2' B; 1 1 1])+D*det([gp2' A; 1 1 1])));                    
    end
end