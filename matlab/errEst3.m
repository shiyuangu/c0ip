% errEst3.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: compute the error estimator based on the solution vector
% and boundary data. Note that the tangential derivative of g from 
% errEst2 is deleted on 01/23/2010. Refer to errEst4 for the
% error estimator including tagential derivative. 
% The return value eta is two times the error estimator defined in the paper

function eta=errEst3(mesh2,elements,u,f,g,gtau,q,penalty)  
 T=mesh2.T;nT=size(T,1);
 if size(u,1)>1
     u=u';
 end
 eta=zeros(nT,1);
  
for i=1:nT
    mp1=mesh2.iP(T(i,4)).coordinate;
    mp2=mesh2.iP(T(i,5)).coordinate;
    mp3=mesh2.iP(T(i,6)).coordinate;
    area=2*det([mp1' mp2' mp3';1 1 1]);
    eta(i)=8/3*area^3*(f(mp1(1),mp1(2))^2+f(mp2(1),mp2(2))^2+f(mp3(1),mp3(2))^2);
end
nEi=size(mesh2.Ei,1);
for i=1:nEi
    indexT(1)=mesh2.Ei(i,4);
    indexT(2)=mesh2.Ei(i,5);
    map(1)=mesh2.Ei(i,3);
    map(2:3)=mesh2.Ei(i,1:2);
    map(4)  =setdiff(mesh2.T(indexT(1),1:3),map(1:3));
    map(5:6)=setdiff(mesh2.T(indexT(1),4:6),map(1:3));
    map(7)  =setdiff(mesh2.T(indexT(2),1:3),map(1:3));
    map(8:9)=setdiff(mesh2.T(indexT(2),4:6),map(1:3));
    p1=mesh2.iP(map(1)).coordinate;
    p2=mesh2.iP(map(2)).coordinate;
    p3=mesh2.iP(map(3)).coordinate;
    p4=mesh2.iP(map(4)).coordinate;
    len=norm(p3-p2,2);
    tau=zeros(2,2); n=zeros(2,2);
    tau(1,1:2)=(p3-p2)/len; n(1,1:2)=[tau(1,2) -tau(1,1)];
    if(det([p2' p3' p4'; 1 1 1])<0)
        tau(2,:)= tau(1,:); n(2,:)= n(1,:);
        tau(1,:)=-tau(1,:); n(1,:)=-n(1,:);
    else
        tau(2,:)=-tau(1,:); n(2,:)=-n(1,:);
    end
    gp1=p1+sqrt(3)/6*len*tau(1,:);
    gp2=p1-sqrt(3)/6*len*tau(1,:);
    local=zeros(9,9); v=zeros(9,3);
    for k=1:2
        for j=1:6
            t3=find(map==elements(indexT(k),j).gI);
            a=elements(indexT(k),j).a;
            A=elements(indexT(k),j).A;
            B=elements(indexT(k),j).B;
            C=n(k,:)*[A(2,1)-A(2,2) A(1,2)-A(1,1)]';
            D=n(k,:)*[B(2,1)-B(2,2) B(1,2)-B(1,1)]';
            v(t3,1)=v(t3,1)-a*(C*det([gp1' B;1 1 1])+D*det([gp1' A; 1 1 1]));
            v(t3,2)=v(t3,2)-a*(C*det([gp2' B;1 1 1])+D*det([gp2' A; 1 1 1]));
            v(t3,3)=v(t3,3)+(-1)^k*2*a*C*D;
        end
    end
    for s=1:9 
      for t=1:9
         local(s,t)=len/2*(v(s,1)*v(t,1)+v(s,2)*v(t,2));
      end
    end
    term1=penalty^2/len*u(map)*local*u(map)';
    eta(indexT(1))=eta(indexT(1))+term1;
    eta(indexT(2))=eta(indexT(2))+term1;
     for s=1:9 
      for t=1:9
         local(s,t)=len*v(s,3)*v(t,3);
      end
     end
    term2=len*u(map)*local*u(map)';
    eta(indexT(1))=eta(indexT(1))+term2;
    eta(indexT(2))=eta(indexT(2))+term2;
end

nEb=size(mesh2.Eb,1); 
for i=1:nEb
    mp=mesh2.iP(mesh2.Eb(i,3)).coordinate;
    p1=mesh2.iP(mesh2.Eb(i,1)).coordinate;
    p2=mesh2.iP(mesh2.Eb(i,2)).coordinate;
    len=norm(p2-p1,2);
    indexT=mesh2.Eb(i,4);
    tau=(p2-p1)/len; n=[tau(2),-tau(1)];
    p3=setdiff(mesh2.T(indexT,1:3),mesh2.Eb(i,1:2));
    p3=mesh2.iP(p3).coordinate;
    if(det([p1' p2' p3';1 1 1])<0)
        tau=-tau; n=-n;
    end
    gp1=mp+sqrt(3)/6*len*tau;
    gp2=mp-sqrt(3)/6*len*tau;
    h=0; k=0;
    for j=1:6
        a=elements(indexT,j).a;
        A=elements(indexT,j).A;
        B=elements(indexT,j).B;
        gI=elements(indexT,j).gI;
        C=n*[A(2,1)-A(2,2) A(1,2)-A(1,1)]';
        D=n*[B(2,1)-B(2,2) B(1,2)-B(1,1)]';
        h=h+u(gI)*a*(C*det([gp1' B;1 1 1])+D*det([gp1' A; 1 1 1]));
        k=k+u(gI)*a*(C*det([gp2' B;1 1 1])+D*det([gp2' A; 1 1 1]));
    end
    term3=penalty^2/2*((g(gp1(1),gp1(2))-h)^2+(g(gp2(1),gp2(2))-k)^2);
    term4=len^4/2*(q(gp1(1),gp1(2))^2+q(gp2(1),gp2(2))^2);
    %gtaub=1/2*( gtau(gp1(1),gp1(2))+gtau(gp2(1),gp2(2)) );
    %gtaub=0;
    %term5=len^2/2*( ( gtau(gp1(1),gp1(2))-gtaub )^2 + (gtau(gp2(1),gp2(2))-gtaub )^2 );
    term5=0;
    eta(indexT)=eta(indexT)+2*(term3+term4+term5);
end