% testAd2.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: test the performance of adaptive mesh refinement

function [u,mesh3,seta,nDOF,err]=testAd2(mesh3,f,g,gtau,q,penalty,tol,maxit)
seta=zeros(1,maxit);
nDOF=zeros(1,maxit);
err=zeros(1,maxit);
for i=1:maxit
    mesh2=meshConverter(mesh3,'32');
    elements=constructP2Elements2(mesh2);
    [u,Ac,ix]=BHSolver4(mesh2,elements,f,g,gtau,q,penalty);
    eta=errEst3(mesh2,elements,u,f,g,gtau,q,penalty);
    seta(i)=sqrt(sum(eta)*0.5);
    nDOF(i)=mesh2.nDOF;
    err(i)=sqrt(errHnorm(mesh2, elements,u,g,penalty));
    s=sprintf('step %d: #dof=%d, sum of eta=%g, true error=%g',i,nDOF(i),seta(i),err(i));
    disp(s);
    if seta(i)<tol
        break;
    else
       [mesh3,updated]=refine2(mesh3,eta,0.3); %adaptively refine
    end
 
end
