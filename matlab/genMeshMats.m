% genMeshMats.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: take the penalty parameter and a mesh as input 
% (refer to the file mesh3_01.mat for the mesh format) 
% and generate all relevant matrices and qualities and
% and output it as .mat files.
%
function genMeshMats(fin,fout,foutReduced,penalty,maxLevel)
if(nargin<5)
    fprintf('Usage: genMeshMats(fin,fout,foutReduced,penalty,maxLevel)\n');
    return; 
end
load(fin); mesh3=mesh3_01;
%penalty=5;
for i=1:maxLevel
    meshes2{i}=meshConverter(mesh3,'32');
    elements{i}=constructP2Elements2(meshes2{i}); 
    As{i}=assemblyTri3(meshes2{i},elements{i})+assemblyEdge3(meshes2{i},elements{i},penalty);  %%these are the C^0 IP matrices.
    %fprintf(1,'As-AsT=%#.8e\n',norm(As{i}-As{i}',inf));
    eigv=eigs(As{i},2,'sa');
    [value,idx]=max(abs(eigv));
    if(eigv(idx)<0 && eigv(idx)>-1e-10)
      fprintf(2,'on level %d, As{i} is not SPD!(eigv=%#0.3e)\n',i,eigv(idx));
      save(fout);
      return; 
    end
    Ss{i}=assemblyStiff2(meshes2{i});  % stiffness matrices for Poisson Solver
    Ms{i}=assemblyMass2(meshes2{i});    % mass matrices 
    if(i>1)
       Is{i-1}=assemblyIntp(meshes2{i-1},meshes2{i},parent);   %% Interperation matrices between meshes. 
    end
    if(i<maxLevel)
      [mesh3,parent]=refineReg(mesh3);   
    end
    fprintf(1,'Mat assembly on level %d is completed on %s\n',i,datestr(now));
end
Vs=constructMGVec(meshes2);            % integral of bases of shape functions over the whole domain

I2s{1}=[]; % This the I_k^{k-1}.
for i=1:maxLevel
    [vB,Phi{i}]=assemblyBPhi(meshes2{i});
    vBinv=vB.^(-1);
    n=max(size(vB));
    Bs{i}=sparse(1:n,1:n,vB,n,n,n);
    Binvs{i}=sparse(1:n,1:n,vBinv,n,n,n);
% the following line is commented otherwise it may not have enough memory for level 7; 
% S2s{i}=Binvs{i}*Ss{i}; %%%Stiffness matrix under the inner product . 
    if(i>1)
       I2s{i}=Binvs{i-1}*Is{i-1}'*Bs{i};
    end
end
% this following matrices only needed for convenience
% commented out on 09/24/2010
% for i=1:k
%     n=size(S2s{i},1);
%     Shats{i}=zeros(n);
%     w=Phi{i};
%     for j=1:n 
%        v=S2s{i}(:,j);
%        Shats{i}(:,j)=v-w*((v'*Bs{i}*w)/(w'*Bs{i}*w));
%     end
% end
% The following is added on 09/21/2010
% vBs and vBinvs are the data entries of Bs, Binvs
for i=1:maxLevel
vBs{i}=diag(Bs{i});vBinvs{i}=diag(Binvs{i});
end
for i=1:maxLevel
   cPhi{i}=vBinvs{i}.*Phi{i}; %corrected Phi in usual l2; for compute spectrum 
end
for i=1:maxLevel
lambda1(i)=computeSpecRia3(i,Ss,Bs,cPhi);
lambda2_PS(i)=computeSpecRia2(i,As,Ss,cPhi);
lambda2_NP(i)=computeSpecRia3(i,As,Bs,cPhi);
fprintf(1,'Spectrum on level %d is completed on %s\n\n',i,datestr(now));
end
save(fout,'-v7.3');
save(foutReduced,'As','Ss','Is','Phi','vBs','vBinvs','lambda1','lambda2_PS','lambda2_NP','meshes2','Ms');