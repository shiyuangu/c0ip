% refineReg.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: regular refinement: updated is the parant of the new triangles
function [mesh3,updated]=refineReg(mesh3)
node=mesh3.node; T=mesh3.T;  
edge=[T(:,[1 2]);T(:,[2 3]);T(:,[1 3])];
edge=unique(sort(edge,2),'rows');
nN=size(node,1); nT=size(T,1); nE=size(edge,1);
updated=zeros(nT,1);
dE2t=sparse(T(:,[1 2 3]), T(:,[2 3 1]), [1:nT 1:nT 1:nT],nN,nN);
p2e=sparse(edge(:,[1 2]),edge(:,[2 1]),[1:nE,1:nE],nN,nN);
p2nv=sparse(nN,nN);  %%the new vertices
for i=1:nT
  indexP=T(i,1:3);
  for j=1:3
      v1=indexP(2); v2=indexP(3);
      vMid=p2nv(v1,v2);
      if vMid==0
          nN=nN+1; 
          node(nN,:)=mean(node([v1 v2],:),1);
          p2nv(v1,v2)=nN;
          p2nv(v2,v1)=nN;
      end
      indexP=indexP([2 3 1]);
  end
  v1=indexP(1); v2=indexP(2); v3=indexP(3);
  v4=p2nv(v2,v3); v5=p2nv(v3,v1); v6=p2nv(v1,v2);
  T(i,:)=[v4 v5 v6];updated(i)=i; 
  T(end+1,:)=[v1 v6 v5]; updated(end+1)=i;
  T(end+1,:)=[v2 v4 v6]; updated(end+1)=i;
  T(end+1,:)=[v3 v5 v4]; updated(end+1)=i;
end
mesh3.node=node; mesh3.T=T;
%figure;trimesh(mesh3.T,mesh3.node(:,1),mesh3.node(:,2),zeros(size(mesh3.node,1),1));