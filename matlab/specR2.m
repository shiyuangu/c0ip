% specR2.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gui <sgu@anl.gov>
% brief: modify power method to compute the spectral radius of
% inv(B)*A on the subspace orthogonal to vector phi. Note that B is
% not invertible in the whole space and assume Ker(B)=const  
function [lambda,v]=specR2(A, B, phi,len)
tol=1e-2;
maxit=10; 
i=0; lambda=0; 
RandStream.setDefaultStream(RandStream('mt19937ar','seed',100));
z0=rand(size(A,1),1); z0=(z0-0.5)*2;
v=proj(z0,phi);
while 1
    i=i+1;
    lambdaP=lambda;
    rhs=A*v;
    v(1)=0;
    v(2:end)=B(2:end,2:end)\rhs(2:end);
    v=shiftOrth(v,phi);
    lambda=v'*A*v/(v'*B*v);
    [nf,index]=max(abs(v));
    v=v/v(index); v=proj(v,phi);
    if (abs((lambda-lambdaP)/lambda)<tol)
        break
    end
    if i>maxit
        disp('fail to converge.')
        break
    end
end

function vout=proj(vin,phi)
x=dot(vin,phi)/(phi'*phi);
vout=vin-x*phi;
return;

function vout=shiftOrth(vin,phi)
x=dot(vin,phi)/sum(phi);
vout=vin-x;