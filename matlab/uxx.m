function uxx = uxx(r,theta)
%UXX
%    UXX = UXX(R,THETA)

%    This function was generated by the Symbolic Math Toolbox version 5.5.
%    22-Aug-2011 15:39:36

t440 = cos(theta);
t442 = r.^2;
t447 = t440.^2;
t452 = t442.*t447;
t441 = t452-1.0;
t443 = sin(theta);
t448 = t443.^2;
t449 = t442.*t448;
t444 = t449-1.0;
t445 = theta.*(2.0./3.0);
t446 = cos(t445);
t450 = t444.^2;
t451 = r.^(1.0e1./3.0);
t453 = t441.^2;
t454 = r.^(4.0./3.0);
t455 = sin(t445);
t456 = r.^(7.0./3.0);
t457 = r.^(1.3e1./3.0);
t458 = 1.0./r;
t459 = r.^(1.0./3.0);
t460 = t450.*t453.*t455.*t459.*(8.0./9.0);
t461 = t441.*t447.*t450.*t455.*t456.*(8.0./3.0);
t462 = t444.*t448.*t453.*t455.*t456.*(8.0./3.0);
t463 = t440.*t443.*t446.*t447.*t450.*t457.*8.0;
t464 = t440.*t441.*t443.*t446.*t450.*t456.*(4.0e1./3.0);
t465 = t440.*t441.*t443.*t444.*t446.*t448.*t457.*1.6e1;
t466 = t460+t461+t462+t463+t464+t465-t440.*t443.*t444.*t446.*t453.*t456.*(4.0e1./3.0)-t440.*t443.*t446.*t448.*t453.*t457.*8.0-t440.*t441.*t443.*t444.*t446.*t447.*t457.*1.6e1;
t467 = t450.*t453.*t454.*t455.*(2.0./3.0);
t468 = t440.*t441.*t443.*t446.*t450.*t451.*4.0;
t469 = t467+t468-t440.*t443.*t444.*t446.*t451.*t453.*4.0;
t470 = r.^(1.6e1./3.0);
uxx = t440.*(t440.*(1.0./r.^(2.0./3.0).*t446.*t450.*t453.*(4.0./9.0)+t446.*t447.^2.*t450.*t451.*8.0+t446.*t448.^2.*t451.*t453.*8.0+t441.*t446.*t447.*t450.*t454.*(4.4e1./3.0)+t444.*t446.*t448.*t453.*t454.*(4.4e1./3.0)+t441.*t444.*t446.*t447.*t448.*t451.*3.2e1)-1.0./r.^2.*t443.*t469+t443.*t458.*t466)+t443.*t458.*(t440.*t466+t443.*(t446.*t450.*t453.*t459.*(4.0./3.0)+t441.*t446.*t447.*t450.*t456.*4.0+t444.*t446.*t448.*t453.*t456.*4.0)-t440.*t458.*t469-t443.*t458.*(t446.*t450.*t453.*t454.*(4.0./9.0)+t441.*t446.*t447.*t450.*t451.*4.0-t441.*t446.*t448.*t450.*t451.*4.0-t444.*t446.*t447.*t451.*t453.*4.0+t444.*t446.*t448.*t451.*t453.*4.0-t446.*t447.*t448.*t450.*t470.*8.0-t446.*t447.*t448.*t453.*t470.*8.0-t440.*t441.*t443.*t450.*t451.*t455.*(1.6e1./3.0)+t440.*t443.*t444.*t451.*t453.*t455.*(1.6e1./3.0)+t441.*t444.*t446.*t447.*t448.*t470.*3.2e1));
