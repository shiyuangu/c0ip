function rv = q0(x,y)
% Q0 - compute q=\p n Delta u for based on extern file dxDu.m dyDu.m. 
%   
r=(x^2+y^2)^(0.5);
theta=acos(x/r);
if(y<0)
    theta=2*pi-theta;
end
  if((x==1 && y>0)||(x==0 &&y<0))
    rv=dxDu(r,theta);
else if(y==1)
    rv=dyDu(r,theta);
    else if(x==-1)
           rv=-dxDu(r,theta);
        else if((x<0 && y==-1) ||(x>0 && y==0))
           rv=-dyDu(r,theta);
            else
                disp('in q0:points being evaluated but should not\n');
            end
        end
    end
end