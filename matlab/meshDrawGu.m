% draw the mesh
function meshDrawGu(mesh,meshType)
switch meshType
    case 1
        T=mesh.T(:,1:3);
        Eb=mesh.Eb;Ei=mesh.Ei;Vp=mesh.Vp; Vn=mesh.Vn;
        Label=4;
        figure
        hold on
        [rowT,colT]=size(T);
        if (Label==1) || (Label== 3)
            [rowVp,colVp]=size(Vp);
            for j=1:rowVp
                text(Vp(j,1),Vp(j,2),int2str(j),'FontSize',20)
            end
            [rowVn,colVn]=size(Vn);
            for j=1:rowVn
                text(Vn(j,1),Vn(j,2),int2str(-j),'FontSize',20)
            end
        end
        %
        if (Label==2)||(Label==3)
            fe=10;
            ft=10;
            r=.02;
            s=.03;
            a=.02;
            for j=1:rowT
                ind=T(j,1);
                if ind>0,
                    x1=Vp(ind,1:2);  % coordinates of vertices in ascending order
                else
                    x1=Vn(-ind,1:2);
                end
                ind=T(j,2);
                if ind>0,
                    x2=Vp(ind,1:2);  % coordinates of vertices in ascending order
                else
                    x2=Vn(-ind,1:2);
                end
                ind=T(j,3);
                if ind>0,
                    x3=Vp(ind,1:2);  % coordinates of vertices in ascending order
                else
                    x3=Vn(-ind,1:2);
                end
                c=(x1+x2+x3)/3;  % coordinates of the center of the triangle
                text(c(1,1),c(1,2),int2str(j),...
                    'FontSize',ft,'Color','r')
            end
        end
        %
        [rowEb,colEb]=size(Eb);
        for k=1:rowEb
            ind=Eb(k,1);
            if ind>0
                x=Vp(ind,1:2);
            else
                x=Vn(-ind,1:2);
            end
            %
            ind=Eb(k,2);
            if ind>0
                y=Vp(ind,1:2);
            else
                y=Vn(-ind,1:2);
            end
            draw(x(1),y(1),x(2),y(2))
           % text(0.5*(x(1)+y(1)),0.5*(x(2)+y(2)),int2str(-k),'FontSize',ft,'Color','k') %%by Gu
        end
        %
        [rowEi,colEi]=size(Ei);
        for k=1:rowEi
            ind=Ei(k,1);
            if ind>0
                x=Vp(ind,1:2);
            else
                x=Vn(-ind,1:2);
            end
            %
            ind=Ei(k,2);
            if ind>0
                y=Vp(ind,1:2);
            else
                y=Vn(-ind,1:2);
            end
            draw(x(1),y(1),x(2),y(2));
          %  text(0.5*(x(1)+y(1)),0.5*(x(2)+y(2)),int2str(k),'FontSize',ft,'Color','k') %%by Gu
        end
        %
        hold off
    case 2
      figure; hold on;
      ft=10;
      nT=size(mesh.T,1);
       for i=1:nT
        c=zeros(3,2);
        for j=1:3;
        c(j,1:2)=mesh.iP(mesh.T(i,j)).coordinate;
        end;
        c=mean(c);
        text(c(1,1),c(1,2),int2str(i),'FontSize',ft,'Color','b');
      end
      nN=mesh.nGI;
      nEi=size(mesh.Ei,1);
      for i=1:nEi
          p1(1:2)=mesh.iP(mesh.Ei(i,1)).coordinate;
          p2(1:2)=mesh.iP(mesh.Ei(i,2)).coordinate;
          draw(p1(1),p2(1),p1(2),p2(2));
      end
      nEb=size(mesh.Eb,1);
      for i=1:nEb
          p1(1:2)=mesh.iP(mesh.Eb(i,1)).coordinate;
          p2(1:2)=mesh.iP(mesh.Eb(i,2)).coordinate;
          draw(p1(1),p2(1),p1(2),p2(2));
      end
      for i=1:nN
        h=text(mesh.iP(i).coordinate(1),mesh.iP(i).coordinate(2),int2str(i),'FontSize',ft);
        if(mesh.iP(i).vertex)
            set(h,'Color','k');
        else 
            set(h,'Color','r');
        end
%         if(mesh.iP(i).boundary)
%             set(h,'FontAngle','italic');
%         else
%             set(h,'FontAngle','normal');
%         end
       % text(mesh.iP(i).coordinate(1),mesh.iP(i).coordinate(2),int2str(i),'FontSize',ft);
      end
      hold off;
    case 3
      figure; hold on;
      ft=10; 
      nT=size(mesh.T,1);
      edge=[mesh.T(:,[2 3]);mesh.T(:,[3 1]); mesh.T(:,[1 2])];
      edge=unique(sort(edge,2),'rows'); nE=size(edge,1);
      for i=1:nT
        c=mean(mesh.node(mesh.T(i,:),:));
        % below: switch on/off for numbering
        % text(c(1,1),c(1,2),int2str(i),'FontSize',ft,'Color','r');
      end
      for i=1:nE
        % c=mean(node(edge(i,:),:));
        p1(1:2)=mesh.node(edge(i,1),:); p2(1:2)=mesh.node(edge(i,2),:);
        draw(p1(1),p2(1),p1(2),p2(2));
      end
      nN=size(mesh.node,1);
%       below: switch on/off for numbering
%       for i=1:nN
%           text(mesh.node(i,1),mesh.node(i,2),int2str(i),'FontSize',ft,'Color','b');
%       end
     hold off;
end
function draw(x1,x2,y1,y2)
line([x1 x2], [y1 y2], 'Color','k');