function rv=errHnorm(mesh2, elements,u,g,penalty)
% using numerical intergration to compute the h-norm:
% exact solution are defined as sub-function
% ||u-u_h||_h=sum|u-u_h|_H^2+sum(penalty/|e|||jump(u-u_h)/n||_L2(e)
 T=mesh2.T;nT=size(T,1);
 if size(u,1)>1
     u=u';
 end
 err=0;
  
for i=1:nT
    uxx=0;uyy=0;uxy=0;
    for p=1:6
        a=elements(i,p).a; 
        A=elements(i,p).A;
        B=elements(i,p).B;
       gI=elements(i,p).gI;
        g11=A(2,1)-A(2,2); g12=A(1,2)-A(1,1);
        g21=B(2,1)-B(2,2); g22=B(1,2)-B(1,1);
        wp=a*[2*g11*g21 g12*g21+g11*g22; g12*g21+g11*g22 2*g22*g12];
        uxx=uxx+wp(1,1)*u(gI);
        uyy=uyy+wp(2,2)*u(gI);
        uxy=uxy+wp(1,2)*u(gI);
    end
    mp1=mesh2.iP(T(i,4)).coordinate;
    mp2=mesh2.iP(T(i,5)).coordinate;
    mp3=mesh2.iP(T(i,6)).coordinate;
    area=2*det([mp1' mp2' mp3';1 1 1]);
    %eta(i)=8/3*area^3*(f(mp1(1),mp1(2))^2+f(mp2(1),mp2(2))^2+f(mp3(1),mp3(2))^2);
    err=err+area/3*((exUxx(mp1)-uxx)^2+(exUyy(mp1)-uyy)^2+(exUxy(mp1)-uxy)^2+...
                   +(exUxx(mp2)-uxx)^2+(exUyy(mp2)-uyy)^2+(exUxy(mp2)-uxy)^2+...
                   +(exUxx(mp3)-uxx)^2+(exUyy(mp3)-uyy)^2+(exUxy(mp3)-uxy)^2);
end
nEi=size(mesh2.Ei,1);
for i=1:nEi
    indexT(1)=mesh2.Ei(i,4);
    indexT(2)=mesh2.Ei(i,5);
    map(1)=mesh2.Ei(i,3);
    map(2:3)=mesh2.Ei(i,1:2);
    map(4)  =setdiff(mesh2.T(indexT(1),1:3),map(1:3));
    map(5:6)=setdiff(mesh2.T(indexT(1),4:6),map(1:3));
    map(7)  =setdiff(mesh2.T(indexT(2),1:3),map(1:3));
    map(8:9)=setdiff(mesh2.T(indexT(2),4:6),map(1:3));
    p1=mesh2.iP(map(1)).coordinate;
    p2=mesh2.iP(map(2)).coordinate;
    p3=mesh2.iP(map(3)).coordinate;
    p4=mesh2.iP(map(4)).coordinate;
    len=norm(p3-p2,2);
    tau=zeros(2,2); n=zeros(2,2);
    tau(1,1:2)=(p3-p2)/len; n(1,1:2)=[tau(1,2) -tau(1,1)];
    if(det([p2' p3' p4'; 1 1 1])<0)
        tau(2,:)= tau(1,:); n(2,:)= n(1,:);
        tau(1,:)=-tau(1,:); n(1,:)=-n(1,:);
    else
        tau(2,:)=-tau(1,:); n(2,:)=-n(1,:);
    end
    gp1=p1+sqrt(3)/6*len*tau(1,:);
    gp2=p1-sqrt(3)/6*len*tau(1,:);
    local=zeros(9,9); v=zeros(9,3);
    for k=1:2
        for j=1:6
            t3=find(map==elements(indexT(k),j).gI);
            a=elements(indexT(k),j).a;
            A=elements(indexT(k),j).A;
            B=elements(indexT(k),j).B;
            C=n(k,:)*[A(2,1)-A(2,2) A(1,2)-A(1,1)]';
            D=n(k,:)*[B(2,1)-B(2,2) B(1,2)-B(1,1)]';
            v(t3,1)=v(t3,1)-a*(C*det([gp1' B;1 1 1])+D*det([gp1' A; 1 1 1]));
            v(t3,2)=v(t3,2)-a*(C*det([gp2' B;1 1 1])+D*det([gp2' A; 1 1 1]));
            v(t3,3)=v(t3,3)+(-1)^k*2*a*C*D;
        end
    end
    for s=1:9 
      for t=1:9
         local(s,t)=len/2*(v(s,1)*v(t,1)+v(s,2)*v(t,2));
      end
    end
    term1=penalty/len*u(map)*local*u(map)'; %%different from the corresponding term in estimator. 
    err=err+term1;
end

nEb=size(mesh2.Eb,1); 
for i=1:nEb
    mp=mesh2.iP(mesh2.Eb(i,3)).coordinate;
    p1=mesh2.iP(mesh2.Eb(i,1)).coordinate;
    p2=mesh2.iP(mesh2.Eb(i,2)).coordinate;
    len=norm(p2-p1,2);
    indexT=mesh2.Eb(i,4);
    tau=(p2-p1)/len; n=[tau(2),-tau(1)];
    p3=setdiff(mesh2.T(indexT,1:3),mesh2.Eb(i,1:2));
    p3=mesh2.iP(p3).coordinate;
    if(det([p1' p2' p3';1 1 1])<0)
        tau=-tau; n=-n;
    end
    gp1=mp+sqrt(3)/6*len*tau;
    gp2=mp-sqrt(3)/6*len*tau;
    h=0; k=0;
    for j=1:6
        a=elements(indexT,j).a;
        A=elements(indexT,j).A;
        B=elements(indexT,j).B;
        gI=elements(indexT,j).gI;
        C=n*[A(2,1)-A(2,2) A(1,2)-A(1,1)]';
        D=n*[B(2,1)-B(2,2) B(1,2)-B(1,1)]';
        h=h+u(gI)*a*(C*det([gp1' B;1 1 1])+D*det([gp1' A; 1 1 1]));
        k=k+u(gI)*a*(C*det([gp2' B;1 1 1])+D*det([gp2' A; 1 1 1]));
    end
    term3=penalty/2*((g(gp1(1),gp1(2))-h)^2+(g(gp2(1),gp2(2))-k)^2); %%different from the estimator penalty^2->penalty
    err=err+term3;
    %term4=len^4/2*(q(gp1(1),gp1(2))^2+q(gp2(1),gp2(2))^2);
    %gtaub=1/2*( gtau(gp1(1),gp1(2))+gtau(gp2(1),gp2(2)) );
    %gtaub=0;
    %term5=len^2/2*( ( gtau(gp1(1),gp1(2))-gtaub )^2 + ( gtau(gp2(1),gp2(2))-gtaub )^2 );
    %eta(indexT)=eta(indexT)+2*(term3+term4+term5);
end
rv=err;

function rv=exUxx(p)
x=p(1);y=p(2);
r=sqrt(x^2+y^2);
theta=acos(x/r);
if(y<0)
    theta=2*pi-theta;
end
% ux=symDx(u,r,theta);uxx=symDx(ux,r,theta);
rv=uxx(r,theta);

function rv=exUxy(p)
x=p(1);y=p(2);
r=sqrt(x^2+y^2);
theta=acos(x/r);
if(y<0)
    theta=2*pi-theta;
end
rv=uxy(r,theta);

function rv=exUyy(p)
x=p(1);y=p(2);
r=sqrt(x^2+y^2);
theta=acos(x/r);
if(y<0)
    theta=2*pi-theta;
end
rv=uyy(r,theta);

 














