% experimentForPaper2.m --- 
% Copyright (C)2013  S. Gu
% author: S. Gu <sgu@anl.gov>
% brief: Use mesh3_01.mat as input file produces the numerical results
%        used in our SIAM paper. 
clear all; 
%load mesh3_01.mat;
infile=input('Enter Input finename:\n','s');
load(infile);
outfile=input('Enter Output filename:','s');

[u,mesh3Uni,setaUni,nDOFUni,errUni]=testReg(mesh3_01,@f0,@g0,@gtau0,@q0,5,1e-2,7);
[u,mesh3,setaAd,nDOFAd,errAd]=testAd2(mesh3_01,@f0,@g0,@gtau0,@q0,5,1e-2,10);
mesh3_11=mesh3;save(outfile);fprintf(1,'Data saved at %s',outfile);
[u,mesh3,setaAd,nDOFAd,errAd]=testAd2(mesh3_01,@f0,@g0,@gtau0,@q0,5,1e-2,20);
mesh3_21=mesh3;save(outfile);fprintf(1,'Data saved at %s',outfile);
[u,mesh3,setaAd,nDOFAd,errAd]=testAd2(mesh3_01,@f0,@g0,@gtau0,@q0,5,1e-2,30);
mesh3_31=mesh3;
[u,mesh3,setaAd,nDOFAd,errAd]=testAd2(mesh3_01,@f0,@g0,@gtau0,@q0,5, ...
                                     1e-2,40);%
mesh3_41=mesh3;
[u,mesh3,setaAd,nDOFAd,errAd]=testAd2(mesh3_01,@f0,@g0,@gtau0,@q0,5,1e-2,50);
%mesh3_51=mesh3;
[u,mesh3, setaAd_10,nDOFAd10,errAd10]=testAd2(mesh3_01,@f0,@g0,@gtau0,@q0,10,1e-2,49);
[u,mesh3, setaAd_20,nDOFAd20,errAd20]=testAd2(mesh3_01,@f0,@g0,@gtau0,@q0,20,1e-2,49);
[u,mesh3, setaAd_50,nDOFAd50,errAd50]=testAd2(mesh3_01,@f0,@g0,@gtau0,@q0,50,1e-2,49);
save(outfile);fprintf(1,'Data saved at %s',outfile); 
% 
% The following is to draw the graph on paper
% x=logspace(1,5);y=x.^(-1/2)*4;
% loglog(nDOFUni,setaUni,'-dk',nDOFUni,errUni,'-sk',nDOFAd,setaAd,'-*k',nDOFAd,errAd,'-+k',x,y,'-ok');
% legend('Estimator on uniform meshes','Error on uniform meshes','Estimator on adaptive meshes','Error on adaptive meshes','Optimal convergence');
% eff_5=setaAd./errAd; eff_10=setaAd_10./errAd10;eff_20=setaAd_20./errAd20;eff_50=setaAd_50./errAd50;
% semilogx(nDOFAd,eff_5,'-+k',nDOFAd10,eff_10,'-*k',nDOFAd20,eff_20, ...
%           '-sk',nDOFAd50,eff_50,'-^k');
% legend('\sigma=5','\sigma=10','\sigma=20','\sigma=50');