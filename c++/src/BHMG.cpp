/**
 * @file   BHMG.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:29:26 2013
 * 
 * @brief: Multigrid solver for biharmonic problems. It can optionally use 
 *         another multigrid solver as a preconditioner in
 *         the smoothing steps. 
 */
#include<cmath>
#include<exception>
#include<iostream>
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"converters.h"
#include"slepceps.h"
#include"BHMG.h"
#include"KSPGu.h"
BHMG::BHMG(const char* path,double _c1,double _c2, double _c3,double _c4)
  :c1(_c1),c2(_c2),c3(_c3),c4(_c4){
  PetscErrorCode ierr;
  char buffer[256];
  PetscBool flag;
  #if MY_MAT==1
      ierr=PetscOptionsGetString(PETSC_NULL,"-vec_type",buffer,256,&flag);CHKERRV(ierr);
      if(!flag || strcmp(buffer,VECSEQCUSP)){
	std::cerr<<"MY_MAT==1,please set -vec_type seqcusp\n";
	ierr=1;CHKERRV(ierr);
      }
      ierr=PetscOptionsGetString(PETSC_NULL,"-gu_mymat",buffer,256,&flag);CHKERRV(ierr);
      if(flag){
	if(!strcmp(buffer,"cuspcsr")){
	  myMatDevType=gu::CUSP_CSR;
	  PetscPrintf(PETSC_COMM_SELF,"-gu_mymat=cuspcsr\n");
	}
      }else{
	 myMatDevType=gu::CUSP_ELL;
         PetscPrintf(PETSC_COMM_SELF,"-gu_mymat=cuspell\n");
      }
  #endif
      MGBHMode='W';PrecMode='V'; mMGNeu=5; dampingScale=0.55;
  readMGOpts();
  pAs=new MyMatArray; pSs=new MyMatArray;pMs=new MyMatArray;
  pIs=new MyMatArray;pIts=new MyMatArray;
  pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;   
  if(read(path)){
      PetscEnd();
      // exit(1);//PetscEnd() has exit(0)
  }
  genMG();
}
void BHMG::readMGOpts(){
    PetscBool flag;
    char mode[2];
    //PetscOptionsGetString(PETSC_NULL,"-gu_MGBHMode",&MGBHMode,1,&flag);
    PetscOptionsGetString(PETSC_NULL,"-gu_MGBHMode",mode,2,&flag);
    MGBHMode=mode[0];
    if(!flag||!(MGBHMode=='V' || MGBHMode=='W' || MGBHMode=='F')){
	fprintf(stderr,"Error:Please specify -gu_MGBHMode\n");
        PetscEnd();
    }else{
      printf("-gu_MGBHMode=%c\n",MGBHMode);
    }

    //PetscOptionsGetString(PETSC_NULL,"-gu_PrecMode",mode,1,&flag);
    PetscOptionsGetString(PETSC_NULL,"-gu_PrecMode",mode,2,&flag);
    PrecMode=mode[0];
    if(!flag||!(PrecMode=='V' || PrecMode=='W' || PrecMode=='F'||PrecMode=='N')){
	fprintf(stderr,"Error:Please specify -gu_PrecMode\n");
        PetscEnd();
    }else printf("-gu_PrecMode=%c\n",PrecMode);
    
    PetscOptionsGetInt(PETSC_NULL,"-gu_mMGNeu",&mMGNeu,&flag);
    if((PrecMode!='N') && (!flag||mMGNeu<0)){
      fprintf(stderr,"Error:Please specify -gu_mMGNeu\n");
      PetscEnd();
    }else printf("-gu_mMGNeu=%d\n",mMGNeu);
    
     PetscOptionsGetInt(PETSC_NULL,"-gu_mMG",&mMG,&flag);
    if(!flag||mMG<0){
      fprintf(stderr,"Error:Please specify -gu_mMG\n");
      PetscEnd();
    }else printf("-gu_mMG=%d\n",mMG);
    
    PetscOptionsGetReal(PETSC_NULL,"-gu_dampingScale",&dampingScale,&flag);
    if(!flag) dampingScale=0.55;
    printf("-gu_dampingScale=%g\n",dampingScale);
   
    } 
int BHMG::read(const char* path){
  mxGuCellArrayT<mxGuSparse> mxAs,mxSs,mxMs,mxIs,mxvBs,mxvBinvs;
  mxGuCellArrayT<mxGuDouble> mxPhi;
  mxGuCellArrayT<mxMesh2> mxMesh2es;
  mxGuDouble mxLambda1,mxLambda2; 
  FILE* fin;
  char fname[256]; 
  int i;
  PetscBool flag;
  PetscErrorCode ierr;

  sprintf(fname,"%s/%s",path, "As");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s\n",fname);
    return 1;
  }
  mxAs.read(fin);
  fclose(fin);
  //pAs->getFrom(mxAs);
  #if MY_MAT
  pAs->getFrom(mxAs,myMatDevType);
  #else
  pAs->getFrom(mxAs);
  #endif

  PetscOptionsGetInt(PETSC_NULL,"-gu_maxlevel",&maxlevel,&flag);
  if(!flag || maxlevel>pAs->size){
  //maxlevel is obtain from pAs->size;
   maxlevel=pAs->size;
  }
  printf("maxlevel=%d\n",maxlevel);

  sprintf(fname,"%s/%s",path, "Ss");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s\n",fname);
    return 1;
  }
  mxSs.read(fin);
  fclose(fin);
  #if MY_MAT
  pSs->getFrom(mxSs,myMatDevType);
  #else
  pSs->getFrom(mxSs);
  #endif
  
  

  sprintf(fname,"%s/%s",path, "Ms");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s\n",fname);
    return 1;
  }
  mxMs.read(fin);
  fclose(fin);
  #if MY_MAT
  pMs->getFrom(mxMs,myMatDevType);  
  #else
  pMs->getFrom(mxMs);  
  #endif

 if(c1<=0.0){ 
    PetscOptionsGetReal(PETSC_NULL,"-c1", &c1, &flag);
    if(!flag){
	printf("c1=0; Singular problem.\n");
	c1=0.0;
    }
  }
 printf("c1=%#.3e\n",c1);
 if(c2<=0.0){
   PetscOptionsGetReal(PETSC_NULL,"-c2", &c2, &flag);
   if(!flag){
	printf("c2=0; no 2nd order term.\n");
	c2=0.0;
      }
 }
 printf("c2=%#.3e\n",c2);
 if(c3<=0.0){
   PetscOptionsGetReal(PETSC_NULL,"-c3",&c3,&flag);
   if(!flag){
     printf("c3=c1.\n");
     c3=c1;
   }
}
 printf("c3=%#.3e\n",c3);
 
PetscOptionsGetReal(PETSC_NULL,"-c4",&c4,&flag);
   if(!flag){
     c4=1.0;
   }
 printf("c4=%#.3e\n",c4);
  /////compute A+cM and S+cM////////
  if(fabs(c1)>0.0){
    for(i=0;i<maxlevel;i++){
      ierr=MatAXPY(pAs->pVal[i],c1,pMs->pVal[i],SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
      ierr=MatAXPY(pAs->pVal[i],c2,pSs->pVal[i],SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
      ierr=MatScale(pSs->pVal[i],c4);CHKERRQ(ierr);
      ierr=MatAXPY(pSs->pVal[i],c3,pMs->pVal[i],DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
    }
  }

  sprintf(fname,"%s/%s",path, "Is");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s\n",fname);
    return 1;
  }
  mxIs.read(fin);
  fclose(fin);
  pIs->getFrom(mxIs);

  pIts->getFromTranspose(*pIs);

  sprintf(fname,"%s/%s",path, "vBs");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s\n",fname);
    return 1;
  }
  mxvBs.read(fin);
  fclose(fin);
  pvBs->getFrom(mxvBs);
  
  sprintf(fname,"%s/%s",path, "vBinvs");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s\n",fname);
    return 1;
  }
  mxvBinvs.read(fin);
  fclose(fin);
  pvBinvs->getFrom(mxvBinvs);

  sprintf(fname,"%s/%s",path, "Phi");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s\n",fname);
    return 1;
  }
  mxPhi.read(fin);
  fclose(fin);
  pPhi->getFrom(mxPhi);

  sprintf(fname,"%s/%s",path, "meshes2");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s\n",fname);
    return 1;
  }
  mxMesh2es.read(fin);
  fclose(fin);
  mesh2es.getFrom(mxMesh2es);


  //for(i=0;i<pAs->size;i++){fscanf(fin,"%le",lambda1+i);lambda1[i]=lambda1[i]*0.55;}
  if(fabs(c1)==0.0 && fabs(c2)==0.0 && fabs(c3)==0.0 && fabs(c4)==1.0){
    sprintf(fname,"%s/%s",path,"lambda1");
    fin=fopen(fname,"r");
    if(fin==NULL){
       fprintf(stderr,"error in opening file %s\n",fname);
       return 1;
    }
    mxLambda1.read(fin); 
    fclose(fin);
    if(mxLambda1.nrow*mxLambda1.ncol!=pAs->size){
      fprintf(stderr,"Error: the size in %s does not match As\n",fname);
      return 1;
    }
    lambda1=new double[pAs->size];
    for(i=0;i<pAs->size;i++)lambda1[i]=mxLambda1.pr[i]*dampingScale;
    if(PrecMode!='N')sprintf(fname,"%s/%s",path,"lambda2_PS");
    else sprintf(fname,"%s/%s",path,"lambda2_NP");
    fin=fopen(fname,"r");
    if(fin==NULL){
      fprintf(stderr,"error in opening file %s\n",fname);
      return 1;
    }
    mxLambda2.read(fin);
    fclose(fin);
    if(mxLambda2.nrow*mxLambda2.ncol!=pAs->size){fprintf(stderr,"Error:the size in %s does not match As\n",fname);return 1;}
    lambda2=new double[pAs->size];for(i=0;i<pAs->size;i++)lambda2[i]=mxLambda2.pr[i]*dampingScale;
  }else{    
    lambda1=new double[maxlevel];
    lambda2=new double[maxlevel];
    FILE* file;
    char fileName[PETSC_MAX_PATH_LEN],buf[4096];
    PetscInt tmpN;
    ierr=PetscOptionsGetString(PETSC_NULL,"-gu_SR_fin",fileName,PETSC_MAX_PATH_LEN,&flag);CHKERRQ(ierr);
    if(flag){
      ierr=PetscFOpen(PETSC_COMM_SELF,fileName,"r",&file);CHKERRQ(ierr);
      //ignore the first line
      fgets(buf,4095,file);
      fscanf(file,"%d\n",&tmpN);
      if(tmpN<maxlevel){
	SETERRQ2(PETSC_COMM_SELF,1,"the file %s should have %d numbers for both lambda1 and lambda2\n",fileName,maxlevel);
      }
      for(i=0;i<maxlevel;i++){
	fscanf(file,"%le %le \n",(double*)(lambda1+i),(double*)(lambda2+i));
	lambda1[i]=dampingScale*lambda1[i];
	lambda2[i]=dampingScale*lambda2[i];
      }
      // for(i=0;i<maxlevel;i++){
      //fscanf(file,"%le\n",(double*)(lambda2+i));
      //	lambda2[i]=dampingScale*lambda2[i];
      //}
      fclose(file);
    }else{
      #if CUDA==1
      SETERRQ(PETSC_COMM_SELF,1,"Please specify spectral radius by -gu_SR_fin or set CUDA=0, recompile, rerun and compute them\n");
      #else 
      printf("Spectral radius is not provided by -gu_SR_fin, and now compute them\n");
    EPS eps;
    const EPSType type;
    PetscInt nev,maxit,nconv,epsits;    //# of requested eigenvalues, max #iterations,#converged eigen-pairs
    PetscScalar eigvr,eigvi;
    PetscReal tol,re,im,errEPS; 
    Vec eigVecr,eigVeci;
    Mat matB;
    EPSConvergedReason reason;
    
    for(i=0;i<maxlevel;i++){
      ierr=EPSCreate(PETSC_COMM_SELF,&eps);CHKERRQ(ierr);
      ierr=EPSSetProblemType(eps,EPS_GHEP);CHKERRQ(ierr);
      ierr=MatDuplicate(pMs->pVal[i],MAT_DO_NOT_COPY_VALUES,&matB);CHKERRQ(ierr);
      ierr=MatDiagonalSet(matB,pvBs->pVal[i],INSERT_VALUES);CHKERRQ(ierr);
      ierr=EPSSetOperators(eps,pSs->pVal[i],matB);CHKERRQ(ierr);
      ierr=EPSSetDimensions(eps,1,PETSC_DECIDE,PETSC_DECIDE);CHKERRQ(ierr);
      ierr=EPSSetWhichEigenpairs(eps,EPS_LARGEST_MAGNITUDE);CHKERRQ(ierr);
      ierr=EPSSetType(eps,EPSPOWER);CHKERRQ(ierr);
      ierr=EPSSetFromOptions(eps);CHKERRQ(ierr);
      ierr=EPSSolve(eps);CHKERRQ(ierr);
      ierr=EPSGetConverged(eps,&nconv);CHKERRQ(ierr);
      if(nconv<=0){
	ierr=EPSGetConvergedReason(eps,&reason);CHKERRQ(ierr);
	ierr=EPSGetIterationNumber(eps,&epsits);CHKERRQ(ierr);	
	fprintf(stderr,"Error: No converged eigen-pair in estimating lambda1\n");
	std::cout<<"ConvergedReason="<<reason<<" epsits="<<epsits<<"\n";
	CHKERRQ(1);
      }
      ierr=MatGetVecs(pSs->pVal[i],&eigVecr,PETSC_NULL);CHKERRQ(ierr);
      ierr=VecDuplicate(eigVecr,&eigVeci);CHKERRQ(ierr);
      ierr=EPSGetEigenpair(eps,0,&eigvr,&eigvi,eigVecr,eigVeci);CHKERRQ(ierr);
      #ifdef PETSC_USE_COMPLEX
	     re=PetscRealPart(eigvr);
	     im=PetscImaginaryPart(eigvr);
      #else
	     re=eigvr;
	     im=eigvi;
      #endif
      lambda1[i]=re*dampingScale;
      printf("lambda1[%d]=%#.3e * %g=%#.3e\n",i,re,dampingScale,lambda1[i]);
      ierr=MatDestroy(&matB);CHKERRQ(ierr);
      ierr=VecDestroy(&eigVecr);CHKERRQ(ierr);
      ierr=VecDestroy(&eigVeci);CHKERRQ(ierr);
      ierr=EPSDestroy(&eps);CHKERRQ(ierr);
    }
   
    for(i=0;i<maxlevel;i++){
      
      ierr=EPSCreate(PETSC_COMM_SELF,&eps);CHKERRQ(ierr);
      ierr=EPSSetProblemType(eps,EPS_GHEP);CHKERRQ(ierr);
       if(PrecMode!='N'){
	ierr=EPSSetOperators(eps,pAs->pVal[i],pSs->pVal[i]);CHKERRQ(ierr);
      }
      else{
	ierr=MatDuplicate(pMs->pVal[i],MAT_DO_NOT_COPY_VALUES,&matB);CHKERRQ(ierr);
	ierr=MatDiagonalSet(matB,pvBs->pVal[i],INSERT_VALUES);CHKERRQ(ierr);
	ierr=EPSSetOperators(eps,pAs->pVal[i],matB);CHKERRQ(ierr);
      }
      ierr=EPSSetDimensions(eps,1,PETSC_DECIDE,PETSC_DECIDE);CHKERRQ(ierr);
      ierr=EPSSetWhichEigenpairs(eps,EPS_LARGEST_MAGNITUDE);CHKERRQ(ierr);
      ierr=EPSSetType(eps,EPSPOWER);CHKERRQ(ierr);
      ierr=EPSSetFromOptions(eps);CHKERRQ(ierr);
     
      ierr=EPSSolve(eps);CHKERRQ(ierr);
      ierr=EPSGetConverged(eps,&nconv);CHKERRQ(ierr); 
      if(nconv<=0){
	ierr=EPSGetConvergedReason(eps,&reason);CHKERRQ(ierr);
	ierr=EPSGetIterationNumber(eps,&epsits);CHKERRQ(ierr);	
	fprintf(stderr,"Error: No converged eigen-pair in estimating lambda2\n");
	std::cout<<"ConvergedReason="<<reason<<" epsits="<<epsits<<"\n";
	CHKERRQ(1);
      }
      ierr=MatGetVecs(pAs->pVal[i],&eigVecr,PETSC_NULL);CHKERRQ(ierr);
      ierr=VecDuplicate(eigVecr,&eigVeci);CHKERRQ(ierr);
      ierr=EPSGetEigenpair(eps,0,&eigvr,&eigvi,eigVecr,eigVeci);CHKERRQ(ierr); 
      #ifdef PETSC_USE_COMPLEX
	     re=PetscRealPart(eigvr);
	     im=PetscImaginaryPart(eigvr);
      #else
	     re=eigvr;
	     im=eigvi;
      #endif
      lambda2[i]=re*dampingScale;
      printf("lambda2[%d]=%#.3e * %g=%#.3e\n",i,re,dampingScale,lambda2[i]);
      if(PrecMode=='N'){
	ierr=MatDestroy(&matB);CHKERRQ(ierr);
      }
      ierr=VecDestroy(&eigVecr);CHKERRQ(ierr);
      ierr=VecDestroy(&eigVeci);CHKERRQ(ierr);
      ierr=EPSDestroy(&eps);CHKERRQ(ierr);
    }
  #endif  
  }
  }
  /////////////////only for debug purpose//////////////////////////////
  for(i=0;i<maxlevel;i++)printf("lambda1[%d]=%g,lambda2[%d]=%g\n",i,lambda1[i],i,lambda2[i]);
  //////////////////////////////////////////////////////////////////// 
  return 0; 
}
void BHMG::genMG(){
  DiagProjSolver *pDiagProjSolver;
  //KSPSolverGu *pCSolverBH, *pCSolverNeu;
  MGSolver *pMGBH=NULL,*pMGNeu=NULL;
  MGData *pMGData=NULL,*pMGDataTmp;
  int k;
  char eventName[256];
  //maxlevel=pAs->size;
  for(k=0;k<maxlevel;k++){
    if(k==0){
      pMGDataTmp=new MGData(0,&(pAs->pVal[0]),&(pSs->pVal[0]),&(pMs->pVal[0]),NULL,NULL,
			    pvBs->pVal,pvBinvs->pVal,pPhi->pVal,
			    NULL,mesh2es.pVal);
      //pCSolverNeu=new KSPSolverGu(pMGDataTmp->pS,pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx,"CSolverNeu");
      //pCSolverBH=new KSPSolverGu(pMGDataTmp->pA,pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx,"CSolverBH");
      if(fabs(c1)>0){
	pCSolverNeu=new gu::KSPCpu(pMGDataTmp->pS,"CSolverNeu");
	pCSolverBH=new gu::KSPCpu(pMGDataTmp->pA,"CSolverBH");
      }else{
	pCSolverNeu=new gu::KSPCpuSingular(&(pMGDataTmp->pS[0]),pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx,"CSolverNeu");
	pCSolverBH=new gu::KSPCpuSingular(&(pMGDataTmp->pA[0]),pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx,"CSolverBH");	
      }
    }
    else{
      pMGDataTmp=new MGData(k,&(pAs->pVal[k]),&(pSs->pVal[k]),&(pMs->pVal[k]),&(pIs->pVal[k-1]),&(pIts->pVal[k-1]),pvBs->pVal+k,pvBinvs->pVal+k,pPhi->pVal+k,pMGData,mesh2es.pVal+k);
    }
    pMGData=pMGDataTmp;
    pMGDataVec.push_back(pMGData);
    sprintf(eventName,"DiagProjSolver %d",k);
    if(fabs(c1)==0.0)
      pDiagProjSolver=new DiagProjSolver(pvBinvs->pVal+k,pPhi->pVal+k,pvBs->pVal+k,eventName);
    else  pDiagProjSolver=new DiagProjSolver(pvBinvs->pVal+k,PETSC_NULL,pvBs->pVal+k,eventName);
    pDiagProjSolvers.push_back(pDiagProjSolver);

    if(PrecMode!='N'){
       sprintf(eventName,"MGNeu %d",k);
       pMGNeu=new MGSolver(k,pMGData,&(pMGData->pS[0]),pCSolverNeu,pDiagProjSolver,mMGNeu,lambda1[k],PrecMode,pMGNeu,eventName);
       pMGNeus.push_back(pMGNeu);
       sprintf(eventName,"MGBH %d",k);
       pMGBH=new MGSolver(k,pMGData,&(pMGData->pA[0]),pCSolverBH,pMGNeu,mMG,lambda2[k],MGBHMode,pMGBH,eventName);
    }else{
      sprintf(eventName,"MGBH %d",k);
      pMGBH=new MGSolver(k,pMGData,&(pMGData->pA[0]),pCSolverBH,pDiagProjSolver,mMG,lambda2[k],MGBHMode,pMGBH,eventName);
    }
    pMGBHs.push_back(pMGBH);
  }
}
void BHMG::releaseMG(){
  for(int l=0;l<maxlevel;l++){
      delete pMGDataVec[l];
      delete pDiagProjSolvers[l];
      if(PrecMode!='N')delete pMGNeus[l];
      delete pMGBHs[l];
    }
    delete pCSolverNeu;
    delete pCSolverBH;
}
BHMG::~BHMG(){
    releaseMG();
    delete []lambda1;  delete []lambda2;
    delete pAs; delete pSs;delete pIs;delete pIts;delete pMs;
    delete pvBs;delete pvBinvs; delete pPhi;
}
