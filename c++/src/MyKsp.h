/**
 * @file   MyKsp.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:26:33 2013
 * 
 * @brief  wrapper class for CUSP GPU linear solver
 * 
 * 
 */
#pragma once
#include"petsc.h"
#include"predefGu.h"
PetscErrorCode MyKSPSolveCUSP(Mat K, Vec rhs, Vec r);
