/**
 * @file   MG.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:23:24 2013
 * 
 * @brief  Multigrid interface. 
 * 
 * 
 */

#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include"MG.h"
#include"others.h"
#include"hooks.h"
//#include"MyVec.h"
#include"predefGu.h"
#include"MyKsp.h"
#include"MyMat.h"
//extern double c1; //This is for indicating whether the problem is singular
//extern PetscBool myKspSolverOn;
MGData::MGData(int level, MyMat* spA, MyMat* spS, MyMat* spM, const MyMat *spI, const MyMat* spIt,  
	       const Vec *spvB, const Vec *spvBinv, const Vec *spPhi,
	       MGData *spNext,const Mesh2* const spMesh2)
  :k(level),pA(spA),pS(spS),pM(spM),pI(spI),pIt(spIt),
   pvB(spvB),pvBinv(spvBinv),pPhi(spPhi),
   pNext(spNext),pMesh2(spMesh2){
  
}   
MGData::~MGData(){
 
}
/*************************************/

/**
 * Diagonal solve on a subspace. 
 * 
 */
DiagProjSolver::DiagProjSolver(const Vec* spvBinv, const Vec* spPhi, const Vec* spvB,const char *seventName)
  :pvBinv(spvBinv),pPhi(spPhi),pvB(spvB),Solver(PETSC_NULL){
  VecDuplicate(*pvBinv,workVec);
  VecDuplicate(*pvBinv,workVec+1);
  //printf("creating %s\n",eventName);
  sprintf(eventName,"%s",seventName);
  PetscLogEventRegister(eventName,0,&EVENT); 
}
DiagProjSolver::~DiagProjSolver(){
  int i;
  for(i=0;i<2;i++)
      VecDestroy(&workVec[i]);
}
PetscErrorCode DiagProjSolver::solve(const Vec& rhs, Vec &r){
  PetscErrorCode ierr;
  PetscLogEventBegin(EVENT,0,0,0,0);
  if(pPhi!=PETSC_NULL){
    ierr=VecPointwiseMult(workVec[0],rhs,*pvBinv);CHKERRQ(ierr);
    ierr=proj(*pPhi,*pvB, workVec[0],workVec[1],r);CHKERRQ(ierr);
  }else 
    ierr=VecPointwiseMult(r,rhs,*pvBinv);CHKERRQ(ierr);
  PetscLogEventEnd(EVENT,0,0,0,0);
  return 0;
}

/*****************************************/
/******************MGSolver***************/
MGSolver::MGSolver(int level,const MGData* spMGData,const MyMat* spK, Solver* spCSolver,
		   Solver* spSmoother, int sm,PetscScalar damping_factor,char smode,MGSolver* spNext,const char* seventName)
  :k(level),pMGData(spMGData),pCSolver(spCSolver),
   pSmoother(spSmoother),m(sm),lambda(damping_factor),
   mode(smode),pNext(spNext),Solver(spK){
  int i;
  PetscErrorCode ierr;
  //ierr=VecCreateSeq(PETSC_COMM_SELF, pMGData->pMesh2->nGI,&(zeroVec));CHKERRV(ierr);
     ierr=MyVecCreate(PETSC_COMM_SELF, pMGData->pMesh2->nGI,&(zeroVec));CHKERRV(ierr); 
  ierr=VecZeroEntries(zeroVec);
  for(i=0;i<2;i++){
    ierr=VecDuplicate(zeroVec,&(workVec[i]));
  }
  for(i=0;i<3;i++){
    ierr=VecDuplicate(zeroVec,&(tmpVec[i]));
  }
  if(k>0){
  //ierr=VecCreateSeq(PETSC_COMM_SELF,pMGData->pNext->pMesh2->nGI,tmpVecL);CHKERRV(ierr);
     ierr=MyVecCreate(PETSC_COMM_SELF,pMGData->pNext->pMesh2->nGI,tmpVecL);CHKERRV(ierr);
    for(i=1;i<3;i++){
      ierr=VecDuplicate(tmpVecL[0],&tmpVecL[i]);
    }
  }
  switch(mode){
  case 'V': break;
  case 'W': break;
  case 'F': break;
  default: fprintf(stderr,"Error:unkonw mode of MGSolver\n");
    exit(1);
  }
  sprintf(eventName,"%s",seventName);
  ierr=PetscLogEventRegister(eventName,0,&EVENT);CHKERRV(ierr);
  #if MULTI_LOGSTAGE
  ierr=PetscLogStageRegister(eventName,&logStage);CHKERRV(ierr);
  #endif
}
MGSolver::~MGSolver(){
  int i;
  VecDestroy(&zeroVec);
  for(i=0;i<2;i++){
    VecDestroy(&workVec[i]);
  }
  for(i=0;i<3;i++){
    VecDestroy(&tmpVec[i]);
  }
  if(k>0){
    for(i=0;i<3;i++)VecDestroy(&tmpVecL[i]);
  }
}
PetscErrorCode MGSolver::solve(const Vec& rhs, Vec& r){
  PetscErrorCode ierr;
  PetscLogEventBegin(EVENT,0,0,0,0);
  switch(mode){
    case 'V':ierr=MGV(rhs,r);CHKERRQ(ierr);break;
    case 'W':ierr=MGW(rhs,r);CHKERRQ(ierr);break;
    case 'F':ierr=MGF(rhs,r);CHKERRQ(ierr);break;
 
  }
  PetscLogEventEnd(EVENT,0,0,0,0);
  return 0;
}
PetscErrorCode MGSolver::solve(const Vec&rhs, const Vec& x0, Vec& r){
  PetscErrorCode ierr;
  ierr=PetscLogEventBegin(EVENT,0,0,0,0);CHKERRQ(ierr);
  switch(mode){
    case 'V':ierr=MGV(rhs,x0,r);CHKERRQ(ierr);break;
    case 'W':ierr=MGW(rhs,x0,r);CHKERRQ(ierr);break;
    case 'F':ierr=MGF(rhs,x0,r);CHKERRQ(ierr);break;
  }
  ierr=PetscLogEventEnd(EVENT,0,0,0,0);CHKERRQ(ierr);
  return 0;
}
PetscErrorCode MGSolver::MGX(const Vec& rhs, const Vec& x0, Vec& r,char X){
  PetscErrorCode ierr;
  int i;
  if (k==0){
    #if MULTI_LOGSTAGE
        ierr=PetscLogStagePush(logStage);CHKERRQ(ierr);
    #endif
    ierr=pCSolver->solve(rhs,r); CHKERRQ(ierr);
    #if MULTI_LOGSTAGE
       ierr=PetscLogStagePop();CHKERRQ(ierr);
    #endif
    return 0;   
  }
  #if MULTI_LOGSTAGE
  ierr=PetscLogStagePush(logStage);CHKERRQ(ierr);
  #endif
  ierr=VecCopy(x0, workVec[0]);CHKERRQ(ierr);
  for(i=0;i<m;i++){
    //compute:tmpVec[0]=rhs-K*workVec[i%2];
    ierr=MatMult(*(pK),workVec[i%2],tmpVec[0]);CHKERRQ(ierr);
    ierr=VecAYPX(tmpVec[0],-1.0,rhs);CHKERRQ(ierr);
    //compute:workVec[(i+1)%2]=workVec[i%2]+(P^{-1}*tmpVec[0])/lambda
    ierr=pSmoother->solve(tmpVec[0],workVec[(i+1)%2]);CHKERRQ(ierr);
    ierr=VecScale(workVec[(i+1)%2],1.0/lambda);CHKERRQ(ierr);
    ierr=VecAYPX(workVec[(i+1)%2],1.0,workVec[i%2]);CHKERRQ(ierr);
    /**Add projection to achieve stability.Mathemacally, it's not needed**/
    //ierr=VecCopy(workVec[(i+1)%2],tmpVec[0]);
    //ierr=proj(*(pMGData->pPhi),*(pMGData->pvB),tmpVec[0],tmpVec[1],workVec[(i+1)%2]); CHKERRQ(ierr);
  }   

  /*compute:tmpVec[0]=rhs-K*workVec[m%2])*/
    ierr=MatMult(*(pK),workVec[m%2],tmpVec[0]);CHKERRQ(ierr);
    ierr=VecAYPX(tmpVec[0],-1.0,rhs);CHKERRQ(ierr);
    /*compute:tmpVecL[0]=I^t*tmpVec[0]*/
    ierr=MatMult(*(pMGData->pIt),tmpVec[0],tmpVecL[0]);CHKERRQ(ierr);
      
    /*compute:tmpVecL[2]=MG_{k-1}(tmpVecL[0]); */ 
    switch(X){
      case 'V':
         ierr=pNext->MGV(tmpVecL[0],tmpVecL[2]);CHKERRQ(ierr);
         break;
      case 'W':
         ierr=pNext->MGW(tmpVecL[0],tmpVecL[1]);CHKERRQ(ierr);
         ierr=pNext->MGW(tmpVecL[0],tmpVecL[1],tmpVecL[2]);CHKERRQ(ierr);
	 break; 
      case 'F':
         ierr=pNext->MGF(tmpVecL[0],tmpVecL[1]);CHKERRQ(ierr);
         ierr=pNext->MGV(tmpVecL[0],tmpVecL[1],tmpVecL[2]);CHKERRQ(ierr);
	 break;
      default:
        fprintf(stderr,"Erro: In MGX, X<>V,W,F\n");
        return(PETSC_ERR_USER);
     
    }
    /*compute::workVec[m%2]=workVec[m%2]+I*tmpVecL[2]*/
    ierr=MatMultAdd(*(pMGData->pI),tmpVecL[2],workVec[m%2],workVec[m%2]);

  for(i=m;i<2*m;i++){
    /*compute:tmpVec[0]=rhs-K*workVec[i%2];*/
    ierr=MatMult(*(pK),workVec[i%2],tmpVec[0]);CHKERRQ(ierr);
    ierr=VecAYPX(tmpVec[0],-1.0,rhs);CHKERRQ(ierr);
    /*compute:workVec[(i+1)%2]=workVec[i%2]+(P^{-1}*tmpVec[0])/lambda*/
    ierr=pSmoother->solve(tmpVec[0],workVec[(i+1)%2]);CHKERRQ(ierr);
    ierr=VecScale(workVec[(i+1)%2],1.0/lambda);CHKERRQ(ierr);
    ierr=VecAYPX(workVec[(i+1)%2],1.0,workVec[i%2]);CHKERRQ(ierr);
    /*Add projection to achieve stability.Mathemacally, it's not needed*/
    //ierr=VecCopy(workVec[(i+1)%2],tmpVec[0]);
    //ierr=proj(*(pMGData->pPhi),*(pMGData->pvB),tmpVec[0],tmpVec[1],workVec[(i+1)%2]); CHKERRQ(ierr);
  }
   //Add projection to achieve stability 
   //ierr=proj(*(pMGData->pPhi),*(pMGData->pvB),workVec[0],workVec[1],r); CHKERRQ(ierr);
   ierr=VecCopy(workVec[0],r);CHKERRQ(ierr);
   #if MULTI_LOGSTAGE
   ierr=PetscLogStagePop();CHKERRQ(ierr);
   #endif
  return 0;
}
PetscErrorCode MGSolver::MGV(const Vec& rhs, Vec&r){
  return MGV(rhs, zeroVec,r);
}
PetscErrorCode MGSolver::MGV(const Vec& rhs, const Vec& x0,Vec& r){
  return MGX(rhs,x0,r,'V');
}
PetscErrorCode MGSolver::MGW(const Vec& rhs, Vec& r){
  return MGW(rhs, zeroVec, r);
}
PetscErrorCode MGSolver::MGW(const Vec& rhs, const Vec& x0,Vec& r){
  return MGX(rhs,x0,r,'W');
}

PetscErrorCode MGSolver::MGF(const Vec& rhs, Vec& r){
  return MGF(rhs,zeroVec,r);
}
PetscErrorCode MGSolver::MGF(const Vec& rhs, const Vec& x0,Vec& r){
  return MGX(rhs,x0,r,'F');
}




