/**
 * @file   CHSolver.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:33:36 2013
 * 
 * @brief  Cahn-Hilliard equation solver.  
 * 
 * 
 */
#include<cstdlib>
#include"CHSolver.h"
#include"KSPGu.h"
#include"FullMGSolver.h"
#include"MCMG.h"
#include"hooks.h"
#include"gfCH.h"
CHSolver::CHSolver(const char* _path, gu::Array2D<bool>& pic_u0){
  int i;
  PetscErrorCode ierr;
  char buffer[256];
  PetscBool flag;
  path=_path;
  
  ierr=readCHOpts(); CHKERRV(ierr);
  setBHCoeff();
  genBH(path);
  //  bhc1=1.0/(dt*eps);bhc2=C1/(eps*eps);
  // /* bhc3 is used in the 2nd problem as a preconditioner*/
  // bhc3=bhc1;
  
  // pBHMG=new BHMG(path,bhc1,bhc2,bhc3);
  // k=pBHMG->maxlevel-1;
  // /****generate solver****/
  // PetscOptionsGetString(PETSC_NULL,"-CH_solver",buffer,255,&flag);
  // // if(!flag){
  // //   fprintf(stderr,"Please specify -CH_solver\n");
  // //   PetscEnd();
  // // }
  // if(!strcmp(buffer,"fullmg")){
  //     printf("using FullMGSolver\n");
  //    pBHSolver=new FullMGSolver(pBHMG->pMGBHs,k,1e-8,100000);
  //   }else
  //   if (!strcmp(buffer,"mcmg")){
  // 	printf("using MCMG\n");
  // 	pBHSolver=new MCMG(pBHMG->pMGBHs[k],1e-8,100000);
  // 	 } else {
  // 	   printf("using CG\n");
  // 	   pBHSolver=new gu::KSPCpu(pBHMG->pMGBHs[k]->pK,"BHSolverCG");
  // 	 }

  //get_u0(pic_u0);
  gen_u0();
  set_u0(pic_u0);
  
  for(i=0;i<6;i++){
    ierr=VecDuplicate(u0,&(workVec[i]));CHKERRV(ierr);
  }
}
CHSolver::CHSolver(const char* _path, Vec initial_state){
  int i,nGI;
  //const Mesh2* pMesh;
  PetscErrorCode ierr;
  //char buffer[256];
  //PetscBool flag;
  //PetscScalar mcmg_tol,fullmg_tol;
  //PetscInt mcmg_max_it,fullmg_max_it;
  path=_path;
  ierr=readCHOpts(); CHKERRV(ierr);
  setBHCoeff();
  genBH(path);
  //  bhc1=1.0/(dt*eps);bhc2=C1/(eps*eps);
  // /* bhc3 is used in the 2nd problem as a preconditioner*/
  // bhc3=bhc1;
  
  // pBHMG=new BHMG(path,bhc1,bhc2,bhc3);
  // k=pBHMG->maxlevel-1;
  // // pBHSolver=new gu::KSPCpu(pBHMG->pMGBHs[k]->pK,"BHSolverCG");
  // /****generate solver****/
  // PetscOptionsGetString(PETSC_NULL,"-CH_solver",buffer,255,&flag);
  // // if(!flag){
  // //   fprintf(stderr,"Please specify -CH_solver\n");
  // //   PetscEnd();
  // // }
  // if(!strcmp(buffer,"fullmg")){
  //     PetscOptionsGetReal(PETSC_NULL,"-fullmg_tol", &fullmg_tol, &flag);	      if(!flag)fullmg_tol=1e-3;
  //     PetscOptionsGetInt(PETSC_NULL,"-fullmg_max_it",&fullmg_max_it,&flag);
  //     if(!flag)fullmg_max_it=100000;
  //     printf("using FullMGSolver,fullmg_tol=%#.3e,fullmg_max_it=%d\n",fullmg_tol, fullmg_max_it);
  //     pBHSolver=new FullMGSolver(pBHMG->pMGBHs,k,fullmg_tol,fullmg_max_it);
  //   }else
  //   if (!strcmp(buffer,"mcmg")){
  //     PetscOptionsGetReal(PETSC_NULL,"-mcmg_tol", &mcmg_tol, &flag);
  //     if(!flag)mcmg_tol=1e-3;
  //     PetscOptionsGetInt(PETSC_NULL,"-mcmg_max_it",&mcmg_max_it,&flag);
  //     if(!flag)mcmg_max_it=100000;
  //     printf("using MCMG,mcmg_tol=%#.3e,mcmg_max_it=%d\n",mcmg_tol, mcmg_max_it);
  //     pBHSolver=new MCMG(pBHMG->pMGBHs[k],mcmg_tol,mcmg_max_it);
  // 	 } else {
  // 	   printf("using CG\n");
  // 	   pBHSolver=new gu::KSPCpu(pBHMG->pMGBHs[k]->pK,"BHSolverCG");
  // 	 }
   gen_u0();
  if(initial_state){
    //pMesh=pBHMG->pMGDataVec[k]->pMesh2;
    //nGI=pBHMG->pMGDataVec[k]->pMesh2->nGI;
    //ierr=MyVecCreate(PETSC_COMM_SELF,nGI,&u0);
    ierr=VecCopy(initial_state,u0);
  } else{
    //ierr=get_u0_random();CHKERRV(ierr);
    ierr=set_u0_random();CHKERRV(ierr);
  }
  for(i=0;i<6;i++){
    ierr=VecDuplicate(u0,&(workVec[i]));CHKERRV(ierr);
  }
}
CHSolver::CHSolver(const char* _path){
  /***generic CHSolver, mainly for the inherit class CHSolverInp ***/
  int i;
  PetscErrorCode ierr;
  char buffer[256];
  char fname[256];
  PetscBool flag;
  gu::Array2D<bool> pic;
  PetscViewer petscviewer;
  
  path=_path;
  
  ierr=readCHOpts(); CHKERRV(ierr);
  setBHCoeff();
  genBH(path);

  /***generate the vectors***/
  gen_u0(); 
  for(i=0;i<6;i++){
    ierr=VecDuplicate(u0,&(workVec[i]));CHKERRV(ierr);
  }
   
  /***set u0 ***/
  ierr=PetscOptionsGetString(PETSC_NULL,"-CH_pic",fname,255,&flag);CHKERRV(ierr);
  if(flag){ 
      if(setVecPic(fname,u0)){
	fprintf(stderr,"failed to read intitial state from image file %s\n",fname);
      }
  }else{
    ierr=PetscOptionsGetString(PETSC_NULL,"-CH_initial_state",fname,255,&flag);CHKERRV(ierr);
     if(flag){
       ierr=PetscViewerBinaryOpen(PETSC_COMM_SELF,fname,FILE_MODE_READ,&petscviewer);CHKERRV(ierr);
       ierr=VecLoad(u0,petscviewer);CHKERRV(ierr);
       ierr=PetscViewerDestroy(&petscviewer);CHKERRV(ierr);
       
     }else{
       printf("random initial data..\n");
       set_u0_random();
     }
  }
}
PetscErrorCode CHSolver::timestepping(Vec &inVec, Vec &outVec){
  PetscErrorCode ierr;
  int i; 
  /***workVec[0]=C1/(eps^2)*(Stiff+bhc3*M)*inVec***/
  ierr=MatMult(pBHMG->pSs->pVal[k],inVec,workVec[0]);CHKERRQ(ierr);
  ierr=VecScale(workVec[0],C1/(eps*eps));CHKERRQ(ierr);

  /***workVec[1]=(1/(dt*eps)-C1*bhc3/(eps^2))*M*inVec***/
  ierr=MatMult(pBHMG->pMs->pVal[k],inVec,workVec[1]);CHKERRQ(ierr);
  ierr=VecScale(workVec[1],1/(dt*eps)-C1*bhc3/(eps*eps));CHKERRQ(ierr);
  
  /***workVec[2]=W'(inVec)***/
  PetscScalar *pV1, *pV2;
  ierr=VecGetArray(inVec,&pV1);CHKERRQ(ierr);
  ierr=VecGetArray(workVec[2],&pV2);CHKERRQ(ierr);
  for(int i=0; i<pBHMG->pMGDataVec[k]->pMesh2->nGI;i++) pV2[i]=dW(pV1[i]); //!!check GI
  ierr=VecRestoreArray(inVec,&pV1);CHKERRQ(ierr);
  ierr=VecRestoreArray(workVec[2],&pV2);CHKERRQ(ierr);

  /***workVec[3]=-1/(eps^2)*(Stiff+bhc3*M)*workVec[2]***/
  ierr=MatMult(pBHMG->pSs->pVal[k],workVec[2],workVec[3]);CHKERRQ(ierr);
  ierr=VecScale(workVec[3],-1.0/(eps*eps));CHKERRQ(ierr);

  /***workVec[4]=1/(eps^2)*bhc3*M*workVec[2]***/
  ierr=MatMult(pBHMG->pMs->pVal[k],workVec[2],workVec[4]);CHKERRQ(ierr);
  ierr=VecScale(workVec[4],bhc3/(eps*eps));CHKERRQ(ierr);

  /***workVec[3]=workVec[3]+workVec[4]***/
  ierr=VecAXPY(workVec[3],1.0,workVec[4]);CHKERRQ(ierr);

  /***workVec[0]=workVec[0]+workVec[1]+workVec[3]***/
  ierr=VecAXPBYPCZ(workVec[0],1.0,1.0,1.0,workVec[1],workVec[3]);CHKERRQ(ierr);

  ierr=pBHSolver->solve(workVec[0],outVec);CHKERRQ(ierr);
  return 0; 
}
PetscErrorCode CHSolver::release(){
  delete pBHMG;
  delete pBHSolver;
  return 0; 
}
//inline PetscScalar CHSolver::dW(PetscScalar x){
//    return x*x*x-1.5*x*x+0.5*x;  //(W(u)=1/4*u^2(u-1)^2;
//  }
CHSolver::~CHSolver(){
   int i;
   PetscErrorCode ierr;
   ierr=release();CHKERRV(ierr);
   ierr=VecDestroy(&u0);CHKERRV(ierr);
   for(i=0;i<6;i++){
     ierr=VecDestroy(&(workVec[i]));CHKERRV(ierr);
  }
}
int CHSolver::gen_u0(){
  /***generate u0 with values unset***/
  int nGI;
  PetscErrorCode ierr;
  nGI=pBHMG->pMGDataVec[k]->pMesh2->nGI;
  ierr=MyVecCreate(PETSC_COMM_SELF,nGI,&u0);CHKERRQ(ierr);
  return 0;
}
int CHSolver::set_u0(gu::Array2D<bool> pic){
  int i,nGI;
  PetscScalar *p;
  double x, y;
  PetscErrorCode ierr;
  const Mesh2* pMesh;
  pMesh=pBHMG->pMGDataVec[k]->pMesh2;
  nGI=pBHMG->pMGDataVec[k]->pMesh2->nGI;
  //ierr=MyVecCreate(PETSC_COMM_SELF,nGI,&u0);CHKERRQ(ierr);
  ierr=VecGetArray(u0, &p); CHKERRQ(ierr);
  
  for(int i=0;i<nGI;i++){
	x=pMesh->xyco[2*i];
	y=pMesh->xyco[2*i+1];
	if(pic.sampleNN(x/lenUnit,y/lenUnit)) p[i]=1.0;
	else p[i]=0.0;
   }
  ierr=VecRestoreArray(u0,&p);CHKERRQ(ierr);
  return 0; 
}
int CHSolver::set_u0_random(){
  int nGI;
  PetscErrorCode ierr;
  const Mesh2* pMesh;
  PetscScalar *p;
  int randnum; 
  pMesh=pBHMG->pMGDataVec[k]->pMesh2;
  nGI=pBHMG->pMGDataVec[k]->pMesh2->nGI;
  //ierr=MyVecCreate(PETSC_COMM_SELF,nGI,&u0);CHKERRQ(ierr);
  
  ierr=VecGetArray(u0, &p); CHKERRQ(ierr);
  srand(44); //initial random seed
  for(int i=0;i<nGI;i++){
    randnum=rand()%10;
    if(randnum>5) p[i]=1.0;
    else p[i]=0.0; 
   }
  ierr=VecRestoreArray(u0,&p);CHKERRQ(ierr);
  
  return 0; 
}
PetscErrorCode CHSolver::readCHOpts(){
  PetscBool flag;
  PetscErrorCode ierr;
  PetscOptionsGetReal(PETSC_NULL,"-gu_CHlenUnit",&lenUnit,&flag); 
  if(!flag){
	fprintf(stderr,"Error:-gu_CHlenUnit must be set for display the mesh and read in images\n");
	PetscEnd(); 
  }else 
       PetscPrintf(PETSC_COMM_SELF,"-gu_CHlenUnit=%#.3e\n",lenUnit);
  ierr=PetscOptionsGetScalar(PETSC_NULL,"-gu_CHeps1",&eps,&flag);CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHeps1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHeps1=%#.3e\n",eps);
  }  
  ierr=PetscOptionsGetScalar(PETSC_NULL,"-gu_CHdt1",&dt,&flag); CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHdt1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHdt1=%#.3e\n",dt);
  } 
  ierr=PetscOptionsGetReal(PETSC_NULL,"-gu_CHlenUnit",&lenUnit,&flag); CHKERRQ(ierr);
  if(!flag){
    lenUnit=1.0;
      }
  PetscPrintf(PETSC_COMM_SELF,"-gu_CHlenUnit=%#.3e\n",lenUnit);
  return 0; 
}
PetscErrorCode CHSolver::computeMass(Vec& x, PetscScalar *pmass){
  PetscErrorCode ierr;
  ierr=MatMult(pBHMG->pMs->pVal[k],x,workVec[0]);CHKERRQ(ierr);
  ierr=VecSum(workVec[0],pmass);CHKERRQ(ierr);
  return 0; 
}
void CHSolver::setBHCoeff(){
  bhc1=1.0/(dt*eps);bhc2=C1/(eps*eps);
  /* bhc3 is used in the 2nd problem as a preconditioner*/
  bhc3=bhc1;
}
void CHSolver::genBH(const char* path){
  /***generate BHMG and pBHSolver***/
  PetscErrorCode ierr;
  char buffer[256];
  PetscBool flag;
  PetscScalar mcmg_tol,fullmg_tol;
  PetscInt mcmg_max_it,fullmg_max_it;  
  pBHMG=new BHMG(path,bhc1,bhc2,bhc3);
  k=pBHMG->maxlevel-1;
  PetscOptionsGetString(PETSC_NULL,"-CH_solver",buffer,255,&flag);
  if(!strcmp(buffer,"fullmg")){
      PetscOptionsGetReal(PETSC_NULL,"-fullmg_tol", &fullmg_tol, &flag);	      if(!flag)fullmg_tol=1e-3;
      PetscOptionsGetInt(PETSC_NULL,"-fullmg_max_it",&fullmg_max_it,&flag);
      if(!flag)fullmg_max_it=100000;
      printf("using FullMGSolver,fullmg_tol=%#.3e,fullmg_max_it=%d\n",fullmg_tol, fullmg_max_it);
      pBHSolver=new FullMGSolver(pBHMG->pMGBHs,k,fullmg_tol,fullmg_max_it);
    }else
    if (!strcmp(buffer,"mcmg")){
      PetscOptionsGetReal(PETSC_NULL,"-mcmg_tol", &mcmg_tol, &flag);
      if(!flag)mcmg_tol=1e-3;
      PetscOptionsGetInt(PETSC_NULL,"-mcmg_max_it",&mcmg_max_it,&flag);
      if(!flag)mcmg_max_it=100000;
      printf("using MCMG,mcmg_tol=%#.3e,mcmg_max_it=%d\n",mcmg_tol, mcmg_max_it);
      pBHSolver=new MCMG(pBHMG->pMGBHs[k],mcmg_tol,mcmg_max_it);
	 } else {
	   printf("using CG\n");
	   pBHSolver=new gu::KSPCpu(pBHMG->pMGBHs[k]->pK,"BHSolverCG");
	 }
}
PetscErrorCode CHSolver::reGenBH(){
  /***release pBHMG and pBHSolver***/
  PetscErrorCode ierr;
  ierr=release();CHKERRQ(ierr);
  
  ierr=readCHOpts2();CHKERRQ(ierr);
  setBHCoeff();
  genBH(path);
}
PetscErrorCode CHSolver::readCHOpts2(){
  PetscBool flag;
  PetscErrorCode ierr; 
  ierr=PetscOptionsGetScalar(PETSC_NULL,"-gu_CHeps2",&eps,&flag);CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHeps2\n");
    PetscEnd();
    return 1; 
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHeps2=%#.3e\n",eps);
  }  
  ierr=PetscOptionsGetScalar(PETSC_NULL,"-gu_CHdt2",&dt,&flag); CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHdt2\n");
    PetscEnd();
    return 1; 
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHdt2=%#.3e\n",dt);
  }
    
  return 0; 
}
int CHSolver::setVecPic(const char *fname, Vec& v){
  int i,nGI;
  PetscScalar *p;
  double x, y;
  PetscErrorCode ierr;
  const Mesh2* pMesh;
  gu::Array2D<bool> pic;
  
  if(readBWImage(fname,pic)){
    fprintf(stderr,"Cannot obtain pictures,Abort!\n");
      PetscEnd();
      return 1; 
  }
 
  pMesh=pBHMG->pMGDataVec[k]->pMesh2;
  nGI=pBHMG->pMGDataVec[k]->pMesh2->nGI;
  ierr=VecGetArray(v, &p); CHKERRQ(ierr);
  
  for(int i=0;i<nGI;i++){
	x=pMesh->xyco[2*i];
	y=pMesh->xyco[2*i+1];
	if(pic.sampleNN(x/lenUnit,y/lenUnit)) p[i]=1.0;
	else p[i]=0.0;
   }
  ierr=VecRestoreArray(v,&p);CHKERRQ(ierr);
  return 0; 
}
/***************************
**replaced by the codes above on 01/28/2012
#include"CHSolver.h"
#include"others.h"
#include"petsc.h"
namespace gu{
  CHSolver::CHSolver(const std::vector<MGData*>& pMGDataVec,const std::vector<MGSolver*>& pMGBHs,
		     int sk,Vec sf,double seps, double sdt,PetscReal rtol)
    :pMGDatas(pMGDataVec),k(sk),eps(seps),dt(sdt),CHksp_rtol(rtol){
    
    pA=pMGDatas[k]->pA; pS=pMGDatas[k]->pS; pM=pMGDatas[k]->pM;
    VecDuplicate(sf,&ones);VecZeroEntries(ones); VecShift(ones,1.0);
    VecDuplicate(sf,&vOrth);MatMult(*pM,ones,vOrth);
    for(int i=0;i<3;i++)VecDuplicate(sf,tmpVec+i);
    ISCreateGeneral(PETSC_COMM_SELF,pMGDatas[k]->pMesh2->nDOF,pMGDatas[k]->pMesh2->idx,&dofs);
    MatGetSubMatrix(*(pMGDatas[k]->pA),dofs,dofs, MAT_INITIAL_MATRIX,&A_R);
    MatGetSubMatrix(*(pMGDatas[k]->pS),dofs,dofs, MAT_INITIAL_MATRIX,&S_R);
    MatGetSubMatrix(*(pMGDatas[k]->pM),dofs,dofs, MAT_INITIAL_MATRIX,&M_R);

    for(int i=0;i<8;i++)MatGetVecs(M_R,tmpVecR+i,PETSC_NULL);

    VecScatterCreate(tmpVec[0],dofs,tmpVecR[0],PETSC_NULL,&scatter); 
    for(int i=0;i<pMGDatas[k]->pMesh2->nGI;i++){
      if(!pMGDatas[k]->pMesh2->free[i]){
	ndof=i;
        break;
      }
    }
    
    VecDuplicate(tmpVecR[0],&inVec_R);
    VecDuplicate(tmpVecR[0],&outVec_R);

    //KSPCreate(PETSC_COMM_SELF,&ksp);
    //KSPSetOperators(ksp,A_R,A_R,SAME_NONZERO_PATTERN);
    //KSPSetType(ksp,KSPCG);
    //KSPSetTolerances(ksp,1e-12,PETSC_DEFAULT,PETSC_DEFAULT,100*pMGDatas[k]->pMesh2->nDOF);
    //KSPSetFromOptions(ksp);
    //KSPSetUp(ksp);

    //////////KSP for timestepping ///////////
    // PCCreate(PETSC_COMM_SELF,&pc);
//     PCSetType(pc,PCNONE);
//     PCSetFromOptions(pc);
//     PCSetOperators(pc,*pA,*pA,SAME_NONZERO_PATTERN);
//     PCSetUp(pc);
//     MatNullSpaceCreate(PETSC_COMM_SELF,PETSC_TRUE,0,PETSC_NULL,&nsp);
//     KSPCreate(PETSC_COMM_SELF,&ksp);
//     KSPSetType(ksp,KSPCG);
//     KSPSetNormType(ksp,KSP_NORM_UNPRECONDITIONED);
//     KSPSetTolerances(ksp,CHksp_rtol,PETSC_DEFAULT,PETSC_DEFAULT,100*pMGDatas[k]->pMesh2->nDOF);
//     KSPSetPC(ksp,pc);
//     KSPSetNullSpace(ksp,nsp);
//     KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);
//     KSPSetFromOptions(ksp);
//     KSPSetUp(ksp); 
    ///////////////////KSP for timestepping2////////////////
    pPCGu=new PCGu(pMGBHs[k],&scatter,ndof);
    
  }
  PetscErrorCode CHSolver::constructKSP(){
    PetscErrorCode ierr;
    //////ksp2////////////////////
    ierr=MatDuplicate(A_R,MAT_COPY_VALUES,&K_R);CHKERRQ(ierr);
    ierr=MatDuplicate(M_R,MAT_COPY_VALUES,&Diag);  CHKERRQ(ierr);
    //////VecCopy(D_R,tmpVecR[0]);
    //////VecScale(tmpVecR[0],sqrt(penalty));
    //////MatDiagonalScale(DiagPen,tmpVecR[0],tmpVecR[0]);
    ierr=MatAYPX(K_R,dt*eps,M_R,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr=MatAXPY(K_R,C1/eps*dt,S_R,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    //////MatAXPY(K_R,dt,DiagPen,SUBSET_NONZERO_PATTERN);

    ierr=PCCreate(PETSC_COMM_SELF,&pc2);CHKERRQ(ierr);
    ierr=PCSetType(pc2,PCSHELL);CHKERRQ(ierr);
    ierr=PCShellSetContext(pc2,(void*)pPCGu);CHKERRQ(ierr);
    ierr=PCShellSetApply(pc2,&gu::shellPCApply);CHKERRQ(ierr);
    ierr=PCSetOperators(pc2,K_R,K_R,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr=PCSetUp(pc2);   CHKERRQ(ierr);
    ierr=KSPCreate(PETSC_COMM_SELF,&ksp2);CHKERRQ(ierr);
    ierr=KSPSetType(ksp2,KSPCG);CHKERRQ(ierr);
    ierr=KSPSetNormType(ksp2,KSP_NORM_UNPRECONDITIONED);CHKERRQ(ierr);
    ierr=KSPSetTolerances(ksp2,CHksp_rtol,PETSC_DEFAULT,PETSC_DEFAULT,100*pMGDatas[k]->pMesh2->nDOF);CHKERRQ(ierr);
    ierr=KSPSetPC(ksp2,pc2);CHKERRQ(ierr);
    ierr=KSPSetInitialGuessNonzero(ksp2,PETSC_TRUE);CHKERRQ(ierr);
    ierr=KSPSetFromOptions(ksp2);CHKERRQ(ierr);
    ierr=KSPSetUp(ksp2);  CHKERRQ(ierr);
    return 0;
  }
  CHSolver::~CHSolver(){
    ///VecDestroy(f);VecDestroy(D); 
    VecDestroy(ones);VecDestroy(vOrth);
    for(int i=0;i<3;i++){
       VecDestroy(tmpVec[i]); 
    }
    for(int i=0;i<8;i++){
	VecDestroy(tmpVecR[i]);
      }
    //VecDestroy(f_R); VecDestroy(D_R);
    VecDestroy(inVec_R); VecDestroy(outVec_R);
    MatDestroy(A_R); MatDestroy(S_R);MatDestroy(M_R);MatDestroy(K_R);MatDestroy(Diag);

    ISDestroy(dofs);
    VecScatterDestroy(scatter);
    //PCDestroy(pc);
    //KSPDestroy(ksp);
    PCDestroy(pc2);
    KSPDestroy(ksp2);
    delete pPCGu;
   
  }
 
 PetscErrorCode CHSolver::timestepping2(Vec &inVec, Vec &outVec){
    PetscScalar *pV1, *pV2;
    KSPConvergedReason reason;
    PetscInt its;
    PetscScalar rnorm;
    PetscErrorCode ierr;
    VecScatterBegin(scatter,inVec,inVec_R,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterEnd(scatter,inVec,inVec_R,INSERT_VALUES,SCATTER_FORWARD);
    //tmpVecR[0]=dt*M*(penalty*D_R.*f_R)
    //VecPointwiseMult(tmpVecR[1],D_R,f_R);
    //VecScale(tmpVecR[1],penalty);
    // MatMult(M_R,tmpVecR[1],tmpVecR[0]);
    //VecScale(tmpVecR[0],dt);
    
    //tmpVecR[0]=C1/eps*dt*S_R*inVec_R;
    ierr=MatMult(S_R,inVec_R,tmpVecR[0]);CHKERRQ(ierr);
    ierr=VecScale(tmpVecR[0],C1/eps*dt);CHKERRQ(ierr);
     
    //tmpVecR[1]=M_R*inVec_R
    MatMult(M_R,inVec_R,tmpVecR[1]);
    //tmpVecR[2]=W'(inVec_R);
    VecGetArray(inVec_R,&pV1);
    VecGetArray(tmpVecR[2],&pV2);
    for(int i=0; i<pMGDatas[k]->pMesh2->nDOF;i++) pV2[i]=dW(pV1[i]);
    VecRestoreArray(inVec_R,&pV1);
    VecRestoreArray(tmpVecR[2],&pV2);
    //tmpVecR[3]=-dt/eps*S_R*tmpVecR[2];
    MatMult(S_R,tmpVecR[2],tmpVecR[3]);
    VecScale(tmpVecR[3],-dt/eps);

    ////tmpVecR[0]=tmpVecR[1]+tmpVecR[3];
    //VecAXPBYPCZ(tmpVecR[0],1.0,1.0,0.0,tmpVecR[1],tmpVecR[3]);
    //tmpVecR[0]=tmpVecR[0]+tmpVecR[1]+tmpVecR[3];
    VecAXPBYPCZ(tmpVecR[0],1.0,1.0,1.0,tmpVecR[1],tmpVecR[3]);    

    
    ierr=KSPSolve(ksp2,tmpVecR[0],outVec_R);CHKERRQ(ierr);
    ierr=KSPGetConvergedReason(ksp2,&reason);CHKERRQ(ierr);
    ierr=KSPGetIterationNumber(ksp2,&its);CHKERRQ(ierr);
    ierr=KSPGetResidualNorm(ksp2,&rnorm);CHKERRQ(ierr);
    printf("\t\t===========================\n");
    if(reason<0)printf("ksp diverge!reason=%d\n",reason);
    printf("#iteration=%d,residualL2Norm=%#.3e\n",its,rnorm);
    printf("\t\t===========================\n");
    ierr=VecScatterBegin(scatter,outVec_R,outVec,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
    ierr=VecScatterEnd(scatter,outVec_R,outVec,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
    return 0;
  }
 //  PetscErrorCode CHSolver::timestepping2(Vec &inVec, Vec &outVec){
//     PetscScalar *pV1, *pV2;
//     KSPConvergedReason reason;
//     PetscInt its;
//     PetscScalar rnorm;
//     PetscErrorCode ierr;
//     VecScatterBegin(scatter,inVec,inVec_R,INSERT_VALUES,SCATTER_FORWARD);
//     VecScatterEnd(scatter,inVec,inVec_R,INSERT_VALUES,SCATTER_FORWARD);
//     //tmpVecR[0]=dt*M*(penalty*D_R.*f_R)
//     VecPointwiseMult(tmpVecR[1],D_R,f_R);
//     VecScale(tmpVecR[1],penalty);
//     MatMult(M_R,tmpVecR[1],tmpVecR[0]);
//     VecScale(tmpVecR[0],dt);

//     //tmpVecR[1]=M_R*inVec_R
//     MatMult(M_R,inVec_R,tmpVecR[1]);
//     //tmpVecR[2]=W'(inVec_R);
//     VecGetArray(inVec_R,&pV1);
//     VecGetArray(tmpVecR[2],&pV2);
//     for(int i=0; i<pMGDatas[k]->pMesh2->nDOF;i++) pV2[i]=dW(pV1[i]);
//     VecRestoreArray(inVec_R,&pV1);
//     VecRestoreArray(tmpVecR[2],&pV2);
//     //tmpVecR[3]=-dt/eps*S_R*tmpVecR[2];
//     MatMult(S_R,tmpVecR[2],tmpVecR[3]);
//     VecScale(tmpVecR[3],-dt/eps);

//     //tmpVecR[0]=tmpVecR[0]+tmpVecR[1]+tmpVecR[3];
//     VecAXPBYPCZ(tmpVecR[0],1.0,1.0,1.0,tmpVecR[1],tmpVecR[3]);
    
//     ierr=KSPSolve(ksp2,tmpVecR[0],outVec_R);CHKERRQ(ierr);
//     ierr=KSPGetConvergedReason(ksp2,&reason);CHKERRQ(ierr);
//     ierr=KSPGetIterationNumber(ksp2,&its);CHKERRQ(ierr);
//     ierr=KSPGetResidualNorm(ksp2,&rnorm);CHKERRQ(ierr);
//     printf("\t\t===========================\n");
//     if(reason<0)printf("ksp diverge!\n");
//     printf("#iteration=%d,residualL2Norm=%#.3e\n",its,rnorm);
//     printf("\t\t===========================\n");
//     ierr=VecScatterBegin(scatter,outVec_R,outVec,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
//     ierr=VecScatterEnd(scatter,outVec_R,outVec,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
//     return 0;
//   }
  inline PetscScalar CHSolver::dW(PetscScalar x){
    return x*x*x-1.5*x*x+0.5*x;  //(W(u)=1/4*u^2(u-1)^2;
  }
  void CHSolver::set_eps(double neweps){
    eps=neweps;
  }
  void CHSolver::set_dt(double newdt){
    dt=newdt;
  }
  void CHSolver::set_ksp_rtol(PetscReal rtol){
    CHksp_rtol=rtol;
  }
  PetscErrorCode CHSolver::regenerate_ksp2(){
    PetscErrorCode ierr;
    ierr=PCDestroy(pc2);CHKERRQ(ierr);
    ierr=KSPDestroy(ksp2);CHKERRQ(ierr);
    ierr=MatDestroy(K_R);CHKERRQ(ierr);
    ierr=MatDestroy(Diag);CHKERRQ(ierr);
    constructKSP(); 
//     ierr=MatDuplicate(A_R,MAT_COPY_VALUES,&K_R);CHKERRQ(ierr);
//     ierr=MatDuplicate(M_R,MAT_COPY_VALUES,&DiagPen);CHKERRQ(ierr);
//     ierr=VecCopy(D_R,tmpVecR[0]);CHKERRQ(ierr);
//     ierr=VecScale(tmpVecR[0],dt*penalty);CHKERRQ(ierr);
//     ierr=VecShift(tmpVecR[0],1.0);CHKERRQ(ierr);
//     ierr=MatDiagonalScale(DiagPen,PETSC_NULL,tmpVecR[0]);CHKERRQ(ierr);

//     ierr=MatAYPX(K_R,dt*eps,DiagPen,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    
//     ierr=PCCreate(PETSC_COMM_SELF,&pc2);CHKERRQ(ierr);
//     ierr=PCSetType(pc2,PCNONE);CHKERRQ(ierr);
//     ierr=PCSetFromOptions(pc2);CHKERRQ(ierr);
//     ierr=PCSetOperators(pc2,K_R,K_R,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
//     ierr=PCSetUp(pc2); CHKERRQ(ierr);  
//     ierr=KSPCreate(PETSC_COMM_SELF,&ksp2);CHKERRQ(ierr);
//     ierr=KSPSetType(ksp2,KSPCG);CHKERRQ(ierr);
//     ierr=KSPSetNormType(ksp2,KSP_NORM_UNPRECONDITIONED);CHKERRQ(ierr);
//     ierr=KSPSetTolerances(ksp2,CHksp_rtol,PETSC_DEFAULT,PETSC_DEFAULT,100*pMGDatas[k]->pMesh2->nDOF);CHKERRQ(ierr);
//     ierr=KSPSetPC(ksp2,pc2);CHKERRQ(ierr);
//     ierr=KSPSetInitialGuessNonzero(ksp2,PETSC_TRUE);CHKERRQ(ierr);
//     ierr=KSPSetFromOptions(ksp2);CHKERRQ(ierr);
//     ierr=KSPSetUp(ksp2);CHKERRQ(ierr);
    return 0;
  }



  ///////////////////////////////mCH/////////////////////////////////
  //////////////////////////////////////////////////////////////////
  mCHSolver::mCHSolver(const std::vector<MGData*> &pMGDataVec,const std::vector<MGSolver*>& pMGBHs,
		       int sk, Vec sf, Vec sD,double seps,double lambda,double sdt,PetscReal rtol)
    :CHSolver(pMGDataVec,pMGBHs,sk,sf,seps,sdt,rtol),penalty(lambda){
    VecDuplicate(sD,&D);VecCopy(sD,D);
    VecDuplicate(sf,&f);VecCopy(sf,f);
    VecDuplicate(tmpVecR[0],&f_R);
    VecDuplicate(tmpVecR[0],&D_R);
    VecScatterBegin(scatter,f,f_R,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterEnd(scatter,f,f_R,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterBegin(scatter,D,D_R,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterEnd(scatter,D,D_R,INSERT_VALUES,SCATTER_FORWARD);
    //constructKSP();
  }
  mCHSolver::~mCHSolver(){
    VecDestroy(f);VecDestroy(D); 
    VecDestroy(f_R); VecDestroy(D_R);
  }
  PetscErrorCode mCHSolver::constructKSP(){
    PetscErrorCode ierr;
    ierr=MatDuplicate(A_R,MAT_COPY_VALUES,&K_R);CHKERRQ(ierr);
    ierr=MatDuplicate(M_R,MAT_COPY_VALUES,&Diag);  CHKERRQ(ierr);
    ierr=VecCopy(D_R,tmpVecR[0]);CHKERRQ(ierr);
    ierr=VecScale(tmpVecR[0],sqrt(penalty));CHKERRQ(ierr);
    ierr=MatDiagonalScale(Diag,tmpVecR[0],tmpVecR[0]);CHKERRQ(ierr);
    ierr=MatAYPX(K_R,dt*eps,M_R,SUBSET_NONZERO_PATTERN);    CHKERRQ(ierr);
    ierr=MatAXPY(K_R,dt,Diag,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);

    ierr=PCCreate(PETSC_COMM_SELF,&pc2);CHKERRQ(ierr);
    ierr=PCSetType(pc2,PCSHELL);CHKERRQ(ierr);
    ierr=PCShellSetContext(pc2,(void*)pPCGu);CHKERRQ(ierr);
    ierr=PCShellSetApply(pc2,&gu::shellPCApply);CHKERRQ(ierr);
    ierr=PCSetOperators(pc2,K_R,K_R,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr=PCSetUp(pc2);   CHKERRQ(ierr);
    ierr=KSPCreate(PETSC_COMM_SELF,&ksp2);CHKERRQ(ierr);
    ierr=KSPSetType(ksp2,KSPCG);CHKERRQ(ierr);
    ierr=KSPSetNormType(ksp2,KSP_NORM_UNPRECONDITIONED);CHKERRQ(ierr);
    ierr=KSPSetTolerances(ksp2,CHksp_rtol,PETSC_DEFAULT,PETSC_DEFAULT,100*pMGDatas[k]->pMesh2->nDOF);CHKERRQ(ierr);
    ierr=KSPSetPC(ksp2,pc2);CHKERRQ(ierr);
    ierr=KSPSetInitialGuessNonzero(ksp2,PETSC_TRUE);CHKERRQ(ierr);
    ierr=KSPSetFromOptions(ksp2);CHKERRQ(ierr);
    ierr=KSPSetUp(ksp2); CHKERRQ(ierr);
    return 0; 
  }
   PetscErrorCode mCHSolver::timestepping2(Vec &inVec, Vec &outVec){
    PetscScalar *pV1, *pV2;
    KSPConvergedReason reason;
    PetscInt its;
    PetscScalar rnorm;
    PetscErrorCode ierr;
    VecScatterBegin(scatter,inVec,inVec_R,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterEnd(scatter,inVec,inVec_R,INSERT_VALUES,SCATTER_FORWARD);
    //tmpVecR[0]=dt*M*(penalty*D_R.*f_R)
    //VecWAXPY(tmpVecR[2],-1.0,inVec_R,f_R);
    VecPointwiseMult(tmpVecR[1],D_R,f_R);
    VecScale(tmpVecR[1],penalty);
    MatMult(M_R,tmpVecR[1],tmpVecR[0]);
    VecScale(tmpVecR[0],dt);

    //tmpVecR[1]=M_R*inVec_R
    MatMult(M_R,inVec_R,tmpVecR[1]);
    //tmpVecR[2]=W'(inVec_R);
    VecGetArray(inVec_R,&pV1);
    VecGetArray(tmpVecR[2],&pV2);
    for(int i=0; i<pMGDatas[k]->pMesh2->nDOF;i++) pV2[i]=dW(pV1[i]);
    VecRestoreArray(inVec_R,&pV1);
    VecRestoreArray(tmpVecR[2],&pV2);
    //tmpVecR[3]=-dt/eps*S_R*tmpVecR[2];
    MatMult(S_R,tmpVecR[2],tmpVecR[3]);
    VecScale(tmpVecR[3],-dt/eps);

    //tmpVecR[0]=tmpVecR[0]+tmpVecR[1]+tmpVecR[3];
    VecAXPBYPCZ(tmpVecR[0],1.0,1.0,1.0,tmpVecR[1],tmpVecR[3]);
    
    ierr=KSPSolve(ksp2,tmpVecR[0],outVec_R);CHKERRQ(ierr);
    ierr=KSPGetConvergedReason(ksp2,&reason);CHKERRQ(ierr);
    ierr=KSPGetIterationNumber(ksp2,&its);CHKERRQ(ierr);
    ierr=KSPGetResidualNorm(ksp2,&rnorm);CHKERRQ(ierr);
    printf("\t\t===========================\n");
    if(reason<0)printf("ksp diverge!reason=%d\n",reason);
    printf("#iteration=%d,residualL2Norm=%#.3e\n",its,rnorm);
    printf("\t\t===========================\n");
    ierr=VecScatterBegin(scatter,outVec_R,outVec,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
    ierr=VecScatterEnd(scatter,outVec_R,outVec,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
    return 0;
  }
  PetscErrorCode shellPCApply(PC pc,Vec rhs, Vec outVec){
    PCGu* pPCGu;
    PCShellGetContext(pc,(void**)&pPCGu);
    return pPCGu->apply(rhs,outVec);
  }
}
*******************************/
