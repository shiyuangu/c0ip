/**
 * @file   gfCH.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:20:19 2013
 * 
 * @brief  Provide functions to read images. 
 */

#pragma once
//int get_xD();
//int get_u0();
//int readCHOpts();
#include"Array2D.h"
int readBWImage(const char* fname, gu::Array2D<bool>& pic);
int readCHPic(const char* path, gu::Array2D<bool>& pic);
int readCHInpR(const char* path, gu::Array2D<bool>& inpR);

