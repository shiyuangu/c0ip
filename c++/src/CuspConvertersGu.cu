/**
 * @file   CuspConvertersGu.cu
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 13:51:25 2013
 * 
 * @brief  convert PETSc Mat to CUSP GPU matrices
 * 
 * 
 */

#if CUDA
#include<cassert>
#include<cstring>
#include"petsc.h"
#include "petscconf.h"
PETSC_CUDA_EXTERN_C_BEGIN
#include "../src/mat/impls/aij/seq/aij.h"          /*I "petscmat.h" I*/
#include "petscbt.h"
#include "../src/vec/vec/impls/dvecimpl.h"
#include "private/vecimpl.h"
PETSC_CUDA_EXTERN_C_END
#undef VecType
#include "../src/mat/impls/aij/seq/seqcusp/cuspmatimpl.h"
#include"MatCpuGpuImplGu.h"
namespace gu{
  PetscErrorCode Mat2Cusp(Mat& s, CuspEll* & pt){
    const char* smt;
    PetscErrorCode ierr;
    Mat_SeqAIJ *a;
    PetscInt m;
    CuspCsrHost* pCuspTmp;
    if(pt!=NULL) delete pt;
    ierr=MatGetType(s,&smt);CHKERRQ(ierr);
    if(strcmp(smt,MATSEQAIJ)){ierr=1;CHKERRQ(ierr);};
    a=(Mat_SeqAIJ*)(s->data);
    if(a->compressedrow.use){ierr=1;CHKERRQ(ierr);}
    m=s->rmap->n;
    try{
       pCuspTmp=new CuspCsrHost;
       pCuspTmp->resize(m,s->cmap->n, a->nz);
       pCuspTmp->row_offsets.assign(a->i,a->i+m+1);
       pCuspTmp->column_indices.assign(a->j,a->j+a->nz);
       pCuspTmp->values.assign(a->a,a->a+a->nz);
       pt=new CuspEll(*pCuspTmp);
       delete pCuspTmp;
    }catch(...){
      SETERRQ(PETSC_COMM_SELF,1,"error in calls for cusp functions\n");
    }
    return 0;
  }
   PetscErrorCode Mat2Cusp(Mat& s, CuspCsr* & pt){
    const char* smt;
    PetscErrorCode ierr;
    Mat_SeqAIJ *a;
    PetscInt m;
    CuspCsrHost* pCuspTmp;
    if(pt!=NULL) delete pt;
    ierr=MatGetType(s,&smt);CHKERRQ(ierr);
    if(strcmp(smt,MATSEQAIJ)){ierr=1;CHKERRQ(ierr);};
    a=(Mat_SeqAIJ*)(s->data);
    if(a->compressedrow.use){ierr=1;CHKERRQ(ierr);}
    m=s->rmap->n;
    try{
       pCuspTmp=new CuspCsrHost;
       pCuspTmp->resize(m,s->cmap->n, a->nz);
       pCuspTmp->row_offsets.assign(a->i,a->i+m+1);
       pCuspTmp->column_indices.assign(a->j,a->j+a->nz);
       pCuspTmp->values.assign(a->a,a->a+a->nz);
       pt=new CuspCsr(*pCuspTmp);
       delete pCuspTmp;
    }catch(...){
      SETERRQ(PETSC_COMM_SELF,1,"error in calls for cusp functions\n");
    }
    return 0;
  }
}
#endif
