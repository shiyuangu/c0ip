#pragma once 
#include<algorithm>
//clamped the value to [a b]
namespace gu{
  template<typename T>
  void clamp(T& v, T a, T b){
    if(v<std::min(a,b)) v=std::min(a,b);
    if(v>std::max(a,b)) v=std::max(a,b);
  }
}
