/****process the screenshot image to become input, that is, chop the borders, change blue/red to black and white, and resample to 256 to 256*/
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std; 

int main( int argc, char** argv )
{ 
	if( argc != 2) 
	{
	 cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
	 return -1;
	}
	
	Mat imageIn,imageOut;
	imageIn = imread(argv[1], CV_LOAD_IMAGE_COLOR);	// Read the file

	if(! imageIn.data )                              // Check for invalid input
	{
		cout <<  "Could not open or find the image" << std::endl ;
		return -1;
	}

	namedWindow( "Display window", CV_WINDOW_AUTOSIZE );// Create a window for display.
	imshow( "Display window", imageIn );                   // Show our image inside it.
        
	waitKey(0);											 // Wait for a keystroke in the window
	cout<<"rows="<<imageIn.rows<<" cols="<<imageIn.cols<<" channel="<<imageIn.channels()<<endl;
	return 0;
}
