/**
 * @file   auxiliary.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:04:53 2013
 * 
 * @brief  Provide functions for debugging and profiling. 
 * 
 * 
 */

#pragma once
#include"petsc.h"
#include<cstdlib>
#include<cstdio>
#include<cassert>
#include<cmath>
PetscErrorCode output(const Mat& M, const char* filename);
PetscErrorCode output(const Vec& V, const char* filename);
PetscErrorCode outputBin(const Vec& V, const char* filename);
PetscErrorCode outputBin(const Mat& A, const char* filename);
PetscErrorCode inputBin(const Vec& V, const char* filename);
PetscErrorCode printToScreen(const IS& is);
PetscInt getInodeCount(const Mat& A);
/***outputMisc is for printing calling arguments,ect. ***/
int outputMisc(int argc,char** argv,const char* fname); 
//class for storing the contration numbers on level k with m smoothing step
class ConNumkm{
public:
  int sizeks,sizems;
  int *ks,*ms;
  PetscScalar *c;
  ConNumkm(int sks,int sms):sizeks(sks),sizems(sms){
    assert(sizeks>0 && sizems>0);
    ks=new int[sizeks];
    ms=new int[sizems];
    c=new PetscScalar[sizeks*sizems];
  }
  ConNumkm(const char* fname){
     FILE *pFile;
      pFile=fopen(fname,"rb");
      if(pFile==NULL){
	fprintf(stderr,"Error: Cannot opening %s\n",fname);
	exit(1);
      }
      fread(&sizeks,sizeof(sizeks),1,pFile);
      fread(&sizems,sizeof(sizems),1,pFile);
      ks=new int[sizeks];
      ms=new int[sizems];
      c=new PetscScalar[sizeks*sizems];
      fread(ks,sizeof(int),sizeks,pFile);
      fread(ms,sizeof(int),sizems,pFile);
      fread(c,sizeof(PetscScalar),sizeks*sizems,pFile);
      fclose(pFile);  
  }
  ~ConNumkm(){
    delete []ks;delete []ms; delete []c;
  }
  PetscScalar& operator()(int level,int m){
    assert(level<sizeks && m<sizems && level>=0 && m>=0);
    return c[level*sizems+m];
  }
    void out(const char* fname){
      FILE *pFile;
      pFile=fopen(fname,"wb");
      if(pFile==NULL){
	fprintf(stderr,"Error: Cannot creat %s\n",fname);
	exit(1);
      }
      fwrite(&sizeks,sizeof(sizeks),1,pFile);
      fwrite(&sizems,sizeof(sizems),1,pFile);
      fwrite(ks,sizeof(int),sizeks,pFile);
      fwrite(ms,sizeof(int),sizems,pFile);
      fwrite(c,sizeof(PetscScalar),sizeks*sizems,pFile);
      fclose(pFile);
   }
    void toTexTable(const char* fname){
      FILE *pFile;
      int i,j;
      pFile=fopen(fname,"w");
      if(pFile==NULL){
	fprintf(stderr,"Error: Cannot creat %s\n",fname);
	exit(1);
      }
      // fprintf(pFile,"\\begin{sidewaystable}[htp]\\footnotesize\n");
      //fprintf(pFile,"\\centering\n");
      fprintf(pFile,"\\begin{tabular}{|");
      for(i=0;i<sizems+1;i++) fprintf(pFile,"c|");
      fprintf(pFile,"}\n");
      fprintf(pFile,"\\hline\n");
      fprintf(pFile,"\\backslashbox{k}{m}");
      for(i=0;i<sizems;i++)fprintf(pFile,"&%d",ms[i]);
      fprintf(pFile,"\\\\ \n");
      fprintf(pFile,"\\hline\n");
      for(i=0;i<sizeks;i++){
	fprintf(pFile,"$%d$",ks[i]);
	for(j=0;j<sizems;j++){
	  fprintf(pFile,"& %#0.3g",fabs((*this)(i,j)));
	}
	fprintf(pFile,"\\\\ \n");
      }
      fprintf(pFile,"\\hline\n");
      fprintf(pFile,"\\end{tabular}\n");
      //fprintf(pFile,"\\caption{}%% from %s\n",fname);
      //fprintf(pFile,"\\end{sidewaystable}");
      fclose(pFile);
    }
};
/////////////////////////////////////////////////
/////////2D table class for output //////////////
/////////It is actually the same as ConNumkm/////
template<typename T1,typename T2, typename T3>
class Table2DGu{
public:
  int sizeks,sizems;
  T1 *ks;
  T2 *ms;
  T3 *c;
  Table2DGu(int sks,int sms):sizeks(sks),sizems(sms){
    assert(sizeks>0 && sizems>0);
    ks=new T1[sizeks];
    ms=new T2[sizems];
    c=new T3[sizeks*sizems];
  }
  Table2DGu(Table2DGu<T1,T2,T3>& source){
    sizeks=source.sizeks; sizems=source.sizems; 
    ks=new T1[sizeks];
    ms=new T2[sizems];
    c=new T3[sizeks*sizems];
    memcpy((void*)ks,(void*)source.ks,sizeks*sizeof(T1));
    memcpy((void*)ms,(void*)source.ms,sizems*sizeof(T2));
    memcpy((void*)c, (void*)source.c, sizeks*sizems*sizeof(T3));
  }
  Table2DGu(const char* fname){
     FILE *pFile;
      pFile=fopen(fname,"rb");
      if(pFile==NULL){
	fprintf(stderr,"Error: Cannot opening %s\n",fname);
	exit(1);
      }
      fread(&sizeks,sizeof(sizeks),1,pFile);
      fread(&sizems,sizeof(sizems),1,pFile);
      ks=new T1[sizeks];
      ms=new T2[sizems];
      c=new T3[sizeks*sizems];
      fread(ks,sizeof(T1),sizeks,pFile);
      fread(ms,sizeof(T2),sizems,pFile);
      fread(c,sizeof(T3),sizeks*sizems,pFile);
      fclose(pFile);  
  }
  ~Table2DGu(){
    delete []ks;delete []ms; delete []c;
  }
  T3& operator()(int level,int m){
    assert(level<sizeks && m<sizems && level>=0 && m>=0);
    return c[level*sizems+m];
  }
    void out(const char* fname){
      FILE *pFile;
      pFile=fopen(fname,"wb");
      if(pFile==NULL){
	fprintf(stderr,"Error: Cannot creat %s\n",fname);
	exit(1);
      }
      fwrite(&sizeks,sizeof(sizeks),1,pFile);
      fwrite(&sizems,sizeof(sizems),1,pFile);
      fwrite(ks,sizeof(T1),sizeks,pFile);
      fwrite(ms,sizeof(T2),sizems,pFile);
      fwrite(c,sizeof(T3),sizeks*sizems,pFile);
      fclose(pFile);
   }
    
};
void toTexTable(Table2DGu<int,int,double>& source,const char* fname);

