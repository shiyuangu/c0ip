//choose Matrix format based on compiler flag
#pragma once
#include"petsc.h"
#include"PetscArrayGu.h"
#include"MatCpuGpuArrayGu.h"
#include"MatCpuGpuGu.h"
#if MY_MAT==1
  typedef gu::MatCpuGpu MyMat;
  typedef gu::MatCpuGpuArray MyMatArray;
  using namespace gu;
#else 
  typedef Mat MyMat;
  typedef PetscMatArray MyMatArray; 
#endif
