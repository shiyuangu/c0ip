/**
 * @file   MG.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:24:16 2013
 * 
 * @brief  Multigrid interface
 * 
 * 
 */

#pragma once
#include"petsc.h"
#include"Mesh2Gu.h"
#include"Solver.h"
#include"predefGu.h"
class MGData{
  // encode the mesh/matrices on level k   
public:
  //mesh/matrices hierarchy
  int k;
  MyMat* pA;
  MyMat*  pS;
  MyMat*  pM;
  const MyMat* const pI;        //interpolation matrices from k-1 to k;  
  const MyMat* const pIt; //interpolation matrices from k to k-1;
  const Vec* const pvB;
  const Vec* const pvBinv;
  const Vec* const pPhi;
  const Mesh2* const pMesh2; 
  // const double lambda;
  //Vec x0;             //initial guess; 
  //Vec zeroVec;
  //work vector
  //Vec tmpVec1, tmpVec2,tmpVec3, tmpVec4, tmpVec5, tmpVec6;// tmp work vectors
  MGData* pNext; //point to the previous mesh stucture
  //void* pPreData;  // Data for preconditioner;
  MGData(int level, MyMat* spA, MyMat* spS,MyMat* spM,const MyMat *spI, const MyMat* spIt,
	 const Vec *spvB,const Vec *spvBinv, const Vec *spPhi, 
	 MGData* spNext,const Mesh2* const spMesh2);
  ~MGData(); 
};

/**
 * DiagProjSolver:diagonal solve with projectio
 */ 
class DiagProjSolver:public Solver{
public:
  DiagProjSolver(const Vec* spvBinv,const Vec* spPhi, const Vec* spvB,const char *eventName="DiagProjSolver");
  ~DiagProjSolver();
  virtual PetscErrorCode solve(const Vec& rhs,Vec &r);
private:
  const Vec * const pvBinv, * const pPhi, * const pvB; //the diagonal matrix.
  Vec workVec[2];
  PetscLogEvent EVENT;
  char eventName[256];
};

class MGSolver:public Solver{
public:
  const MGData *pMGData;
  MGSolver(int level,const MGData* spMGData,const MyMat* spK,Solver* spCSolver, 
	   Solver* spSmoother, int sm,PetscScalar damping_factor,char mode,MGSolver* spNext,
	   const char *eventName="MGSolver");
  ~MGSolver();
  virtual PetscErrorCode solve(const Vec& rhs, Vec & r);
  PetscErrorCode solve(const Vec& rhs, const Vec& x0, Vec& r);
  char mode;
  int m; //presmoothing & postsmoothing step assume the same
  MGSolver *pNext;   //solver on the coarser level
  PetscErrorCode MGV(const Vec& rhs, Vec &r);
  PetscErrorCode MGV(const Vec& rhs, const Vec& x0, Vec& r);
  PetscErrorCode MGW(const Vec& rhs, Vec &r);
  PetscErrorCode MGW(const Vec& rhs, const Vec& x0, Vec &r);
  PetscErrorCode MGF(const Vec& rhs, Vec &r);
  PetscErrorCode MGF(const Vec& rhs, const Vec& x0, Vec &r);
  PetscErrorCode MGX(const Vec&rhs, const Vec& x0, Vec &r, char X);//X=V,W,F
private:
  Solver* const pSmoother;
  Solver* const pCSolver;
  //const MyMat* const pK;
  Vec zeroVec,workVec[2],tmpVec[3],tmpVecL[3];
  PetscScalar lambda;
  int k; //level
  PetscLogEvent EVENT;
  #if MULTI_LOGSTAGE
  PetscLogStage logStage;
  #endif
  char eventName[256];
};

