#pragma once
#include"CHSolver.h"
class CHSolverInp:public CHSolver{
public:
  CHSolverInp(const char* _path);
  virtual ~CHSolverInp();
  virtual PetscErrorCode timestepping(Vec &inVec, Vec &outVec);
  PetscScalar lambda; 
  Vec lambdaX,f;//the \lambda(x) and f(x) in the inpainting model
protected:
  Vec vecPic, vecD; // the original image and inpainting region 
  virtual PetscErrorCode readCHOpts(); 
  virtual void setBHCoeff(); //set bhc1, bhc2, hbc3
  static const double C2=3.0; // used for the convex splitting for the L_2 term;
  int set_vecPic();
  int set_vecD(); 
};
