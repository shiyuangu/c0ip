/*** Obsolete ***/
#include<exception>
#include<iostream>
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"converters.h"
#include"Magick++.h"
#include"gv.h"
#include"gvCH.h"

int get_u0(){
  PetscScalar *p; 
  int nGI;
  double x,y;
  Mesh2 *pMesh;
  PetscBool flag; 
  pMesh=mesh2es.pVal+maxlevel-1;
  nGI=pMesh->nGI;
  //VecCreateSeq(PETSC_COMM_SELF,nGI,&u0);
  MyVecCreate(PETSC_COMM_SELF,nGI,&u0);
  VecGetArray(u0, &p);
  PetscOptionsHasName(PETSC_NULL,"-mCH",&flag);
  if(flag){
    for(int i=0;i<nGI;i++){
      x=pMesh->xyco[2*i];
      y=pMesh->xyco[2*i+1];
      if( inpR.sampleNN(x/lenUnit,y/lenUnit) && pic.sampleNN(x/lenUnit,y/lenUnit)) p[i]=1.0;
      else p[i]=0.0; 
    }
  }else{
      for(int i=0;i<nGI;i++){
	x=pMesh->xyco[2*i];
	y=pMesh->xyco[2*i+1];
	if(pic.sampleNN(x/lenUnit,y/lenUnit)) p[i]=1.0;
	else p[i]=0.0;
      }
  }
  VecRestoreArray(u0,&p);
  return 0;
}
int get_xD(){
  PetscScalar *p;
  int nGI;
  double x,y;
  Mesh2 *pMesh;
  pMesh=mesh2es.pVal+maxlevel-1;
  nGI=pMesh->nGI;
  //VecCreateSeq(PETSC_COMM_SELF,nGI,&xD);
     MyVecCreate(PETSC_COMM_SELF,nGI,&xD);
  VecGetArray(xD, &p);
  for(int i=0;i<nGI;i++){
    x=pMesh->xyco[2*i];
    y=pMesh->xyco[2*i+1];
    if(inpR.sampleNN(x/lenUnit,y/lenUnit)) p[i]=1.0;
    else p[i]=0.0; 
  }
  VecRestoreArray(xD,&p);
  return 0;
}
int readCH(const char* path){
   PetscBool flag;
  char fname[256];
   FILE* fin;
   mxGuLogical mxInpR, mxPic;
   Magick::Image MagImg;
  PetscOptionsGetString(PETSC_NULL,"-CH_D",fname,255,&flag);
  if(!flag){
    sprintf(fname,"%s/%s",path, "D");
    fin=fopen(fname,"rb");
    if(fin==NULL){
      fprintf(stderr,"error in opening file %s",fname);
      return 1;
    }
    mxInpR.read(fin);
    fclose(fin);
    gu::converter(mxInpR,inpR);
  }else{
    try{
      MagImg.read(fname);
      MagImg.zoom("512x512");
      MagImg.sample("128x128");
    }
    catch(Magick::Exception& error){
      std::cout<<"Caught exception during reading an image"<<error.what()<<std::endl;
      return 1;
    }
    //MagImg.display();
    MagImg.negate();
    gu::converter(MagImg,inpR);
  }

  PetscOptionsGetString(PETSC_NULL,"-CH_bg",fname,255,&flag);
  if(!flag){
    sprintf(fname,"%s/%s",path, "bg");
    fin=fopen(fname,"rb");
    if(fin==NULL){
      fprintf(stderr,"error in opening file %s",fname);
      return 1;
    }
    mxPic.read(fin);
    fclose(fin);
    gu::converter(mxPic,pic); 
  }else{
     try{
      MagImg.read(fname);
      MagImg.zoom("512x512");
      MagImg.sample("128x128");
    }
    catch(Magick::Exception& error){
      std::cout<<"Caught exception during reading an image"<<error.what()<<std::endl;
      return 1;
    }
    MagImg.display();
    gu::converter(MagImg,pic);
  }
  return 0;
}
int readCHOpts(){
  PetscBool flag;
  PetscOptionsGetScalar(PETSC_NULL,"-gu_CHeps1",&CHeps1,&flag);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHeps1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHeps1=%#.3e\n",CHeps1);
  }  
  PetscOptionsGetScalar(PETSC_NULL,"-gu_CHdt1",&CHdt1,&flag); 
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHdt1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHdt1=%#.3e\n",CHdt1);
  }
  
  PetscOptionsGetInt(PETSC_NULL,"-gu_CHnt1",&CHnt1,&flag); 
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHnt1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHnt1=%d\n",CHnt1);
  }
  
  PetscOptionsGetReal(PETSC_NULL,"-gu_CHlenUnit",&lenUnit,&flag); 
  if(!flag){
    lenUnit=1.0;
      }
  PetscPrintf(PETSC_COMM_SELF,"-gu_CHlenUnit=%#.3e\n",lenUnit);

  PetscOptionsGetReal(PETSC_NULL,"-gu_CHksp_rtol1",&CHksp_rtol1,&flag); 
  if(!flag){
    CHksp_rtol1=1.0e-2;
      }
  PetscPrintf(PETSC_COMM_SELF,"-gu_CHksp_rtol1=%#.3e\n",CHksp_rtol1);
  
  PetscOptionsHasName(PETSC_NULL,"-mCH",&flag);
  if(flag){
    PetscOptionsGetScalar(PETSC_NULL,"-gu_CHpenalty",&CHpenalty,&flag);
    if(!flag){
      fprintf(stderr,"Error:Please specify -gu_CHpenalty\n");
      PetscEnd();
    }else{
      PetscPrintf(PETSC_COMM_SELF,"-gu_CHpenalty=%#.3e\n",CHpenalty);
    }
    PetscOptionsGetScalar(PETSC_NULL,"-gu_CHeps2",&CHeps2,&flag);
    if(!flag){
      CHeps2=CHeps1;
    }
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHeps2=%#.3e\n",CHeps2);

    PetscOptionsGetScalar(PETSC_NULL,"-gu_CHdt2",&CHdt2,&flag); 
    if(!flag){
      CHdt2=CHdt1;
    }
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHdt2=%#.3e\n",CHdt2);

    PetscOptionsGetInt(PETSC_NULL,"-gu_CHnt2",&CHnt2,&flag); 
    if(!flag){
      CHnt2=CHnt1;
    }
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHnt2=%d\n",CHnt2);

    PetscOptionsGetReal(PETSC_NULL,"-gu_CHksp_rtol2",&CHksp_rtol2,&flag); 
    if(!flag){
      CHksp_rtol2=1.0e-2;
    }
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHksp_rtol2=%#.3e\n",CHksp_rtol2); 
  }

}
