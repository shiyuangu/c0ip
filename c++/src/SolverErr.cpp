/**
 * @file   SolverErr.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:00:03 2013
 * 
 * @brief  Compute the error of a Solver. It takes a vector x 
 *         and compute y=A*x, and use y as an input to the Solver.
 *         Also provides power methods to compute the spectral radius
 *         of the error operator S^{-1}*A-Id.
 */
#include<cstdio>
#include<cstdlib>
#include"SolverErr.h"
#include"others.h"
#include"predefGu.h"
#include"MatCpuGpuGu.h"
extern double c;
SolverErr::~SolverErr(){
  if(initialized){
     VecDestroy(&rhs);
     VecDestroy(&outSolver);
  }
}
PetscErrorCode SolverErr::set(Solver* spSolver){
  PetscInt nrow=0,ncol=0;
  PetscErrorCode ierr;
  pSolver=spSolver;
  ierr=MatGetSize(*(pSolver->pK),&nrow,&ncol);CHKERRQ(ierr);
  if(size!=ncol){   //avoid recreating rhs and outMG if not needed. 
    if(initialized){
      ierr=VecDestroy(&rhs);CHKERRQ(ierr);
      ierr=VecDestroy(&outSolver);CHKERRQ(ierr);
    }
    ierr=MatGetVecs(*(pSolver->pK),&outSolver,&rhs); CHKERRQ(ierr);
  }
   initialized=true;
   return 0;
}
PetscErrorCode SolverErr::error(Vec inVec, Vec err){
  PetscErrorCode ierr;
  if(!initialized){
    fprintf(stderr,"SolverErr is not initialized!\n");
    exit(1);
  }
  ierr=MatMult(*(pSolver->pK),inVec,rhs);CHKERRQ(ierr);
  ierr=pSolver->solve(rhs,outSolver);CHKERRQ(ierr);
  ierr=VecWAXPY(err,-1.0,outSolver,inVec);CHKERRQ(ierr);
  return 0;
}
PetscErrorCode normMGErr(SolverErr* pSolverErr, PetscScalar tol,
			 const Mat* pK,const Vec* pPhi,const Vec* pvB,
			 Vec workVec[],Vec outVec,PetscScalar& lambda){
  //find the norm of the MGErr by power methods
  //the input value in outVec is used as an initial guess
  PetscScalar sigma,err,lambdaP,tmp1,tmp2;
  PetscInt index;
  int step;
  Vec *pEq,*pq;
  PetscErrorCode ierr;
  //VecCopy(x0,outVec);
  pEq=&(workVec[0]); pq=&(workVec[1]); 
  ierr=VecAbsMaxGu(outVec,&index,&sigma);CHKERRQ(ierr);
  VecScale(outVec,1.0/sigma);

  if(pPhi!=PETSC_NULL){
    VecCopy(outVec, workVec[2]);
    proj(*pPhi,*pvB,workVec[2],workVec[3],outVec);
  }
  err=100.0; lambdaP=100.0;
  ierr=pSolverErr->error(outVec,*pEq);CHKERRQ(ierr);
  step=0;
  while(err>tol){
    step++;
    ierr=VecCopy(*pEq,*pq);CHKERRQ(ierr);
    ierr=VecAbsMaxGu(*pq,&index,&sigma);
    if(fabs(sigma)<1e-5){
      fprintf(stderr,"Error Operator appears zeros.\n");
      lambda=0;
      return 0;
    }
    VecScale(*pq,1.0/sigma);
    if(pPhi!=PETSC_NULL){
      VecCopy(*pq,workVec[2]);
      proj(*pPhi,*pvB,workVec[2],workVec[3],*pq);
    }
    ierr=pSolverErr->error(*pq,*pEq);CHKERRQ(ierr);
    innerProduct(*pK,*pEq,*pq,workVec[3],&tmp1);
    innerProduct(*pK,*pq,*pq,workVec[3],&tmp2);
    lambda=tmp1/tmp2;
    err=fabs(lambdaP-lambda)/fabs(lambdaP);
    //VecCopy(*pq,outVec);
    lambdaP=lambda;
  }
  VecCopy(*pq,outVec);
  printf("power method converges in step %d\n",step);
  return 0;
}

/**
 * Class ErrExt
 * 
 */
PetscErrorCode ErrExt::error(Vec inVec,Vec outVec){
  PetscErrorCode ierr;
  if(pPhi!=PETSC_NULL){
    ierr=proj(*pPhi,*pvB,inVec,workVec[0],workVec[1]);CHKERRQ(ierr);
  }
  else{ 
    ierr=VecCopy(inVec,workVec[1]);CHKERRQ(ierr);
  }
  ierr=pErrOp->error(workVec[1],outVec);CHKERRQ(ierr);
  return 0;  
}
