/**
 * @file   MCMG.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:16:10 2013
 * 
 * @brief  Apply multigrid cycles successively to obtain an approximation  
 *         to the equation. 
 * 
 */
#include"MCMG.h"
#include"others.h" //for innerProdut()
#include"hooks.h"
#include"auxiliary.h" //for onput routines for debug purpose;
MCMG::MCMG(MGSolver* _pMGSolver, PetscScalar _tol, PetscInt _maxit):Solver(_pMGSolver->pK),tol(_tol), maxit(_maxit){
  PetscErrorCode ierr;
  PetscInt nrow, ncol;
    pMGSolver=_pMGSolver;
    ierr=MatGetSize(*pK,&nrow,&ncol);CHKERRV(ierr);
    ierr=MyVecCreate(PETSC_COMM_SELF,nrow,workVec);CHKERRV(ierr);
    for(int i=1;i<9;i++){
       ierr=VecDuplicate(workVec[0],workVec+i);CHKERRV(ierr); 
    }
    
    /****create KSP only for debug purpose, only for the nonsingular case****/
    ierr=KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRV(ierr);
    #if MY_MAT==0
    ierr=KSPSetOperators(ksp,*pK,*pK,SAME_NONZERO_PATTERN);CHKERRV(ierr);
    #else
    ierr=KSPSetOperators(ksp,*(pK->pHostMat),*(pK->pHostMat),SAME_NONZERO_PATTERN);CHKERRV(ierr);
    #endif
    ierr=KSPSetType(ksp,KSPCG);CHKERRV(ierr);
    ierr=KSPGetPC(ksp,&prec);CHKERRV(ierr);
    ierr=PCSetType(prec, PCNONE);CHKERRV(ierr);
    ierr=KSPSetTolerances(ksp,1e-10,1e-100,1e5,100*nrow);CHKERRV(ierr);
    ierr=KSPSetUp(ksp);CHKERRV(ierr);
}
MCMG::~MCMG(){
  PetscErrorCode ierr;
  for(int i=0;i<9;i++){
    ierr=VecDestroy(workVec+i);CHKERRV(ierr);
  }
  /***Destroy the tmp KSP object***/
  ierr=KSPDestroy(&ksp);CHKERRV(ierr);
}
PetscErrorCode MCMG::solve(const Vec& rhs, Vec& x){
  int preIter, curIter, counter;
  PetscScalar errEnorm, errEnorm2,xEnorm,curEnorm,ratio;
  PetscErrorCode ierr;
  /****obtain exact solution for debug purpose*****/
  KSPConvergedReason reason;
  bool flag; //keep going? 
  KSPSolve(ksp,rhs,workVec[4]);
  KSPGetConvergedReason(ksp,&reason);
  if(reason<0){
    fprintf(stderr,"In MCMG: ksp solver diverged!");
  }
  ierr=innerProduct(*(pMGSolver->pK),workVec[4],workVec[4],workVec[2],&xEnorm);CHKERRQ(ierr); xEnorm=sqrt(xEnorm);
  // ierr=outputBin(rhs, "tmprhs.dat");CHKERRQ(ierr);
  //ierr=outputBin(workVec[4],"tmpx.dat");CHKERRQ(ierr);
  /****above is for debug purpose ****/
  
  printf("In MCMG solve:\n");
  ierr=VecCopy(x,workVec[0]);CHKERRQ(ierr);
  curIter=0;counter=0; flag=true;
  while(flag){
    preIter=curIter; curIter=(curIter+1)%2; counter++; 
    ierr=pMGSolver->solve(rhs,workVec[preIter],workVec[curIter]);CHKERRQ(ierr);
     
    ierr=VecAXPY(workVec[preIter],-1.0,workVec[curIter]);CHKERRQ(ierr);
    ierr=innerProduct(*(pMGSolver->pK),workVec[preIter],workVec[preIter],workVec[2],&errEnorm);CHKERRQ(ierr); errEnorm=sqrt(errEnorm);
    ierr=innerProduct(*(pMGSolver->pK),workVec[curIter],workVec[curIter],workVec[2],&curEnorm);CHKERRQ(ierr); curEnorm=sqrt(curEnorm);
    ratio=errEnorm/curEnorm;
    printf("iter=%u, errEnorm(successive iter)=%#.3e, errEnorm/curEnorm=%#.3e\n",counter,errEnorm,ratio);
    flag= ratio>tol && counter<=maxit; 
    /*****compute the exact Enorm,only for debug purpose****/
    ierr=VecWAXPY(workVec[5],-1.0,workVec[curIter],workVec[4]);CHKERRQ(ierr);
    ierr=innerProduct(*(pMGSolver->pK),workVec[5],workVec[5],workVec[2],&errEnorm2);CHKERRQ(ierr); errEnorm2=sqrt(errEnorm2);
    //ierr=outputBin(workVec[5],"tmpx2.dat");CHKERRQ(ierr);
    printf("iter=%u, exact Enorm=%#.3e, ratio=%#.3e\n",counter,errEnorm2, errEnorm2/xEnorm);
  }
  ierr=VecCopy(workVec[curIter],x);CHKERRQ(ierr);
  return 0;
}
