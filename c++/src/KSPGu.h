/**
 * @file   KSPGu.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:05:56 2013
 * 
 * @brief  Switch between PETSc Mat format or MatCpuGpu format depends
 *         on the complier flag MY_MAT. It also handles the single problem
 *         from C0IP. 
 * 
 */
#pragma once
#include"petsc.h"
#include"Solver.h"
#include"predefGu.h"
#include"MatCpuGpuGu.h"
#if 0
//////////////KSPSolverGu/////////////////////////
class KSPSolverGu:public Solver{ 
  //solver on the coarsest level
public:
  KSPSolverGu(const Mat* spK, const Vec* spPhi, const Vec* spvB,PetscInt nGI,PetscInt nDOF,PetscInt sidx[],const char *eventName="KSPSolverGu");
  ~KSPSolverGu();
  virtual PetscErrorCode solve(const Vec& rhs, Vec &r);
private:
  //const MGData* pMGData; 
  KSP ksp;
  Mat reducedK;
  Vec reducedSolution,reducedRHS, workVec;
  const Vec * const pvB, * const pPhi;
  IS isfrom;
  PetscInt *idx_from,*idx_to,size,reducedSize;
  PetscScalar* pValues;
  PC prec;   //Petsc preconditioner ;
  //MatNullSpace nsp;
  PetscLogEvent EVENT;
  char eventName[256];
};
#endif
//////////////////////////////////////////////////
namespace gu{
  class KSPCpu:public Solver{
  public:
    KSPCpu(const Mat* spK, const char*seventName="gu::KSPCpu");
    #if MY_MAT==1
    KSPCpu(const MatCpuGpu* spK, const char* seventName="gu::KSPCpu");
    #endif 
    ~KSPCpu();
    virtual PetscErrorCode solve(const Vec& rhs, Vec &r);
  private:
    KSP ksp;
    Mat K;
    Vec workVec[3];
    PC prec;
    PetscLogEvent EVENT;
    char eventName[256];
  };

  
  class KSPCpuSingular:public Solver{
  public:
    KSPCpuSingular(const Mat* spK, const Vec* spPhi, const Vec* spvB,
		   PetscInt nGI,PetscInt nDOF,PetscInt sidx[],const char *seventName="gu:KSPCpuSingular");
    #if MY_MAT==1
    KSPCpuSingular(const MatCpuGpu* spK, const Vec* spPhi, const Vec* spvB,
		   PetscInt nGI,PetscInt nDOF,PetscInt sidx[],const char *seventName="gu:KSPCpuSingular");
    #endif
    ~KSPCpuSingular();
    virtual PetscErrorCode solve(const Vec& rhs, Vec &r);
  private:
    KSP ksp;
    Mat reducedK;
    Vec reducedSolution, reducedRHS,workVec[2];
    Vec  vB, Phi;
    IS isfrom;
    PetscInt *idx_from,*idx_to,size,reducedSize;
    PetscScalar* pValues;
    PC prec;   //Petsc preconditioner ;
    //MatNullSpace nsp;
    PetscLogEvent EVENT;
    char eventName[256];
  };
}
