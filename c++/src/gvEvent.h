//define global variables for PETSc logging/profiling
#pragma once
extern PetscLogEvent eventMyVecDot,eventMyMatMult,eventMatMultCusp,eventMatMultAddCusp;
extern PetscLogStage stageDebug,memoryWatch;
extern PetscLogEvent eventMyKspSolver;
