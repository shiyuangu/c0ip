/**
 * @file   Mesh2Gu.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:35:45 2013
 * 
 * @brief  Mesh of type 2
 * 
 * 
 */

#pragma once
#include"mxGu.h"
#include"mxMesh2Gu.h"
class Mesh2{
public:
  int nGI,nDOF,nT;
  double* xyco;   //x,y coordinates;
  bool *vertex;   //whether a vertex or midpoint;
  bool *boundary; //whether on the boundary or not.
  bool *free;     //whether it is a DOF. 
  int *T; 
  ////conversion tables of two indices; 
  ////GI is the index of all nodal points and GGI is the index of the DOFs.  
  int* idx;
  Mesh2(){nGI=nDOF=0;idx=NULL;}
  ~Mesh2(){release();}
private:
  void release();
};
class Mesh2Array{
public:
  int size;
  Mesh2* pVal;
  Mesh2Array(){size=0;pVal=NULL;}
  ~Mesh2Array(){release();}
  void getFrom(const mxGuCellArrayT<mxMesh2>& mxMesh2es);
private:
  void release();
};
