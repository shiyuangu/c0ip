/**
 * @file   FullMG.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 13:52:09 2013
 * 
 * @brief  connect multigrid cycles to form full multigrid solve.  
 * 
 * 
 */
#pragma once
#include<vector>
#include"petsc.h"
#include"MG.h"
class FullMG{
public:
  FullMG(std::vector<MGSolver*>& spMGs,PetscScalar tol=1.0e-4,PetscInt maxIter=100);
  ~FullMG();
  PetscErrorCode solve(Vec rhs, Vec sol,int level);
  std::vector<MGSolver* > pMGs;
  std::vector<Vec> rhs;
  std::vector<std::vector<Vec> > workVecs;
  PetscScalar tol;
  PetscInt maxIter;
};
