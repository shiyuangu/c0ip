/*** superceded by conNumSLEPC ***/
#include<cstdio>
#include<cstdlib>
#include<vector>
#include"petsc.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
#include"SolverErr.h"
#include"comparison.h"
#include"others.h"
PetscMatArray *pAs,*pSs,*pIs,*pIts;
PetscVecArray *pvBs,*pvBinvs,*pPhi;
Mesh2Array mesh2es;
MGData *pMGData=NULL;
DiagProjSolver *pDiagProjSolver;
KSPSolverGu *pCSolverBH, *pCSolverNeu;
MGSolver *pMGBH=NULL,*pMGNeu=NULL;
double *lambda1, *lambda2;
std::vector<MGData*> pMGDataVec;
std::vector<MGSolver*> pMGBHs;
std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory 
int maxlevel=0;
PetscInt slevel, elevel; // starting and ending level
PetscInt mstart=6, mend=36,mskip=2; //perform smoothing from mstart:mskip:mend
char MGBHMode='W',MGNeuMode='V';
PetscInt mMGNeu=5;   //presmoothing/postsmoothing step
void genMG();


void genMG(){
  MGData *pMGDataTmp;
  int k;
  maxlevel=pAs->size;
  for(k=0;k<maxlevel;k++){
    if(k==0){
      pMGDataTmp=new MGData(0,pAs->pVal,pSs->pVal,NULL,NULL,
			    pvBs->pVal,pvBinvs->pVal,pPhi->pVal,
			    NULL,mesh2es.pVal);
      pCSolverNeu=new KSPSolverGu(pMGDataTmp->pS,pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,
				  pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx);
      pCSolverBH=new KSPSolverGu(pMGDataTmp->pA,pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,
				  pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx);
	      
    }
    else{
      pMGDataTmp=new MGData(k,pAs->pVal+k,pSs->pVal+k,pIs->pVal+k-1,pIts->pVal+k-1,
			    pvBs->pVal+k,pvBinvs->pVal+k,pPhi->pVal+k,
			    pMGData,mesh2es.pVal+k);
    }
    pMGData=pMGDataTmp;
    pMGDataVec.push_back(pMGData);
    pDiagProjSolver=new DiagProjSolver(pvBinvs->pVal+k,pPhi->pVal+k,pvBs->pVal+k);
    pDiagProjSolvers.push_back(pDiagProjSolver);
    pMGNeu=new MGSolver(k,pMGData,pMGData->pS,pCSolverNeu,pDiagProjSolver,mMGNeu,lambda1[k],MGNeuMode,pMGNeu);
    pMGNeus.push_back(pMGNeu);
    pMGBH=new MGSolver(k,pMGData,pMGData->pA,pCSolverBH,pMGNeu,0,lambda2[k],MGBHMode,pMGBH);
    pMGBHs.push_back(pMGBH);
  }
  
  //pMGNeuV=new MGSolver(&preNeu,pCSolverNeu,5,'V');
  //pMGBHV=new MGSolver(&preBH,pCSolverBH,5,'V');
}

static void read(const char* path){
  mxGuCellArrayT<mxGuSparse> mxAs,mxSs,mxIs,mxvBs,mxvBinvs;
  mxGuCellArrayT<mxGuDouble> mxPhi;
  mxGuCellArrayT<mxMesh2> mxMesh2es;

  FILE* fin;
  char fname[256]; 
  int i;

  sprintf(fname,"%s/%s",path, "As");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  mxAs.read(fin);
  fclose(fin);
  pAs->getFrom(mxAs);

  sprintf(fname,"%s/%s",path, "Ss");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  mxSs.read(fin);
  fclose(fin);
  pSs->getFrom(mxSs);  

  sprintf(fname,"%s/%s",path, "Is");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  mxIs.read(fin);
  fclose(fin);
  pIs->getFrom(mxIs);
  pIts->size=pIs->size;
  pIts->pVal=new Mat[pIs->size];
  for(i=0;i<pIs->size;i++){
  MatTranspose(pIs->pVal[i],MAT_INITIAL_MATRIX,&(pIts->pVal[i]));
  }

  sprintf(fname,"%s/%s",path, "vBs");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  mxvBs.read(fin);
  fclose(fin);
  pvBs->getFrom(mxvBs);
  
  sprintf(fname,"%s/%s",path, "vBinvs");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  mxvBinvs.read(fin);
  fclose(fin);
  pvBinvs->getFrom(mxvBinvs);

  sprintf(fname,"%s/%s",path, "Phi");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  mxPhi.read(fin);
  fclose(fin);
  pPhi->getFrom(mxPhi);

  sprintf(fname,"%s/%s",path, "meshes2");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  mxMesh2es.read(fin);
  fclose(fin);
  mesh2es.getFrom(mxMesh2es);

  sprintf(fname,"%s/%s",path,"lambda1");
  fin=fopen(fname,"r");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  lambda1=new double[pAs->size];
  for(i=0;i<pAs->size;i++){fscanf(fin,"%le",lambda1+i);lambda1[i]=lambda1[i]*0.55;}
  fclose(fin);
  
  sprintf(fname,"%s/%s",path,"lambda2_1");
  fin=fopen(fname,"r");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return;
  }
  lambda2=new double[pAs->size];
  for(i=0;i<pAs->size;i++){fscanf(fin,"%le",lambda2+i);lambda2[i]=lambda2[i]*0.55;}
  fclose(fin);
  
}

int main(int argc, char**argv){
  int k,m,l;
  SolverErr *pMGErrBH;
  Vec workVec[6],x0,outVec;
  PetscScalar lambda,tmp1,tmp2,err;
  PetscRandom randomctx; 
  PetscErrorCode ierr;
  PetscTruth flag;
  char tmpString[256];  //for retrieving string parameter from runtime option. 
  if(argc>1){
    PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
    PetscOptionsGetString(PETSC_NULL,"-gu_MGBH_mode",&MGBHMode,1,&flag);
    if(!flag||!(MGBHMode=='V' || MGBHMode=='W' || MGBHMode=='F')){
	fprintf(stderr,"Please specify -gu_MGBH_mode\n");
        PetscEnd();
    }else{
      printf("-gu_MGBH_mode=%c\n",MGBHMode);
    }
    PetscOptionsGetString(PETSC_NULL,"-gu_MGNeu_mode",&MGNeuMode,1,&flag);
    if(!flag||!(MGNeuMode=='V' || MGNeuMode=='W' || MGNeuMode=='F')){
	fprintf(stderr,"Please specify -gu_MGNeu_mode\n");
        PetscEnd();
    }else printf("-gu_MGNeu_mode=%c\n",MGNeuMode);
    PetscOptionsGetInt(PETSC_NULL,"-gu_mMGNeu",&mMGNeu,&flag);
    if(!flag||mMGNeu<0){
      fprintf(stderr,"Please specify -gu_mMGNeu\n");
      PetscEnd();
    }else printf("-gu_mMGNeu=%d\n",mMGNeu);
    PetscOptionsGetInt(PETSC_NULL,"-gu_mstart",&mstart,&flag);
    if(!flag||mstart<0){
      fprintf(stderr,"Please specify -gu_mstart\n");
      PetscEnd();
    }else printf("-gu_mstart=%d\n",mstart);
    PetscOptionsGetInt(PETSC_NULL,"-gu_mskip",&mskip,&flag);
    if(!flag){
      fprintf(stderr,"Please specify -gu_mskip\n");
      PetscEnd();
    }else printf("-gu_mskip=%d\n",mskip);
    PetscOptionsGetInt(PETSC_NULL,"-gu_mend",&mend,&flag);
    if(!flag||mend<0){
      fprintf(stderr,"Please specify -gu_mend\n");
      PetscEnd();
    }else printf("-gu_mend=%d\n",mend);
   
    pAs=new PetscMatArray; pSs=new PetscMatArray;pIs=new PetscMatArray;pIts=new PetscMatArray;
    pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;
    pMGErrBH=new SolverErr;           
    read(argv[1]);
    genMG();
    PetscOptionsGetInt(PETSC_NULL,"-gu_slevel",&slevel,&flag);
    if(!flag)slevel=0;
    printf("-gu_slevel=%d\n",slevel);
    PetscOptionsGetInt(PETSC_NULL,"-gu_elevel",&elevel,&flag);
    if(!flag){
      if(elevel<0||elevel>maxlevel-1){
        fprintf(stderr,"invalid -gu_elevel,reset to %d\n",maxlevel-1);
      }
      elevel=maxlevel-1;
    }
    printf("-gu_elevel=%d\n",elevel);
    ////////////////////////////////
    /////////////compute contration numbers;
    PetscRandomCreate(PETSC_COMM_SELF,&randomctx);
    PetscRandomSetType(randomctx,PETSCRAND48);//?
    for(k=slevel;k<=elevel;k++){
      pMGErrBH->set(pMGBHs[k]);
      for(l=0;l<6;l++){
	//VecCreateSeq(PETSC_COMM_SELF,pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));
	   MyVecCreate(PETSC_COMM_SELF,pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));
      }
      VecDuplicate(workVec[0],&x0);
      VecSetRandom(x0,randomctx);
      VecDuplicate(workVec[0],&outVec);
      VecCopy(x0,workVec[0]);
      proj(*(pMGDataVec[k]->pPhi),*(pMGDataVec[k]->pvB),workVec[0],workVec[1],x0);

      for(m=mstart;m<mend;m+=mskip){
	 for(l=0;l<=k;l++)pMGBHs[l]->m=m;
	 ierr=normMGErr(pMGErrBH,x0,1e-5,
			pMGDataVec[k]->pA,pMGDataVec[k]->pPhi,pMGDataVec[k]->pvB,
			workVec,outVec,lambda);CHKERRQ(ierr);
         //validate whether (lambda,outVec) is the a eigenpair.
	 ierr=pMGErrBH->error(outVec,workVec[0]);CHKERRQ(ierr);
         innerProduct(*(pMGDataVec[k]->pA),workVec[0],workVec[0],workVec[1],&tmp1);
	 innerProduct(*(pMGDataVec[k]->pA),outVec,outVec,workVec[1],&tmp2);
	 err=abs(lambda)-sqrt(tmp1/tmp2);
	 if(err>0.1){
	   printf("something is wrong in normMGErr. rel.err=%g\n",err);
	   exit(1);
	 }else{
	   printf("k=%d,m=%d,contraction=%g\n",k,m,lambda);
	 }
      }
      printf("\n");
      for(l=0;l<6;l++){
	VecDestroy(workVec[l]);
      }
      VecDestroy(x0);VecDestroy(outVec);
    }
    PetscRandomDestroy(randomctx);
    ///////////////////////////////////////////////////////////////////////
    ///////testing MG as preconditioner///////////////////////////////////
  //   PC pc1;
//     MatNullSpace nsp;
//     for(k=0;k<=elevel;k++)pMGBHs[k]->m=2;
//     ierr=PCCreate(PETSC_COMM_SELF,&pc1);CHKERRQ(ierr);
//     ierr=PCSetType(pc1,PCNONE);CHKERRQ(ierr);
//     ierr=PCSetOperators(pc1,*(pMGBHs[elevel]->pK),*(pMGBHs[elevel]->pK),SAME_NONZERO_PATTERN);
//     ierr=MatNullSpaceCreate(PETSC_COMM_SELF,PETSC_TRUE,0,PETSC_NULL,&nsp);CHKERRQ(ierr);
//     ierr=compare(pMGBHs[elevel],pc1,*(pMGBHs[elevel]->pK),nsp,
// 		 *(pMGBHs[elevel]->pMGData->pPhi),*(pMGBHs[elevel]->pMGData->pvB));
//     PCDestroy(pc1);
//     MatNullSpaceDestroy(nsp);
    //////////////////////////////////////////////////////////////////////
    for(l=0;l<maxlevel;l++){
      delete pMGDataVec[l];
      delete pDiagProjSolvers[l];
      delete pMGNeus[l];
      delete pMGBHs[l];
    }
    delete pCSolverNeu;
    delete pCSolverBH;
    delete []lambda1;  delete []lambda2;
  }else{
    printf("Usage: MGBH <matfile_D>");
  }
  delete pAs; delete pSs;delete pIs;delete pIts;
  delete pvBs;delete pvBinvs; delete pPhi;
  delete pMGErrBH;            
  ierr=PetscFinalize();CHKERRQ(ierr);
  return 0;
}
