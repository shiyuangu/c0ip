/**
 * @file   CahnHilliardInp.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:39:38 2013
 * 
 * @brief  Solving Cahn-Hilliard  image inpaiting model. 
 * 
 * 
 */

#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"slepceps.h"
#include"petsc.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
//#include"SolverErr.h" //the following three is for PC 
//#include"PCGu.h"
//#include"comparison.h"
#include"gf.h"
#include"others.h"
#include"auxiliary.h"
#include "gfCH.h"
#include "Array2D.h"
#include "converter.h"
#include "gfIFace.h" 
#include"GLee.h"
//#include<GL/glut.h>
#include<GL/freeglut.h>
//#include<GL/freeglut_ext.h>
#include<GL/glext.h>
/***for DevIL ***/
#define ILUT_USE_OPENGL
//#include<IL/ilut.h>
#include"ilutGLGu.h"

#include"CHSolver.h"
#include"CHSolverInp.h"
#include"predefGu.h"
/***only for preparing petsc logs;
    global variables for logs are in
    gvLog.h***/
#include"MyInit.h"

#include"videoGu.h"
/***the following is for display***/
/***used in OGL callback function***/
const Mesh2* pDispMesh;
Vec *pDispVec;
int nt; //advance nt steps for one click 
const int imgW=128, imgH=128; //intended image size
double lenUnit; 
/***to keep track of time stepping***/
/***used in OGL callback function***/
int currentTimeStep;
double currentTime; 
Vec uPre,uCur;
/***for DevIL****/
ILuint DevilImgName;
/***for output video***/
cv::VideoWriter outputVideo;
//cv::Mat cvImage; 
int windowWidth, windowHeight; 
//char* pbuffer;

/***For performance log***/
PetscLogEvent eventMyVecDot,eventMyMatMult,eventMatMultCusp,eventMatMultAddCusp;
PetscLogStage stageDebug,memoryWatch;
PetscLogEvent eventMyKspSolver;

CHSolver *pCHSolver;
int main(int argc, char** argv){
  gu::Array2D<bool> pic;
  PetscErrorCode ierr;
  PetscBool flag;
  char fin[256],buffer[256],fout[256];
  PetscViewer petscviewer;
  Vec initial_state; //only readin when -CH_initial_state;
  if(argc<=1){
    promptOps();
    /****add prompts for options relates to CH***/
    printf("Example run: ./CahnHilliardInp -mCH -fin ../../data/CHInpSq128SmallP5L9R.mat_D -gu_PrecMode V -gu_mMGNeu 1 -gu_MGBHMode V -gu_mMG 4 -gu_CHlenUnit 0.01 -gu_CHeps1 0.1 -gu_CHeps2 0.001 -gu_CHdt1 0.1 -gu_CHdt2 1e-5 -gu_CHnt1 400 -gu_CHnt2 200 -gu_CHksp_rtol1 1e-6 -gu_SR_fin ../../data/CHInpSq128SmallP5L9R.mat_D/SR.txt -ksp_initial_guess_nonzero -gu_maxlevel 8 -CH_solver cg -mat_no_inode -gu_CHInp_lambda 1e5 -CH_pic ../../data/pics/appleFlip.gif -CH_D ../../data/pics/appleDNFlip.gif -fout apple\n");
    printf("Example run: ./CahnHilliardInp -fin ../../data/CHInpSq128SmallP5L9R.mat_D  -gu_PrecMode V -gu_mMGNeu 1 -gu_MGBHMode V -gu_mMG 4 -gu_CHlenUnit 0.01 -gu_CHeps1 5e-3 -gu_CHdt1 5e-6 -gu_CHnt1 4000 -gu_SR_fin ../../data/CHInpSq128SmallP5L9R.mat_D/SR.txt -ksp_initial_guess_nonzero -gu_maxlevel 8 -CH_solver mcmg -mcmg_tol 1e-3 -mcmg_max_it 1000 -mat_no_inode -fout PhaseSquare2\n");
    return 0;
  }
  /***process options relate to global scope***/
  // PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  ierr=SlepcInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  ierr=PetscOptionsGetInt(PETSC_NULL,"-gu_CHnt1",&nt,&flag); CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHnt1\n");
    PetscEnd();
  }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHnt1=%d\n",nt);
  }
   #if MY_MAT==1
      ierr=PetscOptionsGetString(PETSC_NULL,"-vec_type",buffer,256,&flag);CHKERRQ(ierr);
      if(!flag || strcmp(buffer,VECSEQCUSP)){
	SETERRQ(PETSC_COMM_SELF,1,"MY_MAT==1,please set -vec_type seqcusp");
        PetscEnd();
      }
     
    #endif

   /*** prepare for petsc log***/
      ierr=MyInitialize(); CHKERRQ(ierr); 

  /*** obtain directory of input data ***/
  ierr=PetscOptionsGetString(PETSC_NULL,"-fin",fin,255,&flag);CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Please specify the directory containing data\n");
    PetscEnd();
  }    
  /***with -mCH to switch on Inp ***/
  PetscOptionsHasName(PETSC_NULL,"-mCH",&flag);
  if(flag)    
    pCHSolver=new CHSolverInp(fin);
  else
    pCHSolver=new CHSolver(fin);
  
  if(pCHSolver->u0){
      ierr=VecDuplicate(pCHSolver->u0,&uPre);CHKERRQ(ierr);
      ierr=VecDuplicate(pCHSolver->u0,&uCur);CHKERRQ(ierr);
      ierr=VecCopy(pCHSolver->u0,uCur);CHKERRQ(ierr);
      pDispMesh=pCHSolver->getMesh();
      pDispVec=&uCur;
       /*** the factor 5/4 is mean to leave 20% margin ***/
      /*** the factor 4 is to magnify the image for display***/
      windowWidth=imgW*5/4*4; windowHeight=imgH*5/4*4;
      setupOGL(argc,argv, windowWidth,windowHeight);
      
      /****initialize DevIL****/
      std::cout<<"DevIL is in action...\n";
      ilInit();
      iluInit();
      ilutRenderer(ILUT_OPENGL);
      std::cout<<"Make sure there is only one version of DevIL\n";
      std::cout<<"ilGetInteger(IL_VERSION_NUM):"<<ilGetInteger(IL_VERSION_NUM)
	       <<"  IL_VERSION:"<<IL_VERSION<<std::endl;
      std::cout<<"iluGetInteger(ILU_VERSION_NUM):"<<iluGetInteger(ILU_VERSION_NUM)
	       <<"  ILU_VERSION:"<<ILU_VERSION<<std::endl;
      std::cout<<"ilutGetInteger(ILUT_VERSION_NUM):"<<ilutGetInteger(ILUT_VERSION_NUM)
	       <<"  ILUT_VERSION:"<<ILUT_VERSION<<std::endl;
      ilGenImages(1,&DevilImgName);
      ilBindImage(DevilImgName);
      ilEnable(IL_FILE_OVERWRITE);
      
      ierr=PetscOptionsGetString(PETSC_NULL,"-fout",buffer,255,&flag);CHKERRQ(ierr);
      if(!flag){
	sprintf(buffer,"cahnhilliard");
      }
      sprintf(fout,"%s.avi",buffer);
      outputVideo.open(fout, CV_FOURCC('M','P','4','2'),15, cv::Size(windowWidth,windowHeight), true);
      sprintf(fout,"%s.info",buffer);
      if(outputMisc(argc,argv,fout)){
	fprintf(stderr,"cannot create info file\n");
	return 1; 
      }
      glutMainLoop();
  }
  
  delete pCHSolver;
  ierr=VecDestroy(&uPre);CHKERRQ(ierr);
  ierr=VecDestroy(&uCur);CHKERRQ(ierr);
  //ierr=PetscFinalize();CHKERRQ(ierr);
  ierr=SlepcFinalize();CHKERRQ(ierr);
  return 0;
}
