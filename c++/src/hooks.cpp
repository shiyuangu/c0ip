//hooks functions for debug purpose.
#include "hooks.h"
#include "predefGu.h"
PetscErrorCode MyVecCreate(MPI_Comm comm, PetscInt n, Vec* v){
  PetscErrorCode ierr;
  //ierr=VecCreateSeq(comm, n, v); CHKERRQ(ierr);
  ierr=VecCreate(comm,v);CHKERRQ(ierr);
  ierr=VecSetSizes(*v,PETSC_DECIDE,n);CHKERRQ(ierr);
  //ierr=VecSetType(*v,VECSEQ);CHKERRQ(ierr);
  ierr=VecSetFromOptions(*v);CHKERRQ(ierr);
  return 0;
}
PetscErrorCode MyMatCreate(MPI_Comm comm, PetscInt m, PetscInt n, PetscInt nz, const PetscInt nnz[], Mat *A){
  PetscErrorCode ierr;
  //const MatType type;
  //ierr=MatCreateSeqAIJ(comm,m,n,nz,nnz,A);CHKERRQ(ierr);
  ierr=MatCreate(comm,A);CHKERRQ(ierr);
  ierr=MatSetSizes(*A,PETSC_DECIDE,PETSC_DECIDE,m,n);CHKERRQ(ierr);
  //ierr=MatSetType(*A,MATSEQAIJ);CHKERRQ(ierr);
  ierr=MatSetFromOptions(*A);CHKERRQ(ierr);
  ierr=MatSeqAIJSetPreallocation(*A,nz, nnz);
  //ierr=MatGetType(*A,&type);CHKERRQ(ierr);
  //ierr=PetscPrintf(comm,"MatType is %s\n",type);CHKERRQ(ierr);
  return 0;
}
