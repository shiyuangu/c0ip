///test the performance of multigrid solver as preconditioner. 
#include<cstdio>
#include<cstdlib>
#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"petsc.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
#include"SolverErr.h"
#include"PCGu.h"
#include"comparison.h"
#include"others.h"
#include"auxiliary.h"
PetscMatArray *pAs,*pSs,*pIs,*pIts;
PetscVecArray *pvBs,*pvBinvs,*pPhi;
Mesh2Array mesh2es;
MGData *pMGData=NULL;
DiagProjSolver *pDiagProjSolver;
KSPSolverGu *pCSolverBH, *pCSolverNeu;
MGSolver *pMGBH=NULL,*pMGNeu=NULL;
std::vector<MGData*> pMGDataVec;
std::vector<MGSolver*> pMGBHs;
std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory 
int maxlevel=0;
//parameters to control the MG 
char MGBHMode='W',PrecMode='V';
PetscInt mMGNeu=5;   //presmoothing/postsmoothing step for Neu MG problem 
double dampingScale=0.55; //the damping factors are the estimated spectral radii times damping scales
double *lambda1, *lambda2;   //the damping factors;
//parameters to control the testing level and smoothing steps. 
PetscInt slevel,elevel,sizeks;       
PetscTruth mlogscale;
PetscReal mstartlog,mendlog;
PetscInt msize;
PetscInt mstart=6, mend=36,mskip=2,sizems;
PetscInt *pm; 
PetscInt nRun; // # of run( ramdon sample) for each PC
//to output the performance (iteration numbers and time) for preconditoners.   
Table2DGu<int,int,double> *pTabIts0,*pTabIts1, *pTabTime0,*pTabTime1,*pTabrErr0,*pTabrErr1;

PetscErrorCode shellPCApply(PC,Vec inVec,Vec outVec);


void genMG(){
  MGData *pMGDataTmp;
  int k;
  maxlevel=pAs->size;
  for(k=0;k<maxlevel;k++){
    if(k==0){
      pMGDataTmp=new MGData(0,pAs->pVal,pSs->pVal,NULL,NULL,
			    pvBs->pVal,pvBinvs->pVal,pPhi->pVal,
			    NULL,mesh2es.pVal);
      pCSolverNeu=new KSPSolverGu(pMGDataTmp->pS,pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,
				  pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx);
      pCSolverBH=new KSPSolverGu(pMGDataTmp->pA,pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,
				  pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx);
	      
    }
    else{
      pMGDataTmp=new MGData(k,pAs->pVal+k,pSs->pVal+k,pIs->pVal+k-1,pIts->pVal+k-1,
			    pvBs->pVal+k,pvBinvs->pVal+k,pPhi->pVal+k,
			    pMGData,mesh2es.pVal+k);
    }
    pMGData=pMGDataTmp;
    pMGDataVec.push_back(pMGData);
    pDiagProjSolver=new DiagProjSolver(pvBinvs->pVal+k,pPhi->pVal+k,pvBs->pVal+k);
    pDiagProjSolvers.push_back(pDiagProjSolver);
    if(PrecMode!='N'){
       pMGNeu=new MGSolver(k,pMGData,pMGData->pS,pCSolverNeu,pDiagProjSolver,mMGNeu,lambda1[k],PrecMode,pMGNeu);
       pMGNeus.push_back(pMGNeu);
       pMGBH=new MGSolver(k,pMGData,pMGData->pA,pCSolverBH,pMGNeu,0,lambda2[k],MGBHMode,pMGBH);
    }else{
       pMGBH=new MGSolver(k,pMGData,pMGData->pA,pCSolverBH,pDiagProjSolver,0,lambda2[k],MGBHMode,pMGBH);
    }
    pMGBHs.push_back(pMGBH);
  }
  
}

static int read(const char* path){
  mxGuCellArrayT<mxGuSparse> mxAs,mxSs,mxIs,mxvBs,mxvBinvs;
  mxGuCellArrayT<mxGuDouble> mxPhi;
  mxGuCellArrayT<mxMesh2> mxMesh2es;
  mxGuDouble mxLambda1,mxLambda2;

  FILE* fin;
  char fname[256]; 
  int i;

  sprintf(fname,"%s/%s",path, "As");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  mxAs.read(fin);
  fclose(fin);
  pAs->getFrom(mxAs);

  sprintf(fname,"%s/%s",path, "Ss");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  mxSs.read(fin);
  fclose(fin);
  pSs->getFrom(mxSs);  

  sprintf(fname,"%s/%s",path, "Is");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  mxIs.read(fin);
  fclose(fin);
  pIs->getFrom(mxIs);
  pIts->size=pIs->size;
  pIts->pVal=new Mat[pIs->size];
  for(i=0;i<pIs->size;i++){
  MatTranspose(pIs->pVal[i],MAT_INITIAL_MATRIX,&(pIts->pVal[i]));
  }

  sprintf(fname,"%s/%s",path, "vBs");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  mxvBs.read(fin);
  fclose(fin);
  pvBs->getFrom(mxvBs);
  
  sprintf(fname,"%s/%s",path, "vBinvs");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  mxvBinvs.read(fin);
  fclose(fin);
  pvBinvs->getFrom(mxvBinvs);

  sprintf(fname,"%s/%s",path, "Phi");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  mxPhi.read(fin);
  fclose(fin);
  pPhi->getFrom(mxPhi);

  sprintf(fname,"%s/%s",path, "meshes2");
  fin=fopen(fname,"rb");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  mxMesh2es.read(fin);
  fclose(fin);
  mesh2es.getFrom(mxMesh2es);

  sprintf(fname,"%s/%s",path,"lambda1");
  fin=fopen(fname,"r");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  //for(i=0;i<pAs->size;i++){fscanf(fin,"%le",lambda1+i);lambda1[i]=lambda1[i]*0.55;}
  mxLambda1.read(fin); 
  fclose(fin);
  if(mxLambda1.nrow*mxLambda1.ncol!=pAs->size){fprintf(stderr,"Error: the size in %s does not match As\n",fname);return 1;}
  lambda1=new double[pAs->size];for(i=0;i<pAs->size;i++)lambda1[i]=mxLambda1.pr[i]*dampingScale;
  
  if(PrecMode!='N')sprintf(fname,"%s/%s",path,"lambda2_PS");
  else sprintf(fname,"%s/%s",path,"lambda2_NP");
  fin=fopen(fname,"r");
  if(fin==NULL){
    fprintf(stderr,"error in opening file %s",fname);
    return 1;
  }
  mxLambda2.read(fin);
  fclose(fin);
  if(mxLambda2.nrow*mxLambda2.ncol!=pAs->size){fprintf(stderr,"Error:the size in %s does not match As\n",fname);return 1;}
  lambda2=new double[pAs->size];for(i=0;i<pAs->size;i++)lambda2[i]=mxLambda2.pr[i]*dampingScale;

  /////////////////only for debug purpose//////////////////////////////
  for(i=0;i<pAs->size;i++)printf("lambda1[%d]=%g,lambda2[%d]=%g\n",i,lambda1[i],i,lambda2[i]);
  ////////////////////////////////////////////////////////////////////
  return 0;
  
}

int main(int argc, char**argv){
  int l,k,m;
  PetscInt kCounter,mCounter;
  SolverErr *pMGErrBH;
  Vec workVec[6],x0,outVec;
  PetscScalar tmp1,tmp2,err,tol,powerTol;
  PetscScalar mgErrNorm; //This is the norm of error, NOT the dampingfactor. 
  PetscRandom randomctx; 
  PetscErrorCode ierr;
  PetscTruth flag;
  bool flag2; 
  char outfname[256]; int tmp; //only for output result
  char infname1[256]; std::string infname2; 
  size_t pos;   //for exact only the file name(not the path in argv[1])
  PCGu *pPCGu;  
  PC pc0, pc1;
  MatNullSpace nsp;
  if(argc>1){
    PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
    PetscOptionsGetString(PETSC_NULL,"-gu_MGBHMode",&MGBHMode,1,&flag);
    if(!flag||!(MGBHMode=='V' || MGBHMode=='W' || MGBHMode=='F')){
	fprintf(stderr,"Error:Please specify -gu_MGBHMode\n");
        PetscEnd();
    }else{
      printf("-gu_MGBHMode=%c\n",MGBHMode);
    }
    PetscOptionsGetString(PETSC_NULL,"-gu_PrecMode",&PrecMode,1,&flag);
    if(!flag||!(PrecMode=='V' || PrecMode=='W' || PrecMode=='F'||PrecMode=='N')){
	fprintf(stderr,"Error:Please specify -gu_PrecMode\n");
        PetscEnd();
    }else printf("-gu_PrecMode=%c\n",PrecMode);
    
    PetscOptionsGetInt(PETSC_NULL,"-gu_mMGNeu",&mMGNeu,&flag);
    if((PrecMode!='N')&&(!flag||mMGNeu<0)){
      fprintf(stderr,"Error:Please specify -gu_mMGNeu\n");
      PetscEnd();
    }else printf("-gu_mMGNeu=%d\n",mMGNeu);
    PetscOptionsGetReal(PETSC_NULL,"-gu_dampingScale",&dampingScale,&flag);
    if(!flag) dampingScale=0.55;
    printf("-gu_dampingScale=%g\n",dampingScale);
    PetscOptionsHasName(PETSC_NULL,"-gu_mlogscale",&mlogscale);
    if(!mlogscale){
      PetscOptionsGetInt(PETSC_NULL,"-gu_mstart",&mstart,&flag);
      if(!flag||mstart<=0){
	fprintf(stderr,"Error:Please specify -gu_mstart(>=1)\n or -gu_mlogscale");
	PetscEnd();
      }else printf("-gu_mstart=%d\n",mstart);
      PetscOptionsGetInt(PETSC_NULL,"-gu_mskip",&mskip,&flag);
      if(!flag){
	mskip=1;
	fprintf(stderr,"Warning:-gu_mskip is not specified, set to be %d\n",mskip);
      }else printf("-gu_mskip=%d\n",mskip);
      PetscOptionsGetInt(PETSC_NULL,"-gu_mend",&mend,&flag);
      if(!flag||mend<0){
	mend=mstart+mskip;
	fprintf(stderr,"Warning: -gu_mend is not specified, set to be %d\n",mend); 
      }else printf("-gu_mend=%d\n",mend);
    }else{
      PetscOptionsGetReal(PETSC_NULL,"-gu_mstartlog",&mstartlog,&flag);
      if(!flag){
	fprintf(stderr,"Error: Log scale has been set,-gu_startlog must be specified!\n");
        PetscEnd();
        exit(1);
      }else printf("-gu_mstartlog=10^%g(i.e. %g)\n",mstartlog,pow(10,mstartlog));
      PetscOptionsGetReal(PETSC_NULL,"-gu_mendlog",&mendlog,&flag);
      if(!flag){
	fprintf(stderr,"Error: Log scale has been set,-gu_mendlog must be specified!\n");
        PetscEnd();
        exit(1);
      }printf("-gu_mstartlog=10^%g(i.e. %g)\n",mendlog,pow(10,mendlog));
      PetscOptionsGetInt(PETSC_NULL,"-gu_msize",&msize,&flag);
      if(!flag || msize<1){
	fprintf(stderr,"Error:Log scale has been set,-gu_msize must be specified(>0) (msize=%d)!\n",msize);
	PetscEnd();
        exit(1);
      } 
    } 
     PetscOptionsGetInt(PETSC_NULL,"-gu_nRun",&nRun,&flag);
     if(!flag ||nRun<1){
	fprintf(stderr,"Warning:-gu_nRun is not specified or invalid,set to 1.\n");
        nRun=1;
     }else{
	printf("-gu_nRun=%d\n",nRun);
     }
    
    //read global parametes to define the problem
    pAs=new PetscMatArray; pSs=new PetscMatArray;pIs=new PetscMatArray;pIts=new PetscMatArray;
    pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;
    pMGErrBH=new SolverErr;           
    if(read(argv[1])){
      PetscEnd();
      exit(1);
    }
     genMG();
       
    PetscOptionsGetInt(PETSC_NULL,"-gu_slevel",&slevel,&flag);
    if(!flag)slevel=1;
    printf("-gu_slevel=%d\n",slevel);
    PetscOptionsGetInt(PETSC_NULL,"-gu_elevel",&elevel,&flag);
    if(!flag){
      if(elevel<0||elevel>maxlevel-1){
        fprintf(stderr,"invalid -gu_elevel,reset to %d\n",maxlevel-1);
      }
      elevel=maxlevel-1;
    }
    printf("-gu_elevel=%d\n",elevel); 
    /////////generate the output table///////////////
      sizeks=elevel-slevel+1;
      pTabIts0=new Table2DGu<int,int,double>(sizeks,nRun);
      kCounter=0;for(k=slevel;k<=elevel;k++)pTabIts0->ks[kCounter++]=k;
      for(int i=0;i<nRun;i++) pTabIts0->ms[i]=i;
      pTabTime0=new Table2DGu<int,int,double>(*pTabIts0);
      pTabrErr0=new Table2DGu<int,int,double>(*pTabIts0);
      pTabIts1=new Table2DGu<int,int,double>(*pTabIts0); pTabTime1=new Table2DGu<int,int,double>(*pTabTime0);
      pTabrErr1=new Table2DGu<int,int,double>(*pTabrErr0);
      tmp=mkdir(argv[2],0777);
      if(tmp!=0 && errno!=EEXIST){
         fprintf(stderr,"ERROR %d:unable to mkdir %s; %s\n",errno,argv[2],strerror(errno));
      }
    //////////////generate the testing smoothing steps/////////////
    if(!mlogscale){
      msize=(mend-mstart+mskip-1)/mskip;
      pm=new PetscInt[msize];
      mCounter=0;for(m=mstart;m<mend;m+=mskip) pm[mCounter++]=m;
    } else{
      pm=new PetscInt[msize];
      if(msize>1){
	mCounter=0;
	for(m=0;m<msize;m++) 
	  pm[mCounter++]=(pow(10,mstartlog+(mendlog-mstartlog)/(msize-1)*m)+0.5);
      }
      else pm[0]=pow(10,mstartlog)+0.5;
    }
    ///////////////////////////////////////////////////////////////////
    /////////////test MG as a pc for KSP/////////////////////////////
    ierr=MatNullSpaceCreate(PETSC_COMM_SELF,PETSC_TRUE,0,PETSC_NULL,&nsp);CHKERRQ(ierr);
    ///////////////////create pc0,pc1/////////////////////////////
    ierr=PCCreate(PETSC_COMM_SELF,&pc0);CHKERRQ(ierr);
    ierr=PCCreate(PETSC_COMM_SELF,&pc1);CHKERRQ(ierr);
    ierr=PCSetType(pc0,PCNONE);CHKERRQ(ierr);
    ierr=PCSetFromOptions(pc0);CHKERRQ(ierr);
    ierr=PCSetType(pc1,PCSHELL);CHKERRQ(ierr);

     mCounter=0;
     for(mCounter=0;mCounter<msize;mCounter++){
	 m=pm[mCounter];
	 for(k=slevel;k<=elevel;k++){
	   ///////////////////reset pc1///////////////
	   PCSetOperators(pc0,*(pMGBHs[k]->pK),*(pMGBHs[k]->pK),SAME_NONZERO_PATTERN);
	   PCSetOperators(pc1,*(pMGBHs[k]->pK),*(pMGBHs[k]->pK),SAME_NONZERO_PATTERN);
           
	   /////////reset MG solvers & pc2////////////
	   for(l=0;l<=k;l++)pMGBHs[l]->m=m;
	   pPCGu=new PCGu(pMGBHs[k]);
	   ierr=PCShellSetContext(pc1,(void*)pPCGu);CHKERRQ(ierr);      
	   ierr=PCShellSetApply(pc1,&shellPCApply);CHKERRQ(ierr);
           ierr=PCSetUp(pc0);CHKERRQ(ierr);
           ierr=PCSetUp(pc1);CHKERRQ(ierr);
	   printf("=====================================\n");
	   printf("Testing MG%c%d%c%d on level %d, m=%d:\n",MGBHMode,m,PrecMode,mMGNeu,k,m);
	   ierr=compare(pc0,pc1, KSPCG,*(pMGBHs[k]->pK),
			nsp,*(pMGBHs[k]->pMGData->pPhi),*(pMGBHs[k]->pMGData->pvB),nRun,
			&((*pTabIts0)(k-slevel,0)),&((*pTabTime0)(k-slevel,0)),&((*pTabrErr0)(k-slevel,0)),
			&((*pTabIts1)(k-slevel,0)),&((*pTabTime1)(k-slevel,0)),&((*pTabrErr1)(k-slevel,0)));CHKERRQ(ierr);
          
	   //////////////////free/////////////////////
	   delete pPCGu;
	   printf("=======================================\n"); 
	 }
	 infname2.assign(argv[1]);
	 pos=infname2.find_last_of('/');
	 if(pos==std::string::npos) pos=0;
	 infname2=infname2.substr(pos);
	 strcpy(infname1,infname2.c_str());

	 sprintf(outfname,"%s/%s_%c%c%d_L%d-%d_m%d_its_np",argv[2],infname1,MGBHMode,PrecMode,mMGNeu,slevel,elevel,m);
         pTabIts0->out(outfname);
	 sprintf(outfname,"%s/%s_%c%c%d_L%d-%d_m%d_time_np",argv[2],infname1,MGBHMode,PrecMode,mMGNeu,slevel,elevel,m);
         pTabTime0->out(outfname);
         sprintf(outfname,"%s/%s_%c%c%d_L%d-%d_m%d_rErr_np",argv[2],infname1,MGBHMode,PrecMode,mMGNeu,slevel,elevel,m);
         pTabrErr0->out(outfname);

	 sprintf(outfname,"%s/%s_%c%c%d_L%d-%d_m%d_its",argv[2],infname1,MGBHMode,PrecMode,mMGNeu,slevel,elevel,m);
         pTabIts1->out(outfname);
	 sprintf(outfname,"%s/%s_%c%c%d_L%d-%d_m%d_time",argv[2],infname1,MGBHMode,PrecMode,mMGNeu,slevel,elevel,m);
         pTabTime1->out(outfname);
         sprintf(outfname,"%s/%s_%c%c%d_L%d-%d_m%d_rErr",argv[2],infname1,MGBHMode,PrecMode,mMGNeu,slevel,elevel,m);
         pTabrErr1->out(outfname);
         
     }
    ierr=PCDestroy(pc0); CHKERRQ(ierr);
    ierr=PCDestroy(pc1); CHKERRQ(ierr); 
    ierr=MatNullSpaceDestroy(nsp);CHKERRQ(ierr);
    delete []pm; 
    delete pTabIts0; delete pTabTime0; delete pTabrErr0;
    delete pTabIts1; delete pTabTime1; delete pTabrErr1;
    //delete pConNumkm;
     ///////////////////////////////////////////////////////////////////
    for(l=0;l<maxlevel;l++){
      delete pMGDataVec[l];
      delete pDiagProjSolvers[l];
      if(PrecMode!='N')delete pMGNeus[l];
      delete pMGBHs[l];
    }
    delete pCSolverNeu;
    delete pCSolverBH;
    delete []lambda1;  delete []lambda2;
  }else{
    printf("Usage: testMGPC <matfile_D> <-options>\n");
    printf("-gu_MGBHMode,-gu_PrecMode,-gu_mMGNeu,\n");
    printf("-gu_dampingScale\n");
    printf("-gu_mstart,-gu_mend,-gu_mskip,\n");
    printf("-gu_mlogscale,-gu_mstartlog,-gu_mendlog,-gu_msize,\n");
    printf("-gu_slevel,-gu_elevel,\n");
    printf("-gu_nRun,\n");
    printf("-gu_ksp_monitor,-gu_ksp_convergence,\n");
    printf("-ksp_rtol,-ksp_monitor_short\n");
  }
  delete pAs; delete pSs;delete pIs;delete pIts;
  delete pvBs;delete pvBinvs; delete pPhi;
  delete pMGErrBH;  
  ierr=PetscFinalize();CHKERRQ(ierr);
  return 0;
}

PetscErrorCode shellPCApply(PC pc,Vec rhs, Vec outVec){
  PCGu* pPCGu;
  PCShellGetContext(pc,(void**)&pPCGu);
  return pPCGu->apply(rhs,outVec);
}
