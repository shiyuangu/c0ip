/**
 * @file   MatCpuGpuArrayGu.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:28:33 2013
 * 
 * @brief  An array of MatCpuGpu; provides convertions from mxGuCellArray
 * 
 * 
 */

#if CUDA
#include<iostream>
#include"petsc.h"
#include"MatCpuGpuArrayGu.h"
#include"mxGu.h"
#include"converters.h"
namespace gu{
  PetscErrorCode MatCpuGpuArray::getFrom(const mxGuCellArrayT<mxGuSparse>& source,DevMatType dmt){
    int i;
    PetscErrorCode ierr;
    Mat mattmp;
    MatCpuGpu* pMatCpuGpu;
    if(source.size==0){
      std::cout<<"source array is of size zero. Nothing needs to be done\n";
      return 0;
    }
    size=source.size;
    pVal.reserve(size);
    for(i=0;i<size;i++){
      ierr=MatCreateSeqAIJ(PETSC_COMM_SELF,source.pVal[i].nrow,source.pVal[i].ncol,50,PETSC_NULL,&mattmp);CHKERRQ(ierr);
      mxGuSparse2Mat(source.pVal[i],mattmp);
      pMatCpuGpu=new MatCpuGpu(mattmp,dmt);
      pVal.push_back(*pMatCpuGpu);
      ierr=MatDestroy(&mattmp);CHKERRQ(ierr);     
    }
    return 0;  
  }
  PetscErrorCode MatCpuGpuArray::getFromTranspose(const MatCpuGpuArray& s){
    PetscErrorCode ierr;
    size=s.size;
    pVal.resize(size);
    for(int i=0;i<size;i++){
      ierr=MatTranspose(s.pVal[i],MAT_INITIAL_MATRIX,&(pVal[i]));CHKERRQ(ierr);
    }
    return 0;
  }
}
#endif
