/**
 * @file   MyVec.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:45:22 2013
 * 
 * @brief  Use PETSc or CUSP vector routines depends on whether CUDA is enabled
 * 
 * 
 */

#include "MyVec.h"
extern PetscLogEvent eventMyVecDot;
//extern PetscLogStage stageDebug;
PetscErrorCode MyVecDot(Vec x, Vec y, PetscScalar *val){
  PetscErrorCode ierr;
  //PetscLogStagePush(stageDebug);
  ierr=PetscLogEventBegin(eventMyVecDot,0,0,0,0);CHKERRQ(ierr);
  ierr=VecDot(x,y,val);CHKERRQ(ierr);
  ierr=PetscLogEventEnd(eventMyVecDot,0,0,0,0);CHKERRQ(ierr);
  //PetscLogStagePop();
  return 0;
}
