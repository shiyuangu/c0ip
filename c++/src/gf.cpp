#include<cstdio>
void promptOps(){
    printf("-c1,-c2 -c3, (for Delta^2 u-c2 Delta u+ c1u=f, precoditioned by -Delta u+c3 u=g)\n");
    printf("-gu_MGBHMode,-gu_PrecMode,-gu_mMGNeu,\n");
    printf("-gu_dampingScale\n");
    printf("-gu_mstart,-gu_mend,-gu_mskip,\n");
    printf("-gu_mlogscale,-gu_mstartlog,-gu_mendlog,-gu_msize,\n");
    printf("-gu_slevel,-gu_elevel,-gu_maxlevel\n");
    printf("-gu_nRun,\n");
    printf("-gu_ksp_monitor,-gu_ksp_convergence,\n");
    printf("-ksp_rtol,-ksp_monitor_short\n");
    printf("-gu_CHpenalty,-gu_CHeps1,-gu_CHeps2,-gu_CHdt,-gu_CHlenUnit\n");
    printf("-gu_CHksp_rtol1,-gu_CHksp_rtol2\n");
    printf("-gu_kspSolver\n");
    printf("-gu_mymat cuspcsr");
    printf(" -gu_powerTol,-gu_slevel,-gu_elevel\n");
    printf("-eps_monitor, -eps_nev, -eps_tol,\n");
    printf("-eps_type(power,arnoldi,lanczos,krylovschur,gd,jd)\n");
}
