/**
 * @file   Mesh2Gu.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:35:26 2013
 * 
 * @brief  Mesh of type 2
 * 
 * 
 */

#include"Mesh2Gu.h"
#include"converters.h"
void Mesh2::release(){
  if(nGI>0){
    delete []idx;idx=NULL;
    delete []xyco; xyco=NULL;
    delete []vertex; vertex=NULL;
    delete []boundary; boundary=NULL;
    delete []free; free=NULL;
    delete []T; T=NULL;
    nGI=nDOF=0;
  }
}
void Mesh2Array::release(){
  if(size>0){
    delete []pVal;
    pVal=NULL; size=0;
  }
}

void Mesh2Array::getFrom(const mxGuCellArrayT<mxMesh2>& source){
  if(source.size<=0){
    fprintf(stderr,"source mesh array is empty.Nothing is done.\n");
    return;
  }
  size=source.size;
  pVal=new Mesh2[size];
  for(int i=0;i<size;i++)
    mxMesh2toMesh2(source.pVal[i],pVal[i]);
}
