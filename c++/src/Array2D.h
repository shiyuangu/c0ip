/**
 * @file   Array2D.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 13:43:27 2013
 * 
 * @brief  provide 2D array with sampling methods. 
 * 
 * 
 */
#pragma once
#include"clamp.h"
//2d Array template 
namespace gu{
  //adapted from Func2DGu/handtracking
 template<typename T>
 class Array2D{
 public:
   Array2D(){nrow=ncol=0;value=NULL;}//initialization list might be more C++ like; but since they are built-in type, they are in fact the same. 
   ~Array2D(){
     if(value!=NULL) {delete[]value; value=NULL;}
   }
   void malloc(size_t nrow, size_t ncol){
     release();
     value=new T[nrow*ncol];
     this->nrow=nrow; this->ncol=ncol; 
   }
   void release(){
     if(value!=NULL){delete[] value; value=NULL; nrow=ncol=0;} 
   }
   T& get(size_t x,size_t y){return value[y*ncol+x];}
   size_t nrow, ncol;
   T* value;
   //nearest neighbor sampling. 
   T sampleNN(double x, double y){
     int cx,cy; 
     cx=x+0.5; cy=y+0.5;
     clamp(cx,0,(int)ncol-1);
     clamp(cy,0,(int)nrow-1);
     return get((size_t)cx,(size_t)cy);
   }   
 };
 
} 
