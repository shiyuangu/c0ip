/**
 * @file   MCMG.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:18:46 2013
 * 
 * @brief  Multi cycles MG; successively apply multigrid cycles to 
 *         obtain an approximation solution.
 * 
 */
#pragma once
#include"Solver.h"
#include"MG.h"
class MCMG:public Solver{
public:
  MGSolver* pMGSolver;
  MCMG(MGSolver* _pMGSolver,PetscScalar _tol, PetscInt _maxit);
  ~MCMG();
  PetscErrorCode solve(const Vec& rhs, Vec& x);
  Vec workVec[9];
  PetscScalar tol;
  PetscInt maxit;
  /***KSP for test***/
  KSP ksp;
  PC prec; 
}; 
