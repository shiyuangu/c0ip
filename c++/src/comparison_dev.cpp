/**** Obsolete ***/
#include<cmath>
#include"comparison.h"
#include"others.h"
#include"auxiliary.h"
//////////compare the performace of pc1,pc2 as KSP preconditioner////////////////////// 
//////////to solve Kx=b, b is random vector living is a space orth.//////////////////// 
//////////to phi w.r.t the inner producred represented by vB(diag. matrix)/////////////
//////////-gu_ksp_monitor/-gu_ksp_convergence can be used to monitor/test in K-norm////
PetscErrorCode compare(PC& pc1, PC& pc2, const KSPType ksptype,Mat K, 
		       MatNullSpace nsp, Vec phi,Vec vB,int nRun,
		       double* pIts0,double time0[],double rErr0[],
		       double* pIts1,double time1[],double rErr1[]){
  PetscRandom randomctx;
  PetscErrorCode ierr;
  KSP ksp1, ksp2;
  Vec x,rhs,workVec[2],r,diff;
  PetscScalar alpha1,alpha2,errEnorm1,errEnorm2;
  PetscScalar xnorm,rhsnorm,residualL2norm1,residualL2norm2;//l2-norm
  PetscInt its1,its2,size;
  KSPConvergedReason reason;
  KspCtxGu* pKspCtx;
  PetscBool flg;
  PetscLogDouble stime, etime; 
  MatGetVecs(K,&x,&rhs);
  VecDuplicate(x,&(workVec[0]));
  VecDuplicate(x,&(workVec[1]));
  VecDuplicate(x,&r);
  VecDuplicate(x,&diff);
  VecGetSize(x,&size);
  pKspCtx=new KspCtxGu(K);
  ///////////////////setup ksp1////////////////////////////
  ierr=KSPCreate(PETSC_COMM_SELF,&ksp1);CHKERRQ(ierr);
  ierr=KSPSetType(ksp1,ksptype);CHKERRQ(ierr);
  ierr=KSPSetNormType(ksp1,KSP_NORM_UNPRECONDITIONED);CHKERRQ(ierr);
  ierr=KSPSetTolerances(ksp1,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,10*size);
  //ierr=KSPSetFromOptions(ksp1);CHKERRQ(ierr);
  ierr=KSPSetPC(ksp1,pc1);CHKERRQ(ierr); 
  ierr=KSPSetNullSpace(ksp1,nsp);CHKERRQ(ierr);
  PetscOptionsHasName(PETSC_NULL,"-gu_ksp_monitor",&flg);
  if(flg){
       ierr=KSPMonitorSet(ksp1,&kspMonitorGu,(void*)pKspCtx,PETSC_NULL);CHKERRQ(ierr);
  }
  PetscOptionsHasName(PETSC_NULL,"-gu_ksp_convergence",&flg);
  if(flg){
    ierr=KSPSetConvergenceTest(ksp1,&kspConvgTest,(void*)pKspCtx,PETSC_NULL);CHKERRQ(ierr);
  }
  ierr=KSPSetFromOptions(ksp1);CHKERRQ(ierr);
  ierr=KSPSetUp(ksp1);CHKERRQ(ierr);
  ////////////////setup ksp2////////////////////////////////
  ierr=KSPCreate(PETSC_COMM_SELF,&ksp2);CHKERRQ(ierr);
  ierr=KSPSetType(ksp2,ksptype);CHKERRQ(ierr);
  ierr=KSPSetNormType(ksp2,KSP_NORM_UNPRECONDITIONED);CHKERRQ(ierr);
  ierr=KSPSetTolerances(ksp2,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,10*size);
  ierr=KSPSetFromOptions(ksp2);CHKERRQ(ierr);
  ierr=KSPSetPC(ksp2,pc2);CHKERRQ(ierr); 
  ierr=KSPSetNullSpace(ksp2,nsp);CHKERRQ(ierr);
  PetscOptionsHasName(PETSC_NULL,"-gu_ksp_monitor",&flg);
  if(flg){
       ierr=KSPMonitorSet(ksp2,&kspMonitorGu,(void*)pKspCtx,PETSC_NULL);CHKERRQ(ierr);
  }
  PetscOptionsHasName(PETSC_NULL,"-gu_ksp_convergence",&flg);
  if(flg){
    ierr=KSPSetConvergenceTest(ksp2,&kspConvgTest,(void*)pKspCtx,PETSC_NULL);CHKERRQ(ierr);
  }
  //ierr=KSPSetFromOptions(ksp2);CHKERRQ(ierr);
  ierr=KSPSetUp(ksp2);CHKERRQ(ierr);
  ////////////////solve and compare///////////////////
  ierr=PetscRandomCreate(PETSC_COMM_SELF,&randomctx);CHKERRQ(ierr);
  ierr=PetscRandomSetType(randomctx,PETSCRAND48);CHKERRQ(ierr);
  //ierr=PetscRandomSetSeed(randomctx,2011);CHKERRQ(ierr);
  ierr=PetscRandomSetInterval(randomctx,-1.0,1.0);CHKERRQ(ierr);
  
  for(int i=0;i<nRun;i++){
    printf("----------run %d------------------\n",i);
  //ierr=PetscRandomSeed(randomctx);CHKERRQ(ierr);
  ierr=VecSetRandom(workVec[0],randomctx);CHKERRQ(ierr);
  ierr=proj(phi,vB,workVec[0],workVec[1],x);CHKERRQ(ierr);
  ierr=pKspCtx->copy(x);CHKERRQ(ierr);
  ierr=MatMult(K,x,rhs);CHKERRQ(ierr);
  ierr=VecNorm(x,NORM_2,&xnorm);CHKERRQ(ierr);
  ierr=VecNorm(rhs,NORM_2,&rhsnorm);CHKERRQ(ierr);
  
  ////////////////ksp1 solve////////////////////////////
  PetscGetTime(&stime);
  ierr=KSPSolve(ksp1,rhs,r);CHKERRQ(ierr);
  PetscGetTime(&etime);
  time0[i]=etime-stime; // Be caution about the implicit casting. 
  ierr=KSPGetConvergedReason(ksp1,&reason);CHKERRQ(ierr);
  if(reason<0)printf("\t\tksp1 diverges!\n");
  ierr=shiftOrth(phi,vB,workVec[0],r);CHKERRQ(ierr);
  ierr=VecWAXPY(diff,-1.0,r,x);
  ierr=VecNorm(diff,NORM_2,&alpha1);CHKERRQ(ierr);
  ierr=innerProduct(K,diff,diff,workVec[0],&errEnorm1);CHKERRQ(ierr);
  errEnorm1=sqrt(errEnorm1);
  rErr0[i]=errEnorm1/pKspCtx->xEnorm;
  ierr=KSPGetIterationNumber(ksp1,&its1);CHKERRQ(ierr);
  ierr=KSPGetResidualNorm(ksp1,&residualL2norm1);CHKERRQ(ierr);
  
  printf("\t\t=======================================\n");
  printf("\t\tx_l2-norm:%#0.3e,x_Enorm=%#0.3e, rhs_l2-norm=%#0.3e\n",xnorm,pKspCtx->xEnorm,rhsnorm);
  printf("\t\t=======================================\n"); 
  printf("\t\tksp1 solve is done\n");
  printf("\t\terror(l2):%#0.3e, error(Enorm1):%#0.3e(%0.2g),residual(l2):%#0.3e\n",
	 alpha1,errEnorm1,rErr0[i],residualL2norm1);
  PetscPrintf(PETSC_COMM_SELF,"\t\t iterations=%d, time=%0.3e\n",its1,etime-stime);
  pIts0[i]=its1; 
  ///////////////ksp2 solve/////////////////////////////
  PetscGetTime(&stime);
  ierr=KSPSolve(ksp2,rhs,r);CHKERRQ(ierr);
  PetscGetTime(&etime);
  time1[i]=etime-stime;  //Be caution about the implicit casting
  ierr=KSPGetConvergedReason(ksp2,&reason);CHKERRQ(ierr);
  if(reason<0)printf("\t\tksp2 diverges!\n");
  ierr=shiftOrth(phi,vB,workVec[0],r);CHKERRQ(ierr);
  ierr=VecWAXPY(diff,-1.0,r,x);
  ierr=VecNorm(diff,NORM_2,&alpha2);CHKERRQ(ierr);
  ierr=innerProduct(K,diff,diff,workVec[0],&errEnorm2);CHKERRQ(ierr);
  errEnorm2=sqrt(errEnorm2);
  rErr1[i]=errEnorm2/pKspCtx->xEnorm;
  ierr=KSPGetIterationNumber(ksp2,&its2);CHKERRQ(ierr);
  ierr=KSPGetResidualNorm(ksp2,&residualL2norm2);CHKERRQ(ierr);
  printf("\t\t=======================================\n");
  printf("\t\tksp2 solve is done\n");
  printf("\t\terror(l2):%#0.3e, error(Enorm):%#0.3e(%0.2g),residual(l2):%#0.3e\n",
	 alpha2,errEnorm2,rErr1[i],residualL2norm2);
  PetscPrintf(PETSC_COMM_SELF,"\t\t iterations=%d, time=%0.3e\n",its2,etime-stime);
  pIts1[i]=its2; 
  }
  ierr=PetscRandomDestroy(randomctx);CHKERRQ(ierr);
  ////////////////free///////////////////////////////
  ierr=KSPDestroy(ksp1);CHKERRQ(ierr);
  ierr=KSPDestroy(ksp2);CHKERRQ(ierr);
  ierr=VecDestroy(x);CHKERRQ(ierr);
  ierr=VecDestroy(rhs);CHKERRQ(ierr);
  ierr=VecDestroy(r);CHKERRQ(ierr);
  ierr=VecDestroy(workVec[0]);CHKERRQ(ierr);
  ierr=VecDestroy(workVec[1]);CHKERRQ(ierr);
  ierr=VecDestroy(diff);CHKERRQ(ierr);
  delete pKspCtx;
  return 0;
  
}

PetscErrorCode kspMonitorGu(KSP ksp, PetscInt it, PetscReal rnorm, void * ctx){
  //ctx should contain a workvec and the true solution x
  PetscScalar enorm;
  KspCtxGu* pKspCtx;
  Vec cS,diff;//current solution;
  PetscErrorCode ierr;
 
  ierr=KSPBuildSolution(ksp,PETSC_NULL,&cS);CHKERRQ(ierr);
  pKspCtx=(KspCtxGu*)ctx;
  ierr=VecWAXPY(pKspCtx->workVec[0],-1.0,cS,pKspCtx->x);CHKERRQ(ierr);
  ierr=innerProduct(pKspCtx->K,pKspCtx->workVec[0],pKspCtx->workVec[0],pKspCtx->workVec[1],&enorm);CHKERRQ(ierr);
  enorm=sqrt(enorm);
  printf("In Ksp %d iteration,rnorm=%g,E-norm=%g\n",it,rnorm,enorm);
  return 0;
}
PetscErrorCode kspConvgTest(KSP ksp,PetscInt it,PetscReal rnorm,KSPConvergedReason* pReason,void* spKspCtx){
  PetscErrorCode ierr;
  PetscReal rtol,atol;
  KspCtxGu* pKspCtx;
  PetscScalar errEnorm; //Energy norm of error
  pKspCtx=(KspCtxGu*)spKspCtx;
  ierr=KSPBuildResidual(ksp,pKspCtx->workVec[0],pKspCtx->workVec[1],&pKspCtx->residual);CHKERRQ(ierr);
  ierr=KSPBuildSolution(ksp,PETSC_NULL,&pKspCtx->cS);
  ierr=VecWAXPY(pKspCtx->workVec[0],-1.0,pKspCtx->cS,pKspCtx->x);CHKERRQ(ierr);
  ierr=VecDot(pKspCtx->workVec[0],pKspCtx->residual,&errEnorm);CHKERRQ(ierr);
  errEnorm=sqrt(errEnorm);
  ierr=KSPGetTolerances(ksp,&rtol,&atol,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  if(errEnorm<rtol*pKspCtx->xEnorm) *pReason=KSP_CONVERGED_RTOL;
  else if(errEnorm<atol) *pReason=KSP_CONVERGED_ATOL;
  else *pReason=KSP_CONVERGED_ITERATING;
  return 0;
}

