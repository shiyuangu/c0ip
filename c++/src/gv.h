//define global variables. 
#pragma once
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"Array2D.h"
#include"predefGu.h"
//#include"CHSolver.h"
//extern MyMatArray *pAs,*pSs,*pIs,*pIts,*pMs;
//extern PetscMatArray *pAcMs,*pScMs,*pIs,*pIts,*pMs;
//extern PetscVecArray *pvBs,*pvBinvs,*pPhi;
//extern Mesh2Array mesh2es;
//extern int maxlevel;
//extern double dampingScale; //the damping factors are the estimated spectral radii times damping scales
//extern double *lambda1, *lambda2;   //the damping factors;
//extern double c1,c2;    //for \Delta^2 u-c2\Delta u+c1u=rhs
//extern double c3;       //for preconditioner -\Deltau + c3 u=rhs
//extern std::vector<MGData*> pMGDataVec;

//switches
extern PetscBool myKspSolverOn;
//for profiling
//extern PetscLogEvent eventMyVecDot;
//extern PetscLogStage stageDebug;
//extern PetscLogEvent eventMyKspSolver;
//extern PetscInt count;//tmp
//#if MY_MAT
//extern gu::DevMatType myMatDevType;
//#endif
