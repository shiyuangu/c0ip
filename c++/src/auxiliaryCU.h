/**
 * @file   auxiliaryCU.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:06:01 2013
 * 
 * @brief  Provide functions for debugging and profiling. 
 */
#pragma once
PetscErrorCode printCuspArray1D(void* a);
