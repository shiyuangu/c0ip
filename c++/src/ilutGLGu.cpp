//Graphics/image functions using OpenGL and DevIL;superceded by gfIFace.
#include"ilutGLGu.h"
//#define ILUT_USE_OPENGL
#include"ilut_opengl.h"
#include<GL/glut.h>

/** 
 * taken from the source code of ilut_opengl.c and modified
 *
 */
ILboolean ILAPIENTRY ilutGLScreenGu()
{
  //	ILuint	ViewPort[4];
  int windowWidth, windowHeight; 
	ilutCurImage = ilGetCurImage();
	if (ilutCurImage == NULL) {
		ilSetError(ILUT_ILLEGAL_OPERATION);
		return IL_FALSE;
	}

	//	glGetIntegerv(GL_VIEWPORT, (GLint*)ViewPort);
        windowWidth=glutGet(GLUT_WINDOW_WIDTH);
	windowHeight=glutGet(GLUT_WINDOW_HEIGHT);
	
	// if (!ilTexImage(ViewPort[2], ViewPort[3], 1, 3, IL_RGB, IL_UNSIGNED_BYTE, NULL))
	// 	return IL_FALSE;  // Error already set.
	
	//ilTexImage is used to give the current bound image new attributes. refer to manual for details
	if (!ilTexImage(windowWidth, windowHeight, 1, 3, IL_RGB, IL_UNSIGNED_BYTE, NULL))
		return IL_FALSE;  // Error already set.
		ilutCurImage->Origin = IL_ORIGIN_LOWER_LEFT;

	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	//glReadPixels(0, 0, ViewPort[2], ViewPort[3], GL_RGB, GL_UNSIGNED_BYTE, ilutCurImage->Data);
	glReadPixels(0, 0, windowWidth, windowHeight, GL_RGB, GL_UNSIGNED_BYTE, ilutCurImage->Data);
	return IL_TRUE;
}


#ifndef _WIN32_WCE
ILboolean ILAPIENTRY ilutGLScreenieGu()
{
	FILE		*File;
	ILchar		Buff[255];
	ILuint		i, CurName;
	ILboolean	ReturnVal = IL_TRUE;

	CurName = ilGetCurName();

	// Could go above 128 easily...
	for (i = 0; i < 128; i++) {
#ifndef _UNICODE
		sprintf(Buff, "screen%d.jpg", i);
		File = fopen(Buff, "rb");
#else
		swprintf(Buff, 128, L"screen%d.jpg", i);
		// Windows has a different function, _wfopen, to open UTF16 files,
		//  whereas Linux just uses fopen.
		#ifdef _WIN32
			File = _wfopen(Buff, L"rb");
		#else
			File = fopen((char*)Buff, "rb");
		#endif
#endif
		if (!File)
			break;
		fclose(File);
	}

	if (i == 127) {
		ilSetError(ILUT_COULD_NOT_OPEN_FILE);
		return IL_FALSE;
	}

	iBindImageTemp();
	if (!ilutGLScreenGu()) {
		ReturnVal = IL_FALSE;
	}

	if (ReturnVal)
		ilSave(IL_JPG, Buff);

	ilBindImage(CurName);

	return ReturnVal;
}
#endif//_WIN32_WCE
