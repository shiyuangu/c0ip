#include<cstdio>
#include<cstdlib>
//#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"petsc.h"
#include"petsctime.h"
#include"slepceps.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
//#include"PetscArrayGu.h"
//#include"Mesh2Gu.h"
//#include"MG.h"
//#include"gv.h"
#include"gf.h"
//#include"gfMG.h"
//#include"SolverErr.h"
//#include"comparison.h"
#include"others.h"
#include"auxiliary.h"
//#include"hooks.h"
#include"MyInit.h"
#include"auxiliaryCU.h"
/**predefGu.h defines MyMat which is used as Matrix Type in MG*/
#include"predefGu.h" 
#include"converters.h"
#include"FullMG.h"
#include"BHMG.h"
PetscBool myKspSolverOn;
//parameters to control the testing level and smoothing steps. 
//PetscInt slevel,elevel,sizeks;
PetscInt level;
PetscBool mlogscale;
PetscReal mstartlog,mendlog;
PetscInt msize;
PetscInt mstart=6, mend=36,mskip=2;


//for profiling
PetscLogEvent eventMyVecDot,eventMyMatMult,eventMatMultCusp,eventMatMultAddCusp;
PetscLogEvent stageDebug;
PetscLogEvent eventMyKspSolver;
PetscLogStage memoryWatch;
//PetscInt count; //tmp
int main(int argc, char**argv){
  FullMG* pFullMG;
  BHMG* pBHMG;
  int l,k,m;
  PetscInt nRun;
  Vec workVec[6],x,outVec;
  PetscScalar tmp1,tmp2,err,tol,powerTol;
  PetscScalar mgErrNorm; //This is the norm of error, NOT the dampingfactor. 
  PetscRandom randomctx; 
  PetscErrorCode ierr;
  PetscBool flag;
  bool flag2; 
  char outfname[256]; int tmp; //only for output result
  char fname[256];
  FILE* fin; 
  char buffer[256];
  size_t pos;   //for exact only the file name(not the path in argv[1])
  
  //////for flop counts and timing for combinations of (k,m)  
  Vec rhs; 
  PetscLogEvent EVENT_MGSolver,EVENT_CNeuSolver;
  PetscLogStage stages[2];
  PetscLogDouble sflops, eflops,stime,etime;
  PetscScalar errEnorm,xEnorm;
  PetscLogDouble st,ed,elapsed;
  //for reading in vector
  mxGuDouble mxSol;
  //PetscBool solFlag=PETSC_FALSE; //indicate whether the exact solution is available. 
  //for iterating
  PetscInt maxIter;
  size_t curIter,preIter,counter;
  //PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  ierr=SlepcInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  if(argc>1){
    #if MY_MAT==1
      ierr=PetscOptionsGetString(PETSC_NULL,"-vec_type",buffer,256,&flag);CHKERRQ(ierr);
      if(!flag || strcmp(buffer,VECSEQCUSP)){
	SETERRQ(PETSC_COMM_SELF,1,"MY_MAT==1,please set -vec_type seqcusp");
      }
     
    #endif 
      MyInitialize(); // to log petsc events. 
    PetscOptionsHasName(PETSC_NULL,"-gu_kspSolver",&myKspSolverOn);
    if(myKspSolverOn)printf("myKspSolverOn is on\n");
 
    PetscOptionsGetReal(PETSC_NULL,"-gu_tol",&tol,&flag);
    if(!flag)tol=1e-4;
    printf("-gu_tol=%#.3e\n",tol);
    PetscOptionsGetInt(PETSC_NULL,"-gu_maxIter",&maxIter,&flag);
    if(!flag)maxIter=100;
    printf("-gu_maxIter=%d\n",maxIter);

    /******generate the key objects*********/
    pBHMG=new BHMG(argv[1]);
    pFullMG=new FullMG(pBHMG->pMGBHs,tol,maxIter);

    
    PetscOptionsGetInt(PETSC_NULL,"-gu_level",&level,&flag);
    if(!flag)level=pBHMG->maxlevel-1;
    printf("-gu_level=%d\n",level);
    PetscOptionsGetInt(PETSC_NULL,"-gu_nRun",&nRun,&flag);
    if(!flag)nRun=1;
    printf("-gu_nRun=%d\n",nRun);
     PetscOptionsHasName(PETSC_NULL,"-gu_mlogscale",&mlogscale);
    if(!mlogscale){
      PetscOptionsGetInt(PETSC_NULL,"-gu_mstart",&mstart,&flag);
      if(!flag||mstart<=0){
	fprintf(stderr,"Error:Please specify -gu_mstart(>=1)\n or -gu_mlogscale");
	PetscEnd();
      }else printf("-gu_mstart=%d\n",mstart);
      PetscOptionsGetInt(PETSC_NULL,"-gu_mskip",&mskip,&flag);
      if(!flag){
	mskip=1;
	fprintf(stderr,"Warning:-gu_mskip is not specified, set to be %d\n",mskip);
      }else printf("-gu_mskip=%d\n",mskip);
      PetscOptionsGetInt(PETSC_NULL,"-gu_mend",&mend,&flag);
      if(!flag||mend<0){
	mend=mstart+mskip;
	fprintf(stderr,"Warning: -gu_mend is not specified, set to be %d\n",mend); 
      }else printf("-gu_mend=%d\n",mend);
    }else{
      PetscOptionsGetReal(PETSC_NULL,"-gu_mstartlog",&mstartlog,&flag);
      if(!flag){
	fprintf(stderr,"Error: Log scale has been set,-gu_startlog must be specified!\n");
        PetscEnd();
        exit(1);
      }else printf("-gu_mstartlog=10^%g(i.e. %g)\n",mstartlog,pow(10,mstartlog));
      PetscOptionsGetReal(PETSC_NULL,"-gu_mendlog",&mendlog,&flag);
      if(!flag){
	fprintf(stderr,"Error: Log scale has been set,-gu_mendlog must be specified!\n");
        PetscEnd();
        exit(1);
      }printf("-gu_mstartlog=10^%g(i.e. %g)\n",mendlog,pow(10,mendlog));
      PetscOptionsGetInt(PETSC_NULL,"-gu_msize",&msize,&flag);
      if(!flag || msize<1){
	fprintf(stderr,"Error:Log scale has been set,-gu_msize must be specified(>0) (msize=%d)!\n",msize);
	PetscEnd();
        exit(1);
      }
    }
    ///////////////////////////////////////////////////////////////////
    /**************compute contration numbers********/
    
    PetscRandomCreate(PETSC_COMM_SELF,&randomctx);
    PetscRandomSetType(randomctx,PETSCRAND48);
    PetscRandomSetInterval(randomctx,-1.0,1.0);
     
    /***********create log**************/
    //PetscLogStageRegister("Coarse Neu",&stages[0]);
    PetscLogStageRegister("BHMGSolve Solve",&stages[1]); 
    // PetscLogStageRegister("tester ",&stages[2]); 
    PetscLogEventRegister("MGBHSolver",0,&EVENT_MGSolver);
    PetscLogEventRegister("Coarse Neu solver",0,&EVENT_CNeuSolver);
    /**********log the coarsest level solver for Neumann Problem*********/
   
    // MatGetVecs(*(pCSolverNeu->pK),&x0,&rhs); VecDuplicate(x0,&(workVec[0]));VecDuplicate(x0,&(workVec[1]));
    // VecSetRandom(x0,randomctx);   
    // VecCopy(x0,workVec[0]);
    // proj(*(pMGDataVec[0]->pPhi),*(pMGDataVec[0]->pvB),workVec[0],workVec[1],x0);
    // MatMult(*(pMGDataVec[0]->pA),x0,rhs);
   
    // //PetscGetFlops(&sflops);PetscGetTime(&stime);
    // PetscLogStagePush(stages[0]);
    // //PetscLogEventBegin(EVENT_CNeuSolver,0,0,0,0);
    // pCSolverNeu->solve(rhs,workVec[0]);
    // //PetscLogEventEnd(EVENT_CNeuSolver,0,0,0,0);
    // PetscLogStagePop();
    //PetscGetTime(&etime);PetscGetFlops(&eflops);
    //PetscPrintf(PETSC_COMM_WORLD,"CSolverNeu: flops:%0.3e, time: %0.3e\n",eflops-sflops,etime-stime);

    // ierr=VecDestroy(&x0);CHKERRQ(ierr);
    // ierr=VecDestroy(&rhs);CHKERRQ(ierr);
    // ierr=VecDestroy(&workVec[0]);CHKERRQ(ierr);
    // ierr=VecDestroy(&workVec[1]);CHKERRQ(ierr);
    /********************************************************************/
        
      k=level;
      for(l=0;l<6;l++){
	VecCreateSeq(PETSC_COMM_SELF,pBHMG->pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));
      }
      VecDuplicate(workVec[0],&x);
      VecDuplicate(x,&rhs);
      VecDuplicate(workVec[0],&outVec);

      PetscOptionsHasName(PETSC_NULL,"-gu_gen_sol",&flag);
      if(!flag){
	sprintf(fname,"%s/%s",argv[1], "x");
	fin=fopen(fname,"rb");
	if(fin!=NULL){
	  mxSol.read(fin);
	  mxGuDouble2Vec(mxSol,x);
	  fclose(fin);
	}else{
	  fprintf(stderr,"Cannot open file %s\n",fname);
	}
      }
      if(flag || fin==NULL){
	PetscInt randomSeed;
	printf("generate random x0.\n");
	PetscOptionsGetInt(PETSC_NULL,"-gu_random_seed", &randomSeed,&flag);
	if(flag)PetscRandomSetSeed(randomctx, (unsigned long)randomSeed);
	VecSetRandom(x,randomctx);   
	//proj(*(pBHMG->pMGDataVec[k]->pPhi),*(pBHMG->pMGDataVec[k]->pvB),workVec[0],workVec[1],x);
	shiftOrth(*(pBHMG->pMGDataVec[k]->pPhi),*(pBHMG->pMGDataVec[k]->pvB),workVec[0],x);
      }
      MatMult(*(pBHMG->pMGDataVec[k]->pA),x,rhs);
      
      for(m=mstart;m<mend;m+=mskip){
	for(l=0;l<=k;l++)pBHMG->pMGBHs[l]->m=m;
         
	/********solve and log*********/ 
         PetscLogStagePush(stages[1]);
         PetscLogEventBegin(EVENT_MGSolver,0,0,0,0);
	 ierr=PetscGetTime(&st);CHKERRQ(ierr);
	 for(l=0;l<nRun;l++){
	   ierr=pFullMG->solve(rhs, workVec[0],k);CHKERRQ(ierr);
			 
	 }
	 ierr=PetscGetTime(&ed);CHKERRQ(ierr);
	 elapsed=ed-st;
	 printf("m=%d,nRun=%d,elapsed_time=%g\n",m,nRun,elapsed);
         PetscLogEventEnd(EVENT_MGSolver,0,0,0,0);
         PetscLogStagePop();

         /***********Check the answer: only for debug purpose************/
	 /*****for relative residual*******/
	 ierr=MatMult(*(pBHMG->pMGDataVec[k]->pA),workVec[0],workVec[1]);CHKERRQ(ierr);
	 ierr=VecAYPX(workVec[1],-1.0, rhs);CHKERRQ(ierr);
	 PetscReal residualNorm,rhsNorm;
	 ierr=VecNorm(workVec[1],NORM_2,&residualNorm);CHKERRQ(ierr);
         ierr=VecNorm(rhs,NORM_2,&rhsNorm);CHKERRQ(ierr);
	 printf("||A*xout-rhs||_2/||rhs||_2=%#.3e/%#.3e=%#.3e\n",residualNorm,rhsNorm,residualNorm/rhsNorm);
	 /******check actual error*******/
         ierr=VecWAXPY(workVec[1],-1.0,workVec[0],x);CHKERRQ(ierr);
	 innerProduct(*(pBHMG->pMGDataVec[k]->pA),workVec[1],workVec[1],workVec[2],&errEnorm);errEnorm=sqrt(errEnorm);
         innerProduct(*(pBHMG->pMGDataVec[k]->pA),x,x,workVec[2],&xEnorm);xEnorm=sqrt(xEnorm);
           PetscPrintf(PETSC_COMM_WORLD,"||xout-x||_E/||x||_E=%#.3e/%#.3e=%#.3e\n",errEnorm,xEnorm,errEnorm/xEnorm);
      } 
      printf("\n");
      for(l=0;l<6;l++){
	VecDestroy(&workVec[l]);
      }
      VecDestroy(&x);VecDestroy(&outVec);
      ierr=VecDestroy(&rhs);CHKERRQ(ierr);
      
    PetscRandomDestroy(&randomctx);
    delete pBHMG;
    delete pFullMG;
  }else{
    promptOps();
  }

  ierr=SlepcFinalize();CHKERRQ(ierr);
  return 0;
}
