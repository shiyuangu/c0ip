/**
 * @file   MatCpuGpuArrayGu.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:30:42 2013
 * 
 * @brief  An array of MatCpuGpu; provides convertion form mxGuCellArray
 * 
 * 
 */
#pragma once
#if CUDA
#pragma once
#include<vector>
#include"MatCpuGpuGu.h"
#include"mxGu.h"
namespace gu{
  class MatCpuGpuArray{
  public:
    int size;  
    std::vector<gu::MatCpuGpu> pVal;
    MatCpuGpuArray(){size=0;}
    //~MatCpuGpuArray(){release();}
    PetscErrorCode getFrom(const mxGuCellArrayT<mxGuSparse>& source, DevMatType dmt=CUSP_ELL);
    PetscErrorCode getFromTranspose(const MatCpuGpuArray& s);
    //private:
    //void release();
  };
}
#endif
