///test whether CUSP is running correctly. 
#include<cstdio>
#include<cstdlib>
#include"petsc.h"
int main(int argc, char**argv){
  Mat K;
  Vec inVec, outVec;
  PetscErrorCode ierr;
  PetscScalar *x, *data;
  int size=10;
  PetscInt nRun;
  PetscBool flag;
  
  PetscInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);
  PetscOptionsGetInt(PETSC_NULL,"-gu_nRun",&nRun,&flag);
  if(!flag)nRun=100;
  printf("-gu_nRun=%d\n",nRun);
  //VecCreateSeq(PETSC_COMM_SELF,size,&inVec);
  ierr=VecCreate(PETSC_COMM_SELF,&inVec);CHKERRQ(ierr);
  ierr=VecSetSizes(inVec,size,size);CHKERRQ(ierr);
  ierr=VecSetFromOptions(inVec);CHKERRQ(ierr);
  ierr=VecDuplicate(inVec,&outVec);CHKERRQ(ierr);
  
  //MatCreateSeqAIJ(PETSC_COMM_SELF,size,size,size,PETSC_NULL,&K);
  ierr=MatCreate(PETSC_COMM_SELF,&K);CHKERRQ(ierr);
  ierr=MatSetSizes(K,size,size,size,size);CHKERRQ(ierr);
  ierr=MatSetFromOptions(K);CHKERRQ(ierr);
  data=new PetscScalar[size];

  for(int i=0;i<size; i++) data[i]=i;
  VecSet(inVec,1.0);

  for(int i=0;i<size;i++){
    ierr=MatSetValues(K,1,&i,1,&i,&data[i],INSERT_VALUES);CHKERRQ(ierr);
  }
  ierr=MatAssemblyBegin(K,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr=MatAssemblyEnd(K,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  for(int i=0; i<nRun;i++){
    ierr=MatMult(K,inVec,outVec); CHKERRQ(ierr);
  }
  ierr=VecView(outVec, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
  ierr=MatView(K, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
  
  delete []data;
  MatDestroy(&K);
  VecDestroy(&inVec);
  VecDestroy(&outVec);
  ierr=PetscFinalize();
  return 0;
}
