/**
 * @file   MyVec.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:46:10 2013
 * 
 * @brief  Use PETSc or CUSP routines depends on whether CUDA is enabled.
 * 
 * 
 */
#pragma once
#include "petsc.h"
PetscErrorCode MyVecDot(Vec x, Vec y, PetscScalar *val);
