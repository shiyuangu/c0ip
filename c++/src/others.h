/**
 * @file   others.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:48:20 2013
 * 
 * @brief  Convenient algebra operations. It also use MyXXX hooks   
 *         for debugging/profiling purpose. 
 */
#pragma once
#include"petsc.h"
#include"predefGu.h"
PetscErrorCode proj(const Vec& phi, const Vec& vB,
		    const Vec& inVec, Vec workVec,Vec& outVec);
PetscErrorCode shiftOrth(const Vec& phi, const Vec& vB,
		       Vec workvec,Vec& outVec);//shift outVec to make it orthogonal to Phi w.r.t vB
PetscErrorCode VecAbsMaxGu(Vec x, PetscInt* pIdx, PetscScalar* pVal);
PetscErrorCode innerProduct(Mat A,Vec x, Vec y,Vec workVec,PetscScalar* pVal);

namespace gu{
//subtract a multiple of v1 from inVec to make it orth. to v2 w.r.t.
//the usual inner product.   
PetscErrorCode subtV1orthV2(Vec inVec, Vec outVec, Vec v1,Vec v2);
}
