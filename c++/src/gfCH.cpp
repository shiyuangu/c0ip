/**
 * @file   gfCH.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:19:53 2013
 * 
 * @brief  Provid functions to read images. 
 */
#include<exception>
#include<iostream>
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"converters.h"
#include"Magick++.h"
int readBWImage(const char* fname, gu::Array2D<bool>& pic){
  PetscBool flag;
  FILE* fin;
  mxGuLogical mxPic;
  Magick::Image MagImg;  
  try{
      MagImg.read(fname);
      /***zoom is not in the document but in the tutorial ***/
      MagImg.zoom("128x128"); 
      //MagImg.zoom("512x512");
      //MagImg.sample("128x128");
    }
    catch(Magick::Exception& error){
      std::cout<<"Caught exception during reading an image"<<error.what()<<std::endl;
      return 1;
    }
     //MagImg.display();
    gu::converter(MagImg,pic);
  return 0; 
}
int readCHInpR(const char* path,gu::Array2D<bool>& inpR){
   PetscBool flag;
  char fname[256];
   FILE* fin;
   mxGuLogical mxInpR;
   Magick::Image MagImg;
  PetscOptionsGetString(PETSC_NULL,"-CH_D",fname,255,&flag);
  if(!flag){
    sprintf(fname,"%s/%s",path, "D");
    fin=fopen(fname,"rb");
    if(fin==NULL){
      fprintf(stderr,"error in opening file %s,please specify the inpainting Region\n",fname);
      return 1;
    }
    mxInpR.read(fin);
    fclose(fin);
    gu::converter(mxInpR,inpR);
  }else{
    try{
      MagImg.read(fname);
      MagImg.zoom("512x512");
      MagImg.sample("128x128");
    }
    catch(Magick::Exception& error){
      std::cerr<<"Caught exception during reading an image"<<error.what()<<std::endl<<"Please specify the inpainting region\n";
      return 1;
    }
    //MagImg.display();
    MagImg.negate();
    gu::converter(MagImg,inpR);
  }
  return 0;
}
int readCHPic(const char* path, gu::Array2D<bool>& pic){
  PetscBool flag;
  char fname[256];
  FILE* fin;
  mxGuLogical mxPic;
  Magick::Image MagImg;  
  PetscOptionsGetString(PETSC_NULL,"-CH_pic",fname,255,&flag);
  if(!flag){
    sprintf(fname,"%s/%s",path, "bg");
    fin=fopen(fname,"rb");
    if(fin==NULL){
      fprintf(stderr,"error in opening file %s",fname);
      return 1;
    }
    mxPic.read(fin);
    fclose(fin);
    gu::converter(mxPic,pic); 
  }else{
     try{
      MagImg.read(fname);
      MagImg.zoom("512x512");
      MagImg.sample("128x128");
    }
    catch(Magick::Exception& error){
      std::cout<<"Caught exception during reading an image"<<error.what()<<std::endl;
      return 1;
    }
     //MagImg.display();
    gu::converter(MagImg,pic);
  }
  return 0; 
}
/***commented out on 01/29/2012***
****now in the class of CHSolver***
int readCHOpts(){
  PetscTruth flag;
  PetscOptionsGetScalar(PETSC_NULL,"-gu_CHeps1",&CHeps1,&flag);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHeps1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHeps1=%#.3e\n",CHeps1);
  }  
  PetscOptionsGetScalar(PETSC_NULL,"-gu_CHdt1",&CHdt1,&flag); 
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHdt1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHdt1=%#.3e\n",CHdt1);
  }
  
  PetscOptionsGetInt(PETSC_NULL,"-gu_CHnt1",&CHnt1,&flag); 
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHnt1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHnt1=%d\n",CHnt1);
  }
  
  PetscOptionsGetReal(PETSC_NULL,"-gu_CHlenUnit",&lenUnit,&flag); 
  if(!flag){
    lenUnit=1.0;
      }
  PetscPrintf(PETSC_COMM_SELF,"-gu_CHlenUnit=%#.3e\n",lenUnit);

  PetscOptionsGetReal(PETSC_NULL,"-gu_CHksp_rtol1",&CHksp_rtol1,&flag); 
  if(!flag){
    CHksp_rtol1=1.0e-2;
      }
  PetscPrintf(PETSC_COMM_SELF,"-gu_CHksp_rtol1=%#.3e\n",CHksp_rtol1);
  
  PetscOptionsHasName(PETSC_NULL,"-mCH",&flag);
  if(flag){
    PetscOptionsGetScalar(PETSC_NULL,"-gu_CHpenalty",&CHpenalty,&flag);
    if(!flag){
      fprintf(stderr,"Error:Please specify -gu_CHpenalty\n");
      PetscEnd();
    }else{
      PetscPrintf(PETSC_COMM_SELF,"-gu_CHpenalty=%#.3e\n",CHpenalty);
    }
    PetscOptionsGetScalar(PETSC_NULL,"-gu_CHeps2",&CHeps2,&flag);
    if(!flag){
      CHeps2=CHeps1;
    }
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHeps2=%#.3e\n",CHeps2);

    PetscOptionsGetScalar(PETSC_NULL,"-gu_CHdt2",&CHdt2,&flag); 
    if(!flag){
      CHdt2=CHdt1;
    }
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHdt2=%#.3e\n",CHdt2);

    PetscOptionsGetInt(PETSC_NULL,"-gu_CHnt2",&CHnt2,&flag); 
    if(!flag){
      CHnt2=CHnt1;
    }
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHnt2=%d\n",CHnt2);

    PetscOptionsGetReal(PETSC_NULL,"-gu_CHksp_rtol2",&CHksp_rtol2,&flag); 
    if(!flag){
      CHksp_rtol2=1.0e-2;
    }
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHksp_rtol2=%#.3e\n",CHksp_rtol2); 
  }

}
***********************/
