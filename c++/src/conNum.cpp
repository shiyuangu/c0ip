/*** superceded by conNumSLEPC***/
#include<cstdio>
#include<cstdlib>
#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"petsc.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
#include"gv.h"
#include"gf.h"
#include"gfMG.h"
#include"SolverErr.h"
#include"comparison.h"
#include"others.h"
#include"auxiliary.h"
#define PetscTruth PetscBool
PetscMatArray *pAs,*pSs,*pIs,*pIts,*pMs;
PetscVecArray *pvBs,*pvBinvs,*pPhi;
Mesh2Array mesh2es;
double *lambda1, *lambda2;   //the damping factors;
int maxlevel=0;
double c; 

//MGData *pMGData=NULL;
//DiagProjSolver *pDiagProjSolver;
KSPSolverGu *pCSolverBH, *pCSolverNeu;
//MGSolver *pMGBH=NULL,*pMGNeu=NULL;
std::vector<MGData*> pMGDataVec;
std::vector<MGSolver*> pMGBHs;
std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory 

//parameters to control the MG 
char MGBHMode='W',PrecMode='V';
PetscInt mMGNeu=5;   //presmoothing/postsmoothing step for Neu MG problem 
double dampingScale=0.55; //the damping factors are the estimated spectral radii times damping scales

//parameters to control the testing level and smoothing steps. 
PetscInt slevel,elevel,sizeks;       
PetscBool mlogscale;
PetscReal mstartlog,mendlog;
PetscInt msize;
PetscInt mstart=6, mend=36,mskip=2,sizems; 
ConNumkm *pConNumkm;




// void genMG(){
//   MGData *pMGDataTmp;
//   int k;
//   maxlevel=pAs->size;
//   for(k=0;k<maxlevel;k++){
//     if(k==0){
//       pMGDataTmp=new MGData(0,pAs->pVal,pSs->pVal,NULL,NULL,
// 			    pvBs->pVal,pvBinvs->pVal,pPhi->pVal,
// 			    NULL,mesh2es.pVal);
//       pCSolverNeu=new KSPSolverGu(pMGDataTmp->pS,pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,
// 				  pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx);
//       pCSolverBH=new KSPSolverGu(pMGDataTmp->pA,pMGDataTmp->pPhi,pMGDataTmp->pvB,pMGDataTmp->pMesh2->nGI,
// 				  pMGDataTmp->pMesh2->nDOF,pMGDataTmp->pMesh2->idx);
	      
//     }
//     else{
//       pMGDataTmp=new MGData(k,pAs->pVal+k,pSs->pVal+k,pIs->pVal+k-1,pIts->pVal+k-1,
// 			    pvBs->pVal+k,pvBinvs->pVal+k,pPhi->pVal+k,
// 			    pMGData,mesh2es.pVal+k);
//     }
//     pMGData=pMGDataTmp;
//     pMGDataVec.push_back(pMGData);
//     pDiagProjSolver=new DiagProjSolver(pvBinvs->pVal+k,pPhi->pVal+k,pvBs->pVal+k);
//     pDiagProjSolvers.push_back(pDiagProjSolver);
//     if(PrecMode!='N'){
//        pMGNeu=new MGSolver(k,pMGData,pMGData->pS,pCSolverNeu,pDiagProjSolver,mMGNeu,lambda1[k],PrecMode,pMGNeu);
//        pMGNeus.push_back(pMGNeu);
//        pMGBH=new MGSolver(k,pMGData,pMGData->pA,pCSolverBH,pMGNeu,0,lambda2[k],MGBHMode,pMGBH);
//     }else{
//        pMGBH=new MGSolver(k,pMGData,pMGData->pA,pCSolverBH,pDiagProjSolver,0,lambda2[k],MGBHMode,pMGBH);
//     }
//     pMGBHs.push_back(pMGBH);
//   }
  
// }

// static int read(const char* path){
//   mxGuCellArrayT<mxGuSparse> mxAs,mxSs,mxIs,mxvBs,mxvBinvs;
//   mxGuCellArrayT<mxGuDouble> mxPhi;
//   mxGuCellArrayT<mxMesh2> mxMesh2es;
//   mxGuDouble mxLambda1,mxLambda2;

//   FILE* fin;
//   char fname[256]; 
//   int i;

//   sprintf(fname,"%s/%s",path, "As");
//   fin=fopen(fname,"rb");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   mxAs.read(fin);
//   fclose(fin);
//   pAs->getFrom(mxAs);

//   sprintf(fname,"%s/%s",path, "Ss");
//   fin=fopen(fname,"rb");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   mxSs.read(fin);
//   fclose(fin);
//   pSs->getFrom(mxSs);  

//   sprintf(fname,"%s/%s",path, "Is");
//   fin=fopen(fname,"rb");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   mxIs.read(fin);
//   fclose(fin);
//   pIs->getFrom(mxIs);
//   pIts->size=pIs->size;
//   pIts->pVal=new Mat[pIs->size];
//   for(i=0;i<pIs->size;i++){
//   MatTranspose(pIs->pVal[i],MAT_INITIAL_MATRIX,&(pIts->pVal[i]));
//   }

//   sprintf(fname,"%s/%s",path, "vBs");
//   fin=fopen(fname,"rb");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   mxvBs.read(fin);
//   fclose(fin);
//   pvBs->getFrom(mxvBs);
  
//   sprintf(fname,"%s/%s",path, "vBinvs");
//   fin=fopen(fname,"rb");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   mxvBinvs.read(fin);
//   fclose(fin);
//   pvBinvs->getFrom(mxvBinvs);

//   sprintf(fname,"%s/%s",path, "Phi");
//   fin=fopen(fname,"rb");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   mxPhi.read(fin);
//   fclose(fin);
//   pPhi->getFrom(mxPhi);

//   sprintf(fname,"%s/%s",path, "meshes2");
//   fin=fopen(fname,"rb");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   mxMesh2es.read(fin);
//   fclose(fin);
//   mesh2es.getFrom(mxMesh2es);

//   sprintf(fname,"%s/%s",path,"lambda1");
//   fin=fopen(fname,"r");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   //for(i=0;i<pAs->size;i++){fscanf(fin,"%le",lambda1+i);lambda1[i]=lambda1[i]*0.55;}
//   mxLambda1.read(fin); 
//   fclose(fin);
//   if(mxLambda1.nrow*mxLambda1.ncol!=pAs->size){fprintf(stderr,"Error: the size in %s does not match As\n",fname);return 1;}
//   lambda1=new double[pAs->size];for(i=0;i<pAs->size;i++)lambda1[i]=mxLambda1.pr[i]*dampingScale;
  
//   if(PrecMode!='N')sprintf(fname,"%s/%s",path,"lambda2_PS");
//   else sprintf(fname,"%s/%s",path,"lambda2_NP");
//   fin=fopen(fname,"r");
//   if(fin==NULL){
//     fprintf(stderr,"error in opening file %s",fname);
//     return 1;
//   }
//   mxLambda2.read(fin);
//   fclose(fin);
//   if(mxLambda2.nrow*mxLambda2.ncol!=pAs->size){fprintf(stderr,"Error:the size in %s does not match As\n",fname);return 1;}
//   lambda2=new double[pAs->size];for(i=0;i<pAs->size;i++)lambda2[i]=mxLambda2.pr[i]*dampingScale;

//   /////////////////only for debug purpose//////////////////////////////
//   for(i=0;i<pAs->size;i++)printf("lambda1[%d]=%g,lambda2[%d]=%g\n",i,lambda1[i],i,lambda2[i]);
//   ////////////////////////////////////////////////////////////////////
//   return 0;
  
// }

int main(int argc, char**argv){
  int l,k,m;
  PetscInt kCounter,mCounter;
  SolverErr *pMGErrBH;
  Vec workVec[6],x0,outVec;
  PetscScalar tmp1,tmp2,err,tol,powerTol;
  PetscScalar mgErrNorm; //This is the norm of error, NOT the dampingfactor. 
  PetscRandom randomctx; 
  PetscErrorCode ierr;
  PetscTruth flag;
  bool flag2; 
  char outfname[256]; int tmp; //only for output result
  char infname1[256]; std::string infname2; 
  size_t pos;   //for exact only the file name(not the path in argv[1])
  
  
  if(argc>1){
    PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
   //  PetscOptionsGetString(PETSC_NULL,"-gu_MGBHMode",&MGBHMode,1,&flag);
//     if(!flag||!(MGBHMode=='V' || MGBHMode=='W' || MGBHMode=='F')){
// 	fprintf(stderr,"Error:Please specify -gu_MGBHMode\n");
//         PetscEnd();
//     }else{
//       printf("-gu_MGBHMode=%c\n",MGBHMode);
//     }
//     PetscOptionsGetString(PETSC_NULL,"-gu_PrecMode",&PrecMode,1,&flag);
//     if(!flag||!(PrecMode=='V' || PrecMode=='W' || PrecMode=='F'||PrecMode=='N')){
// 	fprintf(stderr,"Error:Please specify -gu_PrecMode\n");
//         PetscEnd();
//     }else printf("-gu_PrecMode=%c\n",PrecMode);
    
//     PetscOptionsGetInt(PETSC_NULL,"-gu_mMGNeu",&mMGNeu,&flag);
//     if((PrecMode!='N')&&(!flag||mMGNeu<0)){
//       fprintf(stderr,"Error:Please specify -gu_mMGNeu\n");
//       PetscEnd();
//     }else printf("-gu_mMGNeu=%d\n",mMGNeu);
//     PetscOptionsHasName(PETSC_NULL,"-gu_mlogscale",&mlogscale);
   //  if(!mlogscale){
//       PetscOptionsGetInt(PETSC_NULL,"-gu_mstart",&mstart,&flag);
//       if(!flag||mstart<=0){
// 	fprintf(stderr,"Error:Please specify -gu_mstart(>=1)\n or -gu_mlogscale");
// 	PetscEnd();
//       }else printf("-gu_mstart=%d\n",mstart);
//       PetscOptionsGetInt(PETSC_NULL,"-gu_mskip",&mskip,&flag);
//       if(!flag){
// 	mskip=1;
// 	fprintf(stderr,"Warning:-gu_mskip is not specified, set to be %d\n",mskip);
//       }else printf("-gu_mskip=%d\n",mskip);
//       PetscOptionsGetInt(PETSC_NULL,"-gu_mend",&mend,&flag);
//       if(!flag||mend<0){
// 	mend=mstart+mskip;
// 	fprintf(stderr,"Warning: -gu_mend is not specified, set to be %d\n",mend); 
//       }else printf("-gu_mend=%d\n",mend);
//     }else{
//       PetscOptionsGetReal(PETSC_NULL,"-gu_mstartlog",&mstartlog,&flag);
//       if(!flag){
// 	fprintf(stderr,"Error: Log scale has been set,-gu_startlog must be specified!\n");
//         PetscEnd();
//         exit(1);
//       }else printf("-gu_mstartlog=10^%g(i.e. %g)\n",mstartlog,pow(10,mstartlog));
//       PetscOptionsGetReal(PETSC_NULL,"-gu_mendlog",&mendlog,&flag);
//       if(!flag){
// 	fprintf(stderr,"Error: Log scale has been set,-gu_mendlog must be specified!\n");
//         PetscEnd();
//         exit(1);
//       }printf("-gu_mstartlog=10^%g(i.e. %g)\n",mendlog,pow(10,mendlog));
//       PetscOptionsGetInt(PETSC_NULL,"-gu_msize",&msize,&flag);
//       if(!flag || msize<0){
// 	fprintf(stderr,"Error:Log scale has been set,-gu_msize must be specified(>0) (msize=%d)!\n",msize);
// 	PetscEnd();
//         exit(1);
//       } 
//     } 
   
    readMGOpts(); 
   //  pAs=new PetscMatArray; pSs=new PetscMatArray;pMs=new PetscMatArray;
//     pIs=new PetscMatArray;pIts=new PetscMatArray;
//     pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;
//     pMGErrBH=new SolverErr;       
//     if(read(argv[1])){    //read in Mats & Pic
//       PetscEnd();
//       exit(1);
//     }
//     genMG();

    PetscOptionsGetReal(PETSC_NULL,"-gu_powerTol",&powerTol,&flag);
    if(!flag||powerTol<1e-8){
      fprintf(stderr,"Warning: -gu_powerTol is not set or too small,reset it to 1e-4\n");
      powerTol=1e-4;
    }else printf("-gu_powerTol=%g\n",powerTol);
    
    //read global parametes to define the problem
    pAs=new PetscMatArray; pSs=new PetscMatArray;pMs=new PetscMatArray;
    pIs=new PetscMatArray;pIts=new PetscMatArray;
    pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;
    pMGErrBH=new SolverErr;           
    if(read(argv[1])){
      PetscEnd();
      exit(1);
    }
     genMG();
       
    PetscOptionsGetInt(PETSC_NULL,"-gu_slevel",&slevel,&flag);
    if(!flag)slevel=1;
    printf("-gu_slevel=%d\n",slevel);
    PetscOptionsGetInt(PETSC_NULL,"-gu_elevel",&elevel,&flag);
    if(!flag){
      if(elevel<0||elevel>maxlevel-1){
        fprintf(stderr,"invalid -gu_elevel,reset to %d\n",maxlevel-1);
      }
      elevel=maxlevel-1;
    }
    printf("-gu_elevel=%d\n",elevel); 
    
    //////////////generator the testing levels, smoothing steps/////////////
    if(!mlogscale){
      sizems=(mend-mstart+mskip-1)/mskip;
      sizeks=elevel-slevel+1;
      pConNumkm=new ConNumkm(sizeks,sizems);
      kCounter=0;for(k=slevel;k<=elevel;k++) pConNumkm->ks[kCounter++]=k;
      mCounter=0;for(m=mstart;m<mend;m+=mskip) pConNumkm->ms[mCounter++]=m;
    } else{
      sizems=msize;
      sizeks=elevel-slevel+1;
      pConNumkm=new ConNumkm(sizeks,sizems);
      kCounter=0;for(k=slevel;k<=elevel;k++) pConNumkm->ks[kCounter++]=k;
      if(msize>1){
	mCounter=0;
	for(m=0;m<msize;m++) 
	  pConNumkm->ms[mCounter++]=(pow(10,mstartlog+(mendlog-mstartlog)/(msize-1)*m)+0.5);
      }
      else pConNumkm->ms[0]=pow(10,mstartlog)+0.5;
    }
    ///////////////////////////////////////////////////////////////////
    /////////////compute contration numbers/////////////////////////////
    
    PetscRandomCreate(PETSC_COMM_SELF,&randomctx);
    PetscRandomSetType(randomctx,PETSCRAND48);//?
    PetscRandomSetInterval(randomctx,-1.0,1.0);
    for(kCounter=0;kCounter < pConNumkm->sizeks;kCounter++){
      k=pConNumkm->ks[kCounter];
      ///// set or reset the Error Operator 
      pMGErrBH->set(pMGBHs[k]);
      //// set or reset workVec
      for(l=0;l<6;l++){
	//ierr=VecCreateSeq(PETSC_COMM_SELF,pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));CHKERRQ(ierr);
	ierr=MyVecCreate(PETSC_COMM_SELF,pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));CHKERRQ(ierr);
      }
      VecDuplicate(workVec[0],&x0);
      
      ////generate random initial guess:
      ////  initial guesses are different level,but the same for all smoothing steps. 
      VecSetRandom(x0,randomctx);   
      VecDuplicate(workVec[0],&outVec);
      if(fabs(c)==0.0){
	VecCopy(x0,workVec[0]);
	proj(*(pMGDataVec[k]->pPhi),*(pMGDataVec[k]->pvB),workVec[0],workVec[1],x0);//need to change for c=0;
      }
      mCounter=0;
      for(mCounter=0;mCounter<pConNumkm->sizems;mCounter++){
	 m=pConNumkm->ms[mCounter];
         //reset underlying MG solvers of Error Operator;
	 for(l=0;l<=k;l++)pMGBHs[l]->m=m;
         tol=powerTol;flag2=false; 
         ierr=VecCopy(x0,outVec);CHKERRQ(ierr);
         while(!flag2){
           if(fabs(c)==0.0){
	   ierr=normMGErr(pMGErrBH,tol,
			  pMGDataVec[k]->pA,pMGDataVec[k]->pPhi,pMGDataVec[k]->pvB,
			  workVec,outVec,mgErrNorm);CHKERRQ(ierr);
	   }else{
	     ierr=normMGErr(pMGErrBH,tol,
			  pMGDataVec[k]->pA,PETSC_NULL,PETSC_NULL,
			  workVec,outVec,mgErrNorm);CHKERRQ(ierr); 
           }
           //validate whether (mgErrNorm,outVec) is the a eigenpair.
	   ierr=pMGErrBH->error(outVec,workVec[0]);CHKERRQ(ierr);
           innerProduct(*(pMGDataVec[k]->pA),workVec[0],workVec[0],workVec[1],&tmp1);
	   innerProduct(*(pMGDataVec[k]->pA),outVec,outVec,workVec[1],&tmp2);
	   err=abs(mgErrNorm)-sqrt(tmp1/tmp2);
	   if(err>0.1){
             tol=tol*0.3;
	     printf("normMGErr is rel=%g,not accurate enough, reduce tol to.%g\n",err,tol);
	   }else{
	      flag2=true;
	      (*pConNumkm)(kCounter,mCounter)=mgErrNorm;
	      printf("k=%d,m=%d,contraction=%g\n",k,m,mgErrNorm);
	   }
	 }  
      }
      printf("\n");
      for(l=0;l<6;l++){
	VecDestroy(workVec[l]);
      }
      VecDestroy(x0);VecDestroy(outVec);
    }
    PetscRandomDestroy(randomctx);
    //////////////////////////////////////////////////////////////////
    //////////write contration table and free memory///////////
    tmp=mkdir(argv[2],0777);
    if(tmp!=0 && errno!=EEXIST){
      fprintf(stderr,"ERROR %d:unable to mkdir %s; %s\n",errno,argv[2],strerror(errno));
    }
    infname2.assign(argv[1]);
    pos=infname2.find_last_of('/');
    if(pos==std::string::npos) pos=0;
    infname2=infname2.substr(pos);
    strcpy(infname1,infname2.c_str());
    sprintf(outfname,"%s/%s_%c%c%d",argv[2],infname1,MGBHMode,PrecMode,mMGNeu);
    pConNumkm->out(outfname);
    sprintf(outfname,"%s/%s_%c%c%d_texTable.tex",argv[2],infname1,MGBHMode,PrecMode,mMGNeu);
    pConNumkm->toTexTable(outfname);
    delete pConNumkm;
     ///////////////////////////////////////////////////////////////////
   //  for(l=0;l<maxlevel;l++){
//       delete pMGDataVec[l];
//       delete pDiagProjSolvers[l];
//       if(PrecMode!='N')delete pMGNeus[l];
//       delete pMGBHs[l];
//     }
//     delete pCSolverNeu;
//     delete pCSolverBH;
    releaseMG();
    delete []lambda1;  delete []lambda2;
  }else{
    printf("Usage: conNum <matfile_D> <output directory> <-options>\n");
    printf("-gu_MGBHMode -gu_PrecMode -gu_mMGNeu,\n -gu_mlogscale -gu_mstart(-gu_mstartlog) -gu_mend(-gu_mendlog) -gu_mskip(-gu_msize) \n -gu_powerTol,\n -gu_slevel -gu_elevel\n");
  }
  delete pAs; delete pSs;delete pIs;delete pIts;delete pMs;
  delete pvBs;delete pvBinvs; delete pPhi;
  delete pMGErrBH;  
  ierr=PetscFinalize();CHKERRQ(ierr);
  return 0;
}
