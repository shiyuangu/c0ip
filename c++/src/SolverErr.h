/**
 * @file   SolverErr.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:00:03 2013
 * 
 * @brief  Compute the error of a Solver. It takes a vector x 
 *         and compute y=A*x, and use y as an input to the Solver.
 *         Also provides power methods to compute the spectral radius
 *         of the error operator S^{-1}*A-Id.
 */
#pragma once
#include"MG.h"
#include "petsc.h"
#include"Solver.h"
#include<assert.h>
#include<cstdlib>
#include<cstdio>
#include"predefGu.h"
class SolverErr{
public:
  SolverErr(){initialized=false;size=0;}
  ~SolverErr();
  PetscErrorCode set(Solver* spSolver);
  PetscErrorCode error(Vec inVec,Vec outVec);
  //Vec workVec[3]; //workVec is for other functions who needs tmp vectors;
private:
  bool initialized; //indicate weather pMGData and pMGSolver is set;
  Solver* pSolver;
  Vec rhs,outSolver;  //outSolver is for temp use to compute the err. 
  PetscInt size;
}; 
PetscErrorCode normMGErr(SolverErr* pSolverErr, PetscScalar tol,
			 const Mat* pK, const Vec* pPhi, const Vec *pvB,
			 Vec workVec[],Vec outVec,PetscScalar& lambda);//the input value of outVec is used as an initial guess
//represent the Error Operator as a Petsc Shell Matrix
//it also extent the domain of Error Operator  to the whole space 
class ErrExt{
public:
  SolverErr* pErrOp;  
  const Vec *pPhi, *pvB; //for zero extension for ErrOp to the whole space
  ErrExt(SolverErr* spErrOp, const Vec *spPhi,const Vec *spvB){
    pErrOp=spErrOp;pPhi=spPhi; pvB=spvB;
    for(int i=0;i<2;i++)VecDuplicate(*pvB,&(workVec[i]));
  }
  ~ErrExt(){
    PetscErrorCode ierr;
    for(int i=0;i<2;i++){
      ierr=VecDestroy(&workVec[i]);
      if(ierr>0) 
	fprintf(stderr,"Error: in VecDestroy on destructor of ErrExt\n");
    }
  }
  PetscErrorCode error(Vec inVec,Vec outVec);
private:
  Vec workVec[2];
};
