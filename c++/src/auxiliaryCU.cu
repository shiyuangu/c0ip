/**
 * @file   auxiliaryCU.cu
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:05:27 2013
 * 
 * @brief  Provides functions for debugging and profiling. 
 */
#include"petsc.h"
#if CUDA
 #include "petscconf.h"
   PETSC_CUDA_EXTERN_C_BEGIN
  #include "../src/mat/impls/aij/seq/aij.h"          /*I "petscmat.h" I*/
   #include "petscbt.h"
   #include "../src/vec/vec/impls/dvecimpl.h"
   #include "private/vecimpl.h"
   PETSC_CUDA_EXTERN_C_END
   #undef VecType
   #include "../src/mat/impls/aij/seq/seqcusp/cuspmatimpl.h"
   #undef VecType
   #include "cusp/csr_matrix.h"
   #include <cusp/krylov/cg.h>
   #include<cusp/print.h>
#endif
PetscErrorCode printCuspArray1D(void* a){
   #if CUDA
   CUSPARRAY *p=(CUSPARRAY*)a;
   try{
     cusp::print(*p);
   }catch(...){
     SETERRQ(PETSC_COMM_SELF,1,"cusp print error\n"); 
   }
   #endif
   return 0;
}

