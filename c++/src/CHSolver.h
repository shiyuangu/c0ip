/**
 * @file   CHSolver.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:32:53 2013
 * 
 * @brief  Cahn-Hilliard equation solver 
 * 
 * 
 */
#pragma once
#include"petsc.h"
#include"BHMG.h"
#include"Solver.h"
#include"Array2D.h"
#include"predefGu.h"
class CHSolver{
  public:
  CHSolver(const char* path,gu::Array2D<bool>& pic_u0);
  CHSolver(const char* path);//with random initial state;
  CHSolver(const char* path,Vec initial_state);
  CHSolver(){}
  virtual ~CHSolver();
  PetscErrorCode reGenBH();
  virtual PetscErrorCode timestepping(Vec &inVec, Vec &outVec);
  PetscErrorCode computeMass(Vec& x,PetscScalar* pmass); //return the mass associated with u, i.e. sum(MassMatrix\times x);
  int k;       //operate on the k-level of pBHMG
  Vec u0;     //init value;
  double lenUnit; //used to convert image to vector
  double dt;  //time step;
  double eps; //the parameter in CH model. 
  /***return the underlying mesh***/
  const Mesh2* getMesh(){return pBHMG->pMGDataVec[k]->pMesh2;} 
  protected:
  int gen_u0(); //just create u0 with values unset. 
  virtual int set_u0(gu::Array2D<bool> pic); // get the initial u0;
  virtual int set_u0_random(); //random u0
  /***set the value of v from a binary image file;
      v should have been created***/
  int setVecPic(const char * fname, Vec& v); 
  virtual PetscErrorCode readCHOpts();  //read in the options related to CH
  PetscErrorCode readCHOpts2();
  /***the constants in the biharmonic model(see BHMG)***/
  double bhc1,bhc2,bhc3;
  const char* path; //regen needs it;
  virtual void setBHCoeff(); //set bhc1, bhc2, hbc3
  void genBH(const char* path);
  BHMG* pBHMG; //All multigrid matrices.
  Solver* pBHSolver; //the solver used in each time step.
  
  static const double C1=5.0; //used for convex splitting; depends on dW
  // static const double C1=0.0; //used for convex splitting; depends on dW
  // double eps; //the parameter in CH model. 
  // double dt;  //time step;
  //double nt;  //# of steps
  
  PetscScalar dW(PetscScalar x){
    return x*x*x-1.5*x*x+0.5*x;  //(W(u)=1/4*u^2(u-1)^2;
  } //W'(u) in the CH model;
  PetscErrorCode release();//release the memory
  Vec workVec[6];  //the tmporary work vectors. 
};
/******************************************
***commmented on 01/28/2012***
  #pragma once
#include<vector>
#include"petsc.h"
#include"MG.h"
#include"PCGu.h"
namespace gu{
  class CHSolver{
  public:
    CHSolver(const std::vector<MGData*> &pMGDataVec,const std::vector<MGSolver*>& pMGBHs,
	     int sk, Vec sf,double eps,double sdt,PetscReal rtol);
    virtual ~CHSolver();
    //PetscErrorCode timestepping(Vec &inVec,Vec &outVec);
    virtual PetscErrorCode timestepping2(Vec &inVec,Vec &outVec);
    void set_eps(double neweps);
    void set_dt(double newdt);
    void set_ksp_rtol(PetscReal rtol);
    PetscErrorCode regenerate_ksp2();
    virtual PetscErrorCode constructKSP();
  protected:
    //std::vec storing Mats of all level; normally pointing to the global variable pMGDataVec; 
    const std::vector<MGData*>& pMGDatas;
    const int k; //k is the level of the mesh in pMGDataVec  
    static const double C1=5.0;
    double eps,dt; //parameters in the CH inpainting model;
    Vec inVec_R, outVec_R; //inVec(dofs,dofs);
    const Mat *pA, *pS, *pM; 
    Mat A_R,S_R,M_R,K_R; //submatrix of A, i.e., A(dofs, dofs);
    Mat Diag; //only for constructing K_R; 
    IS dofs; //index set of the dofs
    int ndof; //the index of non-dof
    VecScatter scatter; 
    Vec tmpVec[3], tmpVecR[8];
    Vec ones;// all ones vector
    Vec vOrth; // vOrth is a vector, orth. to the solution space
    //KSP ksp;
    //PC pc; 
    KSP ksp2; //for timestepping2; 
    PC pc2;
    PCGu *pPCGu; 
    PetscReal CHksp_rtol; 
    MatNullSpace nsp;   
    //PetscErrorCode solve(Vec &inVec, Vec &outVec);
    PetscScalar dW(PetscScalar x); //W'(u) in the CH model
  };
  class mCHSolver:public CHSolver{
  public:
     mCHSolver(const std::vector<MGData*> &pMGDataVec,const std::vector<MGSolver*>& pMGBHs,
	     int sk, Vec sf, Vec sD,double eps,double lambda,double sdt,PetscReal rtol);
     virtual PetscErrorCode timestepping2(Vec &inVec,Vec &outVec);
     virtual PetscErrorCode constructKSP();
     virtual ~mCHSolver();
  private:
    Vec f,f_R;  //the f in the CH inpainting model; 
    Vec D,D_R;  //indicator for inpainting region; 
    double penalty;

  };
  PetscErrorCode shellPCApply(PC pc,Vec rhs, Vec outVec);
}
*******************************/
