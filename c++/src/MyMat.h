/**
 * @file   MyMat.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:45:04 2013
 * 
 * @brief  Use PETSc or CUSP matrix routines depends on whether CUDA is enabled
 * 
 * 
 */
#pragma once
#include"petsc.h"
#include"predefGu.h"
PetscErrorCode MyMatMult(MyMat K, Vec xx, Vec yy);
