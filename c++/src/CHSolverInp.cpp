#include<iostream>
#include"CHSolverInp.h"
#include"gfCH.h"
CHSolverInp::CHSolverInp(const char* _path){
  int i; 
  PetscErrorCode ierr;
  PetscBool flag;
  char fname[256];
  path=_path;
  
  ierr=readCHOpts(); CHKERRV(ierr);
  setBHCoeff();
  genBH(path);

  /***generate the vectors***/
  gen_u0();
  ierr=VecDuplicate(u0,&vecPic);CHKERRV(ierr);
  ierr=VecDuplicate(u0,&vecD);CHKERRV(ierr);
  ierr=VecDuplicate(u0,&lambdaX);CHKERRV(ierr);
  ierr=VecDuplicate(u0,&f);CHKERRV(ierr);
  for(i=0;i<6;i++){
    ierr=VecDuplicate(u0,&(workVec[i]));CHKERRV(ierr);
  }

  /***set the vectors ***/
 
   ierr=PetscOptionsGetString(PETSC_NULL,"-CH_pic",fname,255,&flag);CHKERRV(ierr);
    if(setVecPic(fname,vecPic)){
	fprintf(stderr,"failed to read intitial state from image file %s\n",fname);
      }
    ierr=PetscOptionsGetString(PETSC_NULL,"-CH_D",fname,255,&flag);CHKERRV(ierr); //notice that the image needs to have been negated. 
    if(setVecPic(fname,vecD)){
	fprintf(stderr,"failed to read intitial state from image file %s\n",fname);
      }
   //ierr=set_vecPic();CHKERRV(ierr);
   //ierr=set_vecD();CHKERRV(ierr);
  ierr=VecPointwiseMult(u0,vecPic,vecD);CHKERRV(ierr);
  ierr=VecCopy(u0,f);CHKERRV(ierr);
  ierr=VecCopy(vecD,lambdaX);CHKERRV(ierr);
  ierr=VecScale(lambdaX,lambda);CHKERRV(ierr);
  
}
PetscErrorCode CHSolverInp::readCHOpts(){
  PetscBool flag;
  PetscErrorCode ierr;
  PetscOptionsGetReal(PETSC_NULL,"-gu_CHlenUnit",&lenUnit,&flag); 
  if(!flag){
	fprintf(stderr,"Error:-gu_CHlenUnit must be set for display the mesh and read in images\n");
	PetscEnd(); 
  }else 
       PetscPrintf(PETSC_COMM_SELF,"-gu_CHlenUnit=%#.3e\n",lenUnit);
  
  ierr=PetscOptionsGetScalar(PETSC_NULL,"-gu_CHeps1",&eps,&flag);CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHeps1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHeps1=%#.3e\n",eps);
  }  
  ierr=PetscOptionsGetScalar(PETSC_NULL,"-gu_CHdt1",&dt,&flag); CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHdt1\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHdt1=%#.3e\n",dt);
  }
  
   ierr=PetscOptionsGetScalar(PETSC_NULL,"-gu_CHInp_lambda",&lambda,&flag);CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHInp_lambda\n");
    PetscEnd();
      }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHInp_lambda=%#.3e\n",lambda);
  }  
  return 0; 
}

void CHSolverInp::setBHCoeff(){
  bhc1=1.0/(dt*eps)+C2*lambda/eps;bhc2=C1/(eps*eps);
  /* bhc3 is used in the 2nd problem as a preconditioner*/
  bhc3=bhc1;
  printf("bhc1=%#.3e,bhc2=%#.3e,bhc3=%#.3e\n",bhc1,bhc2,bhc3);
}

CHSolverInp::~CHSolverInp(){
  PetscErrorCode ierr; 
  ierr=VecDestroy(&vecPic);CHKERRV(ierr);
  ierr=VecDestroy(&vecD);CHKERRV(ierr);
  ierr=VecDestroy(&lambdaX);CHKERRV(ierr);
  ierr=VecDestroy(&f);CHKERRV(ierr);
  //u0 and workVec[...] are released in base class. 
}
int CHSolverInp::set_vecPic(){
  int i,nGI;
  PetscScalar *p;
  double x, y;
  PetscErrorCode ierr;
  const Mesh2* pMesh;
  gu::Array2D<bool> pic;
  
  if(readCHPic(path,pic)){
      std::cerr<<"Cannot obtain pictures,Abort!\n";
      PetscEnd();
      return 1; 
  }
 
  pMesh=pBHMG->pMGDataVec[k]->pMesh2;
  nGI=pBHMG->pMGDataVec[k]->pMesh2->nGI;
  ierr=VecGetArray(vecPic, &p); CHKERRQ(ierr);
  
  for(int i=0;i<nGI;i++){
	x=pMesh->xyco[2*i];
	y=pMesh->xyco[2*i+1];
	if(pic.sampleNN(x/lenUnit,y/lenUnit)) p[i]=1.0;
	else p[i]=0.0;
   }
  ierr=VecRestoreArray(vecPic,&p);CHKERRQ(ierr);
  return 0; 
}
int CHSolverInp::set_vecD(){
  int i,nGI;
  PetscScalar *p;
  double x, y;
  PetscErrorCode ierr;
  const Mesh2* pMesh;
  gu::Array2D<bool> pic;
  
  if(readCHInpR(path,pic)){
      std::cerr<<"Cannot obtain pictures,Abort!\n";
      PetscEnd();
      return 1; 
  }
  
  pMesh=pBHMG->pMGDataVec[k]->pMesh2;
  nGI=pBHMG->pMGDataVec[k]->pMesh2->nGI;
  ierr=VecGetArray(vecD, &p); CHKERRQ(ierr);
  
  for(int i=0;i<nGI;i++){
	x=pMesh->xyco[2*i];
	y=pMesh->xyco[2*i+1];
	if(pic.sampleNN(x/lenUnit,y/lenUnit)) p[i]=1.0;
	else p[i]=0.0;
   }
  ierr=VecRestoreArray(vecD,&p);CHKERRQ(ierr);
  return 0; 
}

PetscErrorCode CHSolverInp::timestepping(Vec &inVec, Vec &outVec){
   PetscErrorCode ierr;
  int i; 
  /***workVec[0]=C1/(eps^2)*(Stiff+bhc3*M)*inVec***/
  ierr=MatMult(pBHMG->pSs->pVal[k],inVec,workVec[0]);CHKERRQ(ierr);
  ierr=VecScale(workVec[0],C1/(eps*eps));CHKERRQ(ierr);

  /***workVec[1]=(1/(dt*eps)+C2*lambda/eps-C1*bhc3/(eps^2))*M*inVec***/
  ierr=MatMult(pBHMG->pMs->pVal[k],inVec,workVec[1]);CHKERRQ(ierr);
  ierr=VecScale(workVec[1],C2*lambda/eps+1/(dt*eps)-C1*bhc3/(eps*eps));CHKERRQ(ierr);
  
  /***workVec[2]=W'(inVec)***/
  PetscScalar *pV1, *pV2;
  ierr=VecGetArray(inVec,&pV1);CHKERRQ(ierr);
  ierr=VecGetArray(workVec[2],&pV2);CHKERRQ(ierr);
  for(int i=0; i<pBHMG->pMGDataVec[k]->pMesh2->nGI;i++) pV2[i]=dW(pV1[i]); //!!check GI
  ierr=VecRestoreArray(inVec,&pV1);CHKERRQ(ierr);
  ierr=VecRestoreArray(workVec[2],&pV2);CHKERRQ(ierr);

  /***workVec[3]=-1/(eps^2)*(Stiff+bhc3*M)*workVec[2]***/
  ierr=MatMult(pBHMG->pSs->pVal[k],workVec[2],workVec[3]);CHKERRQ(ierr);
  ierr=VecScale(workVec[3],-1.0/(eps*eps));CHKERRQ(ierr);

  /***workVec[4]=1/(eps^2)*bhc3*M*workVec[2]***/
  ierr=MatMult(pBHMG->pMs->pVal[k],workVec[2],workVec[4]);CHKERRQ(ierr);
  ierr=VecScale(workVec[4],bhc3/(eps*eps));CHKERRQ(ierr);

  /***workVec[3]=workVec[3]+workVec[4]***/
  /***hence workVec[3]=-1/eps^2*Stiff*W'(inVec)***/
  ierr=VecAXPY(workVec[3],1.0,workVec[4]);CHKERRQ(ierr);
  
  /***workVec[0]=workVec[0]+workVec[1]+workVec[3]***/
  ierr=VecAXPBYPCZ(workVec[0],1.0,1.0,1.0,workVec[1],workVec[3]);CHKERRQ(ierr);
  
  /***the L2 term specific to CH Inpaint****/
   /***workVec[2]=f-inVec ***/
  ierr=VecWAXPY(workVec[2],-1.0,inVec,f);CHKERRQ(ierr);
  
  /***workVec[4]=1/eps(lambdaX.*workVec[2])***/
  ierr=VecPointwiseMult(workVec[4],lambdaX,workVec[2]);CHKERRQ(ierr);
  ierr=VecScale(workVec[4],1.0/eps);CHKERRQ(ierr);

  /***workVec[2]=M*workVec[4] ***/
  ierr=MatMult(pBHMG->pMs->pVal[k],workVec[4],workVec[2]);CHKERRQ(ierr);
  
  /***workVec[0]=workVec[0]+workVec[2]***/
  ierr=VecAXPY(workVec[0],1.0,workVec[2]);CHKERRQ(ierr);
  
  ierr=pBHSolver->solve(workVec[0],outVec);CHKERRQ(ierr);
  return 0; 
}
