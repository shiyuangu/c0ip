//Test opencv is running correctly. 
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include<cv.h>
//#include<highgui.h>
#include <iostream>

using namespace cv;
using namespace std; 

int main( int argc, char** argv )
{ 
	if( argc != 2) 
	{
	 cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
	 return -1;
	}
	
	Mat image;
	image = imread(argv[1], CV_LOAD_IMAGE_COLOR);	// Read the file

	if(! image.data )                              // Check for invalid input
	{
		cout <<  "Could not open or find the image" << std::endl ;
		return -1;
	}

	namedWindow( "Display window", CV_WINDOW_AUTOSIZE );// Create a window for display.
	imshow( "Display window", image );                   // Show our image inside it.

	waitKey(0); // Wait for a keystroke in the window

	VideoWriter outputVideo;
        outputVideo.open("testvideo.avi", CV_FOURCC('M','P','4','2'),30, image.size(), true);
        if (!outputVideo.isOpened()){
        cout  << "Could not open the output video for write: "<< endl;
        return -1;
	}
	cout<<"image width="<<image.size().width<<" height="<<image.size().height<<endl; 
	for(int i=0;i<100;i++){
	  outputVideo<<image;
	}
	cout << "Finished writing" << endl;
	return 0;
}
