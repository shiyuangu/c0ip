/**
 * @file   MyKsp.cu
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:26:33 2013
 * 
 * @brief  wrapper class for CUSP GPU linear solver
 * 
 * 
 */
#include<iostream>
#include"petsc.h"
#include"gvLog.h"
#if CUDA==1
   #include "petscconf.h"
   PETSC_CUDA_EXTERN_C_BEGIN
  #include "../src/mat/impls/aij/seq/aij.h"          /*I "petscmat.h" I*/
   #include "petscbt.h"
   #include "../src/vec/vec/impls/dvecimpl.h"
   #include "private/vecimpl.h"
   PETSC_CUDA_EXTERN_C_END
   #undef VecType
   #include "../src/mat/impls/aij/seq/seqcusp/cuspmatimpl.h"
   #undef VecType
   #include "cusp/csr_matrix.h"
   #include <cusp/krylov/cg.h>
#endif
PetscErrorCode MyKSPSolveCUSP(Mat K,Vec rhs, Vec r){
  PetscErrorCode ierr;
  #if CUDA==1
      CUSPARRAY *rhsarray, *rarray;
      Mat_SeqAIJ *a=(Mat_SeqAIJ*)K;
      Mat_SeqAIJCUSP *cuspstruct;
      cuspstruct=(Mat_SeqAIJCUSP*)K->spptr;
      ierr = MatCUSPCopyToGPU(K);CHKERRQ(ierr);
      ierr = VecCUSPGetArrayRead(rhs,&rhsarray);CHKERRQ(ierr);
      ierr = VecCUSPGetArrayWrite(r,&rarray);CHKERRQ(ierr);
      ierr = VecSet_SeqCUSP(r,0.0);CHKERRQ(ierr);
      try{
	if(a->compressedrow.use){
	  std::cerr<<"Petsc_Matrix use compresssed row format but the cusp::cg is not implemented!"<<std::endl;
	    throw "Petsc_Matrix use compresssed row format but the cusp::cg is not implemented!\n";
	    }else{
	  cusp::default_monitor<PetscScalar>monitor(*rhsarray,100000,1e-5, 1e-50);
	  ierr=PetscLogEventBegin(eventMyKspSolver,0,0,0,0);CHKERRQ(ierr);
	  cusp::krylov::cg(*cuspstruct->mat,*rarray,*rhsarray,monitor);
	  //ierr=WaitForGPU();CHKERRCUSP(ierr);
	  ierr=PetscLogEventEnd(eventMyKspSolver,0,0,0,0);CHKERRQ(ierr);
	  
	}
      }catch (char* ex){
	 SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"CUSP error: %s", ex);
      }
      ierr = VecCUSPRestoreArrayRead(rhs,&rhsarray);CHKERRQ(ierr);
      ierr = VecCUSPRestoreArrayWrite(r,&rarray);CHKERRQ(ierr);
      ierr = WaitForGPU();CHKERRCUSP(ierr);
      return 0;
  #else
     SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"CUDA is not enabled: %s");
  #endif
  
}
