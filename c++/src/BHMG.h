/**
 * @file   BHMG.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:28:30 2013
 * 
 * @brief  Multigrid solver for biharmonic problem. It can optionally use
 *         another multigrid solver as a preconditioner inside the 
 *         the smoothing steps. 
 */
#pragma once
#include<vector>
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
#include"predefGu.h"
class BHMG{
public:
  MyMatArray *pAs,*pSs,*pIs,*pIts,*pMs;
  PetscVecArray *pvBs,*pvBinvs,*pPhi;
  Mesh2Array mesh2es;
  double *lambda1, *lambda2;   //the damping factors;
  double c1,c2;   //for \Delta^ u- c2\Delta u+c1 u
  double c3,c4;      //for the preconditioner -c4\Delta u+c3 u
  int maxlevel;
  #if MY_MAT
  gu::DevMatType myMatDevType;
  #endif
  Solver *pCSolverBH, *pCSolverNeu;
  //MGSolver *pMGBH=NULL,*pMGNeu=NULL;
  std::vector<MGData*> pMGDataVec;
  std::vector<MGSolver*> pMGBHs;
  std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory

//parameters to control the MG 
  char MGBHMode,PrecMode;
  PetscInt mMGNeu;   //presmoothing/postsmoothing step for Neu MG problem
  PetscInt mMG;    //presmoothing/postsmoothing step for outer MG. 
  double dampingScale; //the damping factors are the estimated spectral radii times damping scales
  BHMG(const char* path,double _c1=0.0, double _c2=0.0, double _c3=0.0,double _c4=1.0);
  ~BHMG();
  void genMG();
  void releaseMG();
  void readMGOpts();
  int read(const char* path);//path is a directory containing data file extracted by splitConvert from mathlab .mat file
  
};
