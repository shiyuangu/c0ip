/**
 * @file   auxiliary.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:04:29 2013
 * 
 * @brief  Provide fuctions for debugging and profiling. 
 * 
 * 
 */

#include"auxiliary.h"
#include"predefGu.h"
#include <../src/mat/impls/aij/seq/aij.h>
PetscErrorCode output(const Mat& M, const char* filename){
  PetscViewer matfile;
  PetscViewerASCIIOpen(PETSC_COMM_SELF,filename, &matfile);
  PetscViewerSetFormat(matfile,PETSC_VIEWER_ASCII_MATLAB);
  MatView(M,matfile);
  PetscViewerDestroy(&matfile);
  return 0;
}
PetscErrorCode output(const Vec& V, const char* filename){
  PetscViewer matfile;
  PetscViewerASCIIOpen(PETSC_COMM_SELF,filename, &matfile);
  PetscViewerSetFormat(matfile,PETSC_VIEWER_ASCII_MATLAB);
  VecView(V,matfile);
  PetscViewerDestroy(&matfile);
  return 0;
}
PetscErrorCode outputBin(const Vec& V, const char* filename){
  PetscViewer binfile;
  PetscErrorCode ierr; 
  ierr=PetscViewerBinaryOpen(PETSC_COMM_SELF,filename,FILE_MODE_WRITE,&binfile);CHKERRQ(ierr);
  ierr=VecView(V,binfile);CHKERRQ(ierr);
  PetscViewerDestroy(&binfile);CHKERRQ(ierr);
  return 0;
}
PetscErrorCode outputBin(const Mat& A, const char* filename){
  PetscViewer binfile;
  PetscErrorCode ierr; 
  ierr=PetscViewerBinaryOpen(PETSC_COMM_SELF,filename,FILE_MODE_WRITE,&binfile);CHKERRQ(ierr);
  ierr=MatView(A,binfile);CHKERRQ(ierr);
  PetscViewerDestroy(&binfile);CHKERRQ(ierr);
  return 0;
}
PetscErrorCode inputBin(const Vec& V, const char* filename){
  PetscViewer binfile;
  PetscErrorCode ierr; 
  ierr=PetscViewerBinaryOpen(PETSC_COMM_SELF,filename,FILE_MODE_READ,&binfile);CHKERRQ(ierr);
  ierr=VecLoad(V,binfile);CHKERRQ(ierr);
  PetscViewerDestroy(&binfile);CHKERRQ(ierr);
  return 0;
}

PetscErrorCode printToScreen(const IS& is){
  return ISView(is, PETSC_VIEWER_STDOUT_SELF); 
}
PetscErrorCode printToScreen(const Vec& v){
  return VecView(v, PETSC_VIEWER_STDOUT_SELF);
}
PetscInt getInodeCount(const Mat& A){
   Mat_SeqAIJ      *a = (Mat_SeqAIJ*)A->data;
   return a->inode.node_count; 
}
void toTexTable(Table2DGu<int,int,double>& source,const char* fname){
      FILE *pFile;
      int i,j;
      pFile=fopen(fname,"w");
      if(pFile==NULL){
	fprintf(stderr,"Error: Cannot creat %s\n",fname);
	exit(1);
      }
      fprintf(pFile,"\\begin{tabular}{|");
      for(i=0;i<source.sizems+1;i++) fprintf(pFile,"c|");
      fprintf(pFile,"}\n");
      fprintf(pFile,"\\hline\n");
      fprintf(pFile,"\\backslashbox{k}{m}");
      for(i=0;i<source.sizems;i++)fprintf(pFile,"&%d",source.ms[i]);
      fprintf(pFile,"\\\\ \n");
      fprintf(pFile,"\\hline\n");
      for(i=0;i<source.sizeks;i++){
	fprintf(pFile,"$%d$",source.ks[i]);
	for(j=0;j<source.sizems;j++){
	  fprintf(pFile,"& %#0.3g",fabs(source(i,j)));
	}
	fprintf(pFile,"\\\\ \n");
      }
      fprintf(pFile,"\\hline\n");
      fprintf(pFile,"\\end{tabular}\n");
      fclose(pFile);
}
int outputMisc(int argc,char** argv,const char* fname){
  int i;
  FILE *pFile;
  char buffer[256];
  pFile=fopen(fname,"w");
  if(pFile==NULL){
    fprintf(stderr,"Error: Cannot creat %s\n",fname);
    return 1; 
  }
  for(i=0;i<argc;i++){
    fprintf(pFile,"%s ",argv[i]); 
  }
  fprintf(pFile,"\n\n");
  fprintf(pFile,"hg version:\n");
  fclose(pFile);
  sprintf(buffer,"hg log -r. >> %s ",fname);
  return system(buffer); 
}
