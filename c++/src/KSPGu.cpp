/**
 * @file   KSPGu.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:05:56 2013
 * 
 * @brief  Switch between PETSc Mat format or MatCpuGpu format depends
 *         on the complier flag MY_MAT. It also handles the single problem
 *         from C0IP. 
 * 
 */
#include<cassert>
#include"KSPGu.h"
# if 0
///////////////KSPSolverGu:KSP solver with a shift////////////////////
KSPSolverGu::KSPSolverGu(const Mat* spK, const Vec* spPhi, const Vec* spvB,
			 PetscInt nGI,PetscInt nDOF,PetscInt sidx[],const char *seventName)
  :idx_from(sidx),size(nGI),reducedSize(nDOF),pvB(spvB),pPhi(spPhi),Solver(spK){
  PetscErrorCode ierr;
  int i;
  idx_from=new PetscInt[reducedSize];
  for(i=0;i<reducedSize;i++)idx_from[i]=sidx[i];  
  ierr=VecCreateSeq(PETSC_COMM_SELF,size,&workVec);CHKERRV(ierr);
  //ierr=MyVecCreate(PETSC_COMM_SELF,size,&workVec);CHKERRV(ierr);
  //ierr=VecCreateSeq(PETSC_COMM_SELF,reducedSize,&reducedSolution);CHKERRV(ierr);
    ierr=VecCreateSeq(PETSC_COMM_SELF,reducedSize,&reducedSolution);CHKERRV(ierr);
  ierr=VecDuplicate(reducedSolution,&reducedRHS);
  //ierr=ISCreateGeneral(PETSC_COMM_SELF,reducedSize,idx_from,&isfrom);
  ierr=ISCreateGeneral(PETSC_COMM_SELF,reducedSize,idx_from,PETSC_COPY_VALUES,&isfrom);CHKERRV(ierr);
  ierr=MatGetSubMatrix(*(pK),isfrom,isfrom,MAT_INITIAL_MATRIX,&reducedK);CHKERRV(ierr);
  idx_to=new PetscInt[reducedSize];
  for(i=0;i<reducedSize;i++)idx_to[i]=i;
  pValues=new PetscScalar[reducedSize];
  sprintf(eventName,"%s",seventName);
  ierr=PetscLogEventRegister(eventName,0,&EVENT);CHKERRV(ierr);

  if (!myKspSolverOn){  //if -gu_kspSolver
     ierr=KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRV(ierr);
    if(fabs(c1)==0.0){
      ierr=KSPSetOperators(ksp,reducedK,reducedK,SAME_NONZERO_PATTERN);CHKERRV(ierr);
    }
    else 
      {ierr=KSPSetOperators(ksp,*pK,*pK,SAME_NONZERO_PATTERN);CHKERRV(ierr);}
    ierr=KSPSetType(ksp,KSPCG);CHKERRV(ierr);
    ierr=KSPGetPC(ksp,&prec);CHKERRV(ierr);
    ierr=PCSetType(prec,PCLU);CHKERRV(ierr);
    //ierr=KSPSetFromOptions(ksp);CHKERRV(ierr);
    ierr=KSPSetUp(ksp);CHKERRV(ierr);
  }else{
    #if CUDA==0
    printf("CUAD is not enabled!\n");
    ierr=PETSC_ERR_USER;CHKERRV(ierr);
    #endif
  }
  //MatNullSpaceCreate(PETSC_COMM_SELF,PETSC_TRUE,0,PETSC_NULL,&nsp);
  //KSPSetNullSpace(ksp,nsp);
}
KSPSolverGu::~KSPSolverGu(){
  PetscErrorCode ierr;
  if(!myKspSolverOn){
    ierr=KSPDestroy(&ksp);CHKERRV(ierr);
  }
  ierr=MatDestroy(&reducedK);CHKERRV(ierr);
  ierr=VecDestroy(&workVec);CHKERRV(ierr);
  ierr=VecDestroy(&reducedSolution);CHKERRV(ierr);
  ierr=VecDestroy(&reducedRHS);CHKERRV(ierr);
  ierr=ISDestroy(&isfrom);CHKERRV(ierr);
  delete []idx_from; delete []idx_to;delete []pValues;
  //MatNullSpaceDestroy(nsp);
}
PetscErrorCode KSPSolverGu::solve(const Vec&rhs, Vec &r){
  PetscErrorCode ierr;
  PetscScalar alpha1, alpha2,alpha;
  //PetscScaler sum;
  //ierr=KSPSolve(ksp,rhs,r);CHKERRQ(ierr);
  //ierr=VecSum(rhs,&sum);CHKERRQ(ierr);
  //  if(fabs(sum)>1e-5){
  //printf("Warning: in the KSPCSolver.sum(rhs)=%g\n",sum);
  //}
  PetscLogEventBegin(EVENT,0,0,0,0);
 
  if(fabs(c1)==0.0){
    ierr=VecGetValues(rhs,reducedSize,idx_from,pValues);CHKERRQ(ierr);
    ierr=VecSetValues(reducedRHS,reducedSize,idx_to,pValues,INSERT_VALUES);CHKERRQ(ierr);
    if(!myKspSolverOn){
      ierr=VecZeroEntries(r);CHKERRQ(ierr);
      ierr=KSPSolve(ksp,reducedRHS,reducedSolution);CHKERRQ(ierr);
    }else{
      ierr=MyKSPSolveCUSP(reducedK,reducedRHS,reducedSolution);CHKERRQ(ierr);
    }
    ierr=VecGetValues(reducedSolution,reducedSize,idx_to,pValues);CHKERRQ(ierr);
    ierr=VecSetValues(r,reducedSize,idx_from,pValues,INSERT_VALUES);CHKERRQ(ierr);
    ierr=VecPointwiseMult(workVec,r,*(pvB));CHKERRQ(ierr);
    ierr=MyVecDot(workVec,*(pPhi),&alpha1);CHKERRQ(ierr);
    ierr=MyVecDot(*(pPhi),*(pvB),&alpha2);CHKERRQ(ierr);
    alpha=-1.0*alpha1/alpha2;
    ierr=VecShift(r,alpha);CHKERRQ(ierr);
    }else{
      if(!myKspSolverOn){
        ierr=KSPSolve(ksp,rhs,r);CHKERRQ(ierr);
      }else{
	ierr=MyKSPSolveCUSP(*pK,rhs,r);CHKERRQ(ierr);
      }
        
    } 
  PetscLogEventEnd(EVENT,0,0,0,0);
  return 0;
}
#endif
/******************************************************************/
namespace gu{
  #if MY_MAT==0
  KSPCpu::KSPCpu(const Mat* spK, const char* seventName):Solver(spK){
    PetscErrorCode ierr;
    PetscInt nrow, ncol;
    MatInfo info;
    const MatType mattype;
    sprintf(eventName,"%s",seventName);
    ierr=PetscLogEventRegister(eventName,0,&EVENT);CHKERRV(ierr);
    ierr=MatGetLocalSize(*spK,&nrow, &ncol);CHKERRV(ierr);
    ierr=MatGetInfo(*spK,MAT_LOCAL,&info);CHKERRV(ierr);
    
    /************************************************
      the following lines needed to be changed for MPI
      MatDuplicate cannot be used since spK could be
      of cusp type
    *************************************************/
    /*ierr=MatCreateSeqAIJ(MPI_COMM_SELF,nrow,ncol,nz,PETSC_NULL,&K);CHKERRV(ierr);
      ierr=MatCopy(*spK,K,DIFFERENT_NONZERO_PATTERN);CHKERRV(ierr);*/
    ierr=MatDuplicate(*spK,MAT_COPY_VALUES,&K);CHKERRV(ierr);
    /*****
    ierr=MatGetType(*spK,&mattype);CHKERRV(ierr);
    printf("in KSPCpu,for *spK,mattype=%s\n",mattype);
    ierr=MatGetType(K,&mattype);CHKERRV(ierr);
    printf("in KSPCpu, for K(after Dup.),mattype=%s\n",mattype);
    ierr=MatConvert(K,MATSEQAIJ,MAT_REUSE_MATRIX,&K);CHKERRV(ierr);
    ierr=MatGetType(K,&mattype);CHKERRV(ierr);
    printf("in KSPCpu, for K(after Convert),mattype=%s\n",mattype);
    *****/
    
    ierr=VecCreateSeq(PETSC_COMM_SELF,ncol,&workVec[0]);CHKERRV(ierr);
    for(int i=1; i<3;i++){
      ierr=VecDuplicate(workVec[0],&workVec[i]);CHKERRV(ierr);
    }
    ierr=KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRV(ierr);
    ierr=KSPSetOperators(ksp,K,K,SAME_NONZERO_PATTERN);CHKERRV(ierr);
    //ierr=KSPSetOperators(ksp,*spK,*spK,SAME_NONZERO_PATTERN);CHKERRV(ierr);
    ierr=KSPSetType(ksp,KSPPREONLY);CHKERRV(ierr);
    ierr=KSPGetPC(ksp,&prec);CHKERRV(ierr);
    ierr=PCSetType(prec,PCLU);CHKERRV(ierr);
    //ierr=KSPSetFromOptions(ksp);CHKERRV(ierr);
    ierr=KSPSetUp(ksp);CHKERRV(ierr);
    
  }
  #else 
   KSPCpu::KSPCpu(const MatCpuGpu* _spK, const char* seventName):Solver(_spK){
    PetscErrorCode ierr;
    PetscInt nrow, ncol;
    Mat* spK;
    spK=_spK->pHostMat;
    assert(spK!=NULL);
    
    sprintf(eventName,"%s",seventName);
    ierr=PetscLogEventRegister(eventName,0,&EVENT);CHKERRV(ierr);
    ierr=MatGetLocalSize(*spK,&nrow, &ncol);CHKERRV(ierr);  
    ierr=MatDuplicate(*spK,MAT_COPY_VALUES,&K);CHKERRV(ierr);
    
    ierr=VecCreateSeq(PETSC_COMM_SELF,ncol,&workVec[0]);CHKERRV(ierr);
    for(int i=1; i<3;i++){
      ierr=VecDuplicate(workVec[0],&workVec[i]);CHKERRV(ierr);
    }
    ierr=KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRV(ierr);
    ierr=KSPSetOperators(ksp,K,K,SAME_NONZERO_PATTERN);CHKERRV(ierr);
    ierr=KSPSetType(ksp,KSPPREONLY);CHKERRV(ierr);
    ierr=KSPGetPC(ksp,&prec);CHKERRV(ierr);
    ierr=PCSetType(prec,PCLU);CHKERRV(ierr);
    ierr=KSPSetFromOptions(ksp);CHKERRV(ierr);
    ierr=KSPSetUp(ksp);CHKERRV(ierr);
    
  }
  #endif 
  KSPCpu::~KSPCpu(){
    PetscErrorCode ierr;
    ierr=KSPDestroy(&ksp);CHKERRV(ierr);
    for(int i=0;i<3;i++){
      ierr=VecDestroy(&workVec[i]);CHKERRV(ierr);
    }
    ierr=MatDestroy(&K);CHKERRV(ierr);
  }
  PetscErrorCode KSPCpu::solve(const Vec& rhs, Vec &r){
    PetscErrorCode ierr;
    ierr=PetscLogEventBegin(EVENT,0,0,0,0);CHKERRQ(ierr);
    ierr=VecCopy(r, workVec[0]);CHKERRQ(ierr);
    ierr=VecCopy(rhs, workVec[1]);CHKERRQ(ierr);
    ierr=KSPSolve(ksp,workVec[1],workVec[0]);CHKERRQ(ierr);
    ierr=VecCopy(workVec[0],r);CHKERRQ(ierr);
    ierr=PetscLogEventEnd(EVENT,0,0,0,0);CHKERRQ(ierr);
    return 0;
  }
  #if MY_MAT==0
  KSPCpuSingular::KSPCpuSingular(const Mat* spK, const Vec* spPhi, const Vec* spvB,PetscInt nGI,PetscInt nDOF,PetscInt sidx[],const char *seventName)
    :Solver(spK),idx_from(sidx),size(nGI),reducedSize(nDOF){
    PetscErrorCode ierr;
    Mat reducedKtmp;
    const MatType mattype; 
    sprintf(eventName,"%s",seventName);
    ierr=PetscLogEventRegister(eventName,0,&EVENT);CHKERRV(ierr);
    
    idx_from=new PetscInt[reducedSize];
    for(int i=0;i<reducedSize;i++)idx_from[i]=sidx[i];
    ierr=VecCreateSeq(PETSC_COMM_SELF,size,&workVec[0]);CHKERRV(ierr);
    for(int i=1;i<2;i++){
         ierr=VecDuplicate(workVec[0],&workVec[i]);CHKERRV(ierr);
    }
    ierr=VecCreateSeq(PETSC_COMM_SELF,size,&vB);CHKERRV(ierr);
    ierr=VecCopy(*spvB,vB);CHKERRV(ierr);
    ierr=VecCreateSeq(PETSC_COMM_SELF,size,&Phi);CHKERRV(ierr);
    ierr=VecCopy(*spPhi,Phi);CHKERRV(ierr);
    
    ierr=VecCreateSeq(PETSC_COMM_SELF,reducedSize,&reducedSolution);CHKERRV(ierr);
    ierr=VecDuplicate(reducedSolution,&reducedRHS);CHKERRV(ierr);
    
    ierr=ISCreateGeneral(PETSC_COMM_SELF,reducedSize,idx_from,PETSC_COPY_VALUES,&isfrom);CHKERRV(ierr);
    ierr=MatGetType(*pK,&mattype);CHKERRV(ierr);
    printf("in KSPCpuSingular, for *pK,mattype=%s\n",mattype);
    /****temporarily replaced by the line above
      ierr=MatGetSubMatrix(*(pK),isfrom,isfrom,MAT_INITIAL_MATRIX,&reducedKtmp);CHKERRV(ierr);
     ierr=MatConvert(reducedKtmp,MATSEQAIJ,MAT_INITIAL_MATRIX,&reducedK);CHKERRV(ierr);
    ierr=MatGetType(reducedK,&mattype);CHKERRV(ierr);
    printf("in KSPCpuSingular, after conversion,for reducedK, mattype=%s\n",mattype);
    ******/
 ierr=MatGetSubMatrix(*(pK),isfrom,isfrom,MAT_INITIAL_MATRIX,&reducedK);CHKERRV(ierr);
 
     idx_to=new PetscInt[reducedSize];
     for(int i=0;i<reducedSize;i++)idx_to[i]=i;
     pValues=new PetscScalar[reducedSize];
      ierr=KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRV(ierr);
     ierr=KSPSetOperators(ksp,reducedK,reducedK,SAME_NONZERO_PATTERN);CHKERRV(ierr);
     ierr=KSPSetType(ksp,KSPPREONLY);CHKERRV(ierr);
     ierr=KSPGetPC(ksp,&prec);CHKERRV(ierr);
     ierr=PCSetType(prec,PCLU);CHKERRV(ierr);
     ierr=KSPSetFromOptions(ksp);CHKERRV(ierr);
     ierr=KSPSetUp(ksp);CHKERRV(ierr);
     /***temporarily replaced
     ierr=MatDestroy(&reducedKtmp);CHKERRV(ierr);
     ********/
  }
  #else
    KSPCpuSingular::KSPCpuSingular(const MatCpuGpu* _spK, const Vec* spPhi, const Vec* spvB,PetscInt nGI,PetscInt nDOF,PetscInt sidx[],const char *seventName)
    :Solver(_spK),idx_from(sidx),size(nGI),reducedSize(nDOF){
    PetscErrorCode ierr;
    Mat* spK;
    //const MatType mattype;
    spK=_spK->pHostMat;
    assert(spK!=NULL);
    
    sprintf(eventName,"%s",seventName);
    ierr=PetscLogEventRegister(eventName,0,&EVENT);CHKERRV(ierr);
    idx_from=new PetscInt[reducedSize];
    for(int i=0;i<reducedSize;i++)idx_from[i]=sidx[i];
    ierr=VecCreateSeq(PETSC_COMM_SELF,size,&workVec[0]);CHKERRV(ierr);
    for(int i=1;i<2;i++){
         ierr=VecDuplicate(workVec[0],&workVec[i]);CHKERRV(ierr);
    }
    ierr=VecCreateSeq(PETSC_COMM_SELF,size,&vB);CHKERRV(ierr);
    ierr=VecCopy(*spvB,vB);CHKERRV(ierr);
    ierr=VecCreateSeq(PETSC_COMM_SELF,size,&Phi);CHKERRV(ierr);
    ierr=VecCopy(*spPhi,Phi);CHKERRV(ierr);
    
    ierr=VecCreateSeq(PETSC_COMM_SELF,reducedSize,&reducedSolution);CHKERRV(ierr);
    ierr=VecDuplicate(reducedSolution,&reducedRHS);CHKERRV(ierr);
    
    ierr=ISCreateGeneral(PETSC_COMM_SELF,reducedSize,idx_from,PETSC_COPY_VALUES,&isfrom);CHKERRV(ierr);
    // ierr=MatGetType(*pK,&mattype);CHKERRV(ierr);
    //printf("in KSPCpuSingular, for *pK,mattype=%s\n",mattype);
     ierr=MatGetSubMatrix(*(spK),isfrom,isfrom,MAT_INITIAL_MATRIX,&reducedK);CHKERRV(ierr);
   
     idx_to=new PetscInt[reducedSize];
     for(int i=0;i<reducedSize;i++)idx_to[i]=i;
     pValues=new PetscScalar[reducedSize];
      ierr=KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRV(ierr);
     ierr=KSPSetOperators(ksp,reducedK,reducedK,SAME_NONZERO_PATTERN);CHKERRV(ierr);
     ierr=KSPSetType(ksp,KSPPREONLY);CHKERRV(ierr);
     ierr=KSPGetPC(ksp,&prec);CHKERRV(ierr);
     ierr=PCSetType(prec,PCLU);CHKERRV(ierr);
     ierr=KSPSetFromOptions(ksp);CHKERRV(ierr);
     ierr=KSPSetUp(ksp);CHKERRV(ierr);
  }
  #endif
  KSPCpuSingular::~KSPCpuSingular(){
    PetscErrorCode ierr;
    ierr=KSPDestroy(&ksp);CHKERRV(ierr);
    ierr=MatDestroy(&reducedK);CHKERRV(ierr);
    for(int i=0;i<2;i++){ierr=VecDestroy(&workVec[i]);CHKERRV(ierr);}
    ierr=VecDestroy(&vB);CHKERRV(ierr);
    ierr=VecDestroy(&Phi);CHKERRV(ierr);
    ierr=VecDestroy(&reducedSolution);CHKERRV(ierr);
    ierr=VecDestroy(&reducedRHS);CHKERRV(ierr);
    ierr=ISDestroy(&isfrom);CHKERRV(ierr);
     delete []idx_from; delete []idx_to;delete []pValues;
  }
  PetscErrorCode KSPCpuSingular::solve(const Vec&rhs, Vec &r){
    PetscErrorCode ierr;
    PetscScalar alpha1, alpha2,alpha;
    
    ierr=PetscLogEventBegin(EVENT,0,0,0,0);CHKERRQ(ierr);
    ierr=VecGetValues(rhs,reducedSize,idx_from,pValues);CHKERRQ(ierr);
    ierr=VecSetValues(reducedRHS,reducedSize,idx_to,pValues,INSERT_VALUES);CHKERRQ(ierr);
    ierr=VecZeroEntries(workVec[0]);CHKERRQ(ierr);
    ierr=KSPSolve(ksp,reducedRHS,reducedSolution);CHKERRQ(ierr);
    ierr=VecGetValues(reducedSolution,reducedSize,idx_to,pValues);CHKERRQ(ierr);
    ierr=VecSetValues(workVec[0],reducedSize,idx_from,pValues,INSERT_VALUES);CHKERRQ(ierr);
    ierr=VecPointwiseMult(workVec[1],workVec[0],vB);CHKERRQ(ierr);
    ierr=VecDot(workVec[1],Phi,&alpha1);CHKERRQ(ierr);
    ierr=VecDot(Phi,vB,&alpha2);CHKERRQ(ierr);
    alpha=-1.0*alpha1/alpha2;
    ierr=VecShift(workVec[0],alpha);CHKERRQ(ierr);
    ierr=VecCopy(workVec[0],r);CHKERRQ(ierr); 
    ierr=PetscLogEventEnd(EVENT,0,0,0,0);CHKERRQ(ierr);
    return 0;
  }
}
