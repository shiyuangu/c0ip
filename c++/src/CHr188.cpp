#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"slepceps.h"
#include"petsc.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
//#include"SolverErr.h" //the following three is for PC 
//#include"PCGu.h"
//#include"comparison.h"
#include"gf.h"
#include"others.h"
#include"auxiliary.h"
#include "gfCH.h"
#include "Array2D.h"
#include "converter.h"
#include "gfIFace.h" 
#include"GLee.h"
//#include<GL/glut.h>
#include<GL/freeglut.h>
//#include<GL/freeglut_ext.h>
#include<GL/glext.h>
/***for Devil***/
#define ILUT_USE_OPENGL
//#include<IL/ilut.h>
#include"ilutGLGu.h"

#include"CHSolver.h"
#include"predefGu.h"
/***only for preparing for petsc logs;
    global variables for logs are in
    gvLog.h***/
#include"MyInit.h"

#include"videoGu.h"
/***the following is for display***/
/***used in OGL callback function***/
const Mesh2* pDispMesh;
Vec *pDispVec;
int nt; //advance nt steps for one click 
const int imgW=128, imgH=128; //intended image size
double lenUnit; 
/***to keep track of time stepping***/
/***used in OGL callback function***/
int currentTime;
Vec uPre,uCur;
/***for DevIL****/
ILuint DevilImgName;
/***for output video***/
cv::VideoWriter outputVideo;
//cv::Mat cvImage; 
int windowWidth, windowHeight; 
//char* pbuffer;

/***For performance log***/
PetscLogEvent eventMyVecDot,eventMyMatMult,eventMatMultCusp,eventMatMultAddCusp;
PetscLogStage stageDebug,memoryWatch;
PetscLogEvent eventMyKspSolver;

CHSolver *pCHSolver;
int main(int argc, char** argv){
  gu::Array2D<bool> pic;
  PetscErrorCode ierr;
  PetscBool flag;
  char fname[256];
  PetscViewer petscviewer;
  Vec initial_state; //only readin when -CH_initial_state;
  if(argc<=1){
    promptOps();
    /****add prompts for options relates to CH***/
    return 0;
  }
  /***process options relate to global scope***/
  // PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  ierr=SlepcInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  ierr=PetscOptionsGetInt(PETSC_NULL,"-gu_CHnt1",&nt,&flag); CHKERRQ(ierr);
  if(!flag){
    fprintf(stderr,"Error:Please specify -gu_CHnt1\n");
    PetscEnd();
  }else{
    PetscPrintf(PETSC_COMM_SELF,"-gu_CHnt1=%d\n",nt);
  }
   #if MY_MAT==1
      ierr=PetscOptionsGetString(PETSC_NULL,"-vec_type",buffer,256,&flag);CHKERRQ(ierr);
      if(!flag || strcmp(buffer,VECSEQCUSP)){
	SETERRQ(PETSC_COMM_SELF,1,"MY_MAT==1,please set -vec_type seqcusp");
        PetscEnd();
      }
     
    #endif

   /*** prepare for petsc log***/
      ierr=MyInitialize(); CHKERRQ(ierr); 
      
  /***normal business***/
  ierr=PetscOptionsHasName(PETSC_NULL,"-CH_bg",&flag);CHKERRQ(ierr);
  if(flag){
    if(readCHPic(argv[1],pic)){
      std::cerr<<"Cannot obtain pictures,Abort!\n";
      PetscEnd();
      return 1; 
    }
    pCHSolver=new CHSolver(argv[1],pic);
  }else{
    ierr=PetscOptionsGetString(PETSC_NULL,"-CH_initial_state",fname,255,&flag);CHKERRQ(ierr);
     if(flag){
       ierr=PetscViewerBinaryOpen(PETSC_COMM_SELF,fname,FILE_MODE_READ,&petscviewer);CHKERRQ(ierr);
       ierr=VecCreate(PETSC_COMM_SELF,&initial_state);
       ierr=VecLoad(initial_state,petscviewer);CHKERRQ(ierr);
       pCHSolver=new CHSolver(argv[1],initial_state);
       ierr=VecDestroy(&initial_state);CHKERRQ(ierr);
       ierr=PetscViewerDestroy(&petscviewer);CHKERRQ(ierr);
       
     }else{
       std::cout<<"random initial data..\n"<<std::endl;
       pCHSolver=new CHSolver(argv[1],(Vec)(0));
     }
  }
  if(pCHSolver->u0){
      ierr=VecDuplicate(pCHSolver->u0,&uPre);CHKERRQ(ierr);
      ierr=VecDuplicate(pCHSolver->u0,&uCur);CHKERRQ(ierr);
      ierr=VecCopy(pCHSolver->u0,uCur);CHKERRQ(ierr);
      pDispMesh=pCHSolver->getMesh();
      pDispVec=&uCur;
      setupOGL(argc,argv, imgW*5/4*4,imgH*5/4*4);
      //setupOGL(argc,argv, imgW*5/4,imgH*5/4);
      /****initialize DevIL****/
      std::cout<<"DevIL is in action...\n";
      ilInit();
      iluInit();
      ilutRenderer(ILUT_OPENGL);
      std::cout<<"Make sure there is only one version of DevIL\n";
      std::cout<<"ilGetInteger(IL_VERSION_NUM):"<<ilGetInteger(IL_VERSION_NUM)
	       <<"  IL_VERSION:"<<IL_VERSION<<std::endl;
      std::cout<<"iluGetInteger(ILU_VERSION_NUM):"<<iluGetInteger(ILU_VERSION_NUM)
	       <<"  ILU_VERSION:"<<ILU_VERSION<<std::endl;
      std::cout<<"ilutGetInteger(ILUT_VERSION_NUM):"<<ilutGetInteger(ILUT_VERSION_NUM)
	       <<"  ILUT_VERSION:"<<ILUT_VERSION<<std::endl;
      ilGenImages(1,&DevilImgName);
      ilBindImage(DevilImgName);
      ilEnable(IL_FILE_OVERWRITE);
      
      outputVideo.open("cahnhilliard.avi", CV_FOURCC('M','P','4','2'),15, cv::Size(640,640), true);
      // outputVideo.open("cahnhilliard.avi", CV_FOURCC('W','M','V','2'),15, cv::Size(640,640), true);
      glutMainLoop();
  }
  
  delete pCHSolver;
  ierr=VecDestroy(&uPre);CHKERRQ(ierr);
  ierr=VecDestroy(&uCur);CHKERRQ(ierr);
  //ierr=PetscFinalize();CHKERRQ(ierr);
  ierr=SlepcFinalize();CHKERRQ(ierr);
  return 0;
}
