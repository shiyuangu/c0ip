#include"FullMGSolver.h"
FullMGSolver::FullMGSolver(std::vector<MGSolver*>& _pMGs,int _level,PetscScalar _tol, PetscInt _maxit):Solver(_pMGs[_level]->pK),level(_level){
  pFullMG=new FullMG(_pMGs,_tol,_maxit);
}
FullMGSolver::~FullMGSolver(){
  delete pFullMG; 
}
PetscErrorCode FullMGSolver::solve(const Vec& rhs, Vec& x){
  PetscErrorCode ierr;
  ierr=pFullMG->solve(rhs,x,level);CHKERRQ(ierr);
  return 0; 
}
