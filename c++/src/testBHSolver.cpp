#include<cstdio>
#include<cstdlib>
//#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"petsc.h"
#include"petsctime.h"
#include"slepceps.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
//#include"PetscArrayGu.h"
//#include"Mesh2Gu.h"
//#include"MG.h"
//#include"gv.h"
#include"gf.h"
//#include"gfMG.h"
//#include"SolverErr.h"
//#include"comparison.h"
#include"others.h"
#include"auxiliary.h"
//#include"hooks.h"
#include"MyInit.h"
#include"auxiliaryCU.h"
#include"predefGu.h"
#include"converters.h"
#include"BHMG.h"
#include"FullMG.h"
//MyMatArray *pAs,*pSs,*pIs,*pIts,*pMs;
//PetscVecArray *pvBs,*pvBinvs,*pPhi;
//Mesh2Array mesh2es;
//double *lambda1, *lambda2;   //the damping factors;
//double c1,c2;   //for \Delta^ u- c2\Delta u+c1 u
//double c3;      //for the preconditioner -\Delta u+c3 u
//int maxlevel=0;
//MGData *pMGData=NULL;
//DiagProjSolver *pDiagProjSolver;
// #if MY_MAT
// gu::DevMatType myMatDevType=gu::CUSP_ELL;
// #endif
// Solver *pCSolverBH, *pCSolverNeu;
// //MGSolver *pMGBH=NULL,*pMGNeu=NULL;
// std::vector<MGData*> pMGDataVec;
// std::vector<MGSolver*> pMGBHs;
// std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory

// //parameters to control the MG 
// char MGBHMode='W',PrecMode='V';
// PetscInt mMGNeu=5;   //presmoothing/postsmoothing step for Neu MG problem 
// double dampingScale=0.55; //the damping factors are the estimated spectral radii times damping scales

//for swtiches

PetscBool myKspSolverOn;
//parameters to control the testing level and smoothing steps. 
//PetscInt slevel,elevel,sizeks;
PetscInt level;
PetscBool mlogscale;
PetscReal mstartlog,mendlog;
PetscInt msize;
PetscInt mstart=6, mend=36,mskip=2;


//for profiling
PetscLogEvent eventMyVecDot,eventMyMatMult,eventMatMultCusp,eventMatMultAddCusp;
PetscLogEvent stageDebug;
PetscLogEvent eventMyKspSolver;
PetscLogStage memoryWatch;
//PetscInt count; //tmp
int main(int argc, char**argv){
  BHMG* pbhmg;
  FullMG* pFullMG;
  int l,k,m;
  PetscInt nRun;
  Vec workVec[6],x0,outVec;
  PetscScalar tmp1,tmp2,err,tol,powerTol;
  PetscScalar mgErrNorm; //This is the norm of error, NOT the dampingfactor. 
  PetscRandom randomctx; 
  PetscErrorCode ierr;
  PetscBool flag;
  bool flag2; 
  char outfname[256]; int tmp; //only for output result
  char fname[256];
  FILE* fin; 
  char buffer[256];
  size_t pos;   //for exact only the file name(not the path in argv[1])
  
  //////for flop counts and timing for combinations of (k,m)  
  Vec rhs; 
  PetscLogEvent EVENT_MGSolver,EVENT_CNeuSolver;
  PetscLogStage stages[2];
  PetscLogDouble sflops, eflops,stime,etime;
  PetscScalar errEnorm,x0Enorm;
  PetscLogDouble st,ed,elapsed;
  //for reading in vector
  mxGuDouble mxRhs;
  //for iterating
  PetscInt maxIter;
  size_t curIter,preIter,counter;
  //PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  ierr=SlepcInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  if(argc>1){
    #if MY_MAT==1
      ierr=PetscOptionsGetString(PETSC_NULL,"-vec_type",buffer,256,&flag);CHKERRQ(ierr);
      if(!flag || strcmp(buffer,VECSEQCUSP)){
	SETERRQ(PETSC_COMM_SELF,1,"MY_MAT==1,please set -vec_type seqcusp");
      }
     
    #endif 
    MyInitialize();
    //readMGOpts();
    PetscOptionsHasName(PETSC_NULL,"-gu_kspSolver",&myKspSolverOn);
    if(myKspSolverOn)
      printf("myKspSolverOn is on\n");
    //read global parametes to define the problem
    //pAs=new MyMatArray; pSs=new MyMatArray;pMs=new MyMatArray;
    //pIs=new MyMatArray;pIts=new MyMatArray;
    //pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;   
    //if(read(argv[1])){
    //PetscEnd();
    //exit(1);
    //}
    //genMG();
    pbhmg=new BHMG(argv[1]);
    //pFullMG=new FullMG(pbhmg->pMGBHs);
    PetscOptionsGetInt(PETSC_NULL,"-gu_level",&level,&flag);
    if(!flag)level=pbhmg->maxlevel-1;
    printf("-gu_level=%d\n",level);
    PetscOptionsGetInt(PETSC_NULL,"-gu_nRun",&nRun,&flag);
    if(!flag)nRun=1;
    printf("-gu_nRun=%d\n",nRun);
     PetscOptionsHasName(PETSC_NULL,"-gu_mlogscale",&mlogscale);
    if(!mlogscale){
      PetscOptionsGetInt(PETSC_NULL,"-gu_mstart",&mstart,&flag);
      if(!flag||mstart<=0){
	fprintf(stderr,"Error:Please specify -gu_mstart(>=1)\n or -gu_mlogscale");
	PetscEnd();
      }else printf("-gu_mstart=%d\n",mstart);
      PetscOptionsGetInt(PETSC_NULL,"-gu_mskip",&mskip,&flag);
      if(!flag){
	mskip=1;
	fprintf(stderr,"Warning:-gu_mskip is not specified, set to be %d\n",mskip);
      }else printf("-gu_mskip=%d\n",mskip);
      PetscOptionsGetInt(PETSC_NULL,"-gu_mend",&mend,&flag);
      if(!flag||mend<0){
	mend=mstart+mskip;
	fprintf(stderr,"Warning: -gu_mend is not specified, set to be %d\n",mend); 
      }else printf("-gu_mend=%d\n",mend);
    }else{
      PetscOptionsGetReal(PETSC_NULL,"-gu_mstartlog",&mstartlog,&flag);
      if(!flag){
	fprintf(stderr,"Error: Log scale has been set,-gu_startlog must be specified!\n");
        PetscEnd();
        exit(1);
      }else printf("-gu_mstartlog=10^%g(i.e. %g)\n",mstartlog,pow(10,mstartlog));
      PetscOptionsGetReal(PETSC_NULL,"-gu_mendlog",&mendlog,&flag);
      if(!flag){
	fprintf(stderr,"Error: Log scale has been set,-gu_mendlog must be specified!\n");
        PetscEnd();
        exit(1);
      }printf("-gu_mstartlog=10^%g(i.e. %g)\n",mendlog,pow(10,mendlog));
      PetscOptionsGetInt(PETSC_NULL,"-gu_msize",&msize,&flag);
      if(!flag || msize<1){
	fprintf(stderr,"Error:Log scale has been set,-gu_msize must be specified(>0) (msize=%d)!\n",msize);
	PetscEnd();
        exit(1);
      }
    }
    ///////////////////////////////////////////////////////////////////
    /////////////compute contration numbers/////////////////////////////
    
    PetscRandomCreate(PETSC_COMM_SELF,&randomctx);
    PetscRandomSetType(randomctx,PETSCRAND48);
    PetscRandomSetInterval(randomctx,-1.0,1.0);
     
    //////////create log////////////////
    //PetscLogStageRegister("Coarse Neu",&stages[0]);
    PetscLogStageRegister("BHMGSolve Solve",&stages[1]); 
    // PetscLogStageRegister("tester ",&stages[2]); 
    PetscLogEventRegister("MGBHSolver",0,&EVENT_MGSolver);
    PetscLogEventRegister("Coarse Neu solver",0,&EVENT_CNeuSolver);
    ////////////////log the coarsest level solver for Neumann Problem//////////
   
    // MatGetVecs(*(pCSolverNeu->pK),&x0,&rhs); VecDuplicate(x0,&(workVec[0]));VecDuplicate(x0,&(workVec[1]));
    // VecSetRandom(x0,randomctx);   
    // VecCopy(x0,workVec[0]);
    // proj(*(pMGDataVec[0]->pPhi),*(pMGDataVec[0]->pvB),workVec[0],workVec[1],x0);
    // MatMult(*(pMGDataVec[0]->pA),x0,rhs);
   
    // //PetscGetFlops(&sflops);PetscGetTime(&stime);
    // PetscLogStagePush(stages[0]);
    // //PetscLogEventBegin(EVENT_CNeuSolver,0,0,0,0);
    // pCSolverNeu->solve(rhs,workVec[0]);
    // //PetscLogEventEnd(EVENT_CNeuSolver,0,0,0,0);
    // PetscLogStagePop();
    //PetscGetTime(&etime);PetscGetFlops(&eflops);
    //PetscPrintf(PETSC_COMM_WORLD,"CSolverNeu: flops:%0.3e, time: %0.3e\n",eflops-sflops,etime-stime);

    // ierr=VecDestroy(&x0);CHKERRQ(ierr);
    // ierr=VecDestroy(&rhs);CHKERRQ(ierr);
    // ierr=VecDestroy(&workVec[0]);CHKERRQ(ierr);
    // ierr=VecDestroy(&workVec[1]);CHKERRQ(ierr);
    /////////////////////////////////////////////////////////////////////////
        
      k=level;
      for(l=0;l<6;l++){
	VecCreateSeq(PETSC_COMM_SELF,pbhmg->pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));
      }
      VecDuplicate(workVec[0],&x0);
      VecDuplicate(x0,&rhs);
      VecDuplicate(workVec[0],&outVec);

      PetscOptionsGetReal(PETSC_NULL,"-gu_tol",&tol,&flag);
      if(!flag)tol=1e-4;
      printf("-gu_tol=%#.3e\n",tol);

      PetscOptionsGetInt(PETSC_NULL,"-gu_maxIter",&maxIter,&flag);
      if(!flag)maxIter=100;
      printf("-gu_maxIter=%d\n",maxIter);
      
      sprintf(fname,"%s/%s",argv[1], "rhs");
      fin=fopen(fname,"rb");
      if(fin!=NULL){
        mxRhs.read(fin);
	mxGuDouble2Vec(mxRhs, rhs);
      }else{
	fprintf(stderr,"cannot open file %s,generate random data instead.\n",fname);
	VecSetRandom(x0,randomctx);   
	VecCopy(x0,workVec[0]);
	proj(*(pbhmg->pMGDataVec[k]->pPhi),*(pbhmg->pMGDataVec[k]->pvB),workVec[0],workVec[1],x0);
	MatMult(*(pbhmg->pMGDataVec[k]->pA),x0,rhs);
      }
      for(m=mstart;m<mend;m+=mskip){
	for(l=0;l<=k;l++)pbhmg->pMGBHs[l]->m=m;
         
         //solve and log 
         PetscLogStagePush(stages[1]);
         PetscLogEventBegin(EVENT_MGSolver,0,0,0,0);
	 ierr=PetscGetTime(&st);CHKERRQ(ierr);
	 for(l=0;l<nRun;l++){
            ierr=pbhmg->pMGBHs[k]->solve(rhs,workVec[0]);CHKERRQ(ierr);
	    curIter=0; counter=1; errEnorm=100.0;
	    while(errEnorm>tol && counter<=maxIter){
	      preIter=curIter; curIter=(curIter+1)%2; counter++; 
	      ierr=pbhmg->pMGBHs[k]->solve(rhs,workVec[preIter],workVec[curIter]);CHKERRQ(ierr);
	      ierr=VecAXPY(workVec[preIter],-1.0,workVec[curIter]);CHKERRQ(ierr);
	      ierr=innerProduct(*(pbhmg->pMGDataVec[k]->pA),workVec[preIter],workVec[preIter],workVec[2],&errEnorm);CHKERRQ(ierr); errEnorm=sqrt(errEnorm);
	      printf("iter=%u, errEnorm=%#.3e\n",counter,errEnorm);
	    }
			 
	 }
	 ierr=PetscGetTime(&ed);CHKERRQ(ierr);
	 elapsed=ed-st;
	 printf("m=%d,nRun=%d,elapsed_time=%g\n",m,nRun,elapsed);
	 // ierr=pFullMG->solve(rhs,workVec[0],k);CHKERRQ(ierr);
         PetscLogEventEnd(EVENT_MGSolver,0,0,0,0);
         PetscLogStagePop();
         /////Check the answer: only for debug purpose:
         //ierr=VecWAXPY(workVec[1],-1.0,workVec[0],x0);CHKERRQ(ierr);
	 //innerProduct(*(pMGDataVec[k]->pA),workVec[1],workVec[1],workVec[2],&errEnorm);errEnorm=sqrt(errEnorm);
         //innerProduct(*(pMGDataVec[k]->pA),x0,x0,workVec[2],&x0Enorm);x0Enorm=sqrt(x0Enorm);
         //PetscPrintf(PETSC_COMM_WORLD,"errEnorm/x0Enorm=%0.3e\n",errEnorm/x0Enorm);
      } 
      printf("\n");
      for(l=0;l<6;l++){
	VecDestroy(&workVec[l]);
      }
      VecDestroy(&x0);VecDestroy(&outVec);
      ierr=VecDestroy(&rhs);CHKERRQ(ierr);
      
    PetscRandomDestroy(&randomctx);
    delete pbhmg;
    //delete pFullMG;
     ///////////////////////////////////////////////////////////////////
    //releaseMG();
    //delete []lambda1;  delete []lambda2;
    //delete pAs; delete pSs;delete pIs;delete pIts;delete pMs;
    //delete pvBs;delete pvBinvs; delete pPhi;
  }else{
    promptOps();
  }

  ierr=SlepcFinalize();CHKERRQ(ierr);
  return 0;
}
