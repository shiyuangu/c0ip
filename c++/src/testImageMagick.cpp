//test ImageMagick
#include<iostream>
#include"Magick++.h"
int main(int argc, char** argv){
  Magick::Image MagImg;
   try{
      MagImg.read(argv[1]);
      //MagImg.zoom("512x512");
      //MagImg.sample("128x128");
    }
    catch(Magick::Exception& error){
      std::cout<<"Error:"<<error.what()<<std::endl;
      return 1;
    }
    MagImg.display();
  return 0; 
}
