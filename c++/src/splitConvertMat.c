/**
 * @file   splitConvertMat.c
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:34:53 2013
 * 
 * @brief  This program is modified from the MATLAB sample program 
 *         matdgns. It uses MATLAB-C interface API to parse a .mat 
 *         file and produce an intermediate data format to be used
 *         in Cahn-Hilliard applications. 
 */ 

/*
 * MAT-file diagnose program
 *
 * See the MATLAB API Guide for compiling information.
 *
 * Calling syntax:
 *
 *   matdgns <matfile>
 *
 * It will diagnose the MAT-file named <matfile>.
 *
 * This program demonstrates the use of the following functions:
 *
 *  matClose
 *  matGetDir
 *  matGetNextVariable
 *  matGetNextVariableInfo
 *  matOpen
 *
 * Copyright 1984-2003 The MathWorks, Inc.
 */
/* $Revision: 1.8.4.1 $ */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <asm-generic/errno-base.h>
#include <sys/stat.h>
#include "mat.h"
#include "mex.h"
void        display_subscript(const mxArray *array_ptr, mwSize index);
void        get_characteristics(const mxArray  *array_ptr);
mxClassID   analyze_class(const mxArray *array_ptr);
///////functions by Gu
mxClassID writeClass(FILE *fout,const mxArray *array_ptr);

/* Pass analyze_cell a pointer to a cell mxArray.  Each element
   in a cell mxArray is called a "cell"; each cell holds zero
   or one mxArray.  analyze_cell accesses each cell and displays
   information about it. */  
static void
analyze_cell(const mxArray *cell_array_ptr)
{
  mwSize total_num_of_cells;
  mwIndex index;
  const mxArray *cell_element_ptr;
  
  total_num_of_cells = mxGetNumberOfElements(cell_array_ptr); 
  //mexPrintf("total num of cells = %d\n", total_num_of_cells);
  //mexPrintf("\n");

  /* Each cell mxArray contains m-by-n cells; Each of these cells
     is an mxArray. */ 
  for (index=0; index<total_num_of_cells; index++)  {
    //mexPrintf("\n\n\t\tCell Element: ");
    display_subscript(cell_array_ptr, index);
    //mexPrintf("\n");
    cell_element_ptr = mxGetCell(cell_array_ptr, index);
    if (cell_element_ptr == NULL) {
      mexPrintf("\tEmpty Cell\n");
    } else {
      /* Display a top banner. */
      mexPrintf("------------------------------------------------\n");
      get_characteristics(cell_element_ptr);
      analyze_class(cell_element_ptr);
      mexPrintf("\n");
    }
  }
  mexPrintf("\n");
}


/* Pass analyze_structure a pointer to a structure mxArray.  Each element
   in a structure mxArray holds one or more fields; each field holds zero
   or one mxArray.  analyze_structure accesses every field of every
   element and displays information about it. */ 
static void
analyze_structure(const mxArray *structure_array_ptr)
{
  mwSize total_num_of_elements;
  mwIndex index;
  int number_of_fields, field_index;
  const char  *field_name;
  const mxArray *field_array_ptr;
  

  mexPrintf("\n");
  total_num_of_elements = mxGetNumberOfElements(structure_array_ptr); 
  number_of_fields = mxGetNumberOfFields(structure_array_ptr);
  
  /* Walk through each structure element. */
  for (index=0; index<total_num_of_elements; index++)  {
    
    /* For the given index, walk through each field. */ 
    for (field_index=0; field_index<number_of_fields; field_index++)  {
      //mexPrintf("\n\t\t");
      display_subscript(structure_array_ptr, index);
         field_name = mxGetFieldNameByNumber(structure_array_ptr, 
                                             field_index);
	 //mexPrintf(".%s\n", field_name);
      field_array_ptr = mxGetFieldByNumber(structure_array_ptr, 
					   index, 
					   field_index);
      if (field_array_ptr == NULL) {
	//mexPrintf("\tEmpty Field\n");
      } else {
         /* Display a top banner. */
         //mexPrintf("------------------------------------------------\n");
         get_characteristics(field_array_ptr);
         analyze_class(field_array_ptr);
         mexPrintf("\n");
      }
    }
      //mexPrintf("\n\n");
  }
  
  
}


/* Pass analyze_string a pointer to a char mxArray.  Each element
   in a char mxArray holds one 2-byte character (an mxChar); 
   analyze_string displays the contents of the input char mxArray
   one row at a time.  Since adjoining row elements are NOT stored in 
   successive indices, analyze_string has to do a bit of math to
   figure out where the next letter in a string is stored. */ 
static void
analyze_string(const mxArray *string_array_ptr)
{
  char *buf;
  mwSize number_of_dimensions, buflen; 
  const mwSize *dims;
  mwSize d, page, total_number_of_pages, elements_per_page;
  
  /* Allocate enough memory to hold the converted string. */ 
  buflen = mxGetNumberOfElements(string_array_ptr) + 1;
  buf = mxCalloc(buflen, sizeof(char));
  
  /* Copy the string data from string_array_ptr and place it into buf. */ 
  if (mxGetString(string_array_ptr, buf, buflen) != 0)
    mexErrMsgTxt("Could not convert string data.");
  
  /* Get the shape of the input mxArray. */
  dims = mxGetDimensions(string_array_ptr);
  number_of_dimensions = mxGetNumberOfDimensions(string_array_ptr);
  
  elements_per_page = dims[0] * dims[1];
  /* total_number_of_pages = dims[2] x dims[3] x ... x dims[N-1] */ 
  total_number_of_pages = 1;
  for (d=2; d<number_of_dimensions; d++) {
    total_number_of_pages *= dims[d];
  }
  
  for (page=0; page < total_number_of_pages; page++) {
    mwSize row;
    /* On each page, walk through each row. */ 
    for (row=0; row<dims[0]; row++)  {
      mwSize column;     
      mwSize index = (page * elements_per_page) + row;
      //mexPrintf("\t");
      display_subscript(string_array_ptr, index);
      //mexPrintf(" ");
      
      /* Walk along each column in the current row. */ 
      for (column=0; column<dims[1]; column++) {
        //mexPrintf("%c",buf[index]);
        index += dims[0];
      }
      //mexPrintf("\n");

    }
    
  } 
}


/* Pass analyze_sparse a pointer to a sparse mxArray.  A sparse mxArray
   only stores its nonzero elements.  The values of the nonzero elements 
   are stored in the pr and pi arrays.  The tricky part of analyzing
   sparse mxArray's is figuring out the indices where the nonzero
   elements are stored.  (See the mxSetIr and mxSetJc reference pages
   for details. */  
static void
analyze_sparse(const mxArray *array_ptr)
{
  double  *pr, *pi;
  mwIndex  *ir, *jc;
  mwSize      col, total=0;
  mwIndex   starting_row_index, stopping_row_index, current_row_index;
  mwSize      n;
  
  /* Get the starting positions of all four data arrays. */ 
  pr = mxGetPr(array_ptr);
  pi = mxGetPi(array_ptr);
  ir = mxGetIr(array_ptr);
  jc = mxGetJc(array_ptr);
  
  /* Display the nonzero elements of the sparse array. */ 
  n = mxGetN(array_ptr);
  for (col=0; col<n; col++)  { 
    starting_row_index = jc[col]; 
    stopping_row_index = jc[col+1]; 
    if (starting_row_index == stopping_row_index)
      continue;
    else {
      for (current_row_index = starting_row_index; 
	   current_row_index < stopping_row_index; 
	   current_row_index++)  {
	if (mxIsComplex(array_ptr))  {
	  //mexPrintf("\t(%"FMT_SIZE_T"u,%"FMT_SIZE_T"u) = %g+%g i\n", 
          //          ir[current_row_index]+1, 
          //          col+1, pr[total], pi[total]);
	  total++;
	} else {
	  //mexPrintf("\t(%"FMT_SIZE_T"u,%"FMT_SIZE_T"u) = %g\n", 
          //          ir[current_row_index]+1, 
	  //	    col+1, pr[total++]);
        }
      }
    }
  }
}

static void
analyze_int8(const mxArray *array_ptr)
{
  signed char   *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (signed char *)mxGetData(array_ptr);
  pi = (signed char *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) {
      //mexPrintf(" = %d + %di\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %d\n", *pr++);
    }
  } 
}


static void
analyze_uint8(const mxArray *array_ptr)
{
  unsigned char *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (unsigned char *)mxGetData(array_ptr);
  pi = (unsigned char *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) {
      //mexPrintf(" = %u + %ui\n", *pr, *pi++); 
    } else {
      //mexPrintf(" = %u\n", *pr++);
    }
  } 
}


static void
analyze_int16(const mxArray *array_ptr)
{
  short int *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (short int *)mxGetData(array_ptr);
  pi = (short int *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) {
      //mexPrintf(" = %d + %di\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %d\n", *pr++);
    }
  } 
}


static void
analyze_uint16(const mxArray *array_ptr)
{
  unsigned short int *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (unsigned short int *)mxGetData(array_ptr);
  pi = (unsigned short int *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) {
      //mexPrintf(" = %u + %ui\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %u\n", *pr++);
    }
  } 
}



static void
analyze_int32(const mxArray *array_ptr)
{
  int *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (int *)mxGetData(array_ptr);
  pi = (int *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) {
      //mexPrintf(" = %d + %di\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %d\n", *pr++);
    }
  } 
}


static void
analyze_uint32(const mxArray *array_ptr)
{
  unsigned int *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (unsigned int *)mxGetData(array_ptr);
  pi = (unsigned int *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) { 
      //mexPrintf(" = %u + %ui\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %u\n", *pr++);
    }
  } 
}

static void
analyze_int64(const mxArray *array_ptr)
{
  int64_T *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (int64_T *)mxGetData(array_ptr);
  pi = (int64_T *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) { 
      //mexPrintf(" = %" FMT64 "d + %" FMT64 "di\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %" FMT64 "d\n", *pr++);
    }
  } 
}


static void
analyze_uint64(const mxArray *array_ptr)
{
  uint64_T *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (uint64_T *)mxGetData(array_ptr);
  pi = (uint64_T *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) {
      //mexPrintf(" = %" FMT64 "u + %" FMT64 "ui\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %" FMT64 "u\n", *pr++);
    }
  } 
}


static void
analyze_single(const mxArray *array_ptr)
{
  float *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = (float *)mxGetData(array_ptr);
  pi = (float *)mxGetImagData(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)){ 
      //mexPrintf(" = %g + %gi\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %g\n", *pr++);
    }
  } 
}


static void
analyze_double(const mxArray *array_ptr)
{
  double *pr, *pi; 
  mwSize total_num_of_elements, index; 
  
  pr = mxGetPr(array_ptr);
  pi = mxGetPi(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  
  for (index=0; index<total_num_of_elements; index++)  {
    //mexPrintf("\t");
    display_subscript(array_ptr, index);
    if (mxIsComplex(array_ptr)) { 
      //mexPrintf(" = %g + %gi\n", *pr++, *pi++); 
    } else {
      //mexPrintf(" = %g\n", *pr++);
    }
  } 
}

static void
analyze_logical(const mxArray *array_ptr)
{
    mxLogical *pr;
    mwSize total_num_of_elements, index;
    total_num_of_elements = mxGetNumberOfElements(array_ptr);
    pr = (mxLogical *)mxGetData(array_ptr);
    for (index=0; index<total_num_of_elements; index++)  {
        //mexPrintf("\t");
        display_subscript(array_ptr, index);
        if (*pr++) {
            //mexPrintf(" = true\n");
        } else {
            //mexPrintf(" = false\n");
        }
    }
}


/* Pass analyze_full a pointer to any kind of numeric mxArray.  
   analyze_full figures out what kind of numeric mxArray this is. */ 
static void
analyze_full(const mxArray *numeric_array_ptr)
{
  mxClassID   category;
  
  category = mxGetClassID(numeric_array_ptr);
  switch (category)  {
     case mxINT8_CLASS:   analyze_int8(numeric_array_ptr);   break; 
     case mxUINT8_CLASS:  analyze_uint8(numeric_array_ptr);  break;
     case mxINT16_CLASS:  analyze_int16(numeric_array_ptr);  break;
     case mxUINT16_CLASS: analyze_uint16(numeric_array_ptr); break;
     case mxINT32_CLASS:  analyze_int32(numeric_array_ptr);  break;
     case mxUINT32_CLASS: analyze_uint32(numeric_array_ptr); break;
     case mxINT64_CLASS:  analyze_int64(numeric_array_ptr);  break;
     case mxUINT64_CLASS: analyze_uint64(numeric_array_ptr); break;
     case mxSINGLE_CLASS: analyze_single(numeric_array_ptr); break; 
     case mxDOUBLE_CLASS: analyze_double(numeric_array_ptr); break;
     default: break;
  }
}


/* Display the subscript associated with the given index. */ 
void
display_subscript(const mxArray *array_ptr, mwSize index)
{
  mwSize     inner, subindex, total, d, q, number_of_dimensions; 
  mwSize       *subscript;
  const mwSize *dims;
  
  number_of_dimensions = mxGetNumberOfDimensions(array_ptr);
  subscript = mxCalloc(number_of_dimensions, sizeof(mwSize));
  dims = mxGetDimensions(array_ptr); 
  
  //mexPrintf("(");
  subindex = index;
  for (d = number_of_dimensions-1; ; d--) { /* loop termination is at the end */
    
    for (total=1, inner=0; inner<d; inner++)  
      total *= dims[inner]; 
    
    subscript[d] = subindex / total;
    subindex = subindex % total;
    if (d == 0) {
        break;
    }
  }
  
  for (q=0; q<number_of_dimensions-1; q++) {
    //mexPrintf("%d,", subscript[q] + 1);
  }
  //mexPrintf("%d)", subscript[number_of_dimensions-1] + 1);
  
  mxFree(subscript);
}



/* get_characteristics figures out the size, and category 
   of the input array_ptr, and then displays all this information. */ 
void
get_characteristics(const mxArray *array_ptr)
{
  const char    *class_name;
  const mwSize  *dims;
  char          *shape_string;
  char          *temp_string;
  mwSize        c;
  mwSize        number_of_dimensions; 
  size_t        length_of_shape_string;

  /* Display the mxArray's Dimensions; for example, 5x7x3.  
     If the mxArray's Dimensions are too long to fit, then just
     display the number of dimensions; for example, 12-D. */ 
  number_of_dimensions = mxGetNumberOfDimensions(array_ptr);
  dims = mxGetDimensions(array_ptr);
  
  /* alloc memory for shape_string w.r.t thrice the number of dimensions */
  /* (so that we can also add the 'x')                                   */
  shape_string=(char *)mxCalloc(number_of_dimensions*3,sizeof(char));
  shape_string[0]='\0';
  temp_string=(char *)mxCalloc(64, sizeof(char));

  for (c=0; c<number_of_dimensions; c++) {
    sprintf(temp_string, "%"FMT_SIZE_T"dx", dims[c]);
    strcat(shape_string, temp_string);
  }

  length_of_shape_string = strlen(shape_string);
  /* replace the last 'x' with a space */
  shape_string[length_of_shape_string-1]='\0';
  if (length_of_shape_string > 16) {
    sprintf(shape_string, "%"FMT_SIZE_T"u-D", number_of_dimensions); 
  }
  //mexPrintf("Dimensions: %s\n", shape_string);
  
  /* Display the mxArray's class (category). */
  class_name = mxGetClassName(array_ptr);
  //mexPrintf("Class Name: %s%s\n", class_name,
  //mxIsSparse(array_ptr) ? " (sparse)" : "");
  
  /* Display a bottom banner. */
  //mexPrintf("------------------------------------------------\n");
  
  /* free up memory for shape_string */
  mxFree(shape_string);
}



/* Determine the category (class) of the input array_ptr, and then
   branch to the appropriate analysis routine. */
mxClassID
analyze_class(const mxArray *array_ptr)
{
    mxClassID  category;
    
    category = mxGetClassID(array_ptr);
    
    if (mxIsSparse(array_ptr)) {
       analyze_sparse(array_ptr);
    } else {
       switch (category) {
          case mxLOGICAL_CLASS: analyze_logical(array_ptr);    break;
          case mxCHAR_CLASS:    analyze_string(array_ptr);     break;
          case mxSTRUCT_CLASS:  analyze_structure(array_ptr);  break;
          case mxCELL_CLASS:    analyze_cell(array_ptr);       break;
          case mxUNKNOWN_CLASS: mexWarnMsgTxt("Unknown class."); break;
          default:              analyze_full(array_ptr);       break;
       }
    }
    
    return(category);
}

int diagnose(const char *file) {
  MATFile *pmat;
  const char **dir;
  const char *name;
  int	  ndir;
  int	  i;
  mxArray *pa;
  //by Gu
  FILE *fout;
  char fname[256],path[256];
  int tmp;

  printf("Reading file %s...\n\n", file);

  /*
   * Open file to get directory
   */
  pmat = matOpen(file, "r");
  if (pmat == NULL) {
    printf("Error opening file %s\n", file);
    return(1);
  }
  sprintf(path,"%s_D",file);
  tmp=mkdir(path,0777);
  if(tmp!=EEXIST && tmp!=0){
    printf("Something wrong when creating directory %s\n",path);
  }
  /*
   * get directory of MAT-file
   */
  dir = (const char **)matGetDir(pmat, &ndir);
  if (dir == NULL) {
    printf("Error reading directory of file %s\n", file);
    return(1);
  } else {
    printf("Directory of %s:\n", file);
    for (i=0; i < ndir; i++)
      printf("%s\n",dir[i]);
  }
  mxFree(dir);

  /* In order to use matGetNextXXX correctly, reopen file to read in headers. */
  if (matClose(pmat) != 0) {
    printf("Error closing file %s\n",file);
    return(1);
  }
 
  pmat = matOpen(file, "r");
  if (pmat == NULL) {
    printf("Error reopening file %s\n", file);
    return(1);
  }

  /* Read in each array. */
  printf("\nReading in the actual array contents:\n");
  for (i=0; i<ndir; i++) {
      pa = matGetNextVariable(pmat, &name);
      if (pa == NULL) {
	  printf("Error reading in file %s\n", file);
	  return(1);
      } 
      /*
       * Diagnose array pa
       */
      
      //by Gu
      //analyze_class(pa);
      if( !strcmp(name,"As") || !strcmp(name,"Ss")  || !strcmp(name,"Is")||
	  !strcmp(name,"Bs") || !strcmp(name,"vBs") || !strcmp(name,"Phi")||!strcmp(name,"Ms")||
          !strcmp(name,"vBinvs") || !strcmp(name,"meshes2")|| 
	  !strcmp(name,"lambda1")|| !strcmp(name,"lambda2_PS")||!strcmp(name,"lambda2_NP")
	  ||!strcmp(name,"D")||!strcmp(name,"bg")||!strcmp(name,"rhs")||!strcmp(name,"x")){
        printf("=====================================================\n");
        printf("Writting, array %s has %d dimensions\n",
	     name, mxGetNumberOfDimensions(pa));
	sprintf(fname,"%s/%s",path,name);
        fout=fopen(fname,"w");
        if(fout==NULL){
           printf("error in creating file %s",fname);
           return(1);
        }
	writeClass(fout,pa);
        fclose(fout);
      }
      //by Gu

      mxDestroyArray(pa);
      
  }

  if (matClose(pmat) != 0) {
      printf("Error closing file %s\n",file);
      return(1);
  }
  printf("Done\n");
  return(0);
}

///////////functions by Gu//////////
static void writeLogical(FILE *fout,const mxArray *array_ptr){
    mxLogical *pr;
    bool *pwr;
    mwSize total_num_of_elements, index;
    int m,n,size;
    total_num_of_elements = mxGetNumberOfElements(array_ptr);
    size=total_num_of_elements;
    pwr=(bool*)malloc(size*sizeof(bool));
    // printf("\t\t Logical\n");
    if(pwr==NULL){
      printf("malloc error in writeLogical\n");
       exit(1);
    }
    m=mxGetM(array_ptr);n=mxGetN(array_ptr);
    fwrite(&m,sizeof(int),1,fout);
    fwrite(&n,sizeof(int),1,fout);
    pr = (mxLogical *)mxGetData(array_ptr);
    for (index=0; index<total_num_of_elements; index++)  {
        if (*pr++) {
	  pwr[index]=true;
           
        } else {
            pwr[index]=false;
        }
    }
    fwrite (pwr, sizeof(bool),total_num_of_elements, fout);
    free(pwr);
}
static void writeStructure(FILE *fout,const mxArray *structure_array_ptr){
  mwSize total_num_of_elements;
  mwIndex index;
  int number_of_fields, field_index;
  const char  *field_name;
  const mxArray *field_array_ptr;
  
  int size;
  //printf("\t\t Structure\n");
  total_num_of_elements = mxGetNumberOfElements(structure_array_ptr); 
  size=total_num_of_elements;
  fwrite(&size,sizeof(int),1,fout);

  number_of_fields = mxGetNumberOfFields(structure_array_ptr);
  
  /* Walk through each structure element. */
  for (index=0; index<total_num_of_elements; index++)  {

    /* For the given index, walk through each field. */ 
    for (field_index=0; field_index<number_of_fields; field_index++)  {
      field_name = mxGetFieldNameByNumber(structure_array_ptr, 
                                             field_index);
      //mexPrintf(".%s\n", field_name);
      field_array_ptr = mxGetFieldByNumber(structure_array_ptr, 
					   index, 
					   field_index);
      if (field_array_ptr == NULL) {
	printf("\tEmpty Field in writeStructure\n");
      } else {            
	writeClass(fout,field_array_ptr);
      }
    }
      
  }  
}
static void writeCell(FILE *fout,const mxArray *cell_array_ptr){
  mwSize total_num_of_cells;
  mwIndex index;
  const mxArray *cell_element_ptr;
  int size;
  // printf("\t\t Cell\n");
  total_num_of_cells = mxGetNumberOfElements(cell_array_ptr);
  size=total_num_of_cells;
  fwrite(&size,sizeof(int),1,fout); 
  ////mexPrintf("\t total num of cells = %d\n", total_num_of_cells);
  ////mexPrintf("\n");

  /* Each cell mxArray contains m-by-n cells; Each of these cells
     is an mxArray. */ 
  for (index=0; index<total_num_of_cells; index++)  {
    ////mexPrintf("\n\n\t\tCell Element: ");
    //display_subscript(cell_array_ptr, index);
    ////mexPrintf("\n");
    cell_element_ptr = mxGetCell(cell_array_ptr, index);
    if (cell_element_ptr == NULL) {
      //mexPrintf("\tEmpty Cell\n");
    } else {
      writeClass(fout,cell_element_ptr);
     
    }
  }
  //mexPrintf("done\n");
  
}

static void writeDouble(FILE *fout, const mxArray *array_ptr){
  double *pr, *pi; 
  mwSize total_num_of_elements, index,number_of_dimensions; 
  int m,n,size;
  if(mxIsComplex(array_ptr)){
    printf("in writeDouble, the array is complex!");
    return;
  }
  //printf("\t\t Double\n");
  pr = mxGetPr(array_ptr);
  pi = mxGetPi(array_ptr);
  total_num_of_elements = mxGetNumberOfElements(array_ptr);
  size=total_num_of_elements;
  number_of_dimensions = mxGetNumberOfDimensions(array_ptr);
  if(number_of_dimensions>2){
    printf("in writeDouble,multi-dimension array!");
    return;
  }
  m=mxGetM(array_ptr);n=mxGetN(array_ptr);
  if(size!=m*n){
    printf("in writeDouble,size!=m*n\n");
    return;
  }
  fwrite(&m,sizeof(int), 1, fout);
  fwrite(&n,sizeof(int), 1, fout);
  fwrite(pr, sizeof(double), total_num_of_elements, fout);
}
static void writeFull(FILE *fout,const mxArray *numeric_array_ptr){
  mxClassID   category;
  //printf("\t\t Full\n");
  category = mxGetClassID(numeric_array_ptr);
  switch (category)  {
     case mxINT8_CLASS:   
     case mxUINT8_CLASS:  
     case mxINT16_CLASS:  
     case mxUINT16_CLASS: 
     case mxINT32_CLASS:  
     case mxUINT32_CLASS: 
     case mxINT64_CLASS:  
     case mxUINT64_CLASS: 
     case mxSINGLE_CLASS: printf("In writeFull, unkown class\n");break; 
     case mxDOUBLE_CLASS: writeDouble(fout,numeric_array_ptr); break;
     default: break;
  }
}
static void writeSparse(FILE *fout,const mxArray *array_ptr){
  double  *pr;
  mwIndex  *ir, *jc;
  mwSize      n;
  
  int ncol, nrow, nnz,*irInt,*jcInt,i;
  if(mxIsComplex(array_ptr)){
    printf("in writeSparse,complex numbers!\n");
    return;
  }
  //printf("\t\t Sparse\n");
  /* Get the starting positions of all four data arrays. */ 
  pr = mxGetPr(array_ptr);
  ir = mxGetIr(array_ptr);
  jc = mxGetJc(array_ptr);
  
  nrow=mxGetM(array_ptr); ncol=mxGetN(array_ptr); nnz=jc[ncol];
  fwrite(&nrow,sizeof(int),1,fout);
  fwrite(&ncol,sizeof(int),1,fout);
  fwrite(&nnz,sizeof(int),1,fout);
  /* Display the nonzero elements of the sparse array. */ 
  n = mxGetN(array_ptr);
  irInt=malloc(sizeof(int)*nnz);
  jcInt=malloc(sizeof(int)*(ncol+1));//*
  for(i=0;i<nnz;i++){
    irInt[i]=ir[i];
  }
  for(i=0;i<ncol+1;i++){
    jcInt[i]=jc[i];
  }
  fwrite(irInt,sizeof(int),nnz,fout);
  fwrite(jcInt,sizeof(int),ncol+1,fout); //*
  fwrite(pr,sizeof(double),nnz,fout);
  free(irInt);free(jcInt);
  
}
mxClassID writeClass(FILE *fout,const mxArray *array_ptr){
 mxClassID  category;
    
    category = mxGetClassID(array_ptr);
    
    if (mxIsSparse(array_ptr)) {
        writeSparse(fout,array_ptr);
    } else {
       switch (category) {
          case mxLOGICAL_CLASS: writeLogical(fout,array_ptr);    break;
	    //case mxCHAR_CLASS:    writeString(fout,array_ptr);     break;
          case mxSTRUCT_CLASS:  writeStructure(fout,array_ptr);  break;
          case mxCELL_CLASS:    writeCell(fout,array_ptr);       break;
          case mxUNKNOWN_CLASS: mexWarnMsgTxt("Unknown class."); break;
          default:              writeFull(fout,array_ptr);       break;
       }
    }
    
    return(category);   
}

///////////////////////////////////
int main(int argc, char **argv)
{
  
  int result;

  if (argc > 1) 
     result = diagnose(argv[1]); 
   else{ 
     result = 0; 
     printf("Usage: matdgns <matfile>"); 
    printf(" where <matfile> is the name of the MAT-file");
    printf(" to be diagnosed\n");
  }

  //return (result==0)?EXIT_SUCCESS:EXIT_FAILURE;
  return 0;
  //printf("Hello\n");

}
