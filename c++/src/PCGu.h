/**
 * @file   PCGu.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:47:26 2013
 * 
 * @brief  Wrap a solver as a PC. Constant shift is applied to match KSP solver
 * 
 * 
 */
#pragma once
#include"petsc.h"
#include"Solver.h"
class PCGu{
public:
  PCGu(Solver *spSolver, VecScatter *spScatter, int ndof_s){
    Vec ones; 
    pSolver=spSolver; pScatter=spScatter; ndof=ndof_s;
    MatGetVecs(*(pSolver->pK),&ones,PETSC_NULL);
    VecSet(ones,1.0);VecSum(ones,&size);
    for(int i=0;i<3;i++)
      VecDuplicate(ones,tmpVec+i);

    VecDestroy(&ones);
  }
  ~PCGu(){
    for(int i=0;i<3;i++)
      VecDestroy(&tmpVec[i]);
  }
  PetscErrorCode apply(Vec rhs,Vec outVec){
    PetscScalar alpha1; 
    PetscErrorCode ierr;
    PetscScalar *pV,sum;
    //tmpVec[0]<----rhs(reduced size);
    ierr=VecScatterBegin(*pScatter,rhs,tmpVec[0],INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
    ierr=VecScatterEnd(*pScatter,rhs,tmpVec[0],INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
    
    VecGetArray(tmpVec[0],&pV);
     pV[ndof]=0;
    VecRestoreArray(tmpVec[0],&pV);
    VecSum(tmpVec[0],&sum);
    VecGetArray(tmpVec[0],&pV);
      pV[ndof]=-1.0*sum;
    VecRestoreArray(tmpVec[0],&pV);

    ierr=pSolver->solve(tmpVec[0],tmpVec[1]);CHKERRQ(ierr);
    VecGetArray(tmpVec[1],&pV);
      alpha1=pV[ndof];
    VecRestoreArray(tmpVec[1],&pV);
    ierr=VecShift(tmpVec[1],-1.0*alpha1);CHKERRQ(ierr);
    
    VecScatterBegin(*pScatter,tmpVec[1],outVec,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterEnd(*pScatter,tmpVec[1],outVec,INSERT_VALUES,SCATTER_FORWARD);
    return 0; 
  }
  VecScatter* pScatter;   //from/to reduced size vector to full size vector. 
  int ndof;
  Vec tmpVec[3];
private:
  Solver* pSolver;
  PetscScalar size;  
};
