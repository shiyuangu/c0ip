/**
 * @file   MyInit.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:37:23 2013
 * 
 * @brief  to register events for PETSc logging/profiling.
 * 
 * 
 */
#pragma once
#include"petsc.h"
PetscErrorCode MyInitialize(); 
