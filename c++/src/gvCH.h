//global variables for Cahn-Hilliard solvers. 
#pragma once
//////////////////////////For CH/////////
extern gu::Array2D<bool> inpR,pic;//inpaint region/picture
extern Vec u0;//initial value for CH 
extern Vec xD;// indicator for inpainting region;
extern int imgW, imgH;
extern IS idxDOF;  //degree of freedoms
extern Vec uPre, uCur; 
extern PetscScalar CHpenalty,CHeps1,CHeps2,CHdt1,CHdt2;  //penalty parameter in the CH inpainting model; 
extern PetscInt CHnt1, CHnt2,CHnt;
extern gu::CHSolver *pCHSolver;
extern int currentTime; 
extern double lenUnit;
extern PetscReal CHksp_rtol1, CHksp_rtol2; 
extern double lenUnit;
