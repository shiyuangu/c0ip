//global variables related to Multigrid.
#pragma once 
#include<vector>
#include"MG.h"
#include"petsc.h"

//parameters to control the MG 
//extern char MGBHMode,PrecMode;
//extern PetscInt mMGNeu;   //presmoothing/postsmoothing step for Neu MG problem 
//extern double dampingScale; //the damping factors are the estimated spectral radii times damping scales
//extern double *lambda1, *lambda2;   //the damping factors;

//parameters to control the testing level and smoothing steps. 
//extern PetscInt slevel,elevel,sizeks;       
//extern PetscTruth mlogscale;
//extern PetscBool mlogscale;
//extern PetscReal mstartlog,mendlog;
//extern PetscInt msize;
//extern PetscInt mstart, mend,mskip,sizems;
//extern PetscInt *pm; 
//extern PetscInt nRun; // # of run( ramdon sample) for each PC

//MGSolvers
//extern KSPSolverGu *pCSolverBH, *pCSolverNeu;
//extern Solver *pCSolverBH, *pCSolverNeu;
//extern std::vector<MGData*> pMGDataVec;
//extern std::vector<MGSolver*> pMGBHs;
//extern std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory 

