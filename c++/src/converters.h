//converters.h/converter.cpp: to convert data format
#pragma once
#include"petsc.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"Mesh2Gu.h"
#include"Array2D.h"
#include"Magick++.h"
PetscErrorCode mxGuSparse2Mat(const mxGuSparse& s, Mat& t);
PetscErrorCode mxGuSparse2Vec(const mxGuSparse& s, Vec& t);
PetscErrorCode mxGuDouble2Vec(const mxGuDouble& s, Vec& t);
void mxMesh2toMesh2(const mxMesh2& s, Mesh2& t);
namespace gu{
  int converter(mxGuLogical& s, Array2D<bool>& t);
  int converter(Magick::Image& s, Array2D<bool>& t);
}
