/**
 * @file   flopsTime.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:16:20 2013
 * 
 * @brief  Profile multigrid biharmonic solver. 
 */
#include<cstdio>
#include<cstdlib>
#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"petsc.h"
#include"slepceps.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
//#include"gv.h"
#include"gf.h"
#include"gfMG.h"
#include"SolverErr.h"
#include"comparison.h"
#include"others.h"
#include"auxiliary.h"
#include"hooks.h"
#include"MyInit.h"
#include"auxiliaryCU.h"
#include"predefGu.h"
#include"BHMG.h" 
/********the following is replaced by BHMG******
MyMatArray *pAs,*pSs,*pIs,*pIts,*pMs;
PetscVecArray *pvBs,*pvBinvs,*pPhi;
Mesh2Array mesh2es;
double *lambda1, *lambda2;   //the damping factors;
double c1,c2;   //for \Delta^ u- c2\Delta u+c1 u
double c3;      //for the preconditioner -\Delta u+c3 u
int maxlevel=0;
//MGData *pMGData=NULL;
//DiagProjSolver *pDiagProjSolver;
#if MY_MAT
gu::DevMatType myMatDevType=gu::CUSP_ELL;
#endif
Solver *pCSolverBH, *pCSolverNeu;
//MGSolver *pMGBH=NULL,*pMGNeu=NULL;
std::vector<MGData*> pMGDataVec;
std::vector<MGSolver*> pMGBHs;
std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory

//parameters to control the MG 
char MGBHMode='W',PrecMode='V';
PetscInt mMGNeu=5;   //presmoothing/postsmoothing step for Neu MG problem 
double dampingScale=0.55; //the damping factors are the estimated spectral radii times damping scales
*****************BHMG*************************/
BHMG* pBHMG;
//for swtiches
//PetscBool myKspSolverOn;
//parameters to control the testing level and smoothing steps. 
//PetscInt slevel,elevel,sizeks;       
//PetscBool mlogscale;
//PetscReal mstartlog,mendlog;
//PetscInt msize;
//PetscInt mstart=6, mend=36,mskip=2,sizems; 
//ConNumkm *pConNumkm;

//for profiling
PetscLogEvent eventMyVecDot,eventMyMatMult,eventMatMultCusp,eventMatMultAddCusp;
PetscLogEvent stageDebug;
PetscLogEvent eventMyKspSolver;
PetscLogStage memoryWatch;
//PetscInt count; //tmp
int main(int argc, char**argv){
  int l,k,m;
  //PetscInt kCounter,mCounter;
  PetscInt nRun;
  // SolverErr *pMGErrBH;
  Vec workVec[6],x0,outVec;
  PetscScalar tmp1,tmp2,err,tol;
  PetscScalar mgErrNorm; //This is the norm of error, NOT the dampingfactor. 
  PetscRandom randomctx; 
  PetscErrorCode ierr;
  PetscBool flag;
  bool flag2; 
  // char outfname[256]; int tmp; //only for output result
  char infname1[256]; std::string infname2;
  char buffer[256];
  // size_t pos;   //for exact only the file name(not the path in argv[1])

  //////for flop counts and timing for combinations of (k,m)  
  Vec rhs; 
  PetscLogEvent EVENT_MGSolver,EVENT_CNeuSolver;
  PetscLogStage stages[2];
  PetscLogDouble sflops, eflops,stime,etime;
  PetscScalar errEnorm,x0Enorm;
  if(argc<=1){
    promptOps();
    return 0; 
  }
  
  //PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  ierr=SlepcInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
    /*****the following is moved to BHMG********
    #if MY_MAT==1
      ierr=PetscOptionsGetString(PETSC_NULL,"-vec_type",buffer,256,&flag);CHKERRQ(ierr);
      if(!flag || strcmp(buffer,VECSEQCUSP)){
	SETERRQ(PETSC_COMM_SELF,1,"MY_MAT==1,please set -vec_type seqcusp");
      }
      ierr=PetscOptionsGetString(PETSC_NULL,"-gu_mymat",buffer,256,&flag);CHKERRQ(ierr);
      if(flag){
	if(!strcmp(buffer,"cuspcsr")){
	  myMatDevType=gu::CUSP_CSR;
	  PetscPrintf(PETSC_COMM_SELF,"-gu_mymat=cuspcsr\n");
	}
      }else{
         PetscPrintf(PETSC_COMM_SELF,"-gu_mymat=cuspell\n");
      }
    #endif
    *****************/
    MyInitialize();  //for registering log events and stages
    //readMGOpts();
    //PetscOptionsHasName(PETSC_NULL,"-gu_kspSolver",&myKspSolverOn);
    //if(myKspSolverOn)
    // printf("myKspSolverOn is on\n");
    //read global parametes to define the problem
    /******the following is moved to BHMG******
    pAs=new MyMatArray; pSs=new MyMatArray;pMs=new MyMatArray;
    pIs=new MyMatArray;pIts=new MyMatArray;
    pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;
    pMGErrBH=new SolverErr;           
    if(read(argv[1])){
      PetscEnd();
      exit(1);
    }
     genMG();
    ***********/
    pBHMG=new BHMG(argv[1]);
    
    /********
    PetscOptionsGetInt(PETSC_NULL,"-gu_slevel",&slevel,&flag);
    if(!flag)slevel=1;
    printf("-gu_slevel=%d\n",slevel);
    PetscOptionsGetInt(PETSC_NULL,"-gu_elevel",&elevel,&flag);
    if(!flag){
      if(elevel<0||elevel>pBHMG->maxlevel-1){
        fprintf(stderr,"invalid -gu_elevel,reset to %d\n",pBHMG->maxlevel-1);
      }
      elevel=pBHMG->maxlevel-1;
    }
    printf("-gu_elevel=%d\n",elevel);
    PetscOptionsGetInt(PETSC_NULL,"-gu_mstart",&mstart,&flag);
    if(!flag)mstart=6;
    PetscOptionsGetInt(PETSC_NULL,"-gu_mend",&mend,&flag);
    if(!flag)mend=mstart+1;
    PetscOptionsGetInt(PETSC_NULL,"-gu_mskip",&mskip,&flag);
    if(!flag)mskip=1;
 
    PetscOptionsHasName(PETSC_NULL,"-gu_mlogscale",&mlogscale);
    printf("mstart=%d,mend=%d,mskip=%d,mlogscale=%d\n",mstart,mend,mskip,mlogscale);
    **********/
    PetscOptionsGetInt(PETSC_NULL,"-gu_nRun",&nRun,&flag);
    if(!flag)nRun=100;
    printf("-gu_nRun=%d\n",nRun);
    //////////////generator the testing levels, smoothing steps/////////////
    /*********
    if(!mlogscale){
      sizems=(mend-mstart+mskip-1)/mskip;
      sizeks=elevel-slevel+1;
      pConNumkm=new ConNumkm(sizeks,sizems);
      kCounter=0;for(k=slevel;k<=elevel;k++) pConNumkm->ks[kCounter++]=k;
      mCounter=0;for(m=mstart;m<mend;m+=mskip) pConNumkm->ms[mCounter++]=m;
    } else{
      sizems=msize;
      sizeks=elevel-slevel+1;
      pConNumkm=new ConNumkm(sizeks,sizems);
      kCounter=0;for(k=slevel;k<=elevel;k++) pConNumkm->ks[kCounter++]=k;
      if(msize>1){
	mCounter=0;
	for(m=0;m<msize;m++) 
	  pConNumkm->ms[mCounter++]=(pow(10,mstartlog+(mendlog-mstartlog)/(msize-1)*m)+0.5);
      }
      else pConNumkm->ms[0]=pow(10,mstartlog)+0.5;
      }**********/
    ///////////////////////////////////////////////////////////////////
    /////////////compute contration numbers/////////////////////////////
    
    PetscRandomCreate(PETSC_COMM_SELF,&randomctx);
    PetscRandomSetType(randomctx,PETSCRAND48);
    PetscRandomSetInterval(randomctx,-1.0,1.0);
     
    //////////create log////////////////
    PetscLogStageRegister("Coarse Neu",&stages[0]);
    PetscLogStageRegister("MGBH Solve",&stages[1]); 
    // PetscLogStageRegister("tester ",&stages[2]); 
    PetscLogEventRegister("MGBHSolver",0,&EVENT_MGSolver);
    PetscLogEventRegister("Coarse Neu solver",0,&EVENT_CNeuSolver);
    ////////////////log the coarsest level solver for Neumann Problem//////////
   
    MatGetVecs(*(pBHMG->pCSolverNeu->pK),&x0,&rhs); VecDuplicate(x0,&(workVec[0]));VecDuplicate(x0,&(workVec[1]));
    VecSetRandom(x0,randomctx);   
    VecCopy(x0,workVec[0]);
    proj(*(pBHMG->pMGDataVec[0]->pPhi),*(pBHMG->pMGDataVec[0]->pvB),workVec[0],workVec[1],x0);
    MatMult(*(pBHMG->pMGDataVec[0]->pA),x0,rhs);
   
    //PetscGetFlops(&sflops);PetscGetTime(&stime);
    PetscLogStagePush(stages[0]);
    PetscGetFlops(&sflops);PetscGetTime(&stime);
    //PetscLogEventBegin(EVENT_CNeuSolver,0,0,0,0);
    pBHMG->pCSolverNeu->solve(rhs,workVec[0]);
    //PetscLogEventEnd(EVENT_CNeuSolver,0,0,0,0);
    PetscGetTime(&etime);PetscGetFlops(&eflops);
    PetscLogStagePop();
    //PetscGetTime(&etime);PetscGetFlops(&eflops);
    PetscPrintf(PETSC_COMM_WORLD,"CSolverNeu: flops:%0.3e, time: %0.3e\n",eflops-sflops,etime-stime);

    ierr=VecDestroy(&x0);CHKERRQ(ierr);
    ierr=VecDestroy(&rhs);CHKERRQ(ierr);
    ierr=VecDestroy(&workVec[0]);CHKERRQ(ierr);
    ierr=VecDestroy(&workVec[1]);CHKERRQ(ierr);
    ////////////////////////////////////////////////////////////////////////     
     ///////log the MG Solver///////////////////////
      PetscOptionsGetInt(PETSC_NULL,"-gu_level",&k,&flag);
      if(!flag){
        if(k<0||k>pBHMG->maxlevel-1){
          fprintf(stderr,"invalid -gu_level,reset to %d\n",pBHMG->maxlevel-1);
        }
      k=pBHMG->maxlevel-1;
      }
      //// set or reset workVec
      for(l=0;l<6;l++){
	   MyVecCreate(PETSC_COMM_SELF,pBHMG->pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));
      }
      VecDuplicate(workVec[0],&x0);
      
      ////generate random initial guess:
      ////  initial guesses are different level,but the same for all smoothing steps. 
      VecSetRandom(x0,randomctx);   
      VecDuplicate(workVec[0],&outVec);
      if(fabs(pBHMG->c1)==0.0){
	VecCopy(x0,workVec[0]);
	proj(*(pBHMG->pMGDataVec[k]->pPhi),*(pBHMG->pMGDataVec[k]->pvB),workVec[0],workVec[1],x0);
      }
      VecDuplicate(x0,&rhs);
      MatMult(*(pBHMG->pMGDataVec[k]->pA),x0,rhs);
      PetscOptionsGetInt(PETSC_NULL,"-gu_m",&m,&flag);
      if(!flag){
        if(m<0){
          fprintf(stderr,"m<0,Please reset!\n");
        }
	PetscEnd();
	return 0; 
      }
         //reset underlying MG solvers of Error Operator;
	 for(l=0;l<=k;l++)pBHMG->pMGBHs[l]->m=m;
         
         //solve and log 
         PetscLogStagePush(stages[1]);
         PetscLogEventBegin(EVENT_MGSolver,0,0,0,0);
	 PetscGetFlops(&sflops);PetscGetTime(&stime);
	 for(l=0;l<nRun;l++){   
            ierr=pBHMG->pMGBHs[k]->solve(rhs,workVec[0]);CHKERRQ(ierr);
	 }
	 PetscGetTime(&etime);PetscGetFlops(&eflops);
         PetscLogEventEnd(EVENT_MGSolver,0,0,0,0);
	 PetscPrintf(PETSC_COMM_WORLD,"MGSolve average over %d  runs: flops:%0.3e, time: %0.3e\n",nRun, (eflops-sflops)/nRun,(etime-stime)/nRun);
         PetscLogStagePop();
         /////Check the answer: only for debug purpose:
         ierr=VecWAXPY(workVec[1],-1.0,workVec[0],x0);CHKERRQ(ierr);
	 innerProduct(*(pBHMG->pMGDataVec[k]->pA),workVec[1],workVec[1],workVec[2],&errEnorm);errEnorm=sqrt(errEnorm);
         innerProduct(*(pBHMG->pMGDataVec[k]->pA),x0,x0,workVec[2],&x0Enorm);x0Enorm=sqrt(x0Enorm);
         PetscPrintf(PETSC_COMM_WORLD,"errEnorm/x0Enorm=%0.3e",errEnorm/x0Enorm); 
      printf("\n");
      for(l=0;l<6;l++){
	VecDestroy(&workVec[l]);
      }
      VecDestroy(&x0);VecDestroy(&outVec);
      ierr=VecDestroy(&rhs);CHKERRQ(ierr); 
    PetscRandomDestroy(&randomctx);
    //////////////////////////////////////////////////////////////////
    //////////write contration table and free memory///////////
    // tmp=mkdir(argv[2],0777);
//     if(tmp!=0 && errno!=EEXIST){
//       fprintf(stderr,"ERROR %d:unable to mkdir %s; %s\n",errno,argv[2],strerror(errno));
//     }
//     infname2.assign(argv[1]);
//     pos=infname2.find_last_of('/');
//     if(pos==std::string::npos) pos=0;
//     infname2=infname2.substr(pos);
//     strcpy(infname1,infname2.c_str());
//     sprintf(outfname,"%s/%s_%c%c%d",argv[2],infname1,MGBHMode,PrecMode,mMGNeu);
//     pConNumkm->out(outfname);
//     sprintf(outfname,"%s/%s_%c%c%d_texTable.tex",argv[2],infname1,MGBHMode,PrecMode,mMGNeu);
//     pConNumkm->toTexTable(outfname);
    //delete pConNumkm;
    delete pBHMG; 
     ///////////////////////////////////////////////////////////////////
    /*********
    releaseMG();
    delete []lambda1;  delete []lambda2;
    delete pAs; delete pSs;delete pIs;delete pIts;delete pMs;
    delete pvBs;delete pvBinvs; delete pPhi;
    delete pMGErrBH;
    *********/
 ierr=SlepcFinalize();CHKERRQ(ierr);
  return 0;
}
