/*** Obsolete ***/
#include<iostream>
//#include"GLee.h"
//#include<GL/glut.h>
//#include<GL/freeglut_ext.h>
//#include<GL/freeglut.h>
//#include<GL/glext.h>
/***from Dr.K hw5.cc**/
#define GL_GLEXT_PROTOTYPES  //without this, glWindowPos2i not declared. 
#define GLX_GLXEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glx.h>
#include <GL/glxext.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

#include<cstdio>
#include<cstdlib>
#include"gfIFace.h"
#include"Mesh2Gu.h"
#include"petsc.h"
#include"CHSolver.h"
#include"CHSolverInp.h"

/***for Devil***/
#include"ilutGLGu.h"

/***for opencv**/
#include"videoGu.h"

//#include"gv.h"
static char message[256];
extern CHSolver* pCHSolver;
extern const Mesh2 *pDispMesh; //current level of mesh; 
extern Vec *pDispVec;
extern Vec uPre,uCur;
extern const int imgW=128, imgH=128; //intended image size
extern int nt; //advance n steps for one click
extern int currentTimeStep;
extern double currentTime;
/***for output video***/
extern int windowWidth, windowHeight;
//extern char* pbuffer;
extern cv::VideoWriter outputVideo;

extern CHSolver *pCHSolver; 
static void v2c(PetscScalar v, GLfloat& r, GLfloat& g, GLfloat& b);
static void display(void){
  static int trilist[12];
  GLfloat r,g,b;
  PetscScalar *pa;
  PetscScalar mass;
  CHSolverInp *pCHSolverInp; 
  VecGetArray(*pDispVec,&pa);
  glClear(GL_COLOR_BUFFER_BIT);
  for(int i=0;i<pDispMesh->nT;i++){
    trilist[0]=pDispMesh->T[i*6];
    trilist[1]=pDispMesh->T[i*6+5];
    trilist[2]=pDispMesh->T[i*6+4];
    trilist[3]=pDispMesh->T[i*6+5];
    trilist[4]=pDispMesh->T[i*6+1];
    trilist[5]=pDispMesh->T[i*6+3];
    trilist[6]=pDispMesh->T[i*6+4];
    trilist[7]=pDispMesh->T[i*6+3];
    trilist[8]=pDispMesh->T[i*6+2];
    trilist[9]=pDispMesh->T[i*6+5];
    trilist[10]=pDispMesh->T[i*6+3];
    trilist[11]=pDispMesh->T[i*6+4];
    //glDrawElements(GL_TRIANGLES,12,GL_INT,trilist);
    glBegin(GL_TRIANGLES);
    for(int j=0;j<12;j++){
      v2c(pa[trilist[j]],r,g,b);
      glColor3f(r,g,b);
      glVertex2d(pDispMesh->xyco[2*trilist[j]],pDispMesh->xyco[2*trilist[j]+1]);
    }
    glEnd();
  }
  VecRestoreArray(*pDispVec,&pa);

  /****compute mass***/
  pCHSolver->computeMass(*pDispVec,&mass);
  /****draw text****/
  glColor3f(0.0f,1.0f,0.0f);//need to set glColor before glWindowPos2i
  glWindowPos2i(10,windowHeight-44);
  //sprintf(message,"step=%-8d   t=%-15g mass=%g",currentTime,pCHSolver->dt*currentTime,mass);
  sprintf(message,"step=%-8d   t=%-15f",currentTimeStep,currentTime);
  // printf("%s\n",message);
  glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24,(unsigned char*)message);
  
  glColor3f(0.0f,1.0f,0.0f);//need to set glColor before glWindowPos2i
  glWindowPos2i(320,windowHeight-44);
  sprintf(message,"mass=%g",mass);
  glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24,(unsigned char*)message);
  
  glColor3f(0.0f,1.0f,0.0f);//need to set glColor before glWindowPos2i
  glWindowPos2i(10,24);
  
  if (pCHSolverInp=dynamic_cast<CHSolverInp *>(pCHSolver)){
    sprintf(message,"eps=%#.1e dt=%#.1e lambda=%#.1e",pCHSolver->eps,pCHSolver->dt,pCHSolverInp->lambda);
  }else{
     sprintf(message,"eps=%#.1e dt=%#.1e",pCHSolver->eps,pCHSolver->dt);
  }
  glutBitmapString(GLUT_BITMAP_HELVETICA_18,(unsigned char*)message);
  glutSwapBuffers();
  glutPostRedisplay();
}
//setup OpenGL: wx,wy: the size of the window. 
static void init(void){
  glClearColor(0.0,0.0,0.0,0.0);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0.0, (GLfloat)imgW*pCHSolver->lenUnit,0.0,(GLfloat)imgH*pCHSolver->lenUnit);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  /*flip y-direction;*/
  /*GLfloat M[16]={1.0, 0.0,0.0,0.0,
              0.0,-1.0,0.0,0.0,
              0.0, 0.0,1.0,0.0,
   	     0.0,(GLfloat)imgW*pCHSolver->lenUnit,0.0,1.0};
	     glMultMatrixf(M);*/
  
  //glPixelZoom(1,-1);  // flip y-direction
  //PixelStorei(GL_UNPACK_ALIGNMENT,1);
  //PixelStorei(GL_PACK, ALIGNMENT,1);
  glPolygonMode(GL_BACK,GL_FILL);
  glPolygonMode(GL_FRONT,GL_FILL);
  //glEnableClientState(GL_VERTEX_ARRAY);
  //glVertexPointer(2,GL_DOUBLE,0,pDispMesh->xyco);
}
static void keyboard(unsigned char key, int x, int y){
  switch(key){
  case 27:
    exit(0);
    break;
  }
}
static void processMenu(int value){
  static PetscReal norm;
  static PetscLogDouble stime,etime,dur_time;
  //static int currentTime;
  static ILenum ilErrorCode;
  PetscViewer viewer;
  char fname[256];
  CHSolverInp* pCHSolverInp;
  PetscInt nt2;
  PetscBool flag;
  static bool flag2=false; 
  PetscErrorCode ierr; 
   glPixelStorei(GL_PACK_ALIGNMENT,1);
   char* const pbuffer = (char*) malloc(windowWidth*windowHeight*3);
   
   /***create a mapping for flipping Y; used later for outputing image ***/
   cv::Mat cvImage(windowHeight,windowWidth,CV_8UC3,(unsigned char*)pbuffer);
   cv::Mat cvImageFlipY(cvImage.size(),cvImage.type());
   cv::Mat map_x(cvImage.size(),CV_32FC1);
   cv::Mat map_y(cvImage.size(),CV_32FC1);
   /***flip y***/
   for(int j=0;j<cvImage.rows;j++)
     for(int i=0;i<cvImage.cols;i++){
       map_x.at<float>(j,i)=i;
       map_y.at<float>(j,i)=cvImage.rows-j;
     }
   /***output the first frame***/
   if (!flag2){
     flag2=true;
     glReadPixels(0,0,windowWidth,windowHeight,GL_BGR,GL_UNSIGNED_BYTE,pbuffer);
    cv::remap(cvImage,cvImageFlipY,map_x,map_y,cv::INTER_NEAREST);
    outputVideo<<cvImageFlipY;
   }
  switch(value){
  case 0:  //timestepping
    sprintf(message,"timestepping......");glutSetWindowTitle(message);
    for(int i=0; i<nt;i++){
      currentTimeStep++;
      currentTime=currentTime+pCHSolver->dt;
      
      VecCopy(uCur,uPre);
      PetscGetTime(&stime);
      pCHSolver->timestepping(uPre,uCur);
      PetscGetTime(&etime);
      display();
      dur_time=etime-stime;
    sprintf(message,"time:%d(use %#.3e)",currentTimeStep,dur_time);glutSetWindowTitle(message);
    glReadPixels(0,0,windowWidth,windowHeight,GL_BGR,GL_UNSIGNED_BYTE,pbuffer);
    cv::remap(cvImage,cvImageFlipY,map_x,map_y,cv::INTER_NEAREST);
    outputVideo<<cvImageFlipY;
     
    }
    glutPostRedisplay();
    break;
  case 1://switch to second stage;
    pCHSolver->reGenBH();
    pDispMesh=pCHSolver->getMesh();  
    PetscOptionsGetInt(PETSC_NULL,"-gu_CHnt2",&nt2,&flag); 
    if(flag){
       nt=nt2;
       printf("-gu_CHnt2=%d\n",nt);
    }
    if(pCHSolverInp=dynamic_cast<CHSolverInp *>(pCHSolver)){
      /***shift and rescale to  ***/
       PetscReal vmin,vmax;
       ierr=VecMin(uCur,PETSC_NULL,&vmin);CHKERRV(ierr);
       ierr=VecMax(uCur,PETSC_NULL,&vmax);CHKERRV(ierr);
       ierr=VecShift(uCur,-vmin); CHKERRV(ierr);
       VecNorm(uCur,NORM_INFINITY,&norm);
       printf("============================\n");
       printf("shift and rescale uCur......\n");
       printf("before : min=%#.3e,max=%#.3e\n",vmin,vmax);    
       printf("============================\n");
       VecScale(uCur,1.0/norm);
       ierr=VecMin(uCur,PETSC_NULL,&vmin);CHKERRV(ierr);
       ierr=VecMax(uCur,PETSC_NULL,&vmax);CHKERRV(ierr);
    }
    display();
    glutPostRedisplay(); 
    break;
  case 2: //two stages; nonstop mode;
    /***first stage ***/
    sprintf(message,"timestepping......");glutSetWindowTitle(message);
    for(int i=0; i<nt;i++){
      currentTimeStep++;
      currentTime=currentTime+pCHSolver->dt;
      
      VecCopy(uCur,uPre);
      PetscGetTime(&stime);
      pCHSolver->timestepping(uPre,uCur);
      PetscGetTime(&etime);
      display();
      dur_time=etime-stime;
    sprintf(message,"time:%d(use %#.3e)",currentTime,dur_time);glutSetWindowTitle(message);
    glReadPixels(0,0,windowWidth,windowHeight,GL_BGR,GL_UNSIGNED_BYTE,pbuffer);
    cv::remap(cvImage,cvImageFlipY,map_x,map_y,cv::INTER_NEAREST);
    outputVideo<<cvImageFlipY;     
    }
    /*** prepare for the 2nd stage ***/
    pCHSolver->reGenBH();
    pDispMesh=pCHSolver->getMesh();  
    PetscOptionsGetInt(PETSC_NULL,"-gu_CHnt2",&nt2,&flag); 
    if(flag){
       nt=nt2;
       printf("-gu_CHnt2=%d\n",nt);
    }
    if(pCHSolverInp=dynamic_cast<CHSolverInp *>(pCHSolver)){
      /***shift and rescale to  ***/
       PetscReal vmin,vmax;
       ierr=VecMin(uCur,PETSC_NULL,&vmin);CHKERRV(ierr);
       ierr=VecMax(uCur,PETSC_NULL,&vmax);CHKERRV(ierr);
       ierr=VecShift(uCur,-vmin); CHKERRV(ierr);
       VecNorm(uCur,NORM_INFINITY,&norm);
       printf("============================\n");
       printf("shift and rescale uCur......\n");
       printf("before : min=%#.3e,max=%#.3e\n",vmin,vmax);    
       printf("============================\n");
       VecScale(uCur,1.0/norm);
       ierr=VecMin(uCur,PETSC_NULL,&vmin);CHKERRV(ierr);
       ierr=VecMax(uCur,PETSC_NULL,&vmax);CHKERRV(ierr);
    }
    display();
    /*** 2nd stage ***/
    sprintf(message,"timestepping......");glutSetWindowTitle(message);
    for(int i=0; i<nt;i++){
      currentTimeStep++;
      currentTime=currentTime+pCHSolver->dt;
      
      VecCopy(uCur,uPre);
      PetscGetTime(&stime);
      pCHSolver->timestepping(uPre,uCur);
      PetscGetTime(&etime);
      display();
      dur_time=etime-stime;
    sprintf(message,"time:%d(use %#.3e)",currentTimeStep,dur_time);glutSetWindowTitle(message);
    glReadPixels(0,0,windowWidth,windowHeight,GL_BGR,GL_UNSIGNED_BYTE,pbuffer);
    cv::remap(cvImage,cvImageFlipY,map_x,map_y,cv::INTER_NEAREST);
    outputVideo<<cvImageFlipY;     
    }
    break;
  case 3:
    /***use DevIL***/
    /*
    if(!ilutGLScreenieGu()){
      ilErrorCode=ilGetError();
      std::cout<<"DevIL error:"<<ilErrorCode<<std::endl; 
      }*/
    /***use opencv***/
   
    glReadPixels(0,0,windowWidth,windowHeight,GL_BGR,GL_UNSIGNED_BYTE,pbuffer);
    cv::remap(cvImage,cvImageFlipY,map_x,map_y,cv::INTER_NEAREST);
    //imshow("Display window", cvImage);
     std::cout<<"Please enter file name:";std::cin>>fname;
    imwrite(fname,cvImageFlipY);
    break;
  case 4:
    std::cout<<"Please enter file name:";std::cin>>fname;
    std::cout<<"Writing Petsc Binary file " <<fname<<"\n";
    PetscViewerBinaryOpen(PETSC_COMM_SELF,fname,FILE_MODE_WRITE,&viewer);
    VecView(uCur,viewer);
    PetscViewerDestroy(&viewer);
    break;
  }
  free(pbuffer);
}
static void reshape(int w, int h){
   glViewport((GLsizei)w*0.1,(GLsizei)h*0.1,(GLsizei)w*0.8, (GLsizei)h*0.8);
  //glViewport((GLsizei)0,(GLsizei)0,(GLsizei)w, (GLsizei)h);
  //glGetIntegerv(GL_VIEWPORT,viewport);
   
  /***for retrieve the image***/
  windowWidth=w; windowHeight=h;
}

void setupOGL(int argc,char **argv, int width, int height){
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
  glutInitWindowSize(width, height);
  glutInitWindowPosition(-1,-1); //Let OS determine
  glutCreateWindow("CH Inpainting");
  init();
  const GLubyte* version=glGetString(GL_VERSION);
  printf("Running OpenGL version %s\n", version);
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutCreateMenu(processMenu);
  glutAddMenuEntry("timestepping(record)",0);
   glutAddMenuEntry("switch second stage",1);
  glutAddMenuEntry("two stages(non-stop)",2);
  glutAddMenuEntry("screenshot",3);
  glutAddMenuEntry("save state as petsc Vector",4);
  glutAttachMenu(GLUT_RIGHT_BUTTON);
  //glutMainLoop();
}

//color transfer function
// static void v2c(PetscScalar v, GLfloat& r, GLfloat& g, GLfloat& b){
//   r=g=b=0.0f;
//   v=(v-0.5f)*2.0f;
//   if(v>0) b=(GLfloat)v;
//   else r=(GLfloat)(-v);
// }
static void v2c2(PetscScalar v, GLfloat& r, GLfloat& g, GLfloat& b){
  r=g=b=v;
}

