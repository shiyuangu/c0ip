#pragma once
#include"FullMG.h"
class FullMGSolver:public Solver{
public:
  FullMG* pFullMG;
  int level;
  FullMGSolver(std::vector<MGSolver*>& _pMGs,int _level,PetscScalar _tol, PetscInt _maxit);
  ~FullMGSolver();
  PetscErrorCode solve(const Vec&rhs, Vec& x);
};
