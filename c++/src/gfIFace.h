/**
 * @file   gfIFace.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:23:23 2013
 * 
 * @brief  Setup OpenGL to provide User interface. 
 */

#pragma once
void setupOGL(int argc,char **argv, int width, int height);
