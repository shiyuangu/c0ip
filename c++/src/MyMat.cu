/**
 * @file   MyMat.cu
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:42:37 2013
 * 
 * @brief  Use PETSc or CUSP matrix routines depends on whether CUDA is enabled 
 * 
 * 
 */

#include<iostream>
#include"petsc.h"
#if CUDA==1
   #include "petscconf.h"
   PETSC_CUDA_EXTERN_C_BEGIN
  #include "../src/mat/impls/aij/seq/aij.h"          /*I "petscmat.h" I*/
   #include "petscbt.h"
   #include "../src/vec/vec/impls/dvecimpl.h"
   #include "private/vecimpl.h"
   PETSC_CUDA_EXTERN_C_END
   #undef VecType
   #include "../src/mat/impls/aij/seq/seqcusp/cuspmatimpl.h"
   #undef VecType
   #include "cusp/csr_matrix.h"
   #include <cusp/krylov/cg.h>
#else
   #include"petsc.h"
#endif
extern PetscLogEvent eventMyMatMult;
PetscErrorCode MyMatMult(Mat A,Vec xx, Vec yy){
  PetscErrorCode ierr;
  #if CUDA==1
  Mat_SeqAIJ     *a = (Mat_SeqAIJ*)A->data;
  PetscInt       nonzerorow=0;
  PetscBool      usecprow    = a->compressedrow.use;
  Mat_SeqAIJCUSP *cuspstruct = (Mat_SeqAIJCUSP *)A->spptr;
  CUSPARRAY      *xarray,*yarray;
  PetscFunctionBegin;
  ierr = MatCUSPCopyToGPU(A);CHKERRQ(ierr);
  ierr = VecCUSPGetArrayRead(xx,&xarray);CHKERRQ(ierr);
  ierr = VecCUSPGetArrayWrite(yy,&yarray);CHKERRQ(ierr);
  if (usecprow){ /* use compressed row format */    
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"CUSP error: compressed row is used but not implemented\n");  
  } else { /* do not use compressed row format */
    try {
      ierr=PetscLogEventBegin(eventMyMatMult,0,0,0,0);CHKERRQ(ierr);
      cusp::multiply(*cuspstruct->mat,*xarray,*yarray);
      ierr=PetscLogEventEnd(eventMyMatMult,0,0,0,0);CHKERRQ(ierr);
    } catch(char* ex) {
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"CUSP error: %s", ex);
    } 
  }
  ierr = VecCUSPRestoreArrayRead(xx,&xarray);CHKERRQ(ierr);
  ierr = VecCUSPRestoreArrayWrite(yy,&yarray);CHKERRQ(ierr);
  ierr = WaitForGPU();CHKERRCUSP(ierr);
  ierr = PetscLogFlops(2.0*a->nz - nonzerorow);CHKERRQ(ierr);
  PetscFunctionReturn(0);
  #else
    PetscFunctionBegin;
    ierr=MatMult(A,xx,yy);CHKERRQ(ierr);
    PetscFunctionReturn(0);
  #endif
  
}
