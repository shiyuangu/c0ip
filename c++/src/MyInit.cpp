/**
 * @file   MyInit.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:36:25 2013
 * 
 * @brief  Register events for PETSc logging/profiling
 * 
 * 
 */
#include"MyInit.h"
#include"gvLog.h"
PetscErrorCode MyInitialize(){
  PetscErrorCode ierr;
  ierr=PetscLogStageRegister("MyVecDotStage",&stageDebug);CHKERRQ(ierr);
  ierr=PetscLogEventRegister("MyVecDot",0,&eventMyVecDot);CHKERRQ(ierr);
  ierr=PetscLogEventRegister("MyKspSolver",0,&eventMyKspSolver);CHKERRQ(ierr);
  ierr=PetscLogEventRegister("MyMatMult",0,&eventMyMatMult);CHKERRQ(ierr);
  ierr=PetscLogEventRegister("MatMultCuspGu",0,&eventMatMultCusp);CHKERRQ(ierr);
  ierr=PetscLogEventRegister("MatMultAddCuspGu",0,&eventMatMultAddCusp);CHKERRQ(ierr);
  //ierr=PetscLogStageRegister("memory watch",&memoryWatch);CHKERRQ(ierr);
  return 0;
}
