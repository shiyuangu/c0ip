/*** Obsolete! ***/
#include<cstdio>
#include<cstdlib>
#include"hooks.h"
void PetscVecArray::release(){
  int i;
  if(size>0){
    for(i=0;i<size;i++){
      VecDestroy(pVal[i]);
    }
    delete []pVal;
    size=0;
  }
}
PetscErrorCode PetscVecArray::getFrom(const mxGuCellArrayT<mxGuSparse>& source){
  int i;
  PetscErrorCode ierr;
  if(source.size==0){
    printf("source array is of size zero. Nothing needs to be done!\n");
    return 0;
  }
  size=source.size;
  pVal=new Vec[size];
  for(i=0;i<size;i++){
    //ierr=VecCreateSeq(PETSC_COMM_SELF,source.pVal[i].nnz,&pVal[i]);CHKERRQ(ierr);
       ierr=MyVecCreate(PETSC_COMM_SELF,source.pVal[i].nnz,&pVal[i]);CHKERRQ(ierr);
    mxGuSparse2Vec(source.pVal[i],pVal[i]);
  }
  return 0;
}
PetscErrorCode PetscVecArray::getFrom(const mxGuCellArrayT<mxGuDouble>& source){
  int i;
  PetscErrorCode ierr;
  if(source.size==0){
    printf("source array is of size zero. Nothing needs to be done!\n");
    return 0;
  }
  size=source.size;
  pVal=new Vec[size];
  for(i=0;i<size;i++){
    //ierr=VecCreateSeq(PETSC_COMM_SELF,source.pVal[i].ncol*nrow,&pVal[i]);CHKERRQ(ierr);
    ierr=MyVecCreate(PETSC_COMM_SELF,source.pVal[i].ncol*nrow,&pVal[i]);CHKERRQ(ierr);
    mxGuDouble2Vec(source.pVal[i],pVal[i]);
  }
  return 0;
}
void PetscMatArray::release(){
  int i;
  if(size>0){
    for(i=0;i<size;i++)
      MatDestroy(pVal[i]);
      delete []pVal;
      size=0;
  }
}
PetscErrorCode PetscMatArray::getFrom(const mxGuCellArrayT<mxGuSparse>& source){
  int i;
  PetscErrorCode ierr;
  if(source.size==0){
    printf("source array is of size zero. Nothing needs to be done\n");
  }
  size=source.size;
  pVal=new Mat[size];
  for(i=0;i<size;i++){
    //ierr=MatCreateSeqAIJ(PETSC_COMME_SELF,source.pVal[i].nrow,source.pVal[i].ncol,PETSC_DEFAULT,PETSC_NULL,&pVal[i]);CHKERRQ(ierr);
          ierr=MyMatCreate(PETSC_COMME_SELF,source.pVal[i].nrow,source.pVal[i].ncol,PETSC_DEFAULT,PETSC_NULL,&pVal[i]);CHKERRQ(ierr);
    mxGuSparse2Mat(source.pVal[i],pVal[i]);
  }
  return 0;
}
