/**
 * @file   MatCpuGpuGu.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 01:54:36 2013
 * 
 * @brief  Define a wrapper class for CPU and GPU matrix. Two copies
 *         of a matrix is held and will be synchronized  when it is 
 *         necessary. It is only switched on when the macro MY_MATH==1.
 *         refer to predefGu.h
 */
#if CUDA
#include<cstring>
#include<iostream>
#include<cassert>
#include"MatCpuGpuGu.h"
#include "petscconf.h"
#include"gvLog.h"
PETSC_CUDA_EXTERN_C_BEGIN
#include "../src/mat/impls/aij/seq/aij.h"          /*I "petscmat.h" I*/
#include "petscbt.h"
#include "../src/vec/vec/impls/dvecimpl.h"
#include "private/vecimpl.h"
PETSC_CUDA_EXTERN_C_END
#undef VecType
#include "../src/mat/impls/aij/seq/seqcusp/cuspmatimpl.h"
#include"MatCpuGpuImplGu.h"
#undef VecType
#include<cusp/transpose.h>
#include<cusp/blas.h>
#include<cusp/multiply.h>
namespace gu{
  /*********Refer to aijcusp.cu MatMult_SeqAIJ of Petsc src for details*************/
  MatCpuGpu::MatCpuGpu(const Mat& s, DevMatType dmt):devMatType(dmt){
    const char* smt;
    PetscErrorCode ierr;
    
    ierr=MatGetType(s,&smt);CHKERRV(ierr);
    if(strcmp(smt,"seqaij")==0){
      pHostMat=new Mat;
      ierr=MatDuplicate(s, MAT_COPY_VALUES, pHostMat);CHKERRV(ierr);
      /***Implement polymorphism***/
      switch(devMatType){
      case CUSP_ELL:
        CuspEll *pDevMatCast0; 
	pDevMatCast0=NULL;
	ierr=Mat2Cusp(*pHostMat,pDevMatCast0);CHKERRV(ierr);
	pDevMat=(void*)pDevMatCast0;
	break;
      case CUSP_CSR:
	CuspCsr *pDevMatCast1; 
	pDevMatCast1=NULL;
	ierr=Mat2Cusp(*pHostMat,pDevMatCast1);CHKERRV(ierr);
	pDevMat=(void*)pDevMatCast1;
	break;
      default:
	   std::cerr<<"Cannot handle devMatType\n";
	   ierr=1;CHKERRV(ierr);
      }   

      /****create workVec in host and device******/
      pWorkVecHost=new Vec; 
      ierr=VecCreateSeq(PETSC_COMM_SELF,(*pHostMat)->rmap->n,pWorkVecHost);CHKERRV(ierr);
      try{
	pWorkVecDev=(void*)new CuspVec;
	((CuspVec *)pWorkVecDev)->resize((*pHostMat)->rmap->n);
      }catch(...){
	std::cerr<<"Error during call for cusp functions\n";ierr=1;CHKERRV(ierr);
      }
    }else{
      std::cerr<<"In MatCpuGpuGu::MatCpuGpuGu cannot handle mat type "<<smt<<std::endl;
      ierr=1;CHKERRV(ierr);
    }
  }
  void MatCpuGpu::release(){
    PetscErrorCode ierr;
    if(pHostMat!=NULL){
      ierr=MatDestroy(pHostMat);CHKERRV(ierr);
      delete pHostMat;
      pHostMat=NULL;
    }
    if(pDevMat!=NULL){
        switch(devMatType){
	 case CUSP_ELL:
	   try{
	     delete (CuspEll *)pDevMat;
	     }catch(...){
	     std::cerr<<"In MatCpuGpu::release():cannot release devMat\n"; ierr=1;CHKERRV(ierr);
	   }
	   break;
	case CUSP_CSR:
	   try{
	     delete (CuspCsr *)pDevMat;
	     }catch(...){
	     std::cerr<<"In MatCpuGpu::release():cannot release devMat\n"; ierr=1;CHKERRV(ierr);
	   }
	   break;
	 default:
	   std::cerr<<"Cannot destroy devMatType\n";
	   ierr=1;CHKERRV(ierr);
	 }
    }
    if(pWorkVecDev!=NULL){
      delete (CuspVec *)pWorkVecDev;
    }
    if(pWorkVecHost!=NULL){
      ierr=VecDestroy(pWorkVecHost);CHKERRV(ierr);
    }
    
  }
  /*******Non-member Functions******/
  PetscErrorCode MatTranspose(const MatCpuGpu& s,MatReuse reuse,MatCpuGpu *t){
    PetscErrorCode ierr;
    if(s.pHostMat==NULL||s.pDevMat==NULL){
      SETERRQ(PETSC_COMM_SELF,1,"the source is empty!\n");
    }
    switch(reuse){
    case MAT_INITIAL_MATRIX:
      if(t->pHostMat==NULL && t->pDevMat==NULL){
	t->pHostMat=new Mat;
	ierr=MatTranspose(*(s.pHostMat),MAT_INITIAL_MATRIX,t->pHostMat);CHKERRQ(ierr);
	t->devMatType=s.devMatType;
	switch(t->devMatType){
	case CUSP_ELL:
	  try{
	    t->pDevMat=(void*)(new CuspEll);
	    cusp::transpose(*((CuspEll*)(s.pDevMat)),*((CuspEll*)(t->pDevMat)));
	  }catch(...){
	    SETERRQ(PETSC_COMM_SELF,1,"error in CUSP operatrion!\n");
	  }
	  break;
	case CUSP_CSR:
	  try{
	    t->pDevMat=(void*)(new CuspCsr);
	    cusp::transpose(*((CuspCsr*)(s.pDevMat)),*((CuspCsr*)(t->pDevMat)));
	  }catch(...){
	    SETERRQ(PETSC_COMM_SELF,1,"error in CUSP operatrion!\n");
	  }
	default:
	  SETERRQ(PETSC_COMM_SELF,1,"cannot handle DevMatType");
	}
	
      }else{
	SETERRQ(PETSC_COMM_SELF,1,"the target is not empty!\n");
      }
      break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"MatTranspose cannot handle resue type!\n");
    }
    return 0; 
  }
  PetscErrorCode MatMult(const MatCpuGpu& mat, const Vec& in, Vec& out){
    Mat_SeqAIJ     *a = (Mat_SeqAIJ*)((*(mat.pHostMat))->data);
    CuspVec *inVec, *outVec;
    PetscErrorCode ierr;
#if defined(PETSC_USE_DEBUG)
    const char* vecType;
    ierr=VecGetType(in, &vecType);CHKERRQ(ierr);
    assert(!strcmp(vecType,VECSEQCUSP));
    ierr=VecGetType(out, &vecType);CHKERRQ(ierr);
    assert(!strcmp(vecType,VECSEQCUSP));
#endif
    ierr=PetscLogEventBegin(eventMatMultCusp,0,0,0,0);CHKERRQ(ierr);
    ierr=VecCUSPGetArrayRead(in,&inVec);CHKERRQ(ierr);
    ierr=VecCUSPGetArrayWrite(out,&outVec);CHKERRQ(ierr);
    try{
      switch(mat.devMatType){
      case CUSP_ELL:
	cusp::multiply(*((CuspEll *)mat.pDevMat),*inVec,*outVec);
	break;
      case CUSP_CSR:
	cusp::multiply(*((CuspCsr *)mat.pDevMat),*inVec,*outVec);
	break;
      default:
	throw "the devMatType is not implemented!\n";
	
      }
    }catch(char* ex){
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"CUSP error:%s",ex);
    }
    ierr=VecCUSPRestoreArrayRead(in,&inVec);CHKERRQ(ierr);
    ierr=VecCUSPRestoreArrayWrite(out,&outVec);CHKERRQ(ierr);
    ierr = WaitForGPU();CHKERRCUSP(ierr);
    ierr = PetscLogFlops(2.0*a->nz);CHKERRQ(ierr);
    ierr=PetscLogEventEnd(eventMatMultCusp,0,0,0,0);CHKERRQ(ierr);
    return 0;
  }
  PetscErrorCode MatMultAdd(const MatCpuGpu& mat, const Vec& in0, const Vec& in1, Vec& out){
    /***the following part is only for debug****/
    /***should be commented out for performance**/
    PetscErrorCode ierr;
    Mat_SeqAIJ     *a = (Mat_SeqAIJ*)((*(mat.pHostMat))->data);
    CuspVec *inVec0, *inVec1, *outVec,*pWVDev;
#if defined(PETSC_USE_DEBUG)
    const char* vecType;
    ierr=VecGetType(in0, &vecType);CHKERRQ(ierr);
    assert(!strcmp(vecType,VECSEQCUSP));
    ierr=VecGetType(in1, &vecType);CHKERRQ(ierr);
    assert(!strcmp(vecType,VECSEQCUSP));
    ierr=VecGetType(out, &vecType);CHKERRQ(ierr);
    assert(!strcmp(vecType,VECSEQCUSP));
    assert(mat.pWorkVecDev!=NULL);
#endif
    
    pWVDev=(CuspVec *)mat.pWorkVecDev;  
    ierr=PetscLogEventBegin(eventMatMultAddCusp,0,0,0,0);CHKERRQ(ierr);
    ierr=VecCUSPGetArrayRead(in0,&inVec0);CHKERRQ(ierr);
    ierr=VecCUSPGetArrayRead(in1,&inVec1);CHKERRQ(ierr);
    ierr=VecCUSPGetArrayWrite(out,&outVec);CHKERRQ(ierr);
    try{
      switch(mat.devMatType){
      case CUSP_ELL:
	cusp::multiply(*((CuspEll *)mat.pDevMat),*inVec0,*pWVDev);
	break;
      case CUSP_CSR:
	cusp::multiply(*((CuspCsr *)mat.pDevMat),*inVec0,*pWVDev);
	break;
      default:
	throw "devMatType is not implemented\n";
      }
      cusp::blas::axpby(*pWVDev,*inVec1,*outVec,1.0,1.0);
    }catch(char* ex){
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"CUSP error:%s",ex);
    }
    ierr=VecCUSPRestoreArrayRead(in0,&inVec0);CHKERRQ(ierr);
    ierr=VecCUSPRestoreArrayRead(in1,&inVec1);CHKERRQ(ierr);
    ierr=VecCUSPRestoreArrayWrite(out,&outVec);CHKERRQ(ierr);
    ierr = WaitForGPU();CHKERRCUSP(ierr);
    
    //addition is not counted. 
    ierr = PetscLogFlops(2.0*a->nz);CHKERRQ(ierr);
    ierr=PetscLogEventEnd(eventMatMultAddCusp,0,0,0,0);CHKERRQ(ierr);
    return 0;
  }
  PetscErrorCode MatAXPY(MatCpuGpu& t, PetscScalar a, const MatCpuGpu& s, MatStructure str){
    PetscErrorCode ierr;
    ierr=MatAXPY(*(t.pHostMat),a, *(s.pHostMat),str);CHKERRQ(ierr);
    switch(t.devMatType){
    case CUSP_ELL:
      CuspEll * pDevMatCast0;
      pDevMatCast0=(CuspEll*)t.pDevMat;
      ierr=Mat2Cusp(*(t.pHostMat),pDevMatCast0);CHKERRQ(ierr);
      t.pDevMat=(void*)pDevMatCast0;
      break;
    case CUSP_CSR:
      CuspCsr * pDevMatCast1;
      pDevMatCast1=(CuspCsr*)t.pDevMat;
      ierr=Mat2Cusp(*(t.pHostMat),pDevMatCast1);CHKERRQ(ierr);
      t.pDevMat=(void*)pDevMatCast1;
      break;
    default:
      SETERRQ(PETSC_COMM_SELF,1,"MatAXPY cannot handle matrix type\n");
    }
    return 0;
  }
  PetscErrorCode MatGetVecs(const MatCpuGpu& mat, Vec* right, Vec* left){
    PetscErrorCode ierr;
    PetscInt ncol, nrow;
    if(mat.pHostMat==NULL){ierr=1; CHKERRQ(ierr);}
    nrow=(*(mat.pHostMat))->rmap->n; ncol=(*(mat.pHostMat))->cmap->n;

    if(right!=PETSC_NULL){
      ierr=VecCreate(PETSC_COMM_SELF,right);CHKERRQ(ierr);
      ierr=VecSetSizes(*right,PETSC_DECIDE,ncol);CHKERRQ(ierr);
      ierr=VecSetType(*right,VECSEQCUSP); CHKERRQ(ierr);
    }
    if(left!=PETSC_NULL){
      ierr=VecCreate(PETSC_COMM_SELF,left);CHKERRQ(ierr);
      ierr=VecSetSizes(*left,PETSC_DECIDE,nrow);CHKERRQ(ierr);
      ierr=VecSetType(*left,VECSEQCUSP); CHKERRQ(ierr);
    }
    return 0;
  }
  PetscErrorCode innerProduct(const MatCpuGpu& mat, Vec x, Vec y, Vec workVec,PetscScalar* pVal){
    const char *xType;
    const char *yType;
    const char *wvType;
    CuspVec *px, *py, *pwv;
    PetscErrorCode ierr;
    ierr=VecGetType(x, &xType);CHKERRQ(ierr);
    ierr=VecGetType(y,&yType);CHKERRQ(ierr);
    ierr=VecGetType(workVec,&wvType);CHKERRQ(ierr);
    if(strcmp(xType, VECSEQCUSP)==0 && strcmp(yType, VECSEQCUSP)==0 && strcmp(xType, VECSEQCUSP)==0){
      ierr=VecCUSPGetArrayRead(x,&px);CHKERRQ(ierr);
      ierr=VecCUSPGetArrayRead(y,&py);CHKERRQ(ierr);
      ierr=VecCUSPGetArrayWrite(workVec,&pwv);CHKERRQ(ierr);
      try{
	switch(mat.devMatType){
	case CUSP_ELL:
	  cusp::multiply(*((CuspEll*)mat.pDevMat),*px,*pwv);
	  break;
	case CUSP_CSR:
	  cusp::multiply(*((CuspCsr*)mat.pDevMat),*px,*pwv);
	  break;
	default:
	  throw "DevMatType is not implemented\n";
	}
	*pVal=cusp::blas::dot(*pwv,*py);
      }catch(char* ex){
        SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"CUSP error:%s",ex);
      }
      ierr=VecCUSPRestoreArrayRead(x,&px);CHKERRQ(ierr);
      ierr=VecCUSPRestoreArrayRead(y,&py);CHKERRQ(ierr);
      ierr=VecCUSPRestoreArrayWrite(workVec,&pwv);CHKERRQ(ierr);
    }else{
      ierr=MatMult(*(mat.pHostMat),x,workVec);CHKERRQ(ierr);
      ierr=VecDot(workVec,y,pVal);CHKERRQ(ierr);
    }
    return 0; 
  }
  PetscErrorCode MatGetSize(const MatCpuGpu& mat, PetscInt *nrow, PetscInt *ncol){
    PetscErrorCode ierr;
    ierr=MatGetSize(*(mat.pHostMat),nrow, ncol);CHKERRQ(ierr);
    return 0;
  }
}
#endif
