/**
 * @file   FullMG.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 13:52:09 2013
 * 
 * @brief  connect multigrid cycles to form full multigrid solve.  
 * 
 * 
 */
#include<limits>
#include"FullMG.h"
#include"others.h"
#include"MatCpuGpuGu.h"
FullMG::FullMG(std::vector<MGSolver*>& spMGs,PetscScalar _tol,PetscInt _maxIter):pMGs(spMGs),tol(_tol),maxIter(_maxIter){
  size_t ksize=pMGs.size();
  size_t i;
  PetscErrorCode ierr;
  workVecs.reserve(ksize);
  rhs.resize(ksize);
  for(i=0;i<ksize;i++){
    workVecs.push_back(std::vector<Vec>(3));
    ierr=MatGetVecs(*(pMGs[i]->pK),&(workVecs[i][0]),&(rhs[i]));CHKERRV(ierr);
    ierr=MatGetVecs(*(pMGs[i]->pK),&(workVecs[i][1]),PETSC_NULL);CHKERRV(ierr);
    ierr=MatGetVecs(*(pMGs[i]->pK),&(workVecs[i][2]),PETSC_NULL);CHKERRV(ierr);
  }
}
FullMG::~FullMG(){
  size_t ksize,i;
  PetscErrorCode ierr;
  ksize=workVecs.size();
  for(i=0;i<ksize;i++){
    ierr=VecDestroy(&(workVecs[i][0]));CHKERRV(ierr);
    ierr=VecDestroy(&(workVecs[i][1]));CHKERRV(ierr);
    ierr=VecDestroy(&(workVecs[i][2]));CHKERRV(ierr);
    ierr=VecDestroy(&(rhs[i]));CHKERRV(ierr);
  }
}

PetscErrorCode FullMG::solve(Vec _rhs, Vec sol,int level){
  int i,k,counter,curIter,preIter;
  PetscErrorCode ierr;
  PetscScalar errEnorm,rhsNorm,xEnorm,ratio;
  ierr=VecNorm(_rhs,NORM_2,&rhsNorm);
  if(rhsNorm<std::numeric_limits<PetscReal>::epsilon()){
    printf("warning: rhs is also zeros, set the solution to zeros without any solve\n");
    ierr=VecZeroEntries(sol);CHKERRQ(ierr);
    return 0;
  }else{
    printf("||rhs||_2=%#.3e\n",rhsNorm);
  }
  ierr=VecCopy(_rhs,rhs[level]);CHKERRQ(ierr);
  //ierr=VecScale(rhs[level],1.0/rhsNorm);CHKERRQ(ierr);
  for(i=level-1;i>=0;i--){
    ierr=MatMult(*(pMGs[i+1]->pMGData->pIt),rhs[i+1],rhs[i]);CHKERRQ(ierr);
  }
  
  ierr=pMGs[0]->solve(rhs[0],workVecs[0][0]);CHKERRQ(ierr);
  curIter=0;
  for(k=1;k<=level;k++){
    ierr=MatMult(*(pMGs[k]->pMGData->pI),workVecs[k-1][curIter],workVecs[k][0]);CHKERRQ(ierr);
    curIter=0;counter=0;ratio=1.0; 
    while(ratio>tol && counter<maxIter){
      preIter=curIter; curIter=(curIter+1)%2; counter++;
      ierr=pMGs[k]->solve(rhs[k],workVecs[k][preIter],workVecs[k][curIter]);CHKERRQ(ierr);
      ierr=VecAXPY(workVecs[k][preIter],-1.0,workVecs[k][curIter]);CHKERRQ(ierr);
      ierr=innerProduct(*(pMGs[k]->pMGData->pA),workVecs[k][preIter],workVecs[k][preIter],workVecs[k][2],&errEnorm);CHKERRQ(ierr);errEnorm=sqrt(errEnorm);
      ierr=innerProduct(*(pMGs[k]->pMGData->pA),workVecs[k][curIter],workVecs[k][curIter],workVecs[k][2],&xEnorm);CHKERRQ(ierr);xEnorm=sqrt(xEnorm);
      ratio=errEnorm/xEnorm;
      printf("k=%d,iter=%d,errEnorm=%#.3e,xEnorm=%#.3e,ratio=%#.3e\n",k,counter,errEnorm,xEnorm,ratio);
    }
  }
  //printf("k=%d,iter=%d(on k-level),errEnorm=%#.3e\n",level,counter,errEnorm);
  ierr=VecCopy(workVecs[level][curIter],sol);CHKERRQ(ierr);
  //ierr=VecScale(sol,rhsNorm);CHKERRQ(ierr);
  return 0;
}
