/**
 * @file   others.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:48:20 2013
 * 
 * @brief  Convenient algebra operations. It also use MyXXX hooks   
 *         for debugging/profiling purpose. 
 * 
 */
#include<cmath>
#include"others.h"
#include"MyVec.h"
PetscErrorCode proj(const Vec& phi, const Vec& vB,
		    const Vec& inVec, Vec workVec,Vec& outVec){
  //projection the vector inVec to the subspace orthogonal 
  //to the vector phi w.r.t the inner product represented
  //the diagnoal matrix vBi(represented by vector)
  PetscScalar alpha1,alpha2,alpha;
  PetscErrorCode ierr;
  ierr=VecPointwiseMult(workVec,inVec,vB);CHKERRQ(ierr);
  ierr=MyVecDot(workVec,phi,&alpha1);CHKERRQ(ierr);
  ierr=VecPointwiseMult(workVec,phi,vB);CHKERRQ(ierr);
  ierr=MyVecDot(workVec,phi,&alpha2);CHKERRQ(ierr);
  alpha=-1.0*alpha1/alpha2;
  ierr=VecWAXPY(outVec,alpha,phi,inVec);CHKERRQ(ierr);
  return 0; 
}
PetscErrorCode shiftOrth(const Vec& phi, const Vec& vB,
		       Vec workVec,Vec& r){//shift r to make it orthogonal to Phi w.r.t vB
  PetscErrorCode ierr;
  PetscScalar alpha1,alpha2,alpha;
  ierr=VecPointwiseMult(workVec,r,vB);CHKERRQ(ierr);
  ierr=MyVecDot(workVec,phi,&alpha1);CHKERRQ(ierr);
  ierr=MyVecDot(phi,vB,&alpha2);CHKERRQ(ierr);
  alpha=-1.0*alpha1/alpha2;
  ierr=VecShift(r,alpha);CHKERRQ(ierr);
  return 0;
}
PetscErrorCode innerProduct(Mat A,Vec x, Vec y,Vec workVec,PetscScalar* pVal){
  PetscErrorCode ierr;
  ierr=MatMult(A,x,workVec);CHKERRQ(ierr);
  ierr=MyVecDot(workVec,y,pVal);CHKERRQ(ierr);
  return 0;
}
PetscErrorCode VecAbsMaxGu(Vec x, PetscInt* pIdx, PetscScalar* pVal){
  //find the absmax component of xn
  //not efficient implementation
PetscScalar* array;
PetscInt *idx,size,i;
PetscErrorCode ierr;
 VecGetSize(x,&size);
 idx=new PetscInt[size];array=new PetscScalar[size];
 for(i=0;i<size;i++) idx[i]=i;
 ierr=VecGetValues(x,size,idx,array);CHKERRQ(ierr);
 *pIdx=0;*pVal=array[0];
 for(i=1;i<size;i++){
   if(fabs(array[i])>fabs(*pVal)){
     *pIdx=i; *pVal=array[i];
   }
 }
 PetscLogFlops(size);
 delete []idx; delete []array;
 return 0; 
}  
namespace gu{
  //subtract a multiple of v1 from inVec to make it orth. to v2 w.r.t.
  //the usual inner product.   
  PetscErrorCode subtV1orthV2(Vec inVec, Vec outVec, Vec v1, Vec v2){
    PetscScalar c1,c2,c;
    PetscErrorCode ierr;
    ierr=MyVecDot(inVec,v2,&c1);CHKERRQ(ierr);
    ierr=MyVecDot(v1,v2,&c2);CHKERRQ(ierr);
    c=c1/c2;
    ierr=VecAXPBYPCZ(outVec,1.0,-1.0*c,0.0,inVec,v1);CHKERRQ(ierr);
    return 0;
  }
}
