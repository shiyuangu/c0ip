/**
 * @file   PetscArrayGu.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:49:11 2013
 * 
 * @brief  convert mx format(which is generated by splitConvertMat.c from Matlab .mat format) to PETSc format. 
 *   
 * 
 */
#include<cstdio>
#include<cstdlib>
#include"mxGu.h"
#include"PetscArrayGu.h"
#include"converters.h"
#include"auxiliary.h"
#include"hooks.h"
void PetscVecArray::release(){
  int i;
  if(size>0){
    for(i=0;i<size;i++){
      VecDestroy(&pVal[i]);
    }
    delete []pVal;pVal=NULL;
    size=0;
  }
}
PetscErrorCode PetscVecArray::getFrom(const mxGuCellArrayT<mxGuSparse>& source){
  int i;
  PetscErrorCode ierr;
  if(source.size==0){
    printf("source array is of size zero. Nothing needs to be done!\n");
    return 0;
  }
  size=source.size;
  pVal=new Vec[size];
  for(i=0;i<size;i++){
    //ierr=VecCreateSeq(PETSC_COMM_SELF,source.pVal[i].nnz,&pVal[i]);CHKERRQ(ierr);
       ierr=MyVecCreate(PETSC_COMM_SELF,source.pVal[i].nnz,&pVal[i]);CHKERRQ(ierr);
    mxGuSparse2Vec(source.pVal[i],pVal[i]);
  }
  return 0;
}
PetscErrorCode PetscVecArray::getFrom(const mxGuCellArrayT<mxGuDouble>& source){
  int i;
  PetscErrorCode ierr;
  if(source.size==0){
    printf("source array is of size zero. Nothing needs to be done!\n");
    return 0;
  }
  size=source.size;
  pVal=new Vec[size];
  for(i=0;i<size;i++){
    //ierr=VecCreateSeq(PETSC_COMM_SELF,source.pVal[i].ncol*source.pVal[i].nrow,&pVal[i]);CHKERRQ(ierr);
       ierr=MyVecCreate(PETSC_COMM_SELF,source.pVal[i].ncol*source.pVal[i].nrow,&pVal[i]);CHKERRQ(ierr);
    ierr=mxGuDouble2Vec(source.pVal[i],pVal[i]);CHKERRQ(ierr);
  }
  return 0;
}
void PetscMatArray::release(){
  int i;
  if(size>0){
    for(i=0;i<size;i++)
      MatDestroy(&pVal[i]);
      delete []pVal;
      size=0;pVal=NULL;
  }
}
PetscErrorCode PetscMatArray::getFrom(const mxGuCellArrayT<mxGuSparse>& source){
  int i;
  PetscErrorCode ierr;
  if(source.size==0){
    printf("source array is of size zero. Nothing needs to be done\n");
  }
  size=source.size;
  pVal=new Mat[size];
  for(i=0;i<size;i++){
    //ierr=MatCreateSeqAIJ(PETSC_COMM_SELF,source.pVal[i].nrow,source.pVal[i].ncol,50,PETSC_NULL,&pVal[i]);CHKERRQ(ierr);
          ierr=MyMatCreate(PETSC_COMM_SELF,source.pVal[i].nrow,source.pVal[i].ncol,50,PETSC_NULL,&pVal[i]);CHKERRQ(ierr);
    mxGuSparse2Mat(source.pVal[i],pVal[i]);
    //if(i==1)output(pVal[i],"viewer.m");
  }
  return 0;
}

PetscErrorCode PetscMatArray::getFromTranspose(const PetscMatArray& s){
  PetscErrorCode ierr;
  size=s.size;
  pVal=new Mat[size];
  for(int i=0;i<size;i++){
    ierr=MatTranspose(s.pVal[i],MAT_INITIAL_MATRIX,&(pVal[i]));CHKERRQ(ierr);
  } 
  return 0;
}
