/**
 * @file   comparison.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 15:06:58 2013
 * 
 * @brief  compare the performace two precoditioner. 
 */
#pragma once
#include<cmath>
#include"petsc.h"
#include"Solver.h"
#include"others.h"
PetscErrorCode compare(PC& pc1, PC& pc2, const KSPType ksptype,Mat K, MatNullSpace nsp, Vec phi,Vec vB,int nRun,
		       double its0[], double time0[], double rErr0[],
		       double its1[], double time1[], double rErr1[]);
PetscErrorCode kspMonitorGu(KSP ksp, PetscInt it, PetscReal rnorm, void * ctx);
PetscErrorCode kspConvgTest(KSP ksp,PetscInt it,PetscReal rnorm,KSPConvergedReason* pReason,void* spKspCtx);

//ksp context:for both monitor and convergence
class KspCtxGu{
public:
  Vec x;  //exact solution
  Vec residual,cS;
  Vec workVec[2];
  Mat K; 
  PetscScalar xEnorm; //energy 
  KspCtxGu(Mat &sK){
    MatDuplicate(sK,MAT_COPY_VALUES,&K);  
    MatGetVecs(K,&x,PETSC_NULL);
    // VecDuplicate(x,&residual);
    VecDuplicate(x,&workVec[0]);
    VecDuplicate(x,&workVec[1]);
  }
  ~KspCtxGu(){
    MatDestroy(&K);
    VecDestroy(&x);
    // VecDestroy(residual);
    VecDestroy(&workVec[0]);
    VecDestroy(&workVec[1]);
  }
  PetscErrorCode copy(Vec sx){
    PetscErrorCode ierr;
    ierr=VecCopy(sx,x);CHKERRQ(ierr);
    ierr=innerProduct(K,x,x,workVec[0],&xEnorm);CHKERRQ(ierr);
    xEnorm=sqrt(xEnorm);
    return 0;
  }
};
