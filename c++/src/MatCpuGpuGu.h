/**
 * @file   MatCpuGpuGu.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 01:54:36 2013
 * 
 * @brief  Define a wrapper class for CPU and GPU matrix. Two copies
 *         of a matrix is held and will be synchronized  when it is 
 *         necessary. It is only switched on when the macro MY_MATH==1.
 *         refer to predefGu.h
 */
#if CUDA 
#pragma once
#include"petsc.h"
namespace gu{
  enum DevMatType {X,CUSP_ELL,CUSP_CSR};
  /******MatCpuGpu*****/
  class MatCpuGpu{
  public:
    Mat* pHostMat;
    DevMatType devMatType;
    void* pDevMat;
    Vec* pWorkVecHost; 
    void* pWorkVecDev;   //will be cast to CuspVec in .cu; to avoid spawning cusp 
    MatCpuGpu(){devMatType=X; pHostMat=NULL; pDevMat=NULL;pWorkVecDev=NULL;pWorkVecHost=NULL;}
    MatCpuGpu(const Mat& s,DevMatType dmt=CUSP_ELL);
    ~MatCpuGpu(){release();}
  private:
    void release();
  };

  /*******Non-member Functions******/
  PetscErrorCode MatTranspose(const MatCpuGpu& s,MatReuse reuse,MatCpuGpu *t);
  PetscErrorCode MatMult(const MatCpuGpu& mat, const Vec& in, Vec& out);
  PetscErrorCode MatMultAdd(const MatCpuGpu& mat, const Vec& in0, const Vec& in1, Vec& out);
  PetscErrorCode MatAXPY(MatCpuGpu& t, PetscScalar a, const MatCpuGpu& s, MatStructure str);
  PetscErrorCode MatGetVecs(const MatCpuGpu& mat, Vec* right, Vec* left);
  PetscErrorCode MatGetSize(const MatCpuGpu& mat, PetscInt *nrow, PetscInt *ncol);
  PetscErrorCode innerProduct(const MatCpuGpu& mat, Vec x,  Vec y, Vec workVec, PetscScalar* pVal);
}
#endif
