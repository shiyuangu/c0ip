/**
 * @file   Solver.h
 * @author S. Gu <sgu@anl.gov>
 * @date   Sun Jun 23 14:55:12 2013
 * 
 * @brief  Solver interface
 * 
 * 
 */
#pragma once
#include"petsc.h"
#include "predefGu.h"
class Solver{
public:
  Solver(const MyMat* spK):pK(spK){}
  virtual ~Solver(){}
  const MyMat* const pK;
  virtual PetscErrorCode solve(const Vec& rhs,Vec& output)=0; 
};
