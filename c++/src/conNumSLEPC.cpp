/**
 * @file   conNumSLEPC.cpp
 * @author S. Gu <sgu@anl.gov>
 * @date   Fri Jun 21 12:40:16 2013
 * 
 * @brief  compute the contraction numbers of multigrid solvers. 
 *         if CUDA is enabled, the GPU matrix format can be chosen
 *         by the runtime options -gu_mymat=cuspcsr/cuspell. 
 */
#include<cstdio>
#include<cstdlib>
#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"slepceps.h"
#include"petsc.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
//#include"gv.h"
#include"gf.h"
#include"gfMG.h"
#include"SolverErr.h"
#include"comparison.h"
#include"others.h"
#include"auxiliary.h"
#include"hooks.h"
#include"predefGu.h"
#include"MyInit.h"
#include"BHMG.h"
//PetscMatArray *pAs,*pSs,*pIs,*pIts, *pMs;
//PetscMatArray *pAcMs,*pScMs,*pIs,*pIts, *pMs;
//PetscVecArray *pvBs,*pvBinvs,*pPhi;
//Mesh2Array mesh2es;
//double *lambda1, *lambda2;   //the damping factors;
//double c1,c2;   //for \Delta^ u- c2\Delta u+c1 u
//double c3;      //for the preconditioner -\Delta u+c3 u
BHMG* pBHMG; 
//int maxlevel=0;
//MGData *pMGData=NULL;

//DiagProjSolver *pDiagProjSolver;
//Solver *pCSolverBH, *pCSolverNeu;
//MGSolver *pMGBH=NULL,*pMGNeu=NULL;
//std::vector<MGData*> pMGDataVec;
//std::vector<MGSolver*> pMGBHs;
//std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory 

//parameters to control the MG 
//char MGBHMode='W',PrecMode='V';
//PetscInt mMGNeu=5;   //presmoothing/postsmoothing step for Neu MG problem 
//double dampingScale=0.55; //the damping factors are the estimated spectral radii times damping scales

//for swtiches
//PetscBool myKspSolverOn;

//parameters to control the testing level and smoothing steps. 
PetscInt slevel,elevel,sizeks;       
//PetscTruth mlogscale;
PetscBool mlogscale;
PetscReal mstartlog,mendlog;
PetscInt msize;
PetscInt mstart=6, mend=36,mskip=2,sizems; 
ConNumkm *pConNumkm;

//for profiling
PetscLogEvent eventMyVecDot,eventMyMatMult,eventMatMultCusp, eventMatMultAddCusp;;
PetscLogStage stageDebug,memoryWatch;
PetscLogEvent eventMyKspSolver;

///For EPS eigen solver
EPS eps;
const EPSType type;
PetscInt nev,maxit,nconv,epsits;    //# of requested eigenvalues, max #iterations,#converged eigen-pairs
PetscScalar eigvr,eigvi;
PetscReal tol,re,im,errEPS; 
Vec eigVecr,eigVeci;
PetscErrorCode ErrMatMult(Mat K,Vec x,Vec y){
  void *ctx;
  ErrExt *pErrExt;
  PetscErrorCode ierr;
  MatShellGetContext(K,&ctx);
  pErrExt=(ErrExt*)ctx;
  ierr=pErrExt->error(x,y);CHKERRQ(ierr);
  return 0;
}

int main(int argc, char**argv){
  int l,k,m;
  PetscInt kCounter,mCounter;
  SolverErr *pMGErrBH;
  ErrExt* pErrExt;
  Vec workVec[6],x0,outVec;
  PetscScalar tmp1,tmp2,err,tol;
  PetscScalar mgErrNorm; //This is the norm of error, NOT the dampingfactor. 
  PetscRandom randomctx; 
  PetscErrorCode ierr;
  //PetscTruth flag;
  PetscBool flag;
  bool flag2; 
  char outfname[256]; int tmp; //only for output result
  char infname1[256]; std::string infname2;
  char xfname[256], rhsfname[256];
  size_t pos;   //for exact only the file name(not the path in argv[1])
  Mat ErrMat;   //Petsc Mat for Error Operator 
  Vec phiCopy;
  if(argc<=1){
     printf("Usage: conNumSLEPC <matfile_D> <output directory> <-options>\n");
    promptOps();
    return 0; 
  }
    //PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
    SlepcInitialize(&argc,&argv,(char*)0,(char*)0);
    // readMGOpts(); 
    // pAs=new PetscMatArray; pSs=new PetscMatArray;pMs=new PetscMatArray;
    // pIs=new PetscMatArray;pIts=new PetscMatArray;
    // pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;
    pMGErrBH=new SolverErr;       
    // if(read(argv[1])){
    //   PetscEnd();
    //   exit(1);
    // }
    //  genMG();
    pBHMG=new BHMG(argv[1]);
    

    /****read in test level and smoothing steps***/
    PetscOptionsGetInt(PETSC_NULL,"-gu_slevel",&slevel,&flag);
    if(!flag)slevel=1;
    printf("-gu_slevel=%d\n",slevel);
    PetscOptionsGetInt(PETSC_NULL,"-gu_elevel",&elevel,&flag);
    if(!flag){
      if(elevel<0||elevel>pBHMG->maxlevel-1){
        fprintf(stderr,"invalid -gu_elevel,reset to %d\n",pBHMG->maxlevel-1);
      }
      elevel=pBHMG->maxlevel-1;
    }
    printf("-gu_elevel=%d\n",elevel);
    PetscOptionsHasName(PETSC_NULL,"-gu_mlogscale",&mlogscale);
    if(!mlogscale){
      PetscOptionsGetInt(PETSC_NULL,"-gu_mstart",&mstart,&flag);
      if(!flag)mstart=6;
      PetscOptionsGetInt(PETSC_NULL,"-gu_mend",&mend,&flag);
      if(!flag)mend=mstart+1;
      PetscOptionsGetInt(PETSC_NULL,"-gu_mskip",&mskip,&flag);
      if(!flag)mskip=1;
      printf("mstart=%d,mend=%d,mskip=%d\n",mstart,mend,mskip);
    }else{
      PetscOptionsGetReal(PETSC_NULL,"-gu_mstartlog",&mstartlog,&flag);
      if(!flag)mstartlog=1.0;
      PetscOptionsGetReal(PETSC_NULL,"-gu_mendlog",&mendlog,&flag);
      if(!flag)mendlog=3.0;
      PetscOptionsGetInt(PETSC_NULL,"-gu_msize",&msize,&flag);
      if(!msize)msize=20;
      printf("mstartlog=%g,mendlog=%g,msize=%d\n",mstartlog,mendlog,msize);
    }
  
   
    
    //////////////generator the testing levels, smoothing steps/////////////
    if(!mlogscale){
      sizems=(mend-mstart+mskip-1)/mskip;
      sizeks=elevel-slevel+1;
      pConNumkm=new ConNumkm(sizeks,sizems);
      kCounter=0;for(k=slevel;k<=elevel;k++) pConNumkm->ks[kCounter++]=k;
      mCounter=0;for(m=mstart;m<mend;m+=mskip) pConNumkm->ms[mCounter++]=m;
    } else{
      sizems=msize;
      sizeks=elevel-slevel+1;
      pConNumkm=new ConNumkm(sizeks,sizems);
      kCounter=0;for(k=slevel;k<=elevel;k++) pConNumkm->ks[kCounter++]=k;
      if(msize>1){
	mCounter=0;
	for(m=0;m<msize;m++) 
	  pConNumkm->ms[mCounter++]=(pow(10,mstartlog+(mendlog-mstartlog)/(msize-1)*m)+0.5);
      }
      else pConNumkm->ms[0]=pow(10,mstartlog)+0.5;
    }
    ///////////////////////////////////////////////////////////////////
    /////////////compute contration numbers/////////////////////////////
    
    PetscRandomCreate(PETSC_COMM_SELF,&randomctx);
    PetscRandomSetType(randomctx,PETSCRAND48);//?
    PetscRandomSetInterval(randomctx,-1.0,1.0);
    for(kCounter=0;kCounter < pConNumkm->sizeks;kCounter++){
      k=pConNumkm->ks[kCounter];
      ///// set or reset the Error Operator 
      // pMGErrBH->set(pMGBHs[k]);
      pMGErrBH->set(pBHMG->pMGBHs[k]);
       ///////////////Set up the ErrOperator///////////////
      if(fabs(pBHMG->c1)==0.0){
	pErrExt=new ErrExt(pMGErrBH,pBHMG->pMGDataVec[k]->pPhi,pBHMG->pMGDataVec[k]->pvB);
	VecDuplicate(*(pErrExt->pPhi),&phiCopy);//VecCopy(*(pErrExt->pPhi),phiCopy);
        //This is added in 04/14/2011; this is missing in the previous version;
        ierr=VecPointwiseMult(phiCopy,*(pErrExt->pvB),*(pErrExt->pPhi));CHKERRQ(ierr);//used in EPSSetDeflationSpace 
      }
      else 
      pErrExt=new ErrExt(pMGErrBH,PETSC_NULL,pBHMG->pMGDataVec[k]->pvB);     
      ierr=MatCreateShell(PETSC_COMM_SELF,pBHMG->pMGDataVec[k]->pMesh2->nGI,pBHMG->pMGDataVec[k]->pMesh2->nGI,
			  PETSC_DETERMINE,PETSC_DETERMINE,
			  (void*)pErrExt,&ErrMat);CHKERRQ(ierr);
      ierr=MatSetFromOptions(ErrMat);CHKERRQ(ierr);
      ierr=MatShellSetOperation(ErrMat,MATOP_MULT,(void(*)())(&ErrMatMult));CHKERRQ(ierr);
      ierr=MatShellSetOperation(ErrMat,MATOP_MULT_TRANSPOSE,(void(*)())(&ErrMatMult));CHKERRQ(ierr);
       
      ierr=EPSCreate(PETSC_COMM_SELF,&eps);CHKERRQ(ierr);
      ierr=EPSSetOperators(eps,ErrMat,PETSC_NULL);CHKERRQ(ierr);
      //ierr=EPSSetProblemType(eps,EPS_HEP);CHKERRQ(ierr);//???Check whether is true
      ierr=EPSSetDimensions(eps,1,PETSC_DECIDE,PETSC_DECIDE);CHKERRQ(ierr);
      ierr=EPSSetWhichEigenpairs(eps,EPS_LARGEST_MAGNITUDE);CHKERRQ(ierr);
      ierr=EPSSetType(eps,EPSPOWER);CHKERRQ(ierr);
      if(fabs(pBHMG->c1)==0.0){
	ierr=EPSSetDeflationSpace(eps,1,&phiCopy);//This looks problematic, 
      }
      ierr=EPSSetFromOptions(eps);CHKERRQ(ierr);
      
      //// set or reset workVec
      for(l=0;l<6;l++){
	//VecCreateSeq(PETSC_COMM_SELF,pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));
	   MyVecCreate(PETSC_COMM_SELF,pBHMG->pMGDataVec[k]->pMesh2->nGI,&(workVec[l]));
      }
    
      VecDuplicate(workVec[0],&x0);
      VecDuplicate(x0,&eigVecr);VecDuplicate(x0,&eigVeci);

      PetscOptionsGetString(PETSC_NULL, "-gu_x0", xfname, 255,&flag);
      if(flag){
	ierr=inputBin(x0,xfname);CHKERRQ(ierr);
      }else{
      ////generate random initial guess:
      ////  initial guesses are different level,but the same for all smoothing steps.
	ierr=VecSetRandom(x0,randomctx);CHKERRQ(ierr);
      }
      
      
      VecDuplicate(workVec[0],&outVec);
      if(fabs(pBHMG->c1)==0.0){
	VecCopy(x0,workVec[0]);
	proj(*(pBHMG->pMGDataVec[k]->pPhi),*(pBHMG->pMGDataVec[k]->pvB),workVec[0],workVec[1],x0);
      }
      mCounter=0;
      for(mCounter=0;mCounter<pConNumkm->sizems;mCounter++){
	 m=pConNumkm->ms[mCounter];
         //reset underlying MG solvers of Error Operator;
	 for(l=0;l<=k;l++)pBHMG->pMGBHs[l]->m=m;
         printf("====level:%d,m:%d====\n",k,m);
	 /*********switch on/off for SLEPC solve******/
	 
         ierr=EPSSolve(eps);CHKERRQ(ierr);
         EPSGetType(eps,&type);
         PetscPrintf(PETSC_COMM_WORLD," Solution method: %s\n\n",type);
         EPSGetDimensions(eps,&nev,PETSC_NULL,PETSC_NULL);
         PetscPrintf(PETSC_COMM_WORLD," Number of requested eigenvalues: %d\n",nev);
         EPSGetTolerances(eps,&tol,&maxit);
         PetscPrintf(PETSC_COMM_WORLD," Stopping condition: tol=%0.3e, maxit=%d\n",tol,maxit);
         EPSGetConverged(eps,&nconv);
         printf("||Kx-rx||/||rx||\n");
         if(nconv>0){
           EPSGetIterationNumber(eps,&epsits);
	   EPSGetEigenpair(eps,0,&eigvr,&eigvi,eigVecr,eigVeci);
           EPSComputeRelativeError(eps,0,&errEPS);
	   #ifdef PETSC_USE_COMPLEX
	     re=PetscRealPart(eigvr);
	     im=PetscImaginaryPart(eigvr);
	   #else
	     re=eigvr;
	     im=eigvi;
	   #endif
	     printf("%0.3e%+0.3e i,rel.err=%0.5e, #iteration: %d\n",re,im,errEPS,epsits);
	     (*pConNumkm)(kCounter,mCounter)=re;
	 }else{
	   fprintf(stderr,"Error: No converged eigen-pair\n");
	 }

	 /****switch on/off for power methods****/
	 // ierr=normMGErr(pMGErrBH,1e-4,pBHMG->pMGBHs[k]->pK,PETSC_NULL,pBHMG->pMGDataVec[k]->pvB,workVec,x0,eigvr);
	 // printf("eig=%#.3e\n", eigvr);
	 
         printf("===================\n");

	 //validate the solution
	 ////refer to conNum program

      }
      printf("\n");
      for(l=0;l<6;l++){
	VecDestroy(&workVec[l]);
      }
      VecDestroy(&x0);VecDestroy(&outVec);
      delete pErrExt;
      ierr=MatDestroy(&ErrMat);CHKERRQ(ierr);
      ierr=VecDestroy(&eigVeci);CHKERRQ(ierr);
      ierr=VecDestroy(&eigVecr);CHKERRQ(ierr);
      if(fabs(pBHMG->c1)==0.0)ierr=VecDestroy(&phiCopy);CHKERRQ(ierr);
      ierr=EPSDestroy(&eps);CHKERRQ(ierr);
    }
    PetscRandomDestroy(&randomctx);
    //////////////////////////////////////////////////////////////////
    //////////write contration table and free memory///////////
    tmp=mkdir(argv[2],0777);
    if(tmp!=0 && errno!=EEXIST){
      fprintf(stderr,"ERROR %d:unable to mkdir %s; %s\n",errno,argv[2],strerror(errno));
    }
    infname2.assign(argv[1]);
    pos=infname2.find_last_of('/');
    if(pos==std::string::npos) pos=0;
    infname2=infname2.substr(pos);
    strcpy(infname1,infname2.c_str());
    //sprintf(outfname,"%s/%s_%c%c%d",argv[2],infname1,pBHMG->MGBHMode,pBHMG->PrecMode,pBHMG->mMGNeu);
     sprintf(outfname,"%s/%s_%c%c%d_c1_%g_c2_%g_c3_%g_c4_%g",argv[2],infname1,pBHMG->MGBHMode,pBHMG->PrecMode,pBHMG->mMGNeu,pBHMG->c1,pBHMG->c2,pBHMG->c3,pBHMG->c4);
    pConNumkm->out(outfname);
    sprintf(outfname,"%s/%s_%c%c%d_c1_%g_c2_%g_c3_%g_c4_%g_texTable.tex",argv[2],infname1,pBHMG->MGBHMode,pBHMG->PrecMode,pBHMG->mMGNeu,pBHMG->c1,pBHMG->c2,pBHMG->c3,pBHMG->c4);
    pConNumkm->toTexTable(outfname);
    delete pConNumkm;
    delete pBHMG;
     ///////////////////////////////////////////////////////////////////
   //  for(l=0;l<maxlevel;l++){
//       delete pMGDataVec[l];
//       delete pDiagProjSolvers[l];
//       if(PrecMode!='N')delete pMGNeus[l];
//       delete pMGBHs[l];
//     }
//     delete pCSolverNeu;
//     delete pCSolverBH;
    // releaseMG();
    // delete []lambda1;  delete []lambda2;
    // delete pAs; delete pSs;delete pIs;delete pIts;delete pMs;
    // delete pvBs;delete pvBinvs; delete pPhi;
    // delete pMGErrBH;  

  //ierr=PetscFinalize();CHKERRQ(ierr);
  ierr=SlepcFinalize();CHKERRQ(ierr);
  return 0;
}
