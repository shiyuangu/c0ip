//hook functions for debug purpose
#pragma once 
#include "petsc.h"
PetscErrorCode MyVecCreate(MPI_Comm comm, PetscInt n, Vec* v);
PetscErrorCode MyMatCreate(MPI_Comm comm, PetscInt m, PetscInt n, PetscInt nz, const PetscInt nnz[], Mat *A);
 
