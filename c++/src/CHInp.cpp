#include<cstdio>
#include<cstdlib>
#include<vector>
#include<cstring>
#include<string>
#include<cmath>
#include <asm-generic/errno-base.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/types.h>
#include"petsc.h"
#include"mxGu.h"
#include"mxMesh2Gu.h"
#include"PetscArrayGu.h"
#include"Mesh2Gu.h"
#include"MG.h"
#include"SolverErr.h"
#include"PCGu.h"
#include"comparison.h"
#include"others.h"
#include"auxiliary.h"
#include"gfMG.h"
#include "gf.h"
#include "gfCH.h"
#include "Array2D.h"
#include "converter.h"
#include "gfIFace.h" 
#include"GLee.h"
#include<GL/glut.h>
#include<GL/glext.h>
#include"CHSolver.h"

PetscMatArray *pAs,*pSs,*pIs,*pIts, *pMs;
PetscVecArray *pvBs,*pvBinvs,*pPhi;
Mesh2Array mesh2es;
int maxlevel=0;
double lenUnit=1;

//for CH inpainting
gu::Array2D<bool> inpR,pic;
Vec u0; //init value 
Vec xD; //indicator for inpainting region;
int imgW=128, imgH=128;// image size;
IS idxDOF;     //degree of freedoms
PetscScalar CHpenalty,CHeps1,CHeps2,CHdt1,CHdt2;  //penalty parameter in the CH inpainting model; 
Vec uPre,uCur; //the two successive solutions for timestepping.
int currentTime=0; //currentTime to keep track of timestepping; 
gu::CHSolver *pCHSolver; 
PetscInt CHnt1,CHnt2,CHnt; //# of timestepping for one evolving call for stage 1/2; 
PetscReal CHksp_rtol1, CHksp_rtol2; //control the ksp solver tolerance


/////////////for rendering//////
Mesh2* pDispMesh; 
Vec *pDispVec;

//related to MG
KSPSolverGu *pCSolverBH, *pCSolverNeu;
std::vector<MGData*> pMGDataVec;
std::vector<MGSolver*> pMGBHs;
std::vector<Solver*> pMGNeus, pDiagProjSolvers;  //this is only to free memory 

//parameters to control the MG 
char MGBHMode='W',PrecMode='V';
PetscInt mMGNeu=5;   //presmoothing/postsmoothing step for Neu MG problem 
double dampingScale=0.55; //the damping factors are the estimated spectral radii times damping scales
double *lambda1, *lambda2;   //the damping factors;
//parameters to control the testing level and smoothing steps. 
PetscInt slevel,elevel,sizeks;       
PetscTruth mlogscale;
PetscReal mstartlog,mendlog;
PetscInt msize;
PetscInt mstart=6, mend=36,mskip=2,sizems;
//PetscInt *pm; 
//PetscInt nRun; // # of run( ramdon sample) for each PC
//to output the performance (iteration numbers and time) for preconditoners.   
//Table2DGu<int,int,double> *pTabIts0,*pTabIts1, *pTabTime0,*pTabTime1,*pTabrErr0,*pTabrErr1;

//PetscErrorCode shellPCApply(PC,Vec inVec,Vec outVec);



int main(int argc, char**argv){
  int l,k,m;
  SolverErr *pMGErrBH;
  PetscErrorCode ierr;

  PCGu *pPCGu;  
  PC pc0, pc1;
  MatNullSpace nsp;
  if(argc>1){
    PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); 
    //read global parametes to define the problem
    readMGOpts(); 
    pAs=new PetscMatArray; pSs=new PetscMatArray;pMs=new PetscMatArray;
    pIs=new PetscMatArray;pIts=new PetscMatArray;
    pvBs=new PetscVecArray; pvBinvs=new PetscVecArray;pPhi=new PetscVecArray;
    pMGErrBH=new SolverErr;       
    if(read(argv[1]) || readCH(argv[1])){    //read in Mats & Pic
      PetscEnd();
      exit(1);
    }
     genMG();
     //maxlevel=1;
     //define the CH model
     readCHOpts();
     get_u0();
     get_xD();
     ISCreateGeneral(PETSC_COMM_SELF,mesh2es.pVal[maxlevel-1].nDOF, mesh2es.pVal[maxlevel-1].idx,&idxDOF);
     VecDuplicate(u0,&uPre);  VecDuplicate(u0,&uCur); 
     VecCopy(u0,uPre); VecCopy(u0,uCur);
     //setup for CH stage 1
     m=mstart; k=maxlevel-1;  
     for(l=0;l<=k;l++)pMGBHs[l]->m=m;
     
     PetscTruth flag;
     PetscOptionsHasName(PETSC_NULL,"-mCH",&flag);
     if(flag){
       pCHSolver=new gu::mCHSolver(pMGDataVec,pMGBHs,maxlevel-1,u0,xD,CHeps1,CHpenalty,CHdt1,CHksp_rtol1);
       pCHSolver->constructKSP();
     }else{
       pCHSolver=new gu::CHSolver(pMGDataVec,pMGBHs,maxlevel-1,u0,CHeps1,CHdt1,CHksp_rtol1);
       pCHSolver->constructKSP();
     }

     CHnt=CHnt1;

     pDispMesh=mesh2es.pVal+maxlevel-1;   //point to a level of mesh for display;
     pDispVec=&uCur;
     setupOGL(argc,argv,imgW*5/4*4,imgH*5/4*4);
    
    ///////////////////////////////////////////////////////////////////
    //////////////////MG as a pc for KSP/////////////////////////////
      //  m=mstart; k=maxlevel-1;   
//        ierr=MatNullSpaceCreate(PETSC_COMM_SELF,PETSC_TRUE,0,PETSC_NULL,&nsp);CHKERRQ(ierr);
//        ///////////////////create pc0,pc1/////////////////////////////
//        ierr=PCCreate(PETSC_COMM_SELF,&pc0);CHKERRQ(ierr);
//        ierr=PCCreate(PETSC_COMM_SELF,&pc1);CHKERRQ(ierr);
//        ierr=PCSetType(pc0,PCNONE);CHKERRQ(ierr);
//        ierr=PCSetFromOptions(pc0);CHKERRQ(ierr);
//        ierr=PCSetType(pc1,PCSHELL);CHKERRQ(ierr);
//        ///////////////////reset pc1///////////////
//        PCSetOperators(pc0,*(pMGBHs[k]->pK),*(pMGBHs[k]->pK),SAME_NONZERO_PATTERN);
//        PCSetOperators(pc1,*(pMGBHs[k]->pK),*(pMGBHs[k]->pK),SAME_NONZERO_PATTERN);
//        /////////reset MG solvers & pc2////////////
//        for(l=0;l<=k;l++)pMGBHs[l]->m=m;
//        pPCGu=new PCGu(pMGBHs[k]);
//        ierr=PCShellSetContext(pc1,(void*)pPCGu);CHKERRQ(ierr);      
//        ierr=PCShellSetApply(pc1,&shellPCApply);CHKERRQ(ierr);
//        ierr=PCSetUp(pc0);CHKERRQ(ierr);
//        ierr=PCSetUp(pc1);CHKERRQ(ierr);
       /*********************
           .......use pc0/pc1.................
	*********************/
       glutMainLoop();

       // delete pPCGu;
//        ierr=PCDestroy(pc0); CHKERRQ(ierr);
//        ierr=PCDestroy(pc1); CHKERRQ(ierr); 
//        ierr=MatNullSpaceDestroy(nsp);CHKERRQ(ierr);
   //////////////////////////////////////////////////////////////////////
  
    releaseMG();
    delete []lambda1;  delete []lambda2;
    delete pAs; delete pSs; delete pMs;
    delete pIs;delete pIts;
    delete pvBs;delete pvBinvs; delete pPhi;
    delete pMGErrBH; 
    VecDestroy(u0);VecDestroy(xD);
    VecDestroy(uPre);VecDestroy(uCur);
    ISDestroy(idxDOF);
    delete pCHSolver;
  }else{
    promptOps();
  }
  ierr=PetscFinalize();CHKERRQ(ierr); 
  return 0;
}

// PetscErrorCode shellPCApply(PC pc,Vec rhs, Vec outVec){
//   PCGu* pPCGu;
//   PCShellGetContext(pc,(void**)&pPCGu);
//   return pPCGu->apply(rhs,outVec);
// }
