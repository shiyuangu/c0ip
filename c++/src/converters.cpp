//converters.h/converter.cpp: to convert data format
//assume PetscScalar and PetcsInt are double, int, resp. 
#include<cstdio>
#include<cstdlib>
#include"petsc.h"
#include"converters.h"
#include"converter.h"
#include"Magick++.h"
PetscErrorCode mxGuSparse2Mat(const mxGuSparse& s, Mat& t){
  int j,startid,endid;
  PetscErrorCode ierr;
  MatZeroEntries(t);
  for(j=0;j<s.ncol;j++){
    startid=s.jc[j];endid=s.jc[j+1];
    if(startid<endid){
      ierr=MatSetValues(t,endid-startid,s.ir+startid,1,&j,s.pr+startid,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr=MatAssemblyBegin(t,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr=MatAssemblyEnd(t,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  return 0;
}
PetscErrorCode mxGuSparse2Vec(const mxGuSparse& s, Vec& t){
  PetscErrorCode ierr;

  if(s.ncol!=1 && s.nrow!=1){
    fprintf(stderr,"Error! the source sparse matrix is not a vector");
    return 1;
  }
  VecZeroEntries(t);
  if(s.ncol==1){
    ierr=VecSetValues(t,s.nnz,s.ir,s.pr,INSERT_VALUES);CHKERRQ(ierr);
  }else{
    for(int j=0;j<s.ncol;j++){
      if(s.jc[j]<s.jc[j+1])
	ierr=VecSetValues(t,1,&j,s.pr+s.jc[j],INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr=VecAssemblyBegin(t);
  ierr=VecAssemblyEnd(t);
  return 0;
}
PetscErrorCode mxGuDouble2Vec(const mxGuDouble& s, Vec& t){
  int *index,i;
  PetscErrorCode ierr;
  if(s.ncol!=1 && s.nrow!=1){
    fprintf(stderr,"Warning: the source dense matrix is not a vector. Cont. anyway!");
  }
  index=new int[s.nrow*s.ncol];
  for(i=0;i<s.nrow*s.ncol;i++) index[i]=i;
  ierr=VecSetValues(t,s.nrow*s.ncol,index,s.pr,INSERT_VALUES);CHKERRQ(ierr);
  ierr=VecAssemblyBegin(t);
  ierr=VecAssemblyEnd(t);
  delete []index;
  return 0;
}
void mxMesh2toMesh2(const mxMesh2& source, Mesh2& t){
   int j;
  if (source.nDOF.pr[0]<=0){
    fprintf(stderr,"source mesh has no DOFs.Nothing is done.\n");
    return;
  }
  t.nGI=source.nGI.pr[0];t.nDOF=source.nDOF.pr[0];
  t.idx=new int[t.nDOF];
  if(t.nGI!=source.free.size|| source.free.pr==NULL){
    fprintf(stderr,"something is wrong in mesh convertion.\n");
    return;
  }
  j=0;
  for(int i=0;i<t.nGI;i++){
    if(source.free.pr[i])t.idx[j++]=i;
  }
  
  //for iP;
  t.xyco=new double[2*t.nGI];
  t.vertex=new bool[t.nGI];
  t.boundary=new bool[t.nGI];
  for(int i=0;i<t.nGI; i++){
    t.xyco[2*i]=source.iP.pVal[i].coordinate.pr[0];
    t.xyco[2*i+1]=source.iP.pVal[i].coordinate.pr[1];
    t.vertex[i]=source.iP.pVal[i].vertex.pr[0];
    t.boundary[i]=source.iP.pVal[i].boundary.pr[0]; 
  } 

  //for free; 
  t.free=new bool[t.nGI];
  for(int i=0;i<t.nGI;i++){
    t.free[i]=source.free.pr[i];
  }
  //for T
  t.nT=source.T.nrow;
  t.T=new int[t.nT*6];
  for(int i=0;i<t.nT;i++){
    for(int j=0;j<6;j++){
      t.T[i*6+j]=source.T.pr[j*t.nT+i]-1;  //subtract 1 to 0-base.
      //t.T[i*3+1]=source.T.pr[t.nT+i]-1;
      //t.T[i*3+2]=source.T.pr[2*t.nT+i]-1;
    }
  } 
}
namespace gu{
  int converter(mxGuLogical& s, Array2D<bool>& t){
    t.malloc(s.nrow,s.ncol);
    for(int i=0;i<s.nrow;i++)
      for(int j=0;j<s.ncol;j++)
	t.get(j,i)=s.pr[j*s.nrow+i];
    return 0;
  }
  int converter(Magick::Image& s, Array2D<bool>& t){
    Magick::Color pixel;
    float r,g,b,c;
    t.malloc(s.rows(),s.columns());
    for(size_t i=0; i<s.rows();i++)
      for(size_t j=0;j<s.columns();j++){
	pixel=s.pixelColor(j,i);
	r=pixel.redQuantum()*1.0f/QuantumRange;
	g=pixel.greenQuantum()*1.0f/QuantumRange;
	b=pixel.blueQuantum()*1.0f/QuantumRange;
	c=0.3*r+0.59*g+0.11*b;
        //printf("r=%g,g=%g,b=%g,c=%g\n",r,g,b,c);
        //t.get(j,i)=true;
	 if(c<0.3)
 	  t.get(j,i)=true;
 	else
 	  t.get(j,i)=false;
	
      }
  }
}
