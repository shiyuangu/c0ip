/****Obsolete!*****/
/****This header is not for interface**********/
#if CUDA
#pragma once
#include"petsc.h"
#undef VecType
#include<cusp/ell_matrix.h>
#include<cusp/csr_matrix.h>
namespace gu{
  typedef cusp::ell_matrix<PetscInt,PetscScalar,cusp::device_memory> CuspEll;
  typedef cusp::csr_matrix<PetscInt,PetscScalar,cusp::device_memory> CuspCsr;
  typedef cusp::csr_matrix<PetscInt,PetscScalar,cusp::host_memory> CuspCsrHost;
  typedef cusp::array1d<PetscScalar,cusp::device_memory> CuspVec;
  PetscErrorCode Mat2Cusp(Mat& s, CuspEll* & pt);
  PetscErrorCode Mat2Cusp(Mat& s, CuspCsr* & pt);
}
#endif
